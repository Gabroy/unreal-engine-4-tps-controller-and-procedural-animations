#include "TPSEditorModule.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FTPSEditorModule, TPSEditor);

#define LOCTEXT_NAMESPACE "TPSEditor"

void FTPSEditorModule::StartupModule()
{
    //UE_LOG(TPSEditor, Warning, TEXT("TPSEditor: Module Started"));
}

void FTPSEditorModule::ShutdownModule()
{
    //UE_LOG(TPSEditor, Warning, TEXT("TPSEditor: Module Ended"));
}

#undef LOCTEXT_NAMESPACE