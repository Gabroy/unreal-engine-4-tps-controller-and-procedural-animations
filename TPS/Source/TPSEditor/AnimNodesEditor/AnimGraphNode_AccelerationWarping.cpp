#pragma once

#include "AnimGraphNode_AccelerationWarping.h"

// Title displayed for the node.
FText UAnimGraphNode_AccelerationWarping::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString("Acceleration Warping");
}

// Tooltip displayed when hovering over the node.
FText UAnimGraphNode_AccelerationWarping::GetTooltipText() const
{
	return FText::FromString("Apply acceleration warping to normal pose");
}

// Color for the node's title section background.
FLinearColor UAnimGraphNode_AccelerationWarping::GetNodeTitleColor() const
{
	return FLinearColor(0.25f, 0.25f, 1.0f);
}

FString UAnimGraphNode_AccelerationWarping::GetNodeCategory() const
{
	return TEXT("Procedural Animations");
}