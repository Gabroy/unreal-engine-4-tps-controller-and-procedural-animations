#pragma once

#include "CoreMinimal.h"
#include "AnimGraph/Classes/AnimGraphNode_Base.h"
#include "../../TPS/Base/Animations/AnimNodes/AnimNode_CurvePoseEvaluation.h"
#include "AnimGraphNode_CurvePoseEvaluation.generated.h"

/*
* Animation graph node for implementing the node curve pose evaluation, used for DM.
*/

UCLASS()
class TPSEDITOR_API UAnimGraphNode_CurvePoseEvaluation : public UAnimGraphNode_Base
{
	GENERATED_BODY()

	// Node with evaluation pose with curve functionality.
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_CurvePoseEvaluation Node;

public:

	UAnimGraphNode_CurvePoseEvaluation(){}

	// Title displayed for the node.
	FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;

	// Tooltip displayed when hovering over the node.
	FText GetTooltipText() const override;

	// Color for the node's title section background.
	FLinearColor GetNodeTitleColor() const override;

	// Category the node belongs to.
	FString GetNodeCategory() const override;
};