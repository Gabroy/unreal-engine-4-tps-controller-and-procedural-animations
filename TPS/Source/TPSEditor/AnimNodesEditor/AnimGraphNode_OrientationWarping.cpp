#pragma once

#include "AnimGraphNode_OrientationWarping.h"

// Title displayed for the node.
FText UAnimGraphNode_OrientationWarping::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString("Orientation Warping");
}

// Tooltip displayed when hovering over the node.
FText UAnimGraphNode_OrientationWarping::GetTooltipText() const
{
	return FText::FromString("Apply orientation warping to normal pose");
}

// Color for the node's title section background.
FLinearColor UAnimGraphNode_OrientationWarping::GetNodeTitleColor() const
{
	return FLinearColor(0.25f, 0.25f, 1.0f);
}

FString UAnimGraphNode_OrientationWarping::GetNodeCategory() const
{
	return TEXT("Procedural Animations");
}