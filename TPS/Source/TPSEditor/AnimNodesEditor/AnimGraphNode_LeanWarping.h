#pragma once

#include "CoreMinimal.h"
#include "AnimGraph/Classes/AnimGraphNode_Base.h"
#include "../../TPS/Base/Animations/AnimNodes/AnimNode_LeanWarping.h"
#include "AnimGraphNode_LeanWarping.generated.h"

/*
* Animation graph node for implementing lean warping.
* 
* Information about graph nodes can be found in the following link:
* https://docs.unrealengine.com/en-US/Programming/Animation/AnimNodes/index.html
* 
*/

UCLASS()
class TPSEDITOR_API UAnimGraphNode_LeanWarping : public UAnimGraphNode_Base
{
	GENERATED_BODY()

	// Node with orientation warping functionality.
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_LeanWarping Node;

public:

	UAnimGraphNode_LeanWarping(){}

	// Title displayed for the node.
	FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;

	// Tooltip displayed when hovering over the node.
	FText GetTooltipText() const override;

	// Color for the node's title section background.
	FLinearColor GetNodeTitleColor() const override;

	// Category the node belongs to.
	FString GetNodeCategory() const override;
};