#pragma once

#include "AnimGraphNode_LeanWarping.h"

// Title displayed for the node.
FText UAnimGraphNode_LeanWarping::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString("Lean Warping");
}

// Tooltip displayed when hovering over the node.
FText UAnimGraphNode_LeanWarping::GetTooltipText() const
{
	return FText::FromString("Apply leaning warping to normal pose");
}

// Color for the node's title section background.
FLinearColor UAnimGraphNode_LeanWarping::GetNodeTitleColor() const
{
	return FLinearColor(0.25f, 0.25f, 1.0f);
}

FString UAnimGraphNode_LeanWarping::GetNodeCategory() const
{
	return TEXT("Procedural Animations");
}