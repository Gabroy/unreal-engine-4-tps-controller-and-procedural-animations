#pragma once

#include "AnimGraphNode_CurvePoseEvaluation.h"

// Title displayed for the node.
FText UAnimGraphNode_CurvePoseEvaluation::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return FText::FromString("Curve Pose Evaluation");
}

// Tooltip displayed when hovering over the node.
FText UAnimGraphNode_CurvePoseEvaluation::GetTooltipText() const
{
	return FText::FromString("Use a curve that gets indexed by its Y values to check which keyframe of the sequence should be shown. Allows animations being ruled by locomotion, minimizing foot sliding. Like an inverse root motion.");
}

// Color for the node's title section background.
FLinearColor UAnimGraphNode_CurvePoseEvaluation::GetNodeTitleColor() const
{
	return FLinearColor(0.25f, 0.25f, 1.0f);
}

FString UAnimGraphNode_CurvePoseEvaluation::GetNodeCategory() const
{
	return TEXT("Procedural Animations");
}