using UnrealBuildTool;

public class TPSEditor : ModuleRules
{
    public TPSEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore",
        "AnimGraph", "AnimGraphRuntime" });

        PublicDependencyModuleNames.AddRange(new string[] { "TPS" });

        PrivateDependencyModuleNames.AddRange(new string[] { "UnrealED", "AnimGraph", "AnimGraphRuntime", "BlueprintGraph" });
    }
}