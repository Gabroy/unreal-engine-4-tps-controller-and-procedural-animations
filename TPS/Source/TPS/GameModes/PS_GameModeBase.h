// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PS_GameModeBase.generated.h"

UCLASS()
class TPS_API APS_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
