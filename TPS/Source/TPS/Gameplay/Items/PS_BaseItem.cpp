#include "PS_BaseItem.h"
#include "GameInstance/PS_GameInstanceFunctionLibrary.h"
#include "GameInstance/Managers/PS_ItemManager.h"

/* Constructors */

APS_BaseItemActorInstance::APS_BaseItemActorInstance() : APS_Base_Interactuable()
{

}

void APS_BaseItemActorInstance::BeginPlay()
{
	Super::BeginPlay();

	// If UUID is invalid, this means the value was not setup from outside (normally from a loading process),
	// so we request a new UUID.
	if (!ItemUUID.IsUUIDValid())
	{
		UPS_ItemManager * ItemManager = UPS_GameInstanceFunctionLibrary::GetItemManagerInstance(GetWorld());
		if (ItemManager)
		{
			ItemUUID = ItemManager->GenerateNewUUID();
		}
	}
}

void APS_BaseItemActorInstance::SetSimulatePhysics(bool EnablePhysics)
{
	TArray<UPrimitiveComponent*> PrimitiveComponentsInActor;
	GetComponents<UPrimitiveComponent>(PrimitiveComponentsInActor);
	for (UPrimitiveComponent * Component : PrimitiveComponentsInActor)
	{
		if(!Component)
			continue;

		if (Component != ((UPrimitiveComponent *)OutlineComponent))
		{
			Component->SetSimulatePhysics(EnablePhysics);
			Component->SetCollisionEnabled(EnablePhysics ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision);
		}
	}
}

/* Getters And Setters */

AActor * APS_BaseItemActorInstance::GetItemOwner() const
{
	return ItemOwner;
}

void APS_BaseItemActorInstance::SetItemOwner(AActor * NewItemOwner)
{
	ItemOwner = NewItemOwner;
}

UPS_ItemBaseDefinitionAsset * APS_BaseItemActorInstance::GetItemDefinition() const
{
	return ItemDefinition;
}

EItemCategory APS_BaseItemActorInstance::GetItemCategory() const
{
	return ItemDefinition ? ItemDefinition->GetItemCategory() : EItemCategory::ITEM_Undefined;
}

const FName APS_BaseItemActorInstance::GetItemID() const
{
	return ItemDefinition ? ItemDefinition->GetItemID() : FName();
}

SimpleUUID APS_BaseItemActorInstance::GetItemUUID() const
{
	return ItemUUID.GetUUID();
}

void APS_BaseItemActorInstance::SetItemUUID(UUIDHandle NewUUID)
{
	ItemUUID = NewUUID;
}

const FText APS_BaseItemActorInstance::GetItemDisplayName() const
{
	return ItemDefinition ? ItemDefinition->GetItemDisplayName() : FText();
}

const FText APS_BaseItemActorInstance::GetItemDisplayDescription() const
{
	return ItemDefinition ? ItemDefinition->GetItemDisplayDescription() : FText();
}

ECarryingStance APS_BaseItemActorInstance::GetItemCarryingStance() const
{
	return ItemDefinition ? ItemDefinition->GetCarryingStance() : ECarryingStance::Unarmed;
}