#include "PS_WeaponItem.h"

/* Constructors */

APS_WeaponActorInstance::APS_WeaponActorInstance() : APS_BaseItemActorInstance()
{
	SkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh Component"));
	SetRootComponent(SkeletalMeshComp);

	SkeletalMeshComp->SetSimulatePhysics(false);

	PrimaryActorTick.bCanEverTick = false;
}

void APS_WeaponActorInstance::BeginPlay()
{
	Super::BeginPlay();
}

void APS_WeaponActorInstance::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}