#pragma once

#include "Gameplay/Items/PS_BaseItem.h"
#include "Gameplay/Items/Weapons/PS_WeaponItemDefinition.h"
#include "PS_WeaponItem.generated.h"

class USkeletalMeshComponent;

/*
* Actor class representing a weapon item that is being instantiated in game.
* 
* Inherits from the APS_BaseItemActorInstance class.
*/

UCLASS(Blueprintable)
class APS_WeaponActorInstance : public APS_BaseItemActorInstance
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	/* Variables */

	// Definition of the item.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UPS_WeaponBaseDefinitionAsset * WeaponDefinition = nullptr;

public:

	/* Constructors */

	APS_WeaponActorInstance();

	virtual void Tick(float DeltaTime) override;

	/* Functions. */

	UFUNCTION(BlueprintPure, BlueprintCallable)
	USkeletalMeshComponent * GetSkeletalComponent() const { return SkeletalMeshComp; }

	UFUNCTION(BlueprintPure, BlueprintCallable)
	UPS_WeaponBaseDefinitionAsset * GetWeaponDefinition() const { return WeaponDefinition; }

protected:

	virtual void BeginPlay() override;
};