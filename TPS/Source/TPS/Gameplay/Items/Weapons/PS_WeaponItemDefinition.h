#pragma once

#include "Engine/DataAsset.h"
#include "Gameplay/Items/Weapons/PS_WeaponItemStructs.h"
#include "PS_WeaponItemDefinition.generated.h"

/*
* Weapon data asset.
* 
* Stores data used by weapons related to their definition. Take a look at
* UPS_ItemBaseDefinitionAsset for a better definition of what an UDataAsset does.
* 
* Shared between all instances of the same weapon.
*/

UCLASS(BlueprintType)
class TPS_API UPS_WeaponBaseDefinitionAsset : public UDataAsset
{

protected:

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EWeaponCategory> WeaponCategory;

	// Grip socket of the weapon.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName GripSocket = "Grip";

	// Normally the hand guard for the weapon, if it has any.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName SecondHandSocketPlacement = "HandGuard";

public:

	UFUNCTION(BlueprintCallable)
	EWeaponCategory GetWeaponCategory() const { return WeaponCategory; }

	UFUNCTION(BlueprintCallable)
	const FName & GetGripSocket() const { return GripSocket; }

	UFUNCTION(BlueprintCallable)
	const FName & GetSecondHandSocketPlacement() const { return SecondHandSocketPlacement; }
};