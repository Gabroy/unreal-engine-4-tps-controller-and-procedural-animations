#pragma once

#include "PS_WeaponItemStructs.generated.h"

/*
* Enumerator representing the different weapon categories present in game.
*/
UENUM(BlueprintType)
enum EWeaponCategory
{
	WEAPON_UNDEFINED,
	WEAPON_ONE_HANDED_GUN,
	WEAPON_TWO_HANDED_GUN
};