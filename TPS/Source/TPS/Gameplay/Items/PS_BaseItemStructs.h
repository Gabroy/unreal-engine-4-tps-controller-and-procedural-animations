#pragma once

#include "CoreMinimal.h"
#include "PS_BaseItemStructs.generated.h"

/*
* Enumerator representing the different general item categories present in game.
* 
* General item categories allow coders to easily identify the type of an item. */
UENUM(BlueprintType)
enum EItemCategory
{
	ITEM_Undefined,
	ITEM_Weapon,
	ITEM_Consumable
};