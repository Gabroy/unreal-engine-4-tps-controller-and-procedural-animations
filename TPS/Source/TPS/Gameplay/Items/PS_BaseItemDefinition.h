#pragma once

#include "Engine/DataAsset.h"
#include "Gameplay/Items/PS_BaseItemStructs.h"
#include "Characters/Common/Components/Carrying/PS_Carrying_Structs_Enums.h"
#include "PS_BaseItemDefinition.generated.h"

class APS_BaseItemActorInstance;

/*
* Data asset used for storing the basic definition for an object.
* 
* Data assets are shared between all instances of a same object type. They are used to store general data
* that is unique for all instances.
* 
* Different objects types may also include other definition assets for storing extra information.
*/

UCLASS(BlueprintType)
class TPS_API UPS_ItemBaseDefinitionAsset : public UDataAsset
{

protected:

	GENERATED_BODY()
	
	// Item ID, unique for each type of item. Examples are: Axe, M4A1, Blue_Key_1, ...
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ItemID;

	// General category of the item. Weapon, Consumable, ...
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EItemCategory> ItemCategory;

	// Name of the item to display to users.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText ItemDisplayName;

	// Description of the item.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText ItemDisplayDescription;

	// Class used to instantiate the actual object in the game world as an actor.
	// This class can be destroyed and instantiated multiple times during an item lifescope.
	// Permanent variables storage is responsible of the programmer, preferred method is to keep this values stored
	// in per item type structs inside the inventory class and passed to the actor for modification when instantianted.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (MustImplement = "PS_BaseItemInterface"))
	TSubclassOf<APS_BaseItemActorInstance> ItemActorClass;

	// Item carrying stance when a character carries the item.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<ECarryingStance> CarryingStance;

public:

	// General category of the item. Weapon, Consumable, ...
	UFUNCTION(BlueprintCallable)
	EItemCategory GetItemCategory() const { return ItemCategory; }

	// Item ID, unique for each type of item. Examples are: Axe, M4A1, Blue_Key_1, ...
	UFUNCTION(BlueprintCallable)
	const FName & GetItemID() const { return ItemID; }

	UFUNCTION(BlueprintCallable)
	const FText & GetItemDisplayName() const { return ItemDisplayName; }

	UFUNCTION(BlueprintCallable)
	const FText & GetItemDisplayDescription() const { return ItemDisplayDescription; }

	UFUNCTION(BlueprintCallable)
	TSubclassOf<APS_BaseItemActorInstance> GetItemActorClass() const { return ItemActorClass; }

	UFUNCTION(BlueprintCallable)
	ECarryingStance GetCarryingStance() const { return CarryingStance; }
};