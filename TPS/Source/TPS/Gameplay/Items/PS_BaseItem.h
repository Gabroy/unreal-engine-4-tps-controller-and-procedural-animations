#pragma once

#include "GameInstance/Managers/Base/UUID/PS_SimpleUUIDStructs.h"
#include "Gameplay/Interactuable/PS_Base_Interactuable.h"
#include "Gameplay/Items/PS_BaseItemDefinition.h"
#include "PS_BaseItem.generated.h"

/*
* Actor class representing a base item that is being instantiated in game.
* 
* Items are composed of both an actor class, which will inherit from this base one, and a struct specific to the type of item.
* 
* The actor class is instantiated whenever the item is being shown or present in the world itself, therefore it's responsible
* of implementing the logic, collisions, rendering and replication of the item.
* 
* Nevertheless, actors can be destroyed for example when the item gets saved in the character inventory, in such cases, to allow
* variables to persist the data has to be stored in a specific struct per type of item in the character inventory. This last part
* is responsibility of the programmer.
* 
* Storing permanent data in a struct and removing the actor once not visible allows for a much more optimized inventory.
*/

UCLASS(Blueprintable)
class APS_BaseItemActorInstance : public APS_Base_Interactuable
{
	GENERATED_BODY()

protected:

	// Handle for unique ID per item. Delivered by the ItemManager on item creation.
	// Handle is assigned to item for as long as the item exists, being removed afterwards.
	// If we would ever support serialization, we would have to save the ID inside this handle
	// to be reloaded on loading savegame.
	UUIDHandle ItemUUID;

	// Actor that owns this item. Null if not owned by anyone.
	UPROPERTY(BlueprintReadOnly)
	AActor * ItemOwner = nullptr;

	// Definition of the item.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UPS_ItemBaseDefinitionAsset * ItemDefinition = nullptr;

	virtual void BeginPlay() override;

public:

	/* Constructors */

	APS_BaseItemActorInstance();

	/* Functions. */

	// Enables or disables physics for item.
	void SetSimulatePhysics(bool EnablePhysics);

	/* Getters And Setters */

	UFUNCTION(BlueprintCallable)
	AActor * GetItemOwner() const;

	UFUNCTION(BlueprintCallable)
	void SetItemOwner(AActor * NewItemOwner);

	UFUNCTION(BlueprintCallable)
	UPS_ItemBaseDefinitionAsset * GetItemDefinition() const;

	// Returns the general category the item is part of: Weapon, Consumable, ...
	UFUNCTION(BlueprintCallable)
	EItemCategory GetItemCategory() const;

	// Returns the item ID, unique for each type of item. Examples are: Axe, M4A1, Blue_Key_1, ...
	UFUNCTION(BlueprintCallable)
	const FName GetItemID() const;

	// Returns the unique ID for this specific instance of an item. ID is unique no matter the type of item.
	SimpleUUID GetItemUUID() const;

	// Sets the UniqueID to be used by the item
	// Serialization methods would call this method when loading a previously saved item to return the original ID.
	void SetItemUUID(UUIDHandle NewUUID);

	UFUNCTION(BlueprintCallable)
	const FText GetItemDisplayName() const;

	UFUNCTION(BlueprintCallable)
	const FText GetItemDisplayDescription() const;

	UFUNCTION(BlueprintCallable)
	ECarryingStance GetItemCarryingStance() const;
};