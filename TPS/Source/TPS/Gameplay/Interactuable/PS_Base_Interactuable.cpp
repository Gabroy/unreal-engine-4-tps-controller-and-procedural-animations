#include "PS_Base_Interactuable.h"
#include "Gameplay/Outline/PS_OutlineComponent.h"

APS_Base_Interactuable::APS_Base_Interactuable() : AActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void APS_Base_Interactuable::BeginPlay()
{
	Super::BeginPlay();

	if (RootComponent)
	{
		OutlineComponent = Cast<UPS_OutlineComponent>(NewObject<USceneComponent>(this, UPS_OutlineComponent::StaticClass(), "OutlineComponent"));
		OutlineComponent->RegisterComponent();
		FAttachmentTransformRules TransformRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
		OutlineComponent->AttachToComponent(RootComponent, TransformRules);
		AddInstanceComponent(OutlineComponent);
		OutlineComponent->RefreshOutlineBoundsForActor();
		UE_LOG(LogTemp, Error, TEXT("APS_Base_Interactuable::BeginPlay OutlineComponent Was Not Created. Root Component not found."));
	}
}

EInteractuable_InteractionResult APS_Base_Interactuable::CanInteract_Implementation(
	TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor, FText& ReasonInteraction)
{
	return EInteractuable_InteractionResult::IIR_Yes;
}

void APS_Base_Interactuable::StartInteraction_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor)
{

}

void APS_Base_Interactuable::StopInteraction_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor)
{

}

bool APS_Base_Interactuable::AreInteractionsEnabled_Implementation()
{
	return AllowInteractions;
}

void APS_Base_Interactuable::SetAllowInteractions(bool NewAllowInteractions)
{
	AllowInteractions = NewAllowInteractions;
	if (OutlineComponent)
	{
		OutlineComponent->SetOutlineEnabled(NewAllowInteractions);
	}
}

EInteractuableTypes APS_Base_Interactuable::GetInteractuableType_Implementation()
{
	return EInteractuableTypes::IT_Object;
}

float APS_Base_Interactuable::GetInteractionHoldTime_Implementation()
{
	return -1.0f;
}

void APS_Base_Interactuable::SetInteractionFeedback_Implementation(EInteractor_CanBeInteractedResult InteractuableState)
{
	switch (InteractuableState)
	{
	case EInteractor_CanBeInteractedResult::ICBIR_Yes:
		OutlineComponent->SetOutlineType(EOutlineTypeID::OUTLINE_INTERACTION_ENABLED);
		OutlineComponent->SetOutlinesVisibility(true);
		break;
	case EInteractor_CanBeInteractedResult::ICBIR_Failed_Actor_Conditions:
		OutlineComponent->SetOutlineType(EOutlineTypeID::OUTLINE_INTERACTION_DISABLED);
		OutlineComponent->SetOutlinesVisibility(true);
		break;
	case EInteractor_CanBeInteractedResult::ICBIR_Failed_Interactuable_Conditions:
		OutlineComponent->SetOutlineType(EOutlineTypeID::OUTLINE_INTERACTION_DISABLED);
		OutlineComponent->SetOutlinesVisibility(true);
		break;
	case EInteractor_CanBeInteractedResult::ICBIR_Disabled:
		OutlineComponent->SetOutlinesVisibility(false);
		break;
	default:
		break;
	}
}