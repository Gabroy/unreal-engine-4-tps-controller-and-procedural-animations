#pragma once

#include "PS_InteractorInterface.generated.h"

/*
* Abstract interface that actors that use an interactor component should implement.
* Allow interactuable objects to easily communicate with objects interacting with them when necessary and perform some actions on them.
* 
* Right now is only used to allow interatuable objects to cancel interaction for actors interacting for them, in case it might be necessary.
*/

UINTERFACE(MinimalAPI, Blueprintable)
class UPS_InteractorInterface : public UInterface
{
	GENERATED_BODY()
};

class TPS_API IPS_InteractorInterface
{

	GENERATED_BODY()

public:

	// Forces the interactor to cancel the interaction it's performing if there is one being performed.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void CancelInteraction();
	virtual void CancelInteraction_Implementation() = 0;

	// Called by interactuables once the interaction has finished correctly.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void FinishedInteraction();
	virtual void FinishedInteraction_Implementation() = 0;
};