#pragma once

#include "PS_InteractuableStructs.generated.h"

/*
 * Interactuable Types
 *
 * Add here all interactuable types that the game will support.
 * Types are used by interactor components to identify the type of object being interacted with 
 * and proceed with the custom interaction logic that may be necessary for such specific type in the context of the interaction.
 * 
 * Examples of types might be: Button, Object, Ally, NPC, Enemy, Resource, Door, ...
 */

UENUM(BlueprintType)
enum EInteractuableTypes
{
	IT_Object,
	IT_NPC, // Unused
	IT_Enemy, // Unused
	IT_Door, // Unused
	IT_Stairs, // Unused
	IT_Button // Unused
};

/*
 * Interactuable Interaction Results
 *
 * Returned by interactuable when calling CanInteract function, returns information about whether or not the interaction can be performed.
 * Responsibility of interactors to check conditions before starting interactions.
 */
UENUM(BlueprintType)
enum EInteractuable_InteractionResult
{
	IIR_Failed_ConditionsNotMet,		 // Can not be interacted with due to failing interactuable conditions (object related conditions like missing a key or similar).
	IIR_Failed_OutsideInteractionBounds, // Can not be interacted with due to interactor being outside the interactuable interaction bounds.
	IIR_Yes								 // Can interact with the object.
};

/*
 * Interactor Can Be Interacted Results
 *
 * Returns the results of the Can Be Interacted Check of Interator Components.
 * Can be used to get better information of why we can't interact with a given actor.
 */
UENUM(BlueprintType)
enum EInteractor_CanBeInteractedResult
{
	ICBIR_Disabled,                         // Return this if the interactor is not interactuable (you don't want to show it as interactuable at all).
	ICBIR_Failed_Interactuable_Conditions,	// Return this if we are not meeting interactuable object conditions.
	ICBIR_Failed_Actor_Conditions,          // Return this if the actor doesn't met interactor conditions.
	ICBIR_AlreadyInteracting,               // Return this if we are already interacting with the object.
	ICBIR_Yes,								// Return this if we can interact with the object
};
