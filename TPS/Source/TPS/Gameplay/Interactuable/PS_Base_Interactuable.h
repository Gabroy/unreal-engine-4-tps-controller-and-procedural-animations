#pragma once

#include "GameFramework/Actor.h"
#include "PS_InteractuableInterface.h"
#include "PS_Base_Interactuable.generated.h"

class UPS_OutlineComponent;

/*
* A basic interactuable actor with already working outline functionality.
* Can be inherited by child classes to avoid having to rewrite outlining logic each time.
*/

UCLASS(Abstract)
class TPS_API APS_Base_Interactuable : public AActor, public IPS_InteractuableInterface
{

	GENERATED_BODY()

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UPS_OutlineComponent * OutlineComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	bool AllowInteractions = true;

	virtual void BeginPlay() override;

public:

	APS_Base_Interactuable();


	// Enable or disable interactions with the item. By default items starts with interactions enabled.
	void SetAllowInteractions(bool NewAllowInteractions);

	/* Interface functions */

	// Returns information about whether InstigatorInteractor can interact with the interactuable.
	// Perform object related checks like if the interactuable is in the current state or the interactor has necessary resources.
	// Don't perform actor only checks here, like if the character has hands free or can perform interactions, keep that logic in the interactor component.
	virtual EInteractuable_InteractionResult CanInteract_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor, FText& ReasonInteraction);

	// Called by the Interactor component immediately once it starts interacting with the object.
	// Can be used to know which actors are interacting with an object at any given time.
	virtual void StartInteraction_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor);

	// Called by an IInteractor actor immediately once it stops interacting with the object.
	virtual void StopInteraction_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor);

	// Returns true if the interactuable allows interactions. False if it's disabled and your want it to not be detected by interactuable systems.
	// Use CanInteract for checks for object specific interactuable conditions. This functions is only for disabling interactuable detection by interactors.
	virtual bool AreInteractionsEnabled_Implementation();

	// Returns the type of the interactuable. Used by interactor components to know the type of interactuable they are interacting with
	// and cast to the corresponding class to perform the necessary interaction logic in the necessary context.
	virtual EInteractuableTypes GetInteractuableType_Implementation();

	// Necessary time to start the interaction with the object. Can be used if we want to support interactions that need the player to press a button for a while before the
	// actual interaction is triggered. By default set to -1.0, which means no hold time is computed.
	virtual float GetInteractionHoldTime_Implementation();

	// Called by the Interactor Finder Component or other player related components.
	// Used to make the interactuable display user feedback like outlines, particles, ...
	virtual void SetInteractionFeedback_Implementation(EInteractor_CanBeInteractedResult InteractuableState);
};