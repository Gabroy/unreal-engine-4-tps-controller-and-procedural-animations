#pragma once

#include "PS_InteractorInterface.h"
#include "PS_InteractuableStructs.h"
#include "PS_InteractuableInterface.generated.h"

/*
* Abstract interface all interactuables should implement in order to be interacted with.
*
* Provides some methods to be implemented by interactuables so they can be manipulated by Interactor components.
* Description of what should be implemented by each method is provided in comments.
* 
* Methods provided here don't directly implement triggering actions on interaction as such methods vary depending on object and context.
* Interactor components must use this API to query the type of interactor actor they are dealing with and cast to the base class of the type to have
* access to the actual logic to interact with it.
* 
* The reason why we don't provide this methods is:
* 
*	An interact method for a weapon might not be the same depending if the weapon is in the ground or in the hands of an enemy. In the first
*	case, the Interact method would be used to pick up the gun, and in the second, to take it from the enemy's hands. To keep logic separated and
*	explicit, we prefer to leave those methods to base classes of each interactuable type which can be retrieved with GetInteractuableType.
* 
*	Certain objects might have more complicated interactions than simply a boolean DoInteraction, for example, if we want to support opening doors gradually
*	with the scroll button, which would not be a boolean operation.
* 
*/

UINTERFACE(MinimalAPI, Blueprintable)
class UPS_InteractuableInterface : public UInterface
{
	GENERATED_BODY()
};

class TPS_API IPS_InteractuableInterface
{

	GENERATED_BODY()

public:

	// Returns information about whether InstigatorInteractor can interact with the interactuable.
	// Perform object related check like if the interactuable is in the current state or the interactor has necessary resources.
	// Don't perform actor only checks here, like if the character has hands free or can perform interactions, keep that logic in the interactor component.
	// ReasonInteraction parameter can be used to return object specific user readable text explaining the reason why the interaction failed.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	EInteractuable_InteractionResult CanInteract(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor, FText& ReasonInteraction);
	virtual EInteractuable_InteractionResult CanInteract_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor, FText& ReasonInteraction) { return EInteractuable_InteractionResult::IIR_Yes; }

	// Called by the Interactor component immediately once it starts interacting with the object.
	// Can be used to know which actors are interacting with an object at any given time.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StartInteraction(TScriptInterface<IPS_InteractorInterface> & InstigatorInteractor);
	virtual void StartInteraction_Implementation(TScriptInterface<IPS_InteractorInterface> & InstigatorInteractor) {}

	// Called by an IInteractor actor immediately once it stops interacting with the object.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StopInteraction(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor);
	virtual void StopInteraction_Implementation(TScriptInterface<IPS_InteractorInterface>& InstigatorInteractor) {}

	// Returns true if the interactuable allows interactions. False if it's disabled and your want it to not be detected by interactuable systems.
	// Use CanInteract for checks for object specific interactuable conditions. This functions is only for disabling interactuable detection by interactors.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool AreInteractionsEnabled();
	virtual bool AreInteractionsEnabled_Implementation() { return true; }

	// Returns the type of the interactuable. Used by interactor components to know the type of interactuable they are interacting with
	// and cast to the corresponding class to perform the necessary interaction logic in the necessary context.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	EInteractuableTypes GetInteractuableType();
	virtual EInteractuableTypes GetInteractuableType_Implementation() = 0;

	// Necessary time to start the interaction with the object. Can be used if we want to support interactions that need the player to press a button for a while before the
	// actual interaction is triggered. By default set to -1.0, which means no hold time is computed.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	float GetInteractionHoldTime();
	virtual float GetInteractionHoldTime_Implementation() { return -1.0f; }

	/* User only functions */

	// Called by the Interactor Finder Component or other player related components.
	// Used to make the interactuable display user feedback like outlines, particles, ...
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetInteractionFeedback(EInteractor_CanBeInteractedResult InteractuableState);
	virtual void SetInteractionFeedback_Implementation(EInteractor_CanBeInteractedResult InteractuableState) = 0;
};