#include "PS_OutlineComponent.h"

#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"
#include "BoxSphereBounds.h"

const FOutlineType UOutlineTypeDataAsset::GetOutlineType(EOutlineTypeID OutlineTypeToFetch) const
{
	if (OutlineDataTypes.Contains(OutlineTypeToFetch))
		return OutlineDataTypes[OutlineTypeToFetch];
	return FOutlineType();
}

UPS_OutlineComponent::UPS_OutlineComponent() : UStaticMeshComponent()
{
	/* Disable tick on start */

	PrimaryComponentTick.bCanEverTick = true;

	/* Set default outline cube mesh, material and data asset for outline types. */
	static ConstructorHelpers::FObjectFinder<UOutlineTypeDataAsset> OutlineDataAsset(TEXT("/Game/Gameplay/Interactuables/Outline_Component/OutlineTypeDataAsset"));
	OutlinesTypesDataAsset = OutlineDataAsset.Object;

	/* Disable Tick on Start. Only enable when strictly necessary. */
	SetComponentTickEnabled(false);
}

void UPS_OutlineComponent::BeginPlay()
{
	Super::BeginPlay();

	/* Set collision profile. */

	AActor* OwnerActor = GetOwner();
	if (OwnerActor)
	{
		USceneComponent* RootComponent = OwnerActor->GetRootComponent();
		if (RootComponent)
		{
			if (RootComponent->Mobility == EComponentMobility::Type::Movable)
				SetCollisionProfileName("InteractuableAreaDynamic");
			else
				SetCollisionProfileName("InteractuableAreaStatic");
		}
	}

	/* Set mesh. */

	SetStaticMesh(OutlinesTypesDataAsset->GetOutlineStaticMesh());

	/* Create dynamic material for outlines. */

	UMaterialInterface* MatInterface = OutlinesTypesDataAsset->GetOutlineMaterial();
	if (MatInterface)
	{
		DynMaterial = UMaterialInstanceDynamic::Create(MatInterface, this);
		if (DynMaterial)
		{
			SetMaterial(0, DynMaterial);
		}
	}

	/* Set Outlines for element */

	RefreshOutlineBoundsForActor();
}

void UPS_OutlineComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (StencilNeedsRefresh && CurrentOpacityOutline <= 0.0f)
		RefreshStencil();

	if (IsFadingIn && !StencilNeedsRefresh)
		DoFadeIn(DeltaTime);
	else
		DoFadeOut(DeltaTime);
}

void UPS_OutlineComponent::DoFadeIn(float DeltaTime)
{
	CurrentOpacityOutline = FMath::Min(CurrentOpacityOutline + (DeltaTime * OutlineOpacitySpeed), 1.0f);

	if(DynMaterial)
		DynMaterial->SetScalarParameterValue("OutlineOpacity", CurrentOpacityOutline);

	// Disable once we have reached fade in.
	if (CurrentOpacityOutline >= 1.0f)
		SetComponentTickEnabled(false);
}

void UPS_OutlineComponent::DoFadeOut(float DeltaTime)
{
	CurrentOpacityOutline = FMath::Max(CurrentOpacityOutline - (DeltaTime * OutlineOpacitySpeed), 0.0f);

	if (DynMaterial)
		DynMaterial->SetScalarParameterValue("OutlineOpacity", CurrentOpacityOutline);

	if (CurrentOpacityOutline <= 0.0f)
		OnFadeOutFinished();
}

void UPS_OutlineComponent::OnFadeOutFinished()
{
	if (ShowOutlines)
	{
		// Start fade in again after fade out if still enabled.
		IsFadingIn = true;
	}
	else
	{
		// Disable component tick and set invisible.
		SetComponentTickEnabled(false);
		SetVisibility(false);
		SetCustomDepthForActor(GetOwner(), false, 0, true);
		StencilNeedsRefresh = true;
	}
}

void UPS_OutlineComponent::RefreshStencil()
{
	StencilNeedsRefresh = false;

	if (OutlinesTypesDataAsset)
	{
		FOutlineType OutlineTypeToSet = OutlinesTypesDataAsset->GetOutlineType(CurrentOutlineType);
		int StencilValueToSet = ((int)OutlineTypeToSet.OutlineStencilValue) + 1;
		if (DynMaterial)
		{
			DynMaterial->SetScalarParameterValue("OutlineStencil", StencilValueToSet);
			DynMaterial->SetScalarParameterValue("OutlineWidth", OutlineTypeToSet.OutlineBorderWidth);
			DynMaterial->SetScalarParameterValue("OutlineOpacity", 0.0f);
			DynMaterial->SetVectorParameterValue("OutlineColor", OutlineTypeToSet.OutlineColor);
		}
		SetCustomDepthForActor(GetOwner(), true, StencilValueToSet, true);
	}
}

void UPS_OutlineComponent::SetOutlineEnabled(bool Enabled)
{
	SetCollisionEnabled(Enabled ? ECollisionEnabled::QueryOnly : ECollisionEnabled::NoCollision);
}

void UPS_OutlineComponent::SetOutlinesVisibility(bool EnableOutlines)
{
	// Ignore if we already have the given status.
	if (ShowOutlines == EnableOutlines)
		return;

	ShowOutlines = EnableOutlines;
	if (ShowOutlines)
	{
		SetVisibility(true);
		IsFadingIn = true;
	}
	else
	{
		IsFadingIn = false;
	}

	SetComponentTickEnabled(true);
}

void UPS_OutlineComponent::SetOutlineType(EOutlineTypeID NewOutlineType)
{
	CurrentOutlineType = NewOutlineType;
	StencilNeedsRefresh = true;

	if (ShowOutlines)
	{
		IsFadingIn = false;
		SetComponentTickEnabled(true);
	}
}

// Compute bounds functions.
void UPS_OutlineComponent::RefreshOutlineBoundsForActor()
{
	// Compute bounds for actors and all it's childrens.
	FBoxSphereBounds TotalBounds = ComputeBoundsForActor(GetOwner(), true);

	// Set this component scale to the total bounds of the object.
	float ScaleTransform = TotalBounds.SphereRadius * (0.01f);
	SetRelativeScale3D(FVector(ScaleTransform, ScaleTransform, ScaleTransform));
}

// Returns true if outlines are enabled, false if they are disabled.
bool UPS_OutlineComponent::AreOutlinesEnabled() const
{
	return GetCollisionEnabled() == ECollisionEnabled::QueryOnly;
}

// Returns true if outlines are visible (opacity > 0.0f and not fading out).
bool UPS_OutlineComponent::AreOutlinesVisible() const
{
	return AreOutlinesEnabled() && CurrentOpacityOutline > 0.0f && IsFadingIn;
}

FBoxSphereBounds UPS_OutlineComponent::ComputeBoundsForActor(AActor * ActorToComputeTotalBounds, bool ComputeDescendants)
{
	// Compute bounds for components in actor.
	FBoxSphereBounds BoundsToCalculate = ComputeComponentsBoundsForActor(ActorToComputeTotalBounds);

	if (ComputeDescendants)
	{
		// Compute bounds for attached actors.
		TArray<AActor*> AttachedActors;
		ActorToComputeTotalBounds->GetAttachedActors(AttachedActors);
		for (int i = 0; i < AttachedActors.Num(); ++i)
			BoundsToCalculate = BoundsToCalculate + ComputeBoundsForActor(AttachedActors[i], ComputeDescendants);

		// Compute bounds for child actors.
		TArray<AActor*> ChildActors;
		ActorToComputeTotalBounds->GetAllChildActors(ChildActors);
		for (int i = 0; i < ChildActors.Num(); ++i)
			BoundsToCalculate = BoundsToCalculate + ComputeBoundsForActor(ChildActors[i], ComputeDescendants);
	}

	return BoundsToCalculate;
}

FBoxSphereBounds UPS_OutlineComponent::ComputeComponentsBoundsForActor(AActor * ActorToComputeTotalBounds)
{
	FBoxSphereBounds BoundsToCalculate = FBoxSphereBounds(EForceInit::ForceInitToZero);

	TArray<UActorComponent *> PrimitiveComponents;
	ActorToComputeTotalBounds->GetComponents(UPrimitiveComponent::StaticClass(), PrimitiveComponents);
	for (int i = 0; i < PrimitiveComponents.Num(); ++i)
	{
		UPrimitiveComponent * PrimComponent = (UPrimitiveComponent *)PrimitiveComponents[i];
		if (PrimComponent && PrimComponent != this)
		{
			if (FMath::IsNearlyEqual(BoundsToCalculate.SphereRadius, 0.0f))
				BoundsToCalculate = PrimComponent->Bounds;
			else
				BoundsToCalculate = BoundsToCalculate + PrimComponent->Bounds;
		}
	}

	return BoundsToCalculate;
}

// Set Custom Depth and Stencil.

void UPS_OutlineComponent::SetCustomDepthForActor(AActor* ActorToComputeTotalBounds, bool ActivateCustomDepth, int StencilValue, bool ComputeDescendants)
{
	if (!ActorToComputeTotalBounds)
		return;

	// Compute bounds for components in actor.
	SetCustomDepthForActorComponents(ActorToComputeTotalBounds, ActivateCustomDepth, StencilValue);

	if (ComputeDescendants)
	{
		// Compute bounds for attached actors.
		TArray<AActor*> AttachedActors;
		ActorToComputeTotalBounds->GetAttachedActors(AttachedActors);
		for (int i = 0; i < AttachedActors.Num(); ++i)
			SetCustomDepthForActor(AttachedActors[i], ActivateCustomDepth, StencilValue, ComputeDescendants);

		// Compute bounds for child actors.
		TArray<AActor*> ChildActors;
		ActorToComputeTotalBounds->GetAllChildActors(ChildActors);
		for (int i = 0; i < ChildActors.Num(); ++i)
			SetCustomDepthForActor(ChildActors[i], ActivateCustomDepth, StencilValue, ComputeDescendants);
	}
}

void UPS_OutlineComponent::SetCustomDepthForActorComponents(AActor* ActorToComputeTotalBounds, bool ActivateCustomDepth, int StencilValue)
{
	TArray<UActorComponent*> PrimitiveComponents;
	ActorToComputeTotalBounds->GetComponents(UPrimitiveComponent::StaticClass(), PrimitiveComponents);
	for (int i = 0; i < PrimitiveComponents.Num(); ++i)
	{
		UPrimitiveComponent* PrimComponent = (UPrimitiveComponent*)PrimitiveComponents[i];
		if (PrimComponent && PrimComponent != this)
		{
			PrimComponent->SetRenderCustomDepth(ActivateCustomDepth);
			PrimComponent->SetCustomDepthStencilValue(StencilValue);
		}
	}
}