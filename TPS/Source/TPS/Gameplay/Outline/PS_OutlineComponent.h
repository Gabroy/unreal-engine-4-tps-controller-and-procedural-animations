#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PS_OutlineComponent.generated.h"

UENUM(BlueprintType)
enum EOutlineTypeID
{
	OUTLINE_INTERACTION_DISABLED,
	OUTLINE_INTERACTION_ENABLED
};

USTRUCT(BlueprintType)
struct FOutlineType
{
	GENERATED_BODY()

	// Stencil value. Each outline type should use it's own value to differentiate.
	// Objects with same type will combine their outlines if they are close.
	// In general each outline type has it's own color although it's not strictly mandatory.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EOutlineTypeID> OutlineStencilValue;

	// Color used for the outline type.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FLinearColor OutlineColor;

	// Outline border width.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int OutlineBorderWidth = 1;
};

UCLASS(BlueprintType)
class TPS_API UOutlineTypeDataAsset : public UDataAsset
{

protected:

	GENERATED_BODY()

	// Reference to the static mesh used for displaying outlines for objects inside it's bounds.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMesh * OutlineStaticMeshRef;

	// Reference to the static mesh material used for rendering outlines.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UMaterial * OutlineMaterialRef;

	// Add here all outline types and their data.
	// Can be fetched by the outline component to get the necessary information.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<TEnumAsByte<EOutlineTypeID>, FOutlineType> OutlineDataTypes;

public:

	UFUNCTION(BlueprintCallable)
	UStaticMesh * GetOutlineStaticMesh() const { return OutlineStaticMeshRef; }

	UFUNCTION(BlueprintCallable)
	UMaterial * GetOutlineMaterial() const { return OutlineMaterialRef; }

	UFUNCTION(BlueprintCallable)
	const FOutlineType GetOutlineType(EOutlineTypeID OutlineTypeToFetch) const;
};

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_OutlineComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

protected:

	/* Variables */

	// Ptr to dynamic material instance we use to show outlines and allow fade in/out.
	UPROPERTY(BlueprintReadOnly)
	UMaterialInstanceDynamic * DynMaterial = nullptr;

	// Ptr to UDataAsset with reference to all outlines types. Used to fetch outline info.
	UPROPERTY(BlueprintReadOnly)
	UOutlineTypeDataAsset * OutlinesTypesDataAsset = nullptr;

	// Current outline type used by the object. Changes based on calls by Interactor components
	// to SetInteractionFeedback from interactuable objects interface.
	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<EOutlineTypeID> CurrentOutlineType;

	// True if we want to show up outlines, false if we want to hide them.
	UPROPERTY(BlueprintReadOnly)
	bool ShowOutlines = false;

	UPROPERTY(BlueprintReadOnly)
	bool IsFadingIn = false;

	UPROPERTY(BlueprintReadOnly)
	bool StencilNeedsRefresh = true;

	// Opacity of the outline.
	UPROPERTY(BlueprintReadOnly)
	float CurrentOpacityOutline = 0.0f;

	// How much opacity per second when fading in or out.
	UPROPERTY(BlueprintReadOnly)
	float OutlineOpacitySpeed = 1.0f;


public:

	UPS_OutlineComponent();
	
	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Enables or disables the outline volume completely. If disable, outlines won't show until enabled again.
	UFUNCTION(BlueprintCallable)
	void SetOutlineEnabled(bool Enabled);

	// Sets the visibility for outlines. If false, outlines will hide and won't be visible until this function is called again
	// with true.
	UFUNCTION(BlueprintCallable)
	void SetOutlinesVisibility(bool EnableOutlines);

	// Sets the type of the outline.
	UFUNCTION(BlueprintCallable)
	void SetOutlineType(EOutlineTypeID NewOutlineType);

	// Updates the bounds of the actors. Normally unnecessary unless the actor meshes
	// change, in which case it might very well be necessary.
	UFUNCTION(BlueprintCallable)
	void RefreshOutlineBoundsForActor();

	// Returns true if outlines are enabled, false if they are disabled.
	bool AreOutlinesEnabled() const;

	// Returns true if outlines are visible (opacity > 0.0f and not fading out).
	bool AreOutlinesVisible() const;

private:

	// Fade in - out functions.

	inline void DoFadeIn(float DeltaTime);

	inline void DoFadeOut(float DeltaTime);

	inline void OnFadeOutFinished();

	inline void RefreshStencil();

	// Compute bounds functions.

	UFUNCTION(BlueprintCallable)
	FBoxSphereBounds ComputeBoundsForActor(AActor * ActorToComputeTotalBounds, bool ComputeDescendants = true);

	UFUNCTION(BlueprintCallable)
	FBoxSphereBounds ComputeComponentsBoundsForActor(AActor * ActorToComputeTotalBounds);

	// Set Custom Depth and Stencil.

	UFUNCTION(BlueprintCallable)
	void SetCustomDepthForActor(AActor * ActorToComputeTotalBounds, bool ActivateCustomDepth, int StencilValue, bool ComputeDescendants = true);

	UFUNCTION(BlueprintCallable)
	void SetCustomDepthForActorComponents(AActor * ActorToComputeTotalBounds, bool ActivateCustomDepth, int StencilValue);
};
