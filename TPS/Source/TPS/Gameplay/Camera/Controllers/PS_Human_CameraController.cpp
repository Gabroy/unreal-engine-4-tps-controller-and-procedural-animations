#include "PS_Human_CameraController.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "../PS_PlayerCameraManager.h"
#include "GameFramework/SpringArmComponent.h"

void UPS_Human_CameraController::Init()
{
	AActor * Owner = GetOwner();
	if (!Owner) return;

	Character = Cast<ACharacter>(Owner);
	if (!Character) return;

	TArray<UActorComponent *> CameraArray = Owner->GetComponentsByTag(UCameraComponent::StaticClass(), "FPSCamera");
	if(CameraArray.Num())
		FPSCamera = Cast<UCameraComponent>(CameraArray.Last());

	CameraArray = Owner->GetComponentsByTag(UCameraComponent::StaticClass(), "TPSCamera");
	if(CameraArray.Num())
		TPSCamera = Cast<UCameraComponent>(CameraArray.Last());

	APlayerController * PlayerController = Cast<APlayerController>(Character->GetController());
	if (PlayerController)
	{
		PlayerCameraManager = Cast<APS_PlayerCameraManager>(PlayerController->PlayerCameraManager);
	}

	Start();
}

void UPS_Human_CameraController::ToogleViewMode()
{
	if (IsFPSMode)
		SetTPSMode();
	else
		SetFPSMode();
}

// Sets the camera to FPS.
void UPS_Human_CameraController::SetFPSMode()
{
	if (IsFPSMode) return;
	IsFPSMode = true;

	if(FPSCamera)
		FPSCamera->SetActive(true);

	if(TPSCamera)
		TPSCamera->SetActive(false);

	if (PlayerCameraManager && PlayerCameraManager->HasBlendedCamerasInStack())
		return;

	FViewTargetTransitionParams TransitionParams = GetFPSTPSFinalTransitionParams();
	PlayerCameraManager->SetViewTarget(Character, TransitionParams);
}

// Sets the camera to TPS.
void UPS_Human_CameraController::SetTPSMode()
{
	if (!IsFPSMode) return;
	IsFPSMode = false;

	if(FPSCamera)
		FPSCamera->SetActive(false);

	if(TPSCamera)
		TPSCamera->SetActive(true);

	if (PlayerCameraManager && PlayerCameraManager->HasBlendedCamerasInStack())
		return;

	FViewTargetTransitionParams TransitionParams = GetFPSTPSFinalTransitionParams();
	PlayerCameraManager->SetViewTarget(Character, TransitionParams);
}

FTransform UPS_Human_CameraController::GetCurrentCameraTransform()
{
	return PlayerCameraManager->GetCurrentCamera()->GetTransform();
}

FTransform UPS_Human_CameraController::GetTPSCameraTransform()
{
	return TPSCamera->GetComponentTransform();
}

FTransform UPS_Human_CameraController::GetFPSCameraTransform()
{
	return FPSCamera->GetComponentTransform();
}

void UPS_Human_CameraController::ChangeToDefaultCamera()
{
	ChangeToStateByName("Default", TimeToDefault, true);
}

void UPS_Human_CameraController::ChangeToZoomCamera()
{
	ChangeToStateByName("Aim", TimeToAim, true);
}

void UPS_Human_CameraController::ChangeToSprintCamera()
{
	ChangeToStateByName("Sprint", TimeToSprint, true);
}

FViewTargetTransitionParams UPS_Human_CameraController::GetFPSTPSFinalTransitionParams()
{
	UWorld* World = GetWorld();
	if (!World)
		return FPSTPSTransitionParams;

	FTimerManager& TimeManager = World->GetTimerManager();

	float TimeRemaining = 0.0f;
	if (TimeManager.IsTimerActive(TransitionTimer))
		TimeRemaining = TimeManager.GetTimerRemaining(TransitionTimer);

	FViewTargetTransitionParams Transition = FPSTPSTransitionParams;
	Transition.BlendTime -= TimeRemaining;

	World->GetTimerManager().SetTimer(TransitionTimer, Transition.BlendTime, false);

	return Transition;
}