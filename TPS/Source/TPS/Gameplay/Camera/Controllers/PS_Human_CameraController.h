#pragma once

#include "CoreMinimal.h"
#include "../../../Base/FSM/FSM_Component.h"
#include "PS_Human_CameraController.generated.h"

class APS_PlayerCameraManager;
class UCameraComponent;
class USpringArmComponent;

/*
* The camera controller class for normal humanoid characters.
* 
* Works as a FSM responsible for the transition between states.
*/
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_CameraController : public UFSM_Component
{
protected:

	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	ACharacter * Character = nullptr;

	UPROPERTY(BlueprintReadOnly)
	APS_PlayerCameraManager * PlayerCameraManager = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCameraComponent * FPSCamera = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCameraComponent * TPSCamera = nullptr;

	UPROPERTY(BlueprintReadOnly)
	USpringArmComponent* TPSSpringArm = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FViewTargetTransitionParams FPSTPSTransitionParams;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTimerHandle TransitionTimer;

	bool IsFPSMode = false;


	/* Camera variables. */

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeToDefault = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeToSprint = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeToAim = 0.5f;

public:

	void Init();

	/* FPS and TPS View Modes. */

	// Changes between FPS and TPS and viceversa.
	UFUNCTION(BlueprintCallable)
	void ToogleViewMode();

	UFUNCTION(BlueprintCallable)
	const bool IsInFPSMode() const { return IsFPSMode; }

	// Sets the camera to FPS.
	UFUNCTION(BlueprintCallable)
	void SetFPSMode();

	// Sets the camera to TPS.
	UFUNCTION(BlueprintCallable)
	void SetTPSMode();

	UFUNCTION()
	FTransform GetCurrentCameraTransform();

	UFUNCTION(BlueprintCallable)
	FTransform GetTPSCameraTransform();

	UFUNCTION(BlueprintCallable)
	FTransform GetFPSCameraTransform();

	/* Actions */

	UFUNCTION(BlueprintCallable)
	void ChangeToDefaultCamera();

	UFUNCTION(BlueprintCallable)
	void ChangeToZoomCamera();

	UFUNCTION(BlueprintCallable)
	void ChangeToSprintCamera();

protected:

	FViewTargetTransitionParams GetFPSTPSFinalTransitionParams();
};