#pragma once

#include "CoreMinimal.h"
#include "PS_CameraStructs.h"
#include "../../../Base/FSM/FSM_State.h"
#include "PS_BaseSpringArmCameraState.generated.h"

class UCameraComponent;
class USpringArmComponent;

/*
	Orbit state based on controller's rotation values for a camera
	with a spring arm attached.
*/
UCLASS(BlueprintType, Blueprintable, config = FSMState, meta = (ShortTooltip = "FSM Camera Orbit State."))
class TPS_API UPS_BaseSpringArmCameraState : public UFSM_State
{
protected:

	GENERATED_BODY()

	/* Camera variables. */

	UPROPERTY(BlueprintReadOnly)
	UCameraComponent * CameraComponent;

	UPROPERTY(BlueprintReadOnly)
	USpringArmComponent * CameraSpringArmComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCameraStateProperties CameraStateProperties;

	UPROPERTY(BlueprintReadOnly)
	FCameraStateProperties TransitionStateProperties;

public:

	/* Constructor. */

	UPS_BaseSpringArmCameraState() : UFSM_State(){}

	virtual ~UPS_BaseSpringArmCameraState() {}

	void Init_Implementation() override;

	void OnEnter_Implementation() override;

	void OnTransition_Implementation(float TransitionFactor) override;

	/* Camera state properties. */

	const FCameraStateProperties & GetCameraStateProperties() const { return CameraStateProperties; }

	void SetCameraStateProperties(const FCameraStateProperties & NewCameraStateProperties) { CameraStateProperties = NewCameraStateProperties; }

protected:

	void FetchCameraCurrentValues();
};