#include "PS_BaseSpringArmCameraState.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

void UPS_BaseSpringArmCameraState::Init_Implementation()
{
	AActor * Owner = GetOwnerActor();
	if (!Owner) return;

	CameraComponent = Cast<UCameraComponent>(Owner->GetComponentByClass(UCameraComponent::StaticClass()));
	CameraSpringArmComponent = Cast<USpringArmComponent>(Owner->GetComponentByClass(USpringArmComponent::StaticClass()));
	TickByDefault = false;

	// Fetch data from camera if true.
	if (CameraStateProperties.FetchInitCamera) {
		CameraStateProperties.CameraFOV = CameraComponent->FieldOfView;
		CameraStateProperties.CameraOffset = CameraSpringArmComponent->SocketOffset;
		CameraStateProperties.SpringArmLength = CameraSpringArmComponent->TargetArmLength;
	}
}

void UPS_BaseSpringArmCameraState::OnEnter_Implementation()
{
	// Get sure the camera spring arm component uses the rotation of the player controller (i.e the
	// input of the player).
	// This might be false if we are coming from a target state for a example where the camera
	// rotates to face a target.
	CameraSpringArmComponent->bUsePawnControlRotation = true;
	
	// Camera Spring Arm Component.
	FetchCameraCurrentValues();
}

void UPS_BaseSpringArmCameraState::FetchCameraCurrentValues()
{
	TransitionStateProperties.CameraOffset = CameraSpringArmComponent->SocketOffset;
	TransitionStateProperties.CameraFOV = CameraComponent->FieldOfView;
	TransitionStateProperties.SpringArmLength = CameraSpringArmComponent->TargetArmLength;
}

void UPS_BaseSpringArmCameraState::OnTransition_Implementation(float TransitionFactor)
{
	// Camera Component.
	CameraComponent->FieldOfView = FMath::Lerp(TransitionStateProperties.CameraFOV, CameraStateProperties.CameraFOV, TransitionFactor);
	
	// Camera Spring Arm Component.
	CameraSpringArmComponent->SocketOffset = FMath::Lerp(TransitionStateProperties.CameraOffset, CameraStateProperties.CameraOffset, TransitionFactor);
	CameraSpringArmComponent->TargetArmLength = FMath::Lerp(TransitionStateProperties.SpringArmLength, CameraStateProperties.SpringArmLength, TransitionFactor);
}