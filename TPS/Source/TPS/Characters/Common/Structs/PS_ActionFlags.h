#pragma once

#include "PS_ActionFlags.generated.h"

/*
* Action Struct.
* 
* Implements a cooldown used to implement the behaviour necessary to avoid spamming actions.
* We keep this data in a struct to keep code tidy.
* 
*/
USTRUCT(Blueprintable, BlueprintType)
struct FAction_Flags
{
	GENERATED_BODY()

protected:

	// Time to allow action to be executed again once pressed.
	// Can be used to prevent the spamming of the action for example.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float CooldownActionTime = 0.4f;

	FTimerHandle CooldownTimer;

public:

	// Call this when you want cooldown to get activated. Normally after you execute the action.
	inline void StartCooldown(UWorld * World);

	// Should be called before executing an action that has action flags. Check that we are not
	// in cooldown.
	inline bool IsCooldownFinished();
};

/*
* This struct has the necessary logic to help implement actions that require multiple calls to them in order to get activated
* and will get disabled if they are not issued given a certain time.
* 
* It's used to implement keyboard behaviour where we need to press a key multiple times in a small window of time in order to activate an action,
* if we don't it gets cancelled. It might also be used for other purposes if one deems it useful.
*/

DECLARE_DELEGATE(FDelegateAction)

USTRUCT(Blueprintable, BlueprintType)
struct F_ActionWithTimeWindow
{
	GENERATED_BODY()

protected:

	UWorld * WorldPtr = nullptr;

	// Number of times we need to call the activate function in order for it to be called.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int NumTimesPressedToActivate = 0;

	// Current number of times we have pressed the action. Will return to 0 once action is called or
	// canceled.
	int CurrentNumberOfTimesPressed = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeToCancelCountOnPressed = 0.5f;

	// If true, each time we press do action, timer gets reset.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool ResetTimerOnEachDoAction = false;

	// Delegate called if the action succeds.
	FDelegateAction OnPressedActionEnoughTimes;

	// Delegate called if action times out before it's completed.
	FDelegateAction OnTimeOutDelegate;

	// Handle to the timer.
	FTimerHandle TimerHandle;

public:

	// Call this and initialize the action, pass the world and delegates for what to call when the action is pressed enough time or for when time out happens.
	inline bool Init(UWorld* World, FDelegateAction const& NewOnPressedActionEnoughTimes);

	// Call this and initialize the action, pass the world and delegates for what to call when the action is pressed enough time or for when time out happens.
	inline bool Init(UWorld * NewWorld, FDelegateAction const & NewOnPressedActionEnoughTimes, FDelegateAction const & NewTimeOutDelegate);

	// Call this when you press the action. If the action is completed, it will call the OnPressedEnoughTimes delegate automatically.
	inline void PressAction();

	// Call this if you want to cancel the action for any reason. You can pass a bool set to true if you want to call the timeout function.
	inline void CancelAction(bool CallTimeOutFunction = false);

protected:

	// Called whenever time out is called.
	inline void StartTimeOut();
};