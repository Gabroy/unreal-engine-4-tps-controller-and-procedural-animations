#pragma once

#include "PS_Carrying_Structs_Enums.generated.h"

/*
 * Carrying Stances.
 *
 * Represents the carrying stance that is to be used by characters when carrying a given item.
 * 
 * Stances can be used by all types of characters and are used inside animation FSM
 * and carrying FSM to control the animations characters.
 */
UENUM(BlueprintType)
enum ECarryingStance
{
	Unarmed,
	OneHandedGun,
	TwoHandedGun,
};