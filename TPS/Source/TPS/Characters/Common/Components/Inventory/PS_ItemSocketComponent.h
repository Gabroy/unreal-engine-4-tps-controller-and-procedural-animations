#pragma once

#include "Components/SceneComponent.h"
#include "../../../../Gameplay/Items/PS_BaseItemStructs.h"
#include "PS_ItemSocketComponent.generated.h"

class APS_BaseItemActorInstance;

/*
* Simple component for representing a socket that can carry an item
*
* The component can be used as a socket and contains information about attached actor
* and supported items that can be stored in socket.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_ItemSocketComponent : public USceneComponent
{

	GENERATED_BODY()

protected:

	/* References. */

	// Get the name of the socket, used for identification.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName SocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TEnumAsByte<EItemCategory> SupportedItemCategoryInSocket;

	// Actor attached to socket. Nullptr if no actor is attached.
	UPROPERTY(BlueprintReadOnly)
	APS_BaseItemActorInstance * ActorItemAttachedToSocket = nullptr;

public:

	UPS_ItemSocketComponent();

	// Attaches actor to socket. Returns true if successful,
	// false if socket already has an actor or item is not supported by socket.
	UFUNCTION(BlueprintCallable)
	bool AttachActorToSocket(APS_BaseItemActorInstance * ActorToAttachToSocket);

	// Detaches actor from socket. Returns true if successful, false if socket was empty.
	UFUNCTION(BlueprintCallable)
	APS_BaseItemActorInstance * DettachActorFromSocket();

	// Returns true if socket is empty.
	UFUNCTION(BlueprintCallable)
	bool IsSocketEmpty() const;

	// Returns the socket name, used for easy identification in code.
	UFUNCTION(BlueprintCallable)
	const FName & GetSocketName() const { return SocketName; }

	// Returns the actor present in the socket, nullptr if none.
	UFUNCTION(BlueprintCallable)
	APS_BaseItemActorInstance * GetItemAttachedToSocket() const { return ActorItemAttachedToSocket; }

	// Returns the type of object that the socket supports. 
	UFUNCTION(BlueprintCallable)
	EItemCategory GetSupportedItemCategoryForSocket() const { return SupportedItemCategoryInSocket; }
};
