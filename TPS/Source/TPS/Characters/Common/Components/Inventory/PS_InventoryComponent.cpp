#include "PS_InventoryComponent.h"
#include "PS_ItemSocketComponent.h"
#include "Gameplay/Items/PS_BaseItem.h"

UPS_InventoryComponent::UPS_InventoryComponent() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_InventoryComponent::BeginPlay()
{
	UActorComponent::BeginPlay();

	FetchSocketsData();
}

bool UPS_InventoryComponent::AttachItemToSocket(const FName & SocketName, APS_BaseItemActorInstance* ActorItemAttachedToSocket)
{
	if (SocketsByName.Contains(SocketName))
	{
		UPS_ItemSocketComponent * ItemSocketPtr = SocketsByName[SocketName];
		if (ItemSocketPtr && ItemSocketPtr->IsSocketEmpty())
		{
			return ItemSocketPtr->AttachActorToSocket(ActorItemAttachedToSocket);
		}
	}
	return false;
}

APS_BaseItemActorInstance * UPS_InventoryComponent::DettachItemFromSocket(const FName & SocketName)
{
	if (SocketsByName.Contains(SocketName))
	{
		UPS_ItemSocketComponent * ItemSocketPtr = SocketsByName[SocketName];
		if (ItemSocketPtr && !ItemSocketPtr->IsSocketEmpty())
		{
			return ItemSocketPtr->DettachActorFromSocket();
		}
	}
	return nullptr;
}

bool UPS_InventoryComponent::CanAttachItemToSocket(const FName& SocketName) const
{
	if (SocketsByName.Contains(SocketName))
	{
		UPS_ItemSocketComponent* ItemSocketPtr = SocketsByName[SocketName];
		return ItemSocketPtr && ItemSocketPtr->IsSocketEmpty();
	}
	return false;
}

APS_BaseItemActorInstance * UPS_InventoryComponent::GetItemInSocket(const FName & SocketName) const
{
	if (SocketsByName.Contains(SocketName))
	{
		UPS_ItemSocketComponent* ItemSocketPtr = SocketsByName[SocketName];
		if (ItemSocketPtr)
		{
			return ItemSocketPtr->GetItemAttachedToSocket();
		}
	}
	return nullptr;
}

TArray<APS_BaseItemActorInstance*> UPS_InventoryComponent::GetAllItemsInSockets() const
{
	TArray<APS_BaseItemActorInstance*> ItemsArray = TArray<APS_BaseItemActorInstance*>();
	for (const TPair<FName, UPS_ItemSocketComponent * > & SocketsData : SocketsByName)
	{
		UPS_ItemSocketComponent * ItemSocketPtr = SocketsData.Value;
		if (ItemSocketPtr)
		{
			APS_BaseItemActorInstance * ItemInstance = ItemSocketPtr->GetItemAttachedToSocket();
			if (ItemInstance)
			{
				ItemsArray.Add(ItemInstance);
			}
		}
	}
	return ItemsArray;
}

bool UPS_InventoryComponent::IsSocketEmpty(const FName & SocketName) const
{
	if (SocketsByName.Contains(SocketName))
	{
		UPS_ItemSocketComponent * ItemSocketPtr = SocketsByName[SocketName];
		if (ItemSocketPtr)
		{
			return ItemSocketPtr->IsSocketEmpty();
		}
	}
	return false;
}

void UPS_InventoryComponent::StoreItemInBag(APS_BaseItemActorInstance * ActorItemAttachedToSocket)
{
	if (!ActorItemAttachedToSocket)
	{
		return;
	}

	EItemCategory CategoryOfItem = ActorItemAttachedToSocket->GetItemCategory();
	switch (CategoryOfItem)
	{
	case ITEM_Undefined:
		
		break;

	case ITEM_Weapon:
		StoreWeaponInBag(ActorItemAttachedToSocket);
		break;
	
	case ITEM_Consumable:
		StoreConsumableInBag(ActorItemAttachedToSocket);
		break;

	default:
		break;
	}

}

void UPS_InventoryComponent::StoreWeaponInBag(APS_BaseItemActorInstance* ActorItemAttachedToSocket)
{
	// TODO
}

void UPS_InventoryComponent::StoreConsumableInBag(APS_BaseItemActorInstance* ActorItemAttachedToSocket)
{
	// TODO
}

void UPS_InventoryComponent::FetchSocketsData()
{
	AActor * OwnerActor = GetOwner();
	if (!OwnerActor)
	{
		return;
	}

	TArray<UPS_ItemSocketComponent *> ItemSockets;
	OwnerActor->GetComponents<UPS_ItemSocketComponent>(ItemSockets);
	SocketsByName.Empty();
	for (int i = 0; i < ItemSockets.Num(); ++i)
	{
		UPS_ItemSocketComponent * ItemSock = ItemSockets[i];
		if (ItemSock)
		{
			SocketsByName.Add(ItemSock->GetSocketName(), ItemSock);
		}
	}
}