#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Gameplay/Items/PS_BaseItemStructs.h"
#include "GameInstance/Managers/Base/UUID/PS_SimpleUUIDStructs.h"
#include "PS_InventoryComponent.generated.h"

class ACharacter;
class AController;
class APS_BaseItemActorInstance;
class UPS_ItemSocketComponent;

USTRUCT(BlueprintType)
struct FItemSocketsArray
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	TArray<UPS_ItemSocketComponent *> SocketsArray;
};

USTRUCT()
struct FInventory_ItemData
{
	GENERATED_BODY()

	SimpleUUID UUID;

	TSoftClassPtr<APS_BaseItemActorInstance> ActorClass;

	TArray<SimpleUUID> AttachedItems;
};

/*
* Inventory component used to implement an inventory like system.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_InventoryComponent : public UActorComponent
{

	GENERATED_BODY()

protected:

	// Fetched on start in FetchSocketsData function from item socket components present in char.
	UPROPERTY(BlueprintReadOnly)
	TMap<FName, UPS_ItemSocketComponent *> SocketsByName;

	// TODO
	UPROPERTY()
	TArray<FInventory_ItemData> InventoryBagItems;

protected:

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	ACharacter * Owner;

	UPROPERTY(BlueprintReadOnly)
	AController * OwnerController;

public:

	UPS_InventoryComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	/* Socket Items Functions. */

	// Attached an item to the socket passed as parameter, returns true if successful, false otherwise.
	bool AttachItemToSocket(const FName & SocketName, APS_BaseItemActorInstance * ActorItemAttachedToSocket);

	// Detaches an item from a socket. Returns the ptr, nullptr if there was none.
	APS_BaseItemActorInstance * DettachItemFromSocket(const FName & SocketName);

	bool CanAttachItemToSocket(const FName& SocketName) const;

	bool IsSocketEmpty(const FName& SocketName) const;

	APS_BaseItemActorInstance * GetItemInSocket(const FName& SocketName) const;

	TArray<APS_BaseItemActorInstance *> GetAllItemsInSockets() const;

	/* Inventory Bag Functions */

	void StoreItemInBag(APS_BaseItemActorInstance * ActorItemAttachedToSocket);

private:

	void FetchSocketsData();

	void StoreWeaponInBag(APS_BaseItemActorInstance * ActorItemAttachedToSocket);

	void StoreConsumableInBag(APS_BaseItemActorInstance * ActorItemAttachedToSocket);
};
