#include "PS_ItemSocketComponent.h"
#include "Components/ChildActorComponent.h"
#include "Gameplay/Items/PS_BaseItem.h"

UPS_ItemSocketComponent::UPS_ItemSocketComponent() : USceneComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

bool UPS_ItemSocketComponent::AttachActorToSocket(APS_BaseItemActorInstance * ActorToAttachToSocket)
{
	if (!ActorToAttachToSocket || !IsSocketEmpty())
		return false;

	// Disable physics or we won't be able to attach the actor.
	ActorToAttachToSocket->SetSimulatePhysics(false);

	FAttachmentTransformRules TransformRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
	USceneComponent * RootComponentOfActorToAttach = ActorToAttachToSocket->GetRootComponent();
	if (RootComponentOfActorToAttach)
	{
		bool CorrectAttach = RootComponentOfActorToAttach->AttachToComponent(this, TransformRules);
		ActorItemAttachedToSocket = ActorToAttachToSocket;
	}
	return RootComponentOfActorToAttach != nullptr;
}

APS_BaseItemActorInstance * UPS_ItemSocketComponent::DettachActorFromSocket()
{
	if (IsSocketEmpty())
		return nullptr;

	FDetachmentTransformRules DetachmentRules = FDetachmentTransformRules(EDetachmentRule::KeepWorld, false);
	APS_BaseItemActorInstance * ActorInstance = ActorItemAttachedToSocket;
	ActorItemAttachedToSocket->DetachFromActor(DetachmentRules);
	ActorItemAttachedToSocket = nullptr;
	return ActorInstance;
}

bool UPS_ItemSocketComponent::IsSocketEmpty() const
{
	return ActorItemAttachedToSocket == nullptr;
}