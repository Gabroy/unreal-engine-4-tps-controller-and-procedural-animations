#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Animations/PS_Extended_AnimInstance.h"
#include "PS_Base_MontageComponent.generated.h"

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Base_MontageComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	template<typename T>
	struct FMontageEventData
	{
		T MontageDelegate;
		bool DeleteOnCall;

		FMontageEventData(T NewMontageDelegate, bool NewDeleteOnCall)
			: MontageDelegate(NewMontageDelegate), DeleteOnCall(NewDeleteOnCall){}
	};

	/* Variables */

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent * SkeletalMeshComp;

	int32 CurrentMontageInstance;

	// TMaps for the three diferent types of events.
	TMap<UAnimMontage*, TArray<FMontageEventData<FOnMontageStartedDynDelegate>>> OnMontageStartedMap;
	TMap<UAnimMontage*, TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>>> OnMontageBlendingOutMap;
	TMap<UAnimMontage*, TArray<FMontageEventData<FOnMontageEndedDynDelegate>>> OnMontageEndedMap;

	/* Protected functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnMontageStarted(UAnimMontage* Montage);
	UFUNCTION()
	void OnMontageBlendingOut(UAnimMontage* Montage, bool Interrupted);
	UFUNCTION()
	void OnMontageEnded(UAnimMontage* Montage, bool Interrupted);

public:

	UPS_Base_MontageComponent();
	
	// Plays an animation montage. If ForceMontage is true, montage will be played no matter if another montage is playing above. 
	UFUNCTION(BlueprintCallable)
	bool PlayAnimationMontage(UAnimMontage * NewMontageToPlay, float PlayRate = 1.0f, float StartingPosition = 0.0f, FName StartingSection = "", bool ForceMontage = false);

	UFUNCTION(BlueprintCallable)
	void StopAnimationMontage(UAnimMontage * NewMontageToPlay, float BlendingTimeOut = 1.0f);

	UFUNCTION(BlueprintCallable)
	void SetMontagePlayRate(UAnimMontage* MontagePlayrate, float NewPlayrate);

	UFUNCTION(BlueprintCallable)
	void MontageResume(UAnimMontage* MontageToResume);

	UFUNCTION(BlueprintCallable)
	void MontagePause(UAnimMontage* MontageToPause);

	UFUNCTION(BlueprintCallable)
	void MontageStop(UAnimMontage* MontageToStop, float BlendOutTime);

	// Returns the current montage being played.
	UFUNCTION(BlueprintCallable)
	UAnimMontage * GetCurrentMontagePlaying();

	// Returns the time the montage is playing.
	UFUNCTION(BlueprintCallable)
	float GetMontageCurrentTime(UAnimMontage * MontageToCheck);

	// Returns the montage playrate.
	UFUNCTION(BlueprintCallable)
	float GetMontagePlayrate(UAnimMontage* MontageToCheck);

	// Check if montage passed as parameter is playing.
	UFUNCTION(BlueprintCallable)
	bool IsPlayingMontage(UAnimMontage * MontageToCheck);

	// Returns true if a montage is being played.
	UFUNCTION(BlueprintCallable)
	bool IsAnyMontagePlaying();

	/* On Montage Started Functions */

	// Binds events to a montage when started. By default, bound events will get removed once they are called, so it's the coder responsibility to bind to the event
	// each time the montage is called. Otherwise, UnbindOnMontageEnded can be set to false. There is no prevention from binding an event multiple times. It's the
	// coder's responsibility to take care and prevent this if he has to.
	UFUNCTION(BlueprintCallable)
	void BindOnMontageStarted(UAnimMontage * MontageToBindTo, const FOnMontageStartedDynDelegate & InOnMontageStarted, bool UnbindOnMontageStarted = true);

	// Unbinds an event from a montage. Should be called by coders if they want to unbind an event before it gets called for any reason or, if they have set UnbindOnMontageStarted
	// to false when binding an event. In such case, it's the coder's responsibility to unbind the event when it has to.
	UFUNCTION(BlueprintCallable)
	bool UnbindFromMontageStarted(UAnimMontage * MontageToUnbindFrom, const FOnMontageStartedDynDelegate & InOnMontageStarted);

	// Unbinds all events from On Montage Started.
	UFUNCTION(BlueprintCallable)
	void UnbindAllFromMontageStarted();

	// Checks if the OnMontageStarted event for the given montage is bounded.
	UFUNCTION(BlueprintCallable)
	bool IsEventBoundedToOnMontageStarted(UAnimMontage* MontageToCheck, const FOnMontageStartedDynDelegate & InOnMontageStarted);

	/* On Montage Blending Out Functions */

	// Binds events to a montage when blending out. By default, bound events will get removed once they are called, so it's the coder responsibility to bind to the event
	// each time the montage is called. Otherwise, UnbindOnMontageEnded can be set to false. There is no prevention from binding an event multiple times. It's the
	// coder's responsibility to take care and prevent this if he has to.
	UFUNCTION(BlueprintCallable)
	void BindOnMontageBlendingOut(UAnimMontage * MontageToBindTo, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut, bool UnbindOnMontageBlendingOut = true);

	// Unbinds an event from a montage. Should be called by coders if they want to unbind an event before it gets called for any reason or, if they have set UnbindOnMontageBlendingOut
	// to false when binding an event. In such case, it's the coder's responsibility to unbind the event when it has to.
	UFUNCTION(BlueprintCallable)
	bool UnbindFromMontageBlendingOut(UAnimMontage * MontageToUnbindFrom, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut);

	// Unbinds all events from On Montage Blending Out.
	UFUNCTION(BlueprintCallable)
	void UnbindAllFromMontageBlendingOut();

	// Checks if the OnMontageBlendingOut event for the given montage is bounded.
	UFUNCTION(BlueprintCallable)
	bool IsEventBoundedToOnMontageBlendingOut(UAnimMontage* MontageToCheck, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut);

	/* On Montage Ended Functions */

	// Binds events to a montage when it ends. By default, bound events will get removed once they are called, so it's the coder responsibility to bind to the event
	// each time the montage is called. Otherwise, UnbindOnMontageEnded can be set to false. There is no prevention from binding an event multiple times. It's the
	// coder's responsibility to take care and prevent this if he has to.
	UFUNCTION(BlueprintCallable)
	void BindOnMontageEnded(UAnimMontage * MontageToBindTo, const FOnMontageEndedDynDelegate & InOnMontageEnded, bool UnbindOnMontageEnded = true);

	// Unbinds an event from a montage. Should be called by coders if they want to unbind an event before it gets called for any reason or, if they have set UnbindOnMontageEnded
	// to false when binding an event. In such case, it's the coder's responsibility to unbind the event when it has to.
	UFUNCTION(BlueprintCallable)
	bool UnbindFromMontageEnded(UAnimMontage * MontageToUnbindFrom, const FOnMontageEndedDynDelegate & InOnMontageEnded);

	// Unbinds all events from On Montage Ended.
	UFUNCTION(BlueprintCallable)
	void UnbindAllFromMontageEnded();

	// Checks if the OnMontageEnded event for the given montage is bounded.
	UFUNCTION(BlueprintCallable)
	bool IsEventBoundedToOnMontageEnded(UAnimMontage* MontageToCheck, const FOnMontageEndedDynDelegate & InOnMontageEnded);
};
