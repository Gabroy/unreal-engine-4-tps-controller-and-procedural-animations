#pragma once

#include "Components/ActorComponent.h"
#include "Characters/Human/Components/Locomotion/PS_Locomotion_Structs_DistanceMatching.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationInstance.h"
#include "Characters/Common/Components/Animations/PS_TurningControllerDefs.h"
#include "PS_TurningController.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnTurningBehaviourFinished, bool)

class UAnimSequence;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;

/*
* Responsible of implementing all the necessary logic for implementing turning behavior
* being responsible of turning the character and playing necessary animations.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_TurningController : public UActorComponent
{

	GENERATED_BODY()

protected:

	/* Components */

	UPS_Human_AnimationInstance * AnimationInstance;

	UPS_Human_LocomotionController * LocomotionController;

	UPS_Human_CarryingController* CarryingController;

	/* Base turning variables */

	FDistanceMatchingFloatValues TurningHandler;

	UPROPERTY(BlueprintReadOnly)
	bool IsTurningBehaviour = false;
	
	UPROPERTY(BlueprintReadOnly)
	float CurrentTurningSpeed = 0.0f;

	// Delta after we will start playing 180 degrees turning animations
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Base")
	float Turning180MinDelta = 120.0f;

	/* Turning speeds */

	// Default speed while playing turning animations for an idle character.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turning-Speeds")
	float IdleTurningSpeed = 90.0f;

	/* Turning animations. */

	UPROPERTY(EditAnywhere, Category = "Turning-Animations")
	FTurningAnimationsForContext TurningAnimsStandingUnarmed;
	
	UPROPERTY(EditAnywhere, Category = "Turning-Animations")
	FTurningAnimationsForContext TurningAnimsStandingTwoHandedGun;

	/* Turning callbacks */

	FOnTurningBehaviourFinished OnTurningBehaviourFinished;

	UPROPERTY()
	TArray<FTurningEndedCallbackData> UnbindOnTurningCallbackArray;

public:

	UPS_TurningController();

	virtual void BeginPlay() override;

	void BindTurningEndOrInterrupted(UObject * ObjectBindToTurning, TurningFinishedCallback FuncPtr, bool RemoveBindingOnCallbackCalled = true);

	void UnbindFromTurningEndedCallback(UObject * ObjectToUnbindBindFromCallback);

	void StartTurning(float AngleToTurnTo, float TurningVelocity = -1.0f);

	void StopTurning();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	bool CanTurn() const;

	bool IsTurning() const;

private:

	void PickTurningAnimationsToPlay(float DeltaAngleToTurn);
	void PlayTurningAnimation(const FTurningAnimationsForContext & AnimationsContext, float DeltaAngleToTurn);

	void BroadcastTurningFinished(bool TurningFinishedCompletely);

	void OnLocomotionStateChanged(FName NewStateName, int NewStateID);
	void OnLocomotionStanceChanged(ELocomotionStance NewStance);

	bool CheckCurrentLocomotionStateAllowsTurning() const;
};
