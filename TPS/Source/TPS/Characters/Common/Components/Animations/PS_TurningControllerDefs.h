#pragma once

#include "CoreMinimal.h"
#include "PS_TurningControllerDefs.generated.h"

typedef void (UObject::* TurningFinishedCallback)(bool);

USTRUCT()
struct FTurningEndedCallbackData
{
	GENERATED_BODY()

	UPROPERTY()
	UObject* ObjectBound;

	FDelegateHandle HandleDelegate;
};

/* Turning animations for a given context. */

USTRUCT(Blueprintable)
struct FTurningAnimationsForContext
{
	GENERATED_BODY()

	// Turning animations.
	UPROPERTY(EditAnywhere, Category = "TurningContext")
	UAnimSequence * LeftTurn90 = nullptr;
	UPROPERTY(EditAnywhere, Category = "TurningContext")
	UAnimSequence * LeftTurn180 = nullptr;
	UPROPERTY(EditAnywhere, Category = "TurningContext")
	UAnimSequence * RightTurn90 = nullptr;
	UPROPERTY(EditAnywhere, Category = "TurningContext")
	UAnimSequence * RightTurn180 = nullptr;
};