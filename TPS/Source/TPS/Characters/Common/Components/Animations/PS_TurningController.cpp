#include "PS_TurningController.h"
#include "GameFramework/Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Components/SkeletalMeshComponent.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationInstance.h"
#include "Characters/Human/Components/Locomotion/States/PS_LocState_Interface.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"

UPS_TurningController::UPS_TurningController() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UPS_TurningController::BeginPlay()
{
	Super::BeginPlay();

	ACharacter * Owner = Cast<ACharacter>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_TurningController::Init Owner Returned Null"));
		return;
	}

	LocomotionController = Owner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_TurningController::Init LocomotionController Returned Null"));
		return;
	}
	LocomotionController->OnChangedStateMulticast.AddUObject(this, &UPS_TurningController::OnLocomotionStateChanged);
	LocomotionController->OnChangedStanceMulticast.AddUObject(this, &UPS_TurningController::OnLocomotionStanceChanged);

	CarryingController = Owner->FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_TurningController::Init CarryingController Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Cast<USkeletalMeshComponent>(Owner->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SKMesh)
	{
		AnimationInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimationInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_TurningController::Init_Implementation AnimationInstance Returned Null"));
			return;
		}
	}

	IsTurningBehaviour = false;
	SetComponentTickEnabled(false);
}

void UPS_TurningController::BindTurningEndOrInterrupted(UObject* ObjectBindToTurning, TurningFinishedCallback FuncPtr, bool RemoveBindingOnCallbackCalled)
{
	if (!ObjectBindToTurning)
	{
		return;
	}

	FDelegateHandle HandleDelegate = OnTurningBehaviourFinished.AddUObject(ObjectBindToTurning, FuncPtr);
	
	if (RemoveBindingOnCallbackCalled)
	{
		FTurningEndedCallbackData CallbackData;
		CallbackData.HandleDelegate = HandleDelegate;
		CallbackData.ObjectBound = ObjectBindToTurning;
		UnbindOnTurningCallbackArray.Add(CallbackData);
	}
}

void UPS_TurningController::UnbindFromTurningEndedCallback(UObject * ObjectToUnbindBindFromCallback)
{
	for (auto CallbackDataIterator = UnbindOnTurningCallbackArray.CreateIterator(); CallbackDataIterator; ++CallbackDataIterator)
	{
		FTurningEndedCallbackData & CallbackData = (*CallbackDataIterator);
		if (CallbackData.ObjectBound == ObjectToUnbindBindFromCallback)
		{
			CallbackDataIterator.RemoveCurrent();
			OnTurningBehaviourFinished.Remove(CallbackData.HandleDelegate);
			break;
		}
	}
}

void UPS_TurningController::StartTurning(float AngleToTurnTo, float TurningVelocity)
{
	if (!LocomotionController)
		return;

	LocomotionController->SetTargetRotation(AngleToTurnTo);

	CurrentTurningSpeed = TurningVelocity >= 0.0f ? TurningVelocity : IdleTurningSpeed;

	float DeltaToAngleToTurn = UKismetMathLibrary::NormalizedDeltaRotator(LocomotionController->GetActorCurrentRotation(), FRotator(0.0f, AngleToTurnTo, 0.0f)).Yaw;
	TurningHandler.Init(DeltaToAngleToTurn, 0.0f);

	PickTurningAnimationsToPlay(DeltaToAngleToTurn);

	IsTurningBehaviour = true;
	SetComponentTickEnabled(true);
}

void UPS_TurningController::StopTurning()
{
	if (IsTurning())
	{
		IsTurningBehaviour = false;

		if (AnimationInstance)
		{
			AnimationInstance->StopTurning();
		}

		float DeltaToAngleToTurn = UKismetMathLibrary::NormalizedDeltaRotator(LocomotionController->GetActorCurrentRotation(), LocomotionController->GetActorTargetRotation()).Yaw;
		bool ReachedEndOfTurning = FMath::IsNearlyEqual(DeltaToAngleToTurn, 0.0f, 0.05f);
		BroadcastTurningFinished(ReachedEndOfTurning);

		SetComponentTickEnabled(false);
	}
}

void UPS_TurningController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!LocomotionController)
		return;

	// If moving, we immediately cancel turning behaviour.
	if (LocomotionController->GetIsMoving())
	{
		StopTurning();
		return;
	}

	// Rotate player if updating.
	LocomotionController->UpdateCharacterMeshRotation(DeltaTime, CurrentTurningSpeed);

	// Update animation instance factor so the animation plays correctly.
	if (AnimationInstance)
	{
		float DeltaToAngleToTurn = UKismetMathLibrary::NormalizedDeltaRotator(LocomotionController->GetActorCurrentRotation(), LocomotionController->GetActorTargetRotation()).Yaw;
		float TurnFactor = TurningHandler.GetFactor(DeltaToAngleToTurn);
		AnimationInstance->UpdateTurningAnimation(TurnFactor);

		// Stop turning once we are done.
		bool ReachedEndOfTurning = FMath::IsNearlyEqual(DeltaToAngleToTurn, 0.0f, 0.05f);
		if (ReachedEndOfTurning)
			StopTurning();
	}
}

bool UPS_TurningController::IsTurning() const
{
	return IsTurningBehaviour;
}

bool UPS_TurningController::CanTurn() const
{
	return !IsTurning() && CheckCurrentLocomotionStateAllowsTurning();
}

void UPS_TurningController::PickTurningAnimationsToPlay(float DeltaAngleToTurn)
{
	if (!LocomotionController || !CarryingController)
	{
		return;
	}

	ELocomotionStance CurrentLocomotionStance = LocomotionController->GetLocomotionStance();
	ECarryingStance CurrentCarryingStance = CarryingController->GetCurrentStance();

	switch (CurrentLocomotionStance)
	{
	case ELocomotionStance::Normal:
		switch (CurrentCarryingStance)
		{
		case ECarryingStance::Unarmed:
			PlayTurningAnimation(TurningAnimsStandingUnarmed, DeltaAngleToTurn);
			break;
		case ECarryingStance::TwoHandedGun:
			PlayTurningAnimation(TurningAnimsStandingTwoHandedGun, DeltaAngleToTurn);
			break;
		case ECarryingStance::OneHandedGun:
			PlayTurningAnimation(TurningAnimsStandingUnarmed, DeltaAngleToTurn);
			break;
		default:
			PlayTurningAnimation(TurningAnimsStandingUnarmed, DeltaAngleToTurn);
			break;
		}
		break;

	case ELocomotionStance::Crouch:
		// None for now at least.
		PlayTurningAnimation(TurningAnimsStandingUnarmed, DeltaAngleToTurn);
		break;

	default:
		PlayTurningAnimation(TurningAnimsStandingUnarmed, DeltaAngleToTurn);
		break;
	}
}

void UPS_TurningController::PlayTurningAnimation(const FTurningAnimationsForContext & AnimationsContext, float DeltaAngleToTurn)
{
	// Get animation to play.
	UAnimSequence * TurningAnimation = nullptr;
	float AbsAngle = FMath::Abs(DeltaAngleToTurn);
	if (DeltaAngleToTurn >= 0.0f)
		TurningAnimation = AbsAngle >= Turning180MinDelta ? AnimationsContext.LeftTurn180 : AnimationsContext.LeftTurn90;
	else
		TurningAnimation = AbsAngle >= Turning180MinDelta ? AnimationsContext.RightTurn180 : AnimationsContext.RightTurn90;

	if (AnimationInstance)
		AnimationInstance->PlayTurningAnimation(TurningAnimation);
}

void UPS_TurningController::BroadcastTurningFinished(bool TurningFinishedCompletely)
{
	if (OnTurningBehaviourFinished.IsBound())
		OnTurningBehaviourFinished.Broadcast(TurningFinishedCompletely);

	for (FTurningEndedCallbackData & CallbackData : UnbindOnTurningCallbackArray)
	{
		OnTurningBehaviourFinished.Remove(CallbackData.HandleDelegate);
	}
	UnbindOnTurningCallbackArray.Empty();
}

void UPS_TurningController::OnLocomotionStateChanged(FName NewStateName, int NewStateID)
{
	if (IsTurning() && CheckCurrentLocomotionStateAllowsTurning() == false)
	{
		StopTurning();
	}
}

void UPS_TurningController::OnLocomotionStanceChanged(ELocomotionStance NewStance)
{
	if (IsTurning() && CheckCurrentLocomotionStateAllowsTurning() == false)
	{
		StopTurning();
	}
}

bool UPS_TurningController::CheckCurrentLocomotionStateAllowsTurning() const
{
	if (!LocomotionController)
		return false;

	IPS_LocState_Interface * LocStateInterface = Cast<IPS_LocState_Interface>(LocomotionController->GetCurrentState());
	return LocStateInterface && LocStateInterface->CanTurn();
}