#include "PS_Base_MontageComponent.h"
#include "GameFramework/Character.h"
#include "Components/SkeletalMeshComponent.h"

UPS_Base_MontageComponent::UPS_Base_MontageComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_Base_MontageComponent::BeginPlay()
{
	Super::BeginPlay();	

	OnMontageStartedMap.Empty();
	OnMontageBlendingOutMap.Empty();
	OnMontageEndedMap.Empty();

	ACharacter * Owner = Cast<ACharacter>(GetOwner());
	if (!Owner) return;

	SkeletalMeshComp = Owner->FindComponentByClass<USkeletalMeshComponent>();
	if (!SkeletalMeshComp) return;

	UAnimInstance * SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalAnimInstance) return;

	SkeletalAnimInstance->OnMontageStarted.RemoveDynamic(this, &UPS_Base_MontageComponent::OnMontageStarted);
	SkeletalAnimInstance->OnMontageStarted.AddDynamic(this, &UPS_Base_MontageComponent::OnMontageStarted);

	SkeletalAnimInstance->OnMontageBlendingOut.RemoveDynamic(this, &UPS_Base_MontageComponent::OnMontageBlendingOut);
	SkeletalAnimInstance->OnMontageBlendingOut.AddDynamic(this, &UPS_Base_MontageComponent::OnMontageBlendingOut);

	SkeletalAnimInstance->OnMontageEnded.RemoveDynamic(this, &UPS_Base_MontageComponent::OnMontageEnded);
	SkeletalAnimInstance->OnMontageEnded.AddDynamic(this, &UPS_Base_MontageComponent::OnMontageEnded);
}

void UPS_Base_MontageComponent::OnMontageStarted(UAnimMontage* Montage)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>> * EventsForMontage = OnMontageStartedMap.Find(Montage);
	if (EventsForMontage)
	{
		for (int i = EventsForMontage->Num() - 1; i >= 0; --i)
		{
			FMontageEventData<FOnMontageStartedDynDelegate> & MontData = (*EventsForMontage)[i];
			MontData.MontageDelegate.Execute(Montage);
			if (MontData.DeleteOnCall)
				EventsForMontage->RemoveAt(i, 1, true);
		}
	}
}

void UPS_Base_MontageComponent::OnMontageBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(Montage);
	if (EventsForMontage)
	{
		for (int i = EventsForMontage->Num() - 1; i >= 0; --i)
		{
			FMontageEventData<FOnMontageBlendingOutDynDelegate> & MontData = (*EventsForMontage)[i];
			MontData.MontageDelegate.Execute(Montage, Interrupted);
			if (MontData.DeleteOnCall)
				EventsForMontage->RemoveAt(i, 1, true);
		}
	}
}

void UPS_Base_MontageComponent::OnMontageEnded(UAnimMontage* Montage, bool Interrupted)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>> * EventsForMontage = OnMontageEndedMap.Find(Montage);
	if (EventsForMontage)
	{
		for (int i = EventsForMontage->Num() - 1; i >= 0; --i)
		{
			FMontageEventData<FOnMontageEndedDynDelegate> & MontData = (*EventsForMontage)[i];
			MontData.MontageDelegate.Execute(Montage, Interrupted);
			if (MontData.DeleteOnCall)
				EventsForMontage->RemoveAt(i, 1, true);
		}
	}
}

bool UPS_Base_MontageComponent::PlayAnimationMontage(UAnimMontage * NewMontageToPlay, float PlayRate, float StartingPosition, FName StartingSection, bool ForceMontage)
{
	if (!SkeletalMeshComp) return false;

	UAnimInstance * SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if(!SkeletalMeshComp) return false;

	if (!ForceMontage)
	{
		if (SkeletalAnimInstance->IsAnyMontagePlaying())
			return false;
	}

	const float MontageLength = SkeletalAnimInstance->Montage_Play(NewMontageToPlay, PlayRate, EMontagePlayReturnType::MontageLength, StartingPosition);
	bool bPlayedSuccessfully = (MontageLength > 0.f);

	if(!bPlayedSuccessfully) return false;

	if (FAnimMontageInstance * MontageInstance = SkeletalAnimInstance->GetActiveInstanceForMontage(NewMontageToPlay))
	{
		CurrentMontageInstance = MontageInstance->GetInstanceID();
	}

	if (StartingSection != NAME_None)
	{
		SkeletalAnimInstance->Montage_JumpToSection(StartingSection, NewMontageToPlay);
	}

	return true;
}

void UPS_Base_MontageComponent::StopAnimationMontage(UAnimMontage* NewMontageToPlay, float BlendingTimeOut)
{
	if (!SkeletalMeshComp) return;

	UAnimInstance* SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalMeshComp) return;
	
	SkeletalAnimInstance->Montage_Stop(BlendingTimeOut, NewMontageToPlay);
}

void UPS_Base_MontageComponent::SetMontagePlayRate(UAnimMontage* MontagePlayrate, float NewPlayrate)
{
	if (!MontagePlayrate || !SkeletalMeshComp)
		return;

	UAnimInstance * SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalMeshComp) return;

	SkeletalAnimInstance->Montage_SetPlayRate(MontagePlayrate, NewPlayrate);
}

UAnimMontage * UPS_Base_MontageComponent::GetCurrentMontagePlaying()
{
	if (SkeletalMeshComp)
	{
		UAnimInstance* AnimInstance = SkeletalMeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			return AnimInstance->GetCurrentActiveMontage();
		}
	}

	return nullptr;
}

float UPS_Base_MontageComponent::GetMontageCurrentTime(UAnimMontage* MontageToCheck)
{
	if (SkeletalMeshComp)
	{
		UAnimInstance * AnimInstance = SkeletalMeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			return AnimInstance->Montage_GetPosition(MontageToCheck);
		}
	}
	
	return 0.0f;
}

void UPS_Base_MontageComponent::MontageResume(UAnimMontage* MontageToResume)
{
	if (!MontageToResume || !SkeletalMeshComp)
		return;

	UAnimInstance * SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalMeshComp) return;

	return SkeletalAnimInstance->Montage_Resume(MontageToResume);
}

void UPS_Base_MontageComponent::MontagePause(UAnimMontage * MontageToPause)
{
	if (!MontageToPause || !SkeletalMeshComp)
		return;

	UAnimInstance* SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalMeshComp) return;

	return SkeletalAnimInstance->Montage_Pause(MontageToPause);
}

void UPS_Base_MontageComponent::MontageStop(UAnimMontage* MontageToStop, float BlendOutTime)
{
	if (!MontageToStop || !SkeletalMeshComp)
		return;

	UAnimInstance* SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalMeshComp) return;

	return SkeletalAnimInstance->Montage_Stop(BlendOutTime, MontageToStop);
}

float UPS_Base_MontageComponent::GetMontagePlayrate(UAnimMontage* MontageToCheck)
{
	if (!MontageToCheck || !SkeletalMeshComp)
		return 0.0f;

	UAnimInstance* SkeletalAnimInstance = SkeletalMeshComp->GetAnimInstance();
	if (!SkeletalMeshComp) return 0.0f;

	return SkeletalAnimInstance->Montage_GetPlayRate(MontageToCheck);
}

bool UPS_Base_MontageComponent::IsPlayingMontage(UAnimMontage* MontageToCheck)
{
	if (SkeletalMeshComp)
	{
		UAnimInstance* AnimInstance = SkeletalMeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			return MontageToCheck == AnimInstance->GetCurrentActiveMontage();
		}
	}

	return nullptr;
}

bool UPS_Base_MontageComponent::IsAnyMontagePlaying()
{
	if (SkeletalMeshComp)
	{
		UAnimInstance* AnimInstance = SkeletalMeshComp->GetAnimInstance();
		if (AnimInstance)
		{
			return AnimInstance->IsAnyMontagePlaying();
		}
	}

	return false;
}

/* On Montage Started Functions */

void UPS_Base_MontageComponent::BindOnMontageStarted(UAnimMontage* MontageToBindTo, const FOnMontageStartedDynDelegate & InOnMontageStarted, bool UnbindOnMontageStarted)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>> * EventsForMontage = OnMontageStartedMap.Find(MontageToBindTo);
	if (!EventsForMontage)
	{
		TArray<FMontageEventData<FOnMontageStartedDynDelegate>> NewEventArray;
		NewEventArray.Add(FMontageEventData<FOnMontageStartedDynDelegate>(InOnMontageStarted, UnbindOnMontageStarted));
		OnMontageStartedMap.Add(MontageToBindTo, NewEventArray);
	}
	else
	{
		EventsForMontage->Add(FMontageEventData<FOnMontageStartedDynDelegate>(InOnMontageStarted, UnbindOnMontageStarted));
	}
}

bool UPS_Base_MontageComponent::UnbindFromMontageStarted(UAnimMontage * MontageToUnbindFrom, const FOnMontageStartedDynDelegate & InOnMontageStarted)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>> * EventsForMontage = OnMontageStartedMap.Find(MontageToUnbindFrom);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageStarted)
			{
				EventsForMontage->RemoveAt(i);
				return true;
			}
		}
	}
	return false;
}

void UPS_Base_MontageComponent::UnbindAllFromMontageStarted()
{
	OnMontageStartedMap.Empty();
}

bool UPS_Base_MontageComponent::IsEventBoundedToOnMontageStarted(UAnimMontage * MontageToCheck, const FOnMontageStartedDynDelegate & InOnMontageStarted)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>>* EventsForMontage = OnMontageStartedMap.Find(MontageToCheck);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageStarted)
			{
				return true;
			}
		}
	}
	return false;
}

/* On Montage Blending Out Functions */

void UPS_Base_MontageComponent::BindOnMontageBlendingOut(UAnimMontage * MontageToBindTo, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut, bool UnbindOnMontageBlendingOut)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(MontageToBindTo);
	if (!EventsForMontage)
	{
		TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> NewEventArray;
		NewEventArray.Add(FMontageEventData<FOnMontageBlendingOutDynDelegate>(InOnMontageBlendingOut, UnbindOnMontageBlendingOut));
		OnMontageBlendingOutMap.Add(MontageToBindTo, NewEventArray);
	}
	else
	{
		EventsForMontage->Add(FMontageEventData<FOnMontageBlendingOutDynDelegate>(InOnMontageBlendingOut, UnbindOnMontageBlendingOut));
	}
}

bool UPS_Base_MontageComponent::UnbindFromMontageBlendingOut(UAnimMontage * MontageToUnbindFrom, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(MontageToUnbindFrom);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageBlendingOut)
			{
				EventsForMontage->RemoveAt(i);
				return true;
			}
		}
	}
	return false;
}

void UPS_Base_MontageComponent::UnbindAllFromMontageBlendingOut()
{
	OnMontageBlendingOutMap.Empty();
}

bool UPS_Base_MontageComponent::IsEventBoundedToOnMontageBlendingOut(UAnimMontage * MontageToCheck, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(MontageToCheck);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageBlendingOut)
			{
				return true;
			}
		}
	}
	return false;
}

/* On Montage Ended Functions */

void UPS_Base_MontageComponent::BindOnMontageEnded(UAnimMontage* MontageToBindTo, const FOnMontageEndedDynDelegate & InOnMontageEnded, bool UnbindOnMontageEnded)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>> * EventsForMontage = OnMontageEndedMap.Find(MontageToBindTo);
	if (!EventsForMontage)
	{
		TArray<FMontageEventData<FOnMontageEndedDynDelegate>> NewEventArray;
		NewEventArray.Add(FMontageEventData<FOnMontageEndedDynDelegate>(InOnMontageEnded, UnbindOnMontageEnded));
		OnMontageEndedMap.Add(MontageToBindTo, NewEventArray);
	}
	else
	{
		EventsForMontage->Add(FMontageEventData<FOnMontageEndedDynDelegate>(InOnMontageEnded, UnbindOnMontageEnded));
	}
}

bool UPS_Base_MontageComponent::UnbindFromMontageEnded(UAnimMontage * MontageToUnbindFrom, const FOnMontageEndedDynDelegate & InOnMontageEnded)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>> * EventsForMontage = OnMontageEndedMap.Find(MontageToUnbindFrom);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageEnded)
			{
				EventsForMontage->RemoveAt(i);
				return true;
			}
		}
	}
	return false;
}

void UPS_Base_MontageComponent::UnbindAllFromMontageEnded()
{
	OnMontageEndedMap.Empty();
}

bool UPS_Base_MontageComponent::IsEventBoundedToOnMontageEnded(UAnimMontage* MontageToCheck, const FOnMontageEndedDynDelegate & InOnMontageEnded)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>>* EventsForMontage = OnMontageEndedMap.Find(MontageToCheck);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageEnded)
			{
				return true;
			}
		}
	}
	return false;
}