#include "PS_Base_InteractorComponent.h"
#include "Gameplay/Interactuable/PS_InteractuableStructs.h"
#include "Gameplay/Interactuable/PS_InteractorInterface.h"
#include "Gameplay/Interactuable/PS_InteractuableInterface.h"

UPS_Base_InteractorComponent::UPS_Base_InteractorComponent() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_Base_InteractorComponent::BeginPlay()
{
	Super::BeginPlay();

	AActor * OwnerActor = GetOwner();
	if (!OwnerActor)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Base_InteractorComponent::BeginPlay GetOwner Returned Null"));
		return;
	}

	if (OwnerActor->GetClass()->ImplementsInterface(UPS_InteractorInterface::StaticClass()))
	{
		InteractorInterface.SetInterface(Cast<IPS_InteractorInterface>(OwnerActor));
		InteractorInterface.SetObject(OwnerActor);
	}

	if (!InteractorInterface)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Base_InteractorComponent::Init InteractorInterface Returned Null"));
		return;
	}
}

bool UPS_Base_InteractorComponent::StartInteraction(TScriptInterface<IPS_InteractuableInterface> & InteractiveActor)
{
	// Check we allow interactions. And we are are not already interacting with the object.
	if ((!CheckInteractionsAreEnabled()) || (CurrentInteractingObjectInterface == InteractiveActor))
		return false;

	// Check all conditions for interactions if bool is true.
	FText InteractionReasonInfo;
	EInteractor_CanBeInteractedResult InteractorResult = CanInteractWithInteractuable(InteractiveActor, InteractionReasonInfo);
	if (InteractorResult != EInteractor_CanBeInteractedResult::ICBIR_Yes)
		return false;

	// Set the interactuable as the currently interacting object.
	CurrentInteractingObjectInterface = InteractiveActor;

	// Based on the type of interactuable, start the necessary interaction behaviour.
	OnStartInteractionBehaviour();

	return true;
}

bool UPS_Base_InteractorComponent::CancelInteraction()
{
	// Check we are actually interacting with an object.
	if (CurrentInteractingObjectInterface.GetObject() == nullptr)
		return false;

	// Based on the type of interactuable, cancel the necessary interaction behaviour.
	OnCancelInteractionBehaviour();

	return true;
}

void UPS_Base_InteractorComponent::FinishInteraction()
{
	CurrentInteractingObjectInterface.SetInterface(nullptr);
	CurrentInteractingObjectInterface.SetObject(nullptr);
}

EInteractor_CanBeInteractedResult UPS_Base_InteractorComponent::CanInteractWithInteractuable(
	TScriptInterface<IPS_InteractuableInterface>& InteractuableActorInterface, FText& ReasonInteraction)
{
	ReasonInteraction = FText();

	// Prevent interacting if we are already interacting with the object.
	if (CurrentInteractingObjectInterface == InteractuableActorInterface)
		return EInteractor_CanBeInteractedResult::ICBIR_AlreadyInteracting;

	// Check we can interact with the actor.
	UObject* InteractorObject = InteractuableActorInterface.GetObject();
	if (!IPS_InteractuableInterface::Execute_AreInteractionsEnabled(InteractorObject))
		return EInteractor_CanBeInteractedResult::ICBIR_Disabled;

	// Return not interactuable if we are out of bounds.
	EInteractuable_InteractionResult CanInteractWith = IPS_InteractuableInterface::Execute_CanInteract(InteractorObject, InteractorInterface, ReasonInteraction);
	if (CanInteractWith == EInteractuable_InteractionResult::IIR_Failed_OutsideInteractionBounds)
		return EInteractor_CanBeInteractedResult::ICBIR_Disabled;

	// If we have not met object conditions, return failed interactuable conditions.
	if (CanInteractWith == EInteractuable_InteractionResult::IIR_Failed_ConditionsNotMet)
		return EInteractor_CanBeInteractedResult::ICBIR_Failed_Interactuable_Conditions;

	// Perform actor checks here in the future.
	return CheckActorCanInteractWithInteractuable(InteractuableActorInterface, ReasonInteraction);
}

bool UPS_Base_InteractorComponent::CheckInteractionsAreEnabled()
{
	return InteractionsAreEnabled > 0;
}

void UPS_Base_InteractorComponent::SetInteractorEnabled(bool EnableInteractions)
{
	bool DifferentValue = CheckInteractionsAreEnabled() != EnableInteractions;
	if (DifferentValue)
	{
		InteractionsAreEnabled = InteractionsAreEnabled + EnableInteractions ? 1 : -1;
		OnInteractorEnabledCallback.Broadcast(InteractionsAreEnabled > 0);
	}
}