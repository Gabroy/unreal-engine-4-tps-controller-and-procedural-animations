#pragma once

#include "CoreMinimal.h"
#include "Gameplay/Interactuable/PS_InteractuableStructs.h"
#include "PS_Base_InteractorComponent.generated.h"

class IPS_InteractuableInterface;
class IPS_InteractorInterface;

/*
* Interactor components are responsible of implementing the necessary logic for allowing a character to correctly
* interact with different types of interactuables.
* 
* This abstract base class should be implemented by child classes filling the logic that all interactor component should have.
* Allows overriding a series of predefined functions for fast adaption of interactor components to each case.
* 
* Examples of the type of logic that should be implemented inside these components can be found below:
* 
* For objects, we should implement all the functions necessary to pick the item. These are: montages and also storing objects once we pick them.
* For stairs, we should implemente the logic to set the locomotion controller to stair mode and play any necessary montages if there are any.
* For doors, we should implement the logic for opening doors, ...
* Etcetera ...
* 
*/

DECLARE_MULTICAST_DELEGATE_OneParam(FInteractorComponentEnabledStatus, bool)

UCLASS(Abstract, BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Base_InteractorComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Interactor variables. */

	// Interactions are disabled if < 1.
	int InteractionsAreEnabled = 1;
	
	UPROPERTY(BlueprintReadOnly)
	TScriptInterface<IPS_InteractorInterface> InteractorInterface;

	UPROPERTY(BlueprintReadOnly)
	TScriptInterface<IPS_InteractuableInterface> CurrentInteractingObjectInterface;

	/* Protected functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UPS_Base_InteractorComponent();

	// Callback called when interactor gets enabled or disabled.
	FInteractorComponentEnabledStatus OnInteractorEnabledCallback;

	// Call this method when you want to start interacting with an actor that implements the interactuable interface.
	// Returns true if success.
	UFUNCTION(BlueprintCallable)
	bool StartInteraction(TScriptInterface<IPS_InteractuableInterface> & InteractiveActor);

	// Stops an ongoing interaction if there is one going on.
	// Returns true if success.
	UFUNCTION(BlueprintCallable)
	bool CancelInteraction();

	// Checks the interactuable meets all conditions for interactions. These includes both interactuable and interactor conditions.
	// Returns an enum explaining whether we allow interactions or not, and if such, why.
	// ReasonInteraction returns user friendly text explaining reason why interaction failed.
	UFUNCTION(BlueprintCallable)
	EInteractor_CanBeInteractedResult CanInteractWithInteractuable(TScriptInterface<IPS_InteractuableInterface>& InteractuableActorInterface, FText& ReasonInteraction);

	// Checks if we can perform interactions. Returns true if so.
	// Include here all checks that can disable interactions on a whole and are not related to specific interactuable objects.
	UFUNCTION(BlueprintCallable)
	virtual bool CheckInteractionsAreEnabled();

	/* Callbacks and others. */

	// Allows activating or deactivating interactions completely.
	UFUNCTION(BlueprintCallable)
	void SetInteractorEnabled(bool EnableInteractions);

	// Returns the current object we are interacting with.
	UFUNCTION(BlueprintCallable)
	TScriptInterface<IPS_InteractuableInterface> & GetCurrentInteractingObjectInterface() { return CurrentInteractingObjectInterface; }

protected:

	// Implement here any start interaction based on the type of interactuable we are interacting with.
	// This interactuable is stored in CurrentInteractingObjectInterface.
	virtual void OnStartInteractionBehaviour(){}

	// Implement here any stop interaction based on the type of interactuable we are interacting with.
	// This interactuable is stored in CurrentInteractingObjectInterface.
	virtual void OnCancelInteractionBehaviour(){}

	// Implement here any checks related to the actor with this component that should be performed to decide
	// if we can interact with the interactuable. For example, if the actor has hands free for example or is in the correct mode to interact
	// with other interactuables.
	virtual EInteractor_CanBeInteractedResult CheckActorCanInteractWithInteractuable(TScriptInterface<IPS_InteractuableInterface>& InteractuableActorInterface, FText& ReasonInteraction) { return EInteractor_CanBeInteractedResult::ICBIR_Yes; }

	// Should be called by interaction methods once they are done.
	void FinishInteraction();
};
