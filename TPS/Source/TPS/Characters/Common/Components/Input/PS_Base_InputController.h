#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_Component.h"
#include "Components/InputComponent.h"
#include "PS_Base_InputController.generated.h"

/*
* Base Input controller FSM.
* 
* Base class for inputs controllers. Provides some necessary functionality bundled with the FSM code
* for manipulating input bindings.
* 
* Classes that inherit from this can implement bindings in the base class or any substate if necessary.
* 
* Take a look at HumanInputController or Human Carrying Input Controller for examples.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Base_InputController : public UFSM_Component
{

protected:

	GENERATED_BODY()

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	UInputComponent * InputComponent;

public:

	// Initializes the controller, storing all necessary values.
	virtual void Init();

	// Tick Component. Update states.
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	/* Input functions */

	template<class UserClass>
	FInputAxisBinding & BindAxis(const FName AxisName, UserClass* Object, typename FInputAxisHandlerSignature::TUObjectMethodDelegate<UserClass>::FMethodPtr Func)
	{
		return InputComponent->BindAxis(AxisName, Object, Func);
	}
	
	template<class UserClass>
	FInputActionBinding & BindAction(const FName ActionName, const EInputEvent KeyEvent, UserClass* Object, typename FInputActionHandlerSignature::TUObjectMethodDelegate< UserClass >::FMethodPtr Func)
	{
		return InputComponent->BindAction(ActionName, KeyEvent, Object, Func);
	}
	
	void RemoveActionBindingForHandle(const int32 Handle);
	
	void RemoveAxis(const FInputAxisBinding & AxisBind);
};
