#include "PS_Base_InputController.h"

void UPS_Base_InputController::Init()
{
	AActor * Owner = GetOwner();
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Base_InputController::Init Owner Returned Null"));
		return;
	}

	InputComponent = Owner->FindComponentByClass<UInputComponent>();
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Base_InputController::Init InputComponent Returned Null"));
		return;
	}

	// Start FSM.
	Start();
}

void UPS_Base_InputController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	// Here we update the current locomotion state.
	UFSM_Component::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

/* Input functions */

void UPS_Base_InputController::RemoveActionBindingForHandle(const int32 Handle)
{
	if (InputComponent)
		InputComponent->RemoveActionBindingForHandle(Handle);
}

void UPS_Base_InputController::RemoveAxis(const FInputAxisBinding& AxisBind)
{
	if (!InputComponent)
		return;

	for (int i = 0; i < InputComponent->AxisBindings.Num(); ++i)
	{
		FInputAxisBinding& AxisToCheckToRemove = InputComponent->AxisBindings[i];

		if (AxisToCheckToRemove.AxisName == AxisBind.AxisName) {
			InputComponent->AxisBindings.RemoveAt(i);
			break;
		}
	}
}