#include "PS_Extended_AnimInstance.h"

/* Protected functions. */

void UPS_Extended_AnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

	OnMontageStartedMap.Empty();
	OnMontageBlendingOutMap.Empty();
	OnMontageEndedMap.Empty();

	/* Bind to montage delegates from anim instance */

	OnMontageStarted.RemoveDynamic(this, &UPS_Extended_AnimInstance::OnMontageStartedDispatch);
	OnMontageStarted.AddDynamic(this, &UPS_Extended_AnimInstance::OnMontageStartedDispatch);
	OnMontageBlendingOut.RemoveDynamic(this, &UPS_Extended_AnimInstance::OnMontageBlendingOutDispatch);
	OnMontageBlendingOut.AddDynamic(this, &UPS_Extended_AnimInstance::OnMontageBlendingOutDispatch);
	OnMontageEnded.RemoveDynamic(this, &UPS_Extended_AnimInstance::OnMontageEndedDispatch);
	OnMontageEnded.AddDynamic(this, &UPS_Extended_AnimInstance::OnMontageEndedDispatch);
}

// Called when montage event OnStarted is called.
void UPS_Extended_AnimInstance::OnMontageStartedDispatch(UAnimMontage * Montage)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>> * EventsForMontage = OnMontageStartedMap.Find(Montage);
	if (EventsForMontage)
	{
		for (int i = EventsForMontage->Num() - 1; i >= 0; --i)
		{
			FMontageEventData<FOnMontageStartedDynDelegate> & MontData = (*EventsForMontage)[i];
			MontData.MontageDelegate.ExecuteIfBound(Montage);
			if (MontData.DeleteOnCall)
				EventsForMontage->RemoveAt(i, 1, true);
		}
	}
}

// Called when montage event OnBlendingOutOrInterrupted is called.
void UPS_Extended_AnimInstance::OnMontageBlendingOutDispatch(UAnimMontage* Montage, bool Interrupted)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(Montage);
	if (EventsForMontage)
	{
		for (int i = EventsForMontage->Num() - 1; i >= 0; --i)
		{
			FMontageEventData<FOnMontageBlendingOutDynDelegate> & MontData = (*EventsForMontage)[i];
			MontData.MontageDelegate.ExecuteIfBound(Montage, Interrupted);
			if (MontData.DeleteOnCall)
				EventsForMontage->RemoveAt(i, 1, true);
		}
	}
}

// Called when montage event OnEnded is called.
void UPS_Extended_AnimInstance::OnMontageEndedDispatch(UAnimMontage* Montage, bool Interrupted)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>> * EventsForMontage = OnMontageEndedMap.Find(Montage);
	if (EventsForMontage)
	{
		for (int i = EventsForMontage->Num() - 1; i >= 0; --i)
		{
			FMontageEventData<FOnMontageEndedDynDelegate> & MontData = (*EventsForMontage)[i];
			MontData.MontageDelegate.ExecuteIfBound(Montage, Interrupted);
			if (MontData.DeleteOnCall)
				EventsForMontage->RemoveAt(i, 1, true);
		}
	}
}

/* Montage functionality */

// Plays an animation montage. If ForceMontage is true, montage will be played no matter if another montage is playing above. 
bool UPS_Extended_AnimInstance::PlayAnimationMontage(UAnimMontage * NewMontageToPlay, float PlayRate, float StartingPosition, FName StartingSection, bool ForceMontage)
{
	if (!ForceMontage)
	{
		if (IsAnyMontagePlaying())
			return false;
	}

	const float MontageLength = Montage_Play(NewMontageToPlay, PlayRate, EMontagePlayReturnType::MontageLength, StartingPosition);
	bool bPlayedSuccessfully = (MontageLength > 0.f);

	if(!bPlayedSuccessfully) return false;

	if (FAnimMontageInstance * MontageInstance = GetActiveInstanceForMontage(NewMontageToPlay))
	{
		CurrentMontageInstance = MontageInstance->GetInstanceID();
	}

	if (StartingSection != NAME_None)
	{
		Montage_JumpToSection(StartingSection, NewMontageToPlay);
	}

	return true;
}

// Resumes the montage if it was paused before.
void UPS_Extended_AnimInstance::MontageResume(UAnimMontage * MontageToResume)
{
	Montage_Resume(MontageToResume);
}

// Pauses the montage if it is playing.
void UPS_Extended_AnimInstance::MontagePause(UAnimMontage* MontageToPause)
{
	Montage_Pause(MontageToPause);
}

// Stops the montage if there are any playing.
void UPS_Extended_AnimInstance::MontageStopPlaying(float BlendOutTime)
{
	Montage_Stop(BlendOutTime, nullptr);
}

// Stops the montage if it is played. Blend out time controls how long it will take until the montage blends out and gets removed.
void UPS_Extended_AnimInstance::MontageStop(UAnimMontage* MontageToStop, float BlendOutTime)
{
	Montage_Stop(BlendOutTime, MontageToStop);
}

// Sets the play rate for the given montage.
void UPS_Extended_AnimInstance::SetMontagePlayRate(UAnimMontage * MontagToModify, float NewPlayrate)
{
	Montage_SetPlayRate(MontagToModify, NewPlayrate);
}

// Returns the current montage being played.
UAnimMontage * UPS_Extended_AnimInstance::GetCurrentMontagePlaying()
{
	return GetCurrentActiveMontage();
}

// Returns the time the montage is playing.
float UPS_Extended_AnimInstance::GetMontageCurrentTime(UAnimMontage * MontageToCheck)
{
	return Montage_GetPosition(MontageToCheck);
}

// Returns the montage playrate.
float UPS_Extended_AnimInstance::GetMontagePlayrate(UAnimMontage* MontageToCheck)
{
	return Montage_GetPlayRate(MontageToCheck);
}

// Check if montage passed as parameter is playing.
bool UPS_Extended_AnimInstance::IsPlayingMontage(UAnimMontage* MontageToCheck)
{
	return MontageToCheck != nullptr && MontageToCheck == GetCurrentActiveMontage();
}

// Check if any montage is being played.
bool UPS_Extended_AnimInstance::IsPlayingAnyMontage()
{
	return GetCurrentActiveMontage() != nullptr;
}

/* On Montage Started Functions */

void UPS_Extended_AnimInstance::BindOnMontageStarted(UAnimMontage* MontageToBindTo, const FOnMontageStartedDynDelegate & InOnMontageStarted, bool UnbindOnMontageStarted)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>> * EventsForMontage = OnMontageStartedMap.Find(MontageToBindTo);
	if (!EventsForMontage)
	{
		TArray<FMontageEventData<FOnMontageStartedDynDelegate>> NewEventArray;
		NewEventArray.Add(FMontageEventData<FOnMontageStartedDynDelegate>(InOnMontageStarted, UnbindOnMontageStarted));
		OnMontageStartedMap.Add(MontageToBindTo, NewEventArray);
	}
	else
	{
		EventsForMontage->Add(FMontageEventData<FOnMontageStartedDynDelegate>(InOnMontageStarted, UnbindOnMontageStarted));
	}
}

bool UPS_Extended_AnimInstance::UnbindFromMontageStarted(UAnimMontage * MontageToUnbindFrom, const FOnMontageStartedDynDelegate & InOnMontageStarted)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>> * EventsForMontage = OnMontageStartedMap.Find(MontageToUnbindFrom);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageStarted)
			{
				EventsForMontage->RemoveAt(i);
				return true;
			}
		}
	}
	return false;
}

void UPS_Extended_AnimInstance::UnbindAllFromMontageStarted()
{
	OnMontageStartedMap.Empty();
}

bool UPS_Extended_AnimInstance::IsEventBoundedToOnMontageStarted(UAnimMontage * MontageToCheck, const FOnMontageStartedDynDelegate & InOnMontageStarted)
{
	TArray<FMontageEventData<FOnMontageStartedDynDelegate>>* EventsForMontage = OnMontageStartedMap.Find(MontageToCheck);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageStarted)
			{
				return true;
			}
		}
	}
	return false;
}

/* On Montage Blending Out Functions */

void UPS_Extended_AnimInstance::BindOnMontageBlendingOut(UAnimMontage * MontageToBindTo, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut, bool UnbindOnMontageBlendingOut)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(MontageToBindTo);
	if (!EventsForMontage)
	{
		TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> NewEventArray;
		NewEventArray.Add(FMontageEventData<FOnMontageBlendingOutDynDelegate>(InOnMontageBlendingOut, UnbindOnMontageBlendingOut));
		OnMontageBlendingOutMap.Add(MontageToBindTo, NewEventArray);
	}
	else
	{
		EventsForMontage->Add(FMontageEventData<FOnMontageBlendingOutDynDelegate>(InOnMontageBlendingOut, UnbindOnMontageBlendingOut));
	}
}

bool UPS_Extended_AnimInstance::UnbindFromMontageBlendingOut(UAnimMontage * MontageToUnbindFrom, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(MontageToUnbindFrom);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageBlendingOut)
			{
				EventsForMontage->RemoveAt(i);
				return true;
			}
		}
	}
	return false;
}

void UPS_Extended_AnimInstance::UnbindAllFromMontageBlendingOut()
{
	OnMontageBlendingOutMap.Empty();
}

bool UPS_Extended_AnimInstance::IsEventBoundedToOnMontageBlendingOut(UAnimMontage * MontageToCheck, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut)
{
	TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>> * EventsForMontage = OnMontageBlendingOutMap.Find(MontageToCheck);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageBlendingOut)
			{
				return true;
			}
		}
	}
	return false;
}

/* On Montage Ended Functions */

void UPS_Extended_AnimInstance::BindOnMontageEnded(UAnimMontage* MontageToBindTo, const FOnMontageEndedDynDelegate & InOnMontageEnded, bool UnbindOnMontageEnded)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>> * EventsForMontage = OnMontageEndedMap.Find(MontageToBindTo);
	if (!EventsForMontage)
	{
		TArray<FMontageEventData<FOnMontageEndedDynDelegate>> NewEventArray;
		NewEventArray.Add(FMontageEventData<FOnMontageEndedDynDelegate>(InOnMontageEnded, UnbindOnMontageEnded));
		OnMontageEndedMap.Add(MontageToBindTo, NewEventArray);
	}
	else
	{
		EventsForMontage->Add(FMontageEventData<FOnMontageEndedDynDelegate>(InOnMontageEnded, UnbindOnMontageEnded));
	}
}

bool UPS_Extended_AnimInstance::UnbindFromMontageEnded(UAnimMontage * MontageToUnbindFrom, const FOnMontageEndedDynDelegate & InOnMontageEnded)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>> * EventsForMontage = OnMontageEndedMap.Find(MontageToUnbindFrom);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageEnded)
			{
				EventsForMontage->RemoveAt(i);
				return true;
			}
		}
	}
	return false;
}

void UPS_Extended_AnimInstance::UnbindAllFromMontageEnded()
{
	OnMontageEndedMap.Empty();
}

bool UPS_Extended_AnimInstance::IsEventBoundedToOnMontageEnded(UAnimMontage* MontageToCheck, const FOnMontageEndedDynDelegate & InOnMontageEnded)
{
	TArray<FMontageEventData<FOnMontageEndedDynDelegate>>* EventsForMontage = OnMontageEndedMap.Find(MontageToCheck);
	if (EventsForMontage)
	{
		for (int i = 0; i < EventsForMontage->Num(); ++i)
		{
			if ((*EventsForMontage)[i].MontageDelegate == InOnMontageEnded)
			{
				return true;
			}
		}
	}
	return false;
}