#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PS_Extended_AnimInstance.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnMontageStartedDynDelegate, UAnimMontage*, Montage);
DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnMontageEndedDynDelegate, UAnimMontage*, Montage, bool, bInterrupted);
DECLARE_DYNAMIC_DELEGATE_TwoParams(FOnMontageBlendingOutDynDelegate, UAnimMontage*, Montage, bool, bInterrupted);

/*
* Extended Animation Instance C++ class definition.
*
* Provides an extended animation instance C++ class with a simplified API for calling and manipulating montages through C++ code.
* 
* The simplified API helps hiding the queries to animation classes and other components necessary to request information about montages.
* Furthermore, we also provide a simple method for binding for montage events that supports automatic unbinding once the montage event is called.
* 
* Extra functionality for other animation aspects could be added here if necessary.
* 
*/
UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Extended_AnimInstance : public UAnimInstance
{
	GENERATED_BODY()

protected:

	/* Holds a callback and information about deleting it once called or not.
	Used to implement the three montage events and allow binding to them easily. */
	template<typename T>
	struct FMontageEventData
	{
		T MontageDelegate;
		bool DeleteOnCall;

		FMontageEventData(T NewMontageDelegate, bool NewDeleteOnCall)
			: MontageDelegate(NewMontageDelegate), DeleteOnCall(NewDeleteOnCall){}
	};

	/* Variables */

	// Current montage we are playing right now.
	int32 CurrentMontageInstance;

	// Montage events. Each map controls a different montage event. Key is montage and array are all delegates binded to the montage.
	TMap<UAnimMontage*, TArray<FMontageEventData<FOnMontageStartedDynDelegate>>> OnMontageStartedMap;
	TMap<UAnimMontage*, TArray<FMontageEventData<FOnMontageBlendingOutDynDelegate>>> OnMontageBlendingOutMap;
	TMap<UAnimMontage*, TArray<FMontageEventData<FOnMontageEndedDynDelegate>>> OnMontageEndedMap;

	/* Protected functions. */

	// Called when the game starts
	virtual void NativeBeginPlay() override;

	// Called when montage event OnStarted is called.
	UFUNCTION()
	void OnMontageStartedDispatch(UAnimMontage* Montage);

	// Called when montage event OnBlendingOutOrInterrupted is called.
	UFUNCTION()
	void OnMontageBlendingOutDispatch(UAnimMontage* Montage, bool Interrupted);

	// Called when montage event OnEnded is called.
	UFUNCTION()
	void OnMontageEndedDispatch(UAnimMontage* Montage, bool Interrupted);

public:

	/* Montage functionality */
	
	// Plays an animation montage. If ForceMontage is true, montage will be played no matter if another montage is playing above. 
	UFUNCTION(BlueprintCallable)
	bool PlayAnimationMontage(UAnimMontage * NewMontageToPlay, float PlayRate = 1.0f, float StartingPosition = 0.0f, FName StartingSection = "", bool ForceMontage = false);

	// Resumes the montage if it was paused before.
	UFUNCTION(BlueprintCallable)
	void MontageResume(UAnimMontage* MontageToResume);

	// Pauses the montage if it is playing.
	UFUNCTION(BlueprintCallable)
	void MontagePause(UAnimMontage* MontageToPause);

	// Stops the montage if there are any playing.
	UFUNCTION(BlueprintCallable)
	void MontageStopPlaying(float BlendOutTime);

	// Stops the montage if it is played. Blend out time controls how long it will take until the montage blends out and gets removed.
	UFUNCTION(BlueprintCallable)
	void MontageStop(UAnimMontage* MontageToStop, float BlendOutTime);

	// Sets the play rate for the given montage.
	UFUNCTION(BlueprintCallable)
	void SetMontagePlayRate(UAnimMontage* MontagToModify, float NewPlayrate);

	// Returns the current montage being played.
	UFUNCTION(BlueprintCallable)
	UAnimMontage * GetCurrentMontagePlaying();

	// Returns the time the montage is playing.
	UFUNCTION(BlueprintCallable)
	float GetMontageCurrentTime(UAnimMontage * MontageToCheck);

	// Returns the montage playrate.
	UFUNCTION(BlueprintCallable)
	float GetMontagePlayrate(UAnimMontage* MontageToCheck);

	// Check if montage passed as parameter is playing.
	UFUNCTION(BlueprintCallable)
	bool IsPlayingMontage(UAnimMontage * MontageToCheck);

	// Check if any montage is being played.
	UFUNCTION(BlueprintCallable)
	bool IsPlayingAnyMontage();

	/* On Montage Started Functions */

	// Binds events to a montage when started. By default, bound events will get removed once they are called, so it's the coder responsibility to bind to the event
	// each time the montage is called. Otherwise, UnbindOnMontageEnded can be set to false. There is no prevention from binding an event multiple times. It's the
	// coder's responsibility to take care and prevent this if he has to.
	UFUNCTION(BlueprintCallable)
	void BindOnMontageStarted(UAnimMontage * MontageToBindTo, const FOnMontageStartedDynDelegate & InOnMontageStarted, bool UnbindOnMontageStarted = true);

	// Unbinds an event from a montage. Should be called by coders if they want to unbind an event before it gets called for any reason or, if they have set UnbindOnMontageStarted
	// to false when binding an event. In such case, it's the coder's responsibility to unbind the event when it has to.
	UFUNCTION(BlueprintCallable)
	bool UnbindFromMontageStarted(UAnimMontage * MontageToUnbindFrom, const FOnMontageStartedDynDelegate & InOnMontageStarted);

	// Unbinds all events from On Montage Started.
	UFUNCTION(BlueprintCallable)
	void UnbindAllFromMontageStarted();

	// Checks if the OnMontageStarted event for the given montage is bounded.
	UFUNCTION(BlueprintCallable)
	bool IsEventBoundedToOnMontageStarted(UAnimMontage* MontageToCheck, const FOnMontageStartedDynDelegate & InOnMontageStarted);

	/* On Montage Blending Out Functions */

	// Binds events to a montage when blending out. By default, bound events will get removed once they are called, so it's the coder responsibility to bind to the event
	// each time the montage is called. Otherwise, UnbindOnMontageEnded can be set to false. There is no prevention from binding an event multiple times. It's the
	// coder's responsibility to take care and prevent this if he has to.
	UFUNCTION(BlueprintCallable)
	void BindOnMontageBlendingOut(UAnimMontage * MontageToBindTo, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut, bool UnbindOnMontageBlendingOut = true);

	// Unbinds an event from a montage. Should be called by coders if they want to unbind an event before it gets called for any reason or, if they have set UnbindOnMontageBlendingOut
	// to false when binding an event. In such case, it's the coder's responsibility to unbind the event when it has to.
	UFUNCTION(BlueprintCallable)
	bool UnbindFromMontageBlendingOut(UAnimMontage * MontageToUnbindFrom, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut);

	// Unbinds all events from On Montage Blending Out.
	UFUNCTION(BlueprintCallable)
	void UnbindAllFromMontageBlendingOut();

	// Checks if the OnMontageBlendingOut event for the given montage is bounded.
	UFUNCTION(BlueprintCallable)
	bool IsEventBoundedToOnMontageBlendingOut(UAnimMontage* MontageToCheck, const FOnMontageBlendingOutDynDelegate & InOnMontageBlendingOut);

	/* On Montage Ended Functions */

	// Binds events to a montage when it ends. By default, bound events will get removed once they are called, so it's the coder responsibility to bind to the event
	// each time the montage is called. Otherwise, UnbindOnMontageEnded can be set to false. There is no prevention from binding an event multiple times. It's the
	// coder's responsibility to take care and prevent this if he has to.
	UFUNCTION(BlueprintCallable)
	void BindOnMontageEnded(UAnimMontage * MontageToBindTo, const FOnMontageEndedDynDelegate & InOnMontageEnded, bool UnbindOnMontageEnded = true);

	// Unbinds an event from a montage. Should be called by coders if they want to unbind an event before it gets called for any reason or, if they have set UnbindOnMontageEnded
	// to false when binding an event. In such case, it's the coder's responsibility to unbind the event when it has to.
	UFUNCTION(BlueprintCallable)
	bool UnbindFromMontageEnded(UAnimMontage * MontageToUnbindFrom, const FOnMontageEndedDynDelegate & InOnMontageEnded);

	// Unbinds all events from On Montage Ended.
	UFUNCTION(BlueprintCallable)
	void UnbindAllFromMontageEnded();

	// Checks if the OnMontageEnded event for the given montage is bounded.
	UFUNCTION(BlueprintCallable)
	bool IsEventBoundedToOnMontageEnded(UAnimMontage* MontageToCheck, const FOnMontageEndedDynDelegate & InOnMontageEnded);
};
