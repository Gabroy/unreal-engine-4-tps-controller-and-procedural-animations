#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Base/ActionFlag/PS_ActionFlags.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKStructs.h"
#include "Characters/Common/Components/Animations/PS_TurningControllerDefs.h"
#include "Components/Inventory/PS_Human_InventoryStructs.h"
#include "PS_Character.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UPS_Human_CameraController;
class UPS_Human_Input_GeneralController;
class UPS_Human_Input_CarryingController;
class USkeletalMeshComponent;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;
class UPS_TurningController;
class UPS_Human_InteractorComponent;
class UPS_Human_InteractorFinderComponent;
class UPS_Human_VaultingComponent;
class UPS_Human_ClimbingComponent;
class UPS_Human_AnimationInstance;
class UPS_Human_MontageController;

class UPS_LocState_Climb;

/*
* PS Character class. Represents a normal human character in the game.
* 
* This class has two different functions:
* 
* First of all, it acts as the main class representing a character in the game. This means one of its duties is
* to act as a nexus between components, calling their functions or throwing events when necessary so other controllers
* can listen and do as they need. Furthermore, it also provides an API with actions that can be called by other
* components whenever they need to perform an action. For example, the InputController or a BT may call the Jumping or
* crouching functions.
* 
* The second responsibility of the character controller is to implement the necessary code to be able to call
* actions for the character (mostly by calling other components) and to control when such actions can be executed.
* Such checks should only involve general conditions like whether the actions is enabled, if it can be executed when montages are being played,
* or if we have some other flags that might not allow us to execute such actions, (for example, we are carrying a weapon but this is not a different state
* in the input or bt).
* 
* To avoid too much complexity, we shouldn't query for states like (while grounded, sprinting, running, ...). And keep those checks
* directly in the respective input FSM or the AI BT inside the current state code.
* 
*/

UCLASS()
class TPS_API APS_Character : public ACharacter
{
	GENERATED_BODY()

protected:

	/* Components. */

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent * CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent * SpringArmComp;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_CameraController * CameraController;
	
	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_Input_GeneralController * GeneralInputController;
	
	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_Input_CarryingController * CarryingInputController;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent * SkeletalMeshComp;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_LocomotionController * LocomotionController;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_CarryingController * CarryingController;

	UPROPERTY(BlueprintReadOnly)
	UPS_TurningController * TurningController;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_InteractorComponent * InteractorComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_InteractorFinderComponent* InteractorFinderComp;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_VaultingComponent * VaultingComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_ClimbingComponent * ClimbingComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_AnimationInstance * AnimInstance;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_MontageController * MontageController;

	/* Input movement vars. */

	// Used by sprinting and walking modes.
	float SpeedModifier = 1.0f;

	/* Flags. */

	UPROPERTY(BlueprintReadOnly)
	bool IsMovementEnabled = true;

	UPROPERTY(BlueprintReadOnly)
	bool AreActionsEnabled = true;

	UPROPERTY(EditAnywhere, Category = "Actions")
	FAction_Flags CrouchCooldown;

	UPROPERTY(BlueprintReadOnly)
	bool CanToggleFeet = true;

	// Cooldown to allow hanging to edge again.
	UPROPERTY(EditAnywhere, Category = "Actions")
	FAction_Flags HangLedgeCooldown;

	bool WantsToJump = false;

public:

	APS_Character();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/* Actions. */

	// Actions that can be executed by the character. In general actions are implemented by three different
	// functions. A start function, that starts an action if it can be executed, an end action, that stops the
	// action, and, a CanDo Action that checks if a given action can be executed.
	// To keep conditions simple and, mostly separated, checks should only implement action specific queries
	// like checking if actions are enabled, montages are being played, or we have some other flags (weapons equipped)
	// that doesn't allow us to execute the action.
	// Querying for the current state we are at should be left for the FSMs or BTs from which actions are called to check when they can be called.

	// Move actions based on controller rotation or direction.
	UFUNCTION(BlueprintCallable)
	void MoveForward(float AxisValue);
	UFUNCTION(BlueprintCallable)
	void MoveSideways(float AxisValue);
	UFUNCTION(BlueprintCallable)
	void MoveInDir(float AxisValue, const FVector & MoveDir);

	// Walk action. If we are in a state that supports walking, this action will apply a walking effect.
	UFUNCTION(BlueprintCallable)
	void DoWalk();
	UFUNCTION(BlueprintCallable)
	void DoStopWalk();
	UFUNCTION(BlueprintCallable)
	bool CanDoWalk();

	// Sprint action.
	UFUNCTION(BlueprintCallable)
	void DoSprint();
	UFUNCTION(BlueprintCallable)
	void DoStopSprint();
	UFUNCTION(BlueprintCallable)
	bool CanDoSprint();

	// Crouch actions.
	UFUNCTION(BlueprintCallable)
	void DoCrouch();
	UFUNCTION(BlueprintCallable)
	void DoUncrouch();
	UFUNCTION(BlueprintCallable)
	bool CanDoCrouch();
	UFUNCTION(BlueprintCallable)
	bool CanDoUncrouch();

	// Jump actions.
	UFUNCTION(BlueprintCallable)
	void DoIntendJump();
	UFUNCTION(BlueprintCallable)
	bool CanDoJump();
	// Performs the actual jump. Called by the anim notify of the jump animation
	// once we deem estimate to actually jump.
	UFUNCTION(BlueprintCallable)
	void PerformJump();

	// Change movement facing mode.
	UFUNCTION(BlueprintCallable)
	void ChangeToVelocityFacingMode();
	UFUNCTION(BlueprintCallable)
	void ChangeToCameraFacingMode();

	// Aim action.
	UFUNCTION(BlueprintCallable)
	void DoAim();
	UFUNCTION(BlueprintCallable)
	void DoUnAim();
	UFUNCTION(BlueprintCallable)
	bool CanDoAim();

	// Interact action.
	UFUNCTION(BlueprintCallable)
	void DoInteract();
	UFUNCTION(BlueprintCallable)
	void DoStopInteract();
	UFUNCTION(BlueprintCallable)
	bool CanInteract();

	// Drop object.
	UFUNCTION(BlueprintCallable)
	void DropCarryingItem();
	UFUNCTION(BlueprintCallable)
	bool CanDropCarryingItem();

	/* Change Item functions */

	UFUNCTION(BlueprintCallable)
	void ChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave);
	UFUNCTION(BlueprintCallable)
	bool CanChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave);
	UFUNCTION(BlueprintCallable)
	void CancelChangeOrSaveItem();
	
	// Rolling.
	UFUNCTION(BlueprintCallable)
	void DoRolling();
	UFUNCTION(BlueprintCallable)
	bool CanDoRolling();

	// Slide.
	UFUNCTION(BlueprintCallable)
	void DoSlide();
	UFUNCTION(BlueprintCallable)
	bool CanDoSlide();

	// Turning.
	UFUNCTION(BlueprintCallable)
	void StartTurning(float AngleToTurnTo, float TurningVelocity = -1.0f);
	void BindToTurningFinishedEvent(UObject * ObjectBindToTurning, TurningFinishedCallback FuncPtr, bool RemoveBindingOnCallbackCalled = true);
	void UnbindFromTurningFinishedEvent(UObject * ObjectBindToTurning);
	UFUNCTION(BlueprintCallable)
	void StopTurning();
	UFUNCTION(BlueprintCallable)
	bool CanTurn() const;

	// Vaulting.
	UFUNCTION(BlueprintCallable)
	bool TryVaulting();

	// Climbing.
	UFUNCTION(BlueprintCallable)
	bool TryClimbing();

	/* Weapon actions */

	UFUNCTION(BlueprintCallable)
	void SetHolsterWeapon(bool HolsterWeapon);
	UFUNCTION(BlueprintCallable)
	bool IsDoingHolster();

	/* Hanging actions  */

	UFUNCTION(BlueprintCallable)
	bool TryLedgeGrab();
	UFUNCTION(BlueprintCallable)
	bool CanLedgeGrab();
	UFUNCTION(BlueprintCallable)
	void Hanging_TryMove(float Axis);
	UFUNCTION(BlueprintCallable)
	void Hanging_TryClimb(float Axis);
	UFUNCTION(BlueprintCallable)
	void Hanging_PerformJumpingFromHanged();
	UFUNCTION(BlueprintCallable)
	void Hanging_PerformVerticalJump();
	UFUNCTION(BlueprintCallable)
	void Hanging_PerformHorizontalJump(bool Right);
	UFUNCTION(BlueprintCallable)
	void Hanging_ReleaseHang();
	UFUNCTION(BlueprintCallable)
	void Hanging_Crouch();
	UFUNCTION(BlueprintCallable)
	void Hanging_Stand();
	UFUNCTION(BlueprintCallable)
	void Hanging_Walk();
	UFUNCTION(BlueprintCallable)
	void Hanging_StopWalk();

	/* Other actions */

	// Feet toogle.
	UFUNCTION(BlueprintCallable)
	void DoToggleStandingFoot();
	UFUNCTION(BlueprintCallable)
	void SetCanToogleFeet(bool Enabled);

	// Set speed modifier.
	UFUNCTION(BlueprintCallable)
	void SetSpeedModifier(float NewSpeedModifier);

	// Enable or disable ragdolls.
	UFUNCTION(BlueprintCallable)
	void SetRagdollStatus(bool EnableRagdolls);

	// Blocks character movement and actions if true.
	// Normally used by animations that don't allow actions nor movement
	// while they are happening.
	UFUNCTION(BlueprintCallable)
	void SetEnableCharacterMovementAndActions(bool EnableCharacter);

	// Enables or disables character movement.
	UFUNCTION(BlueprintCallable)
	void SetEnableMovement(bool EnableMovement);

	// Enables or disables character actions.
	UFUNCTION(BlueprintCallable)
	void SetEnableActions(bool EnableActions);

	UFUNCTION(BlueprintCallable)
	void SetWantsToJump(bool NewWantsToJump) {	WantsToJump = NewWantsToJump; }

	/* Getters */

	// Returns the speed of the character according to
	// the locomotion controller.
	UFUNCTION(BlueprintCallable)
	float GetSpeed() const;
	// Returns true if movement is enabled (we can move).
	UFUNCTION(BlueprintCallable)
	bool GetIsMovementEnabled() const { return IsMovementEnabled; }
	// Returns true if actions are enabled.
	UFUNCTION(BlueprintCallable)
	bool GetAreActionsEnabled() const { return AreActionsEnabled; }
	// Returns the location of the actor from the base of the feet and not the middle of the actor.
	UFUNCTION(BlueprintCallable)
	FVector GetActorLocationFromFeet() const;
	// Returns true if the direction passed as parameter is pointing in same dir as character.
	UFUNCTION(BlueprintCallable)
	bool IsDirFacingCharacterForward(const FVector & Direction) const;

protected:

	virtual void BeginPlay() override;

	void CreateComponents();

	void InitParameters();

	// Event received when the movement mode of the character movement component changes.
	// I use a callback instead of overriding the virtual. There seems to be a crash if we override it and I don't know exactly why.
	// Used to handle push states like falling, flying or swimming that can affect the player at any given moment.
	// It's the components duty to decide which state to end up changing to.
	UFUNCTION()
	void OnMovementModeChangedCallback(ACharacter * Character, EMovementMode PrevMovementMode, uint8 PreviousCustomMode);
};
