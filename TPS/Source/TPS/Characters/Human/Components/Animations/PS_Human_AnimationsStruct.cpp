#include "PS_Human_AnimationsStruct.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationInstance.h"

/* ------------------------ Idle Animations ------------------------ */

FIdleRandomAnimation FIdleAnimationsContainer::GetRandomIdleAnimation(UPS_Human_LocomotionController * LocomotionController, UPS_Human_CarryingController * CarryingController)
{
	if (!LocomotionController || !CarryingController)
		return FIdleRandomAnimation();

	FIdleRandomAnimation RandomIdleAnimation;
	ELocomotionStance CurrentLocomotionStance = LocomotionController->GetLocomotionStance();
	ECarryingStance CurrentCarryingStance = CarryingController->GetCurrentStance();

	switch (CurrentLocomotionStance)
	{
	case ELocomotionStance::Normal:
		switch (CurrentCarryingStance)
		{
		case ECarryingStance::Unarmed:
			RandomIdleAnimation = PickRandomIdleAnimation(StandingUnarmedIdleAnimations);
			break;
		case ECarryingStance::TwoHandedGun:
			RandomIdleAnimation = PickRandomIdleAnimation(StandingTwoHandedGunIdleAnimations);
			break;
		case ECarryingStance::OneHandedGun:
			RandomIdleAnimation = PickRandomIdleAnimation(StandingTwoHandedGunIdleAnimations);
			break;
		default:
			RandomIdleAnimation = PickRandomIdleAnimation(StandingUnarmedIdleAnimations);
			break;
		}
		break;

	case ELocomotionStance::Crouch:
		break;

	default:
		break;
	}

	return RandomIdleAnimation;
}

void FIdleAnimationsContainer::ComputeTotalWeightsForAnims()
{
	ComputeTotalWeightsForArray(StandingUnarmedIdleAnimations);
	ComputeTotalWeightsForArray(StandingTwoHandedGunIdleAnimations);
}

void FIdleAnimationsContainer::ComputeTotalWeightsForArray(TArray<FIdleRandomAnimation> & AnimsToCompute)
{
	// Compute total weight.
	float TotalWeight = 0.0f;
	for (int i = 0; i < AnimsToCompute.Num(); ++i)
	{
		FIdleRandomAnimation & IdleAnim = AnimsToCompute[i];
		TotalWeight += IdleAnim.WeightAnimation;
	}

	// Use it to compute final weight.
	float AccumulatedValue = 0.0f;
	for (int i = 0; i < AnimsToCompute.Num(); ++i)
	{
		FIdleRandomAnimation& IdleAnim = AnimsToCompute[i];
		IdleAnim.FinalWeight = AccumulatedValue + IdleAnim.WeightAnimation / TotalWeight;
		AccumulatedValue += IdleAnim.WeightAnimation / TotalWeight;
	}
}

FIdleRandomAnimation FIdleAnimationsContainer::PickRandomIdleAnimation(const TArray<FIdleRandomAnimation> & IdleAnimations)
{
	if (IdleAnimations.Num() < 1)
	{
		return FIdleRandomAnimation();
	}

	// Pick Random Animation.
	float RandomValue = FMath::RandRange(0.0f, 1.0f);
	for (int i = 0; i < IdleAnimations.Num(); ++i)
	{
		const FIdleRandomAnimation & IdleAnim = IdleAnimations[i];
		if (IdleAnim.FinalWeight >= RandomValue)
		{
			return IdleAnim;
		}
	}

	return IdleAnimations.Last();
}

/* ------------------------ Jumping Animations ------------------------ */

FJumpingMontage FJumpingAnimationsContainer::GetRandomJumpAnimation(UPS_Human_LocomotionController * LocomotionController, UPS_Human_CarryingController * CarryingController, EAnimationFeetPosition CurrentFeetPosition)
{
	if (!LocomotionController || !CarryingController)
		return FJumpingMontage();

	FJumpingMontage RandomJumpAnimation;
	ELocomotionStance CurrentLocomotionStance = LocomotionController->GetLocomotionStance();
	ECarryingStance CurrentCarryingStance = CarryingController->GetCurrentStance();

	switch (CurrentLocomotionStance)
	{
	case ELocomotionStance::Normal:
		switch (CurrentCarryingStance)
		{
		case ECarryingStance::Unarmed:
			RandomJumpAnimation = PickRandomJumpAnimation(LocomotionController, CurrentFeetPosition, StandingUnarmedJumpingAnimations);
			break;
		case ECarryingStance::TwoHandedGun:
			RandomJumpAnimation = PickRandomJumpAnimation(LocomotionController, CurrentFeetPosition, StandingTwoHandedGunJumpingAnimations);
			break;
		case ECarryingStance::OneHandedGun:
			RandomJumpAnimation = PickRandomJumpAnimation(LocomotionController, CurrentFeetPosition, StandingUnarmedJumpingAnimations);
			break;
		default:
			RandomJumpAnimation = PickRandomJumpAnimation(LocomotionController, CurrentFeetPosition, StandingUnarmedJumpingAnimations);
			break;
		}
		break;

	case ELocomotionStance::Crouch:
		RandomJumpAnimation = PickRandomJumpAnimation(LocomotionController, CurrentFeetPosition, StandingUnarmedJumpingAnimations);
		break;

	default:
		RandomJumpAnimation = PickRandomJumpAnimation(LocomotionController, CurrentFeetPosition, StandingUnarmedJumpingAnimations);
		break;
	}

	return RandomJumpAnimation;
}

FJumpingMontage FJumpingAnimationsContainer::PickRandomJumpAnimation(UPS_Human_LocomotionController * LocomotionController, EAnimationFeetPosition CurrentFeetPosition, const TArray<FJumpingMontage> & JumpAnimations)
{
	float CharacterSpeed = LocomotionController->GetSpeed();

	// Pick candidate functions.
	TArray<int> PossibleCandidates;
	for (int i = 0; i < JumpAnimations.Num(); ++i)
	{
		const FJumpingMontage & MontageData = JumpAnimations[i];

		// Ignore montages that have different starting feet position.
		if (MontageData.ShouldUseIntendedFeetToPlay && (MontageData.IntendedFeetPosition != CurrentFeetPosition))
			continue;

		// Add montage data based on player speed.
		if (CharacterSpeed >= MontageData.MinSpeedForPlaying && CharacterSpeed <= MontageData.MaxSpeedForPlaying)
		{
			PossibleCandidates.Add(i);
			continue;
		}
	}

	// Can't jump.
	if (PossibleCandidates.Num() < 1)
	{
		return FJumpingMontage();
	}

	int CandidateIndex = FMath::RandRange(0, PossibleCandidates.Num() - 1);
	return JumpAnimations[PossibleCandidates[CandidateIndex]];
}

/* ------------------------ Blend Spaces Variations Animations ------------------------ */

void FBlendSpaceVariationsPool::ChangeActiveVariation(FBlendSpaceVariationPair & BSVariationToUpdate, bool UpdateToBaseVariation)
{
	if (UpdateToBaseVariation)
		ChangeToBaseBSVariation(BSVariationToUpdate);
	else
		ChangeToRandomBSVariation(BSVariationToUpdate);
}

void FBlendSpaceVariationsPool::ChangeToBaseBSVariation(FBlendSpaceVariationPair & BSVariationToUpdate)
{
	if (BlendSpacesPool.Num() == 0)
	{
		return;
	}

	FBlendSpaceVariation & BaseVariation = BlendSpacesPool[0];
	UBlendSpaceBase * UsingVariation = BSVariationToUpdate.UsingVariation1 ? BSVariationToUpdate.BSVariation1 : BSVariationToUpdate.BSVariation2;

	if (UsingVariation != BaseVariation.BlendSpaceVariation)
	{
		BSVariationToUpdate.UsingVariation1 = !BSVariationToUpdate.UsingVariation1;
		if (BSVariationToUpdate.UsingVariation1)
			BSVariationToUpdate.BSVariation1 = BaseVariation.BlendSpaceVariation;
		else
			BSVariationToUpdate.BSVariation2 = BaseVariation.BlendSpaceVariation;

		BSVariationToUpdate.TimeToBlendToBS = BaseVariation.BlendTime;
	}
}

void FBlendSpaceVariationsPool::ChangeToRandomBSVariation(FBlendSpaceVariationPair & BSVariationToUpdate)
{
	if (BlendSpacesPool.Num() == 0)
	{
		return;
	}

	FBlendSpaceVariation & NewRandomBS = GetRandomBS(BSVariationToUpdate.UsingVariation1 ? BSVariationToUpdate.BSVariation1 : BSVariationToUpdate.BSVariation2);

	BSVariationToUpdate.UsingVariation1 = !BSVariationToUpdate.UsingVariation1;
	if (BSVariationToUpdate.UsingVariation1)
		BSVariationToUpdate.BSVariation1 = NewRandomBS.BlendSpaceVariation;
	else
		BSVariationToUpdate.BSVariation2 = NewRandomBS.BlendSpaceVariation;

	BSVariationToUpdate.TimeToBlendToBS = NewRandomBS.BlendTime;
}

FBlendSpaceVariation & FBlendSpaceVariationsPool::GetRandomBS(UBlendSpaceBase * BlendSpaceToExclude)
{
	int AnimIndex = FMath::RandRange(0, BlendSpacesPool.Num() - 1);
	
	// If already used pick next one.
	if (BlendSpaceToExclude && BlendSpacesPool[AnimIndex].BlendSpaceVariation == BlendSpaceToExclude)
		AnimIndex = (AnimIndex + 1) % BlendSpacesPool.Num();

	return BlendSpacesPool[AnimIndex];
}

void FBlendSpacesForStancesContainer::Init(UPS_Human_AnimationInstance * NewAnimationInstance)
{
	AnimationInstance = NewAnimationInstance;
}

void FBlendSpacesForStancesContainer::SetupBaseBSValues(UPS_Human_AnimationInstance * NewAnimationInstance)
{
	AnimationInstance = NewAnimationInstance;
	if (AnimationInstance)
	{
		StandingUnarmedPool.ChangeActiveVariation(AnimationInstance->StandingUnarmedPair, true);
		StandingTwoHandedPool.ChangeActiveVariation(AnimationInstance->StandingTwoHandedGunPair, true);
	}
}

void FBlendSpacesForStancesContainer::UpdateCurrentActiveVariation(bool UpdateToBaseVariation)
{
	if (!AnimationInstance->LocomotionController || !AnimationInstance->CarryingController)
		return;

	ELocomotionStance CurrentLocomotionStance = AnimationInstance->LocomotionController->GetLocomotionStance();

	switch (CurrentLocomotionStance)
	{
	case ELocomotionStance::Normal:
		UpdateStandingBS(UpdateToBaseVariation);
		break;

	case ELocomotionStance::Crouch:
		// Todo in the future maybe if we have anims.
		break;
	default:
		break;
	}
}

void FBlendSpacesForStancesContainer::UpdateStandingBS(bool UpdateToBaseVariation)
{
	ECarryingStance CurrentCarryingStance = AnimationInstance->CarryingController->GetCurrentStance();
	switch (CurrentCarryingStance)
	{
	case ECarryingStance::Unarmed:
		StandingUnarmedPool.ChangeActiveVariation(AnimationInstance->StandingUnarmedPair, UpdateToBaseVariation);
		break;
	case ECarryingStance::OneHandedGun:
		break;
	case ECarryingStance::TwoHandedGun:
		StandingTwoHandedPool.ChangeActiveVariation(AnimationInstance->StandingTwoHandedGunPair, UpdateToBaseVariation);
		break;
	default:
		break;
	}
}

void FBlendSpacesForStancesContainer::UpdateCrouchedBS(bool UpdateToBaseVariation)
{
	// Todo in the future maybe if we have anims.
}