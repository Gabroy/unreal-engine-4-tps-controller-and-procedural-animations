#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Characters/Human/Components/Inventory/PS_Human_InventoryStructs.h"
#include "PS_Human_MontageController.generated.h"

class APS_Character;
class UPS_Human_AnimationInstance;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;
class UPS_Human_InventoryComponent;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_MontageController : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Components */
	
	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UPS_Human_CarryingController * CarryingController = nullptr;

	UPROPERTY()
	UPS_Human_InventoryComponent * InventoryComponent = nullptr;

	/* Ptr to function that can be used by montages to execute */
	typedef void (UPS_Human_MontageController::* MontageTickFunctionPtr)(float);
	MontageTickFunctionPtr MontageTickFunction = nullptr;

	/* Full Body Locomotion Montages */
	
	/* Rolling montage */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rolling Montages")
	UAnimMontage * RollingForwardMontage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rolling Montages")
	float RollingAnimSpeed = 1.35f;

	/* Slide montage. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sliding Montages")
	UAnimMontage * SlidingMontage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sliding Montages")
	float MinSlideSpeedNecessaryForSliding = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sliding Montages")
	float SlideAnimSpeed = 1.0f;

	/* Pickup montages.
	Two versions, one for objects above waist and another for those below waist of player. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Montages")
	UAnimMontage * AboveWaistPickupMontage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Montages")
	UAnimMontage * BelowWaistPickupMontage;
	UPROPERTY(BlueprintReadOnly)
	UAnimMontage * LastPlayedPickupMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Montages")
	float MaxHeightToPlayBelowPickupMontage = 120.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Montages")
	float PickupAnimSpeed = 1.0f;

	/* Change weapons montages. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Change Weapon Montages")
	TMap<TEnumAsByte<EInventoryItemSlot>, UAnimMontage *> SaveItemAnims;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Change Weapon Montages")
	TMap<TEnumAsByte<EInventoryItemSlot>, UAnimMontage *> TakeItemAnims;
	UPROPERTY(BlueprintReadOnly)
	UAnimMontage * LastWeaponChangeMontage = nullptr;
	
public:

	UPS_Human_MontageController();

	virtual void BeginPlay();

	/* Rolling. */
	void PlayRollingMontage(bool ForceMontage);
	bool CanPlayRollingMontage(bool ForceMontage);

	/* Slide. */
	void PlaySlideMontage(bool ForceMontage);
	bool CanPlaySlideMontage(bool ForceMontage);

	/* Pickup. */
	void PlayPickupMontage(bool ForceMontage, const FVector & PickupLocation);
	bool IsPickupMontagePlaying() const;
	void CancelPickupMontage();

	/* Save and Take Items. */
	void PlaySaveCarryingItemMontage(bool ForceMontage, EInventoryItemSlot SocketMarkup);
	void PlayTakeItemItemMontage(bool ForceMontage, EInventoryItemSlot SocketMarkup);
	void CancelChangeItemMontage();
	bool IsWeaponChangeMontagePlaying() const;

private:

	UFUNCTION()
	void OnRollingMontageStarted();
	UFUNCTION()
	void OnRollingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	UFUNCTION()
	void OnSlideMontageStarted();
	UFUNCTION()
	void OnSlideMontageEndedOrBlendingOut(UAnimMontage * Montage, bool Interrupted);

	UFUNCTION()
	void OnPickupMontageStarted();
	UFUNCTION()
	void OnPickupMontageEndedOrBlendingOut(UAnimMontage * Montage, bool Interrupted);

	UFUNCTION()
	void OnSaveCarryingItemMontageStarted();
	UFUNCTION()
	void OnSaveCarryingItemMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	UFUNCTION()
	void OnTakeItemMontageStarted();
	UFUNCTION()
	void OnTakeItemMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);
};
