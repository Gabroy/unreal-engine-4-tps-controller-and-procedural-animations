#include "PS_Human_MontageController.h"
#include "Characters/Human/PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Characters/Human/Components/Inventory/PS_Human_InventoryComponent.h"

UPS_Human_MontageController::UPS_Human_MontageController()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_Human_MontageController::BeginPlay()
{
	Super::BeginPlay();

	AActor * Actor = GetOwner();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageController::BeginPlay CharacterOwner Returned Null"));
		return;
	}

	LocController = Actor->FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageController::BeginPlay LocController Returned Null"));
		return;
	}

	CarryingController = Actor->FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageController::BeginPlay CarryingController Returned Null"));
		return;
	}

	InventoryComponent = Actor->FindComponentByClass<UPS_Human_InventoryComponent>();
	if (!InventoryComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageController::BeginPlay InventoryComponent Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Actor->FindComponentByClass<USkeletalMeshComponent>();
	if (SKMesh)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageController::BeginPlay AnimInstance Returned Null"));
			return;
		}
	}
}

/* Rolling Montage. */

void UPS_Human_MontageController::PlayRollingMontage(bool ForceMontage)
{
	if (!AnimInstance)
		return;

	bool Success = AnimInstance->PlayAnimationMontage(RollingForwardMontage, RollingAnimSpeed, 0.0, "None", ForceMontage);
	if (!Success)
		return;

	OnRollingMontageStarted();

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageController::OnRollingMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(RollingForwardMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageController::OnRollingMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageEnded(RollingForwardMontage, EndedMontage);
}

bool UPS_Human_MontageController::CanPlayRollingMontage(bool ForceMontage)
{
	ELocomotionMode StateLocomotion = (ELocomotionMode)LocController->GetCurrentStateID();
	bool LocomotionStateAllowsRolling = StateLocomotion == ELocomotionMode::Grounded;

	bool IsMontageForcedOrNoMontagePlaying = ForceMontage || (!AnimInstance->IsAnyMontagePlaying() && !AnimInstance->IsPlayingMontage(RollingForwardMontage));

	return LocomotionStateAllowsRolling && IsMontageForcedOrNoMontagePlaying;
}

void UPS_Human_MontageController::OnRollingMontageStarted()
{
	if(CharacterOwner)
		CharacterOwner->SetEnableCharacterMovementAndActions(false);

	if(LocController)
		LocController->SetLegIKEnabledStatus(false);
}

void UPS_Human_MontageController::OnRollingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	if (CharacterOwner)
		CharacterOwner->SetEnableCharacterMovementAndActions(true);

	if (LocController)
		LocController->SetLegIKEnabledStatus(true);
}

/* Slide. */

void UPS_Human_MontageController::PlaySlideMontage(bool ForceMontage)
{
	if (!AnimInstance)
		return;

	bool Success = AnimInstance->PlayAnimationMontage(SlidingMontage, SlideAnimSpeed, 0.0, "None", ForceMontage);
	if (!Success)
		return;

	OnSlideMontageStarted();

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageController::OnSlideMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(SlidingMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageController::OnSlideMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageEnded(SlidingMontage, EndedMontage);
}

bool UPS_Human_MontageController::CanPlaySlideMontage(bool ForceMontage)
{
	ELocomotionMode StateLocomotion = (ELocomotionMode)LocController->GetCurrentStateID();
	bool LocomotionStateAllowsRolling = StateLocomotion == ELocomotionMode::Grounded;

	bool HasEnoughSpeedForSliding = (LocController->GetSpeed() >= MinSlideSpeedNecessaryForSliding);
	bool IsMontageForcedOrNoMontagePlaying = ForceMontage || (!AnimInstance->IsAnyMontagePlaying() && !AnimInstance->IsPlayingMontage(RollingForwardMontage));

	return LocomotionStateAllowsRolling && HasEnoughSpeedForSliding && IsMontageForcedOrNoMontagePlaying;
}

void UPS_Human_MontageController::OnSlideMontageStarted()
{
	if (CharacterOwner)
	{
		CharacterOwner->DoCrouch();
		CharacterOwner->SetEnableCharacterMovementAndActions(false);
	}

	if (LocController)
	{
		LocController->SetLegIKEnabledStatus(false);
	}
}

void UPS_Human_MontageController::OnSlideMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	if (CharacterOwner)
	{
		CharacterOwner->SetEnableCharacterMovementAndActions(true);
		CharacterOwner->DoUncrouch();
	}

	if (LocController)
	{
		LocController->SetLegIKEnabledStatus(true);
	}
}

/* Pickup. */

void UPS_Human_MontageController::PlayPickupMontage(bool ForceMontage, const FVector & PickupLocation)
{
	if (!AnimInstance || !CharacterOwner)
		return;

	// Compute height relative to player, if value is positive, it's above the player MaxHeightToPlayBelowPickupMontage, otherwise, below.
	float ObjectHeightRelativeToPlayer = PickupLocation.Z - (CharacterOwner->GetActorLocationFromFeet().Z + MaxHeightToPlayBelowPickupMontage);
	bool IsAbovePlayer = (ObjectHeightRelativeToPlayer > 0.0f);

	LastPlayedPickupMontage = IsAbovePlayer ? AboveWaistPickupMontage : BelowWaistPickupMontage;
	bool Success = AnimInstance->PlayAnimationMontage(LastPlayedPickupMontage, PickupAnimSpeed, 0.0, "None", ForceMontage);
	if (!Success)
		return;

	OnPickupMontageStarted();

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageController::OnPickupMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(LastPlayedPickupMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageController::OnPickupMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageEnded(LastPlayedPickupMontage, EndedMontage);
}

bool UPS_Human_MontageController::IsPickupMontagePlaying() const
{
	if (!AnimInstance)
		return false;

	return AnimInstance->IsPlayingMontage(LastPlayedPickupMontage);
}

void UPS_Human_MontageController::CancelPickupMontage()
{
	if (AnimInstance)
	{
		AnimInstance->MontageStop(LastPlayedPickupMontage, 0.25f);
	}
}

void UPS_Human_MontageController::OnPickupMontageStarted()
{
	if (CharacterOwner)
	{
		CharacterOwner->SetEnableCharacterMovementAndActions(false);
	}

	if (LocController)
	{
		LocController->SetUpperBodyIKDisabled();
	}
}

void UPS_Human_MontageController::OnPickupMontageEndedOrBlendingOut(UAnimMontage * Montage, bool Interrupted)
{
	LastPlayedPickupMontage = nullptr;

	if (CharacterOwner)
	{
		CharacterOwner->SetEnableCharacterMovementAndActions(true);
	}

	if (CarryingController)
	{
		CarryingController->SetUpperBodyIKEnabledBasedOnCarryingStance();
	}

	if (InventoryComponent)
	{
		InventoryComponent->FinishGrabItem(Interrupted);
	}
}

/* Save and Take Items. */
void UPS_Human_MontageController::PlaySaveCarryingItemMontage(bool ForceMontage, EInventoryItemSlot SocketMarkup)
{
	if (!AnimInstance || !CharacterOwner)
		return;

	if (!SaveItemAnims.Contains(SocketMarkup))
		return;

	LastWeaponChangeMontage = SaveItemAnims[SocketMarkup];
	if (!LastWeaponChangeMontage)
		return;

	bool Success = AnimInstance->PlayAnimationMontage(LastWeaponChangeMontage, 1.0f, 0.0, "None", ForceMontage);
	if (!Success)
		return;

	OnSaveCarryingItemMontageStarted();

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageController::OnSaveCarryingItemMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(LastWeaponChangeMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageController::OnSaveCarryingItemMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageEnded(LastWeaponChangeMontage, EndedMontage);
}

void UPS_Human_MontageController::CancelChangeItemMontage()
{
	if (AnimInstance && LastWeaponChangeMontage)
	{
		AnimInstance->MontageStop(LastWeaponChangeMontage, 0.25f);
	}
}

bool UPS_Human_MontageController::IsWeaponChangeMontagePlaying() const
{
	if (!AnimInstance)
		return false;

	return AnimInstance->IsPlayingMontage(LastWeaponChangeMontage);
}

void UPS_Human_MontageController::OnSaveCarryingItemMontageStarted()
{
	if (CharacterOwner)
	{
		CharacterOwner->SetEnableActions(false);
	}

	if (LocController)
	{
		LocController->SetUpperBodyIKDisabled();
	}

	if (AnimInstance)
	{
		AnimInstance->SetUpperBodyBlendingStatus(true);
	}
}

void UPS_Human_MontageController::OnSaveCarryingItemMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	LastWeaponChangeMontage = nullptr;

	if (CharacterOwner)
	{
		CharacterOwner->SetEnableActions(true);
	}

	if (LocController)
	{
		LocController->SetUpperBodyIKEnabled(EUpperTorsoIKModes::UB_IK_GROUNDED_UNARMED);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSaveCarryingItemMontageEnded(Interrupted);
	}

	if (AnimInstance)
	{
		AnimInstance->SetUpperBodyBlendingStatus(false);
	}
}

void UPS_Human_MontageController::PlayTakeItemItemMontage(bool ForceMontage, EInventoryItemSlot SocketMarkup)
{
	if (!AnimInstance || !CharacterOwner)
		return;

	if (!TakeItemAnims.Contains(SocketMarkup))
		return;

	LastWeaponChangeMontage = TakeItemAnims[SocketMarkup];
	if (!LastWeaponChangeMontage)
		return;

	bool Success = AnimInstance->PlayAnimationMontage(LastWeaponChangeMontage, 1.0f, 0.0, "None", ForceMontage);
	if (!Success)
		return;

	OnTakeItemMontageStarted();

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageController::OnTakeItemMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(LastWeaponChangeMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageController::OnTakeItemMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageEnded(LastWeaponChangeMontage, EndedMontage);
}

void UPS_Human_MontageController::OnTakeItemMontageStarted()
{
	if (CharacterOwner)
	{
		CharacterOwner->SetEnableActions(false);
	}

	if (LocController)
	{
		LocController->SetUpperBodyIKDisabled();
	}

	if (AnimInstance)
	{
		AnimInstance->SetUpperBodyBlendingStatus(true);
	}
}

void UPS_Human_MontageController::OnTakeItemMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	LastWeaponChangeMontage = nullptr;

	if (CharacterOwner)
	{
		CharacterOwner->SetEnableActions(true);
	}

	if (CarryingController)
	{
		CarryingController->SetUpperBodyIKEnabledBasedOnCarryingStance();
	}

	if (AnimInstance)
	{
		AnimInstance->SetUpperBodyBlendingStatus(false);
	}
}