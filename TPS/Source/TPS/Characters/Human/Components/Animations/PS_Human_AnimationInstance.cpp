#include "PS_Human_AnimationInstance.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Kismet/KismetMathLibrary.h"

void UPS_Human_AnimationInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

	AActor * Owner = (AActor*)TryGetPawnOwner();
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_AnimationInstance::NativeBeginPlay Owner Returned Null"));
		return;
	}

	Character = Cast<APS_Character>(Owner);
	if (!Character)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_AnimationInstance::NativeBeginPlay APS_Character Returned Null"));
		return;
	}

	LocomotionController = Owner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_AnimationInstance::Init LocomotionController Returned Null"));
		return;
	}
	LocomotionController->OnChangedStanceMulticast.AddUObject(this, &UPS_Human_AnimationInstance::OnLocomotionStanceChanged);

	CarryingController = Owner->FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_AnimationInstance::Init CarryingController Returned Null"));
		return;
	}
	CarryingController->OnCarryingStanceChanged.AddUObject(this, &UPS_Human_AnimationInstance::OnCarryingStanceChanged);

	InventoryComponent = Owner->FindComponentByClass<UPS_Human_InventoryComponent>();
	if (!InventoryComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_AnimationInstance::Init InventoryComponent Returned Null"));
		return;
	}

	BlendSpacesPerStancesContainer.Init(this);
}

void UPS_Human_AnimationInstance::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);
	
	IdleAnimationsContainer.ComputeTotalWeightsForAnims();
	BlendSpacesPerStancesContainer.SetupBaseBSValues(this);
}

/* Idle */

void UPS_Human_AnimationInstance::OnIdleStateEntered()
{
	OnGroundedIdleEntered.ExecuteIfBound();
}

void UPS_Human_AnimationInstance::OnIdleStateExit()
{
	OnGroundedIdleExit.ExecuteIfBound();
}

void UPS_Human_AnimationInstance::PlayRandomIdleAnimation()
{
	IdleAnimationToPlay = IdleAnimationsContainer.GetRandomIdleAnimation(LocomotionController, CarryingController);
	IntendedFeetPosition = IdleAnimationToPlay.FeetPosition;
}

// Allows setting the intended feet position from code if we want.
void UPS_Human_AnimationInstance::SetIntendedFootPosition(TEnumAsByte<EAnimationFeetPosition> NewFeetPosition)
{
	IntendedFeetPosition = NewFeetPosition;
}

/* Grounded */

void UPS_Human_AnimationInstance::UpdateCurrentActiveBSVariation()
{
	BlendSpacesPerStancesContainer.UpdateCurrentActiveVariation();
}

void UPS_Human_AnimationInstance::StartTimerToUpdateCurrentActiveBSVariation(float MinTimeBetweenVariations, float MaxTimeBetweenVariations)
{
	CurrentMinTimeBetweenVariationsChanges = MinTimeBetweenVariations;
	CurrentMaxTimeBetweenVariationsChanges = MaxTimeBetweenVariations;
	float TimeToCall = UKismetMathLibrary::RandomFloatInRange(CurrentMinTimeBetweenVariationsChanges, CurrentMaxTimeBetweenVariationsChanges);
	UWorld * World = GetWorld();
	if (World)
		World->GetTimerManager().SetTimer(UpdateVariationsTimerHandle, this, &UPS_Human_AnimationInstance::UpdateCurrentActiveBSVariation, TimeToCall, false);
}

void UPS_Human_AnimationInstance::StopTimerToUpdateCurrentActiveBSVariation()
{
	UWorld* World = GetWorld();
	if (World)
		World->GetTimerManager().ClearTimer(UpdateVariationsTimerHandle);
}

void UPS_Human_AnimationInstance::OnTimerUpdateCurrentActiveBSVariation()
{
	UpdateCurrentActiveBSVariation();

	float TimeToCall = UKismetMathLibrary::RandomFloatInRange(CurrentMinTimeBetweenVariationsChanges, CurrentMaxTimeBetweenVariationsChanges);
	UWorld * World = GetWorld();
	if (World)
		World->GetTimerManager().SetTimer(UpdateVariationsTimerHandle, this, &UPS_Human_AnimationInstance::UpdateCurrentActiveBSVariation, TimeToCall, false);
}


void UPS_Human_AnimationInstance::OnLocomotionStanceChanged(ELocomotionStance NewStance)
{
	BlendSpacesPerStancesContainer.UpdateCurrentActiveVariation();
}

void UPS_Human_AnimationInstance::OnCarryingStanceChanged(ECarryingStance NewStance)
{
	BlendSpacesPerStancesContainer.UpdateCurrentActiveVariation();
}

/* Carrying variables */

void UPS_Human_AnimationInstance::SetAimOffset(float TargetYaw, float TargetPitch)
{
	AimOffsetYaw = FMath::Clamp(TargetYaw, -90.0f, 90.0f);
	AimOffsetPitch = FMath::Clamp(TargetPitch, -90.0f, 90.0f);
}

void UPS_Human_AnimationInstance::UpdateAimOffsetVariables(float TargetYaw, float TargetPitch, float DeltaTime, float YawInterpolationSpeed, float PitchInterpolationSpeed)
{
	TargetYaw = FMath::Clamp(TargetYaw, -90.0f, 90.0f);
	TargetPitch = FMath::Clamp(TargetPitch, -90.0f, 90.0f);
	AimOffsetYaw = UKismetMathLibrary::FInterpTo(AimOffsetYaw, TargetYaw, DeltaTime, YawInterpolationSpeed);
	AimOffsetPitch = UKismetMathLibrary::FInterpTo(AimOffsetPitch, TargetPitch, DeltaTime, PitchInterpolationSpeed);
}

/* Upper Body Blending */

void UPS_Human_AnimationInstance::SetUpperBodyBlendingStatus(bool ActivateUpperBoddy)
{
	UpperBody_FSMOverrideCounter += ActivateUpperBoddy ? 1 : -1;
	UpperBody_FSMOverride = UpperBody_FSMOverrideCounter > 0;
}

/* Jumping */

void UPS_Human_AnimationInstance::PlayJumpingAnimation()
{
	FJumpingMontage JumpingAnimation = JumpingAnimationsContainer.GetRandomJumpAnimation(LocomotionController, CarryingController, CurrentFeetPosition);
	if (!JumpingAnimation.IsValidMontage())
	{
		Character->SetWantsToJump(false);
		return;
	}
	
	if (JumpingAnimation.JumpingMontage)
	{
		if (PlayAnimationMontage(JumpingAnimation.JumpingMontage, 1.0f, 0.0, "None", true))
		{
			FOnMontageBlendingOutDynDelegate BlendingOutMontage;
			BlendingOutMontage.BindDynamic(this, &UPS_Human_AnimationInstance::OnJumpingMontageEndedOrBlendingOut);
			BindOnMontageBlendingOut(JumpingAnimation.JumpingMontage, BlendingOutMontage);

			FOnMontageEndedDynDelegate EndedMontage;
			EndedMontage.BindDynamic(this, &UPS_Human_AnimationInstance::OnJumpingMontageEndedOrBlendingOut);
			BindOnMontageEnded(JumpingAnimation.JumpingMontage, EndedMontage);
		}
	}
}

UFUNCTION(BlueprintCallable)
void UPS_Human_AnimationInstance::OnJumpingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	Character->SetWantsToJump(false);
}

/* Turning */

// Plays a turning animations.
void UPS_Human_AnimationInstance::PlayTurningAnimation(UAnimSequence * AnimTurnToPlay)
{
	TurningAnimationToPlay = AnimTurnToPlay;
	Turning = true;
}

// Updates the turning factor used to rule the turning animation. Goes from 0.0f to 1.0f.
void UPS_Human_AnimationInstance::UpdateTurningAnimation(float NewTurningFactor)
{
	TurningFactor = NewTurningFactor;
}

// Returns true if we are turning.
bool UPS_Human_AnimationInstance::IsTurning()
{
	return Turning;
}

// Stops turning behavior.
void UPS_Human_AnimationInstance::StopTurning()
{
	Turning = false;
}

/* In Air Landing Montages */

void UPS_Human_AnimationInstance::PlayInAirLandingMontage(UAnimMontage * LandingMontage)
{
	PlayAnimationMontage(LandingMontage, 1.0f, 0.0, "None", true);
	PlayLandingAnimation = false;
}

void UPS_Human_AnimationInstance::PlayInAirLandingAnimation(UAnimSequence* LandingSequence, float BlendOutTime, bool BlockMovementOnStart, EAnimationFeetPosition NewFeetPosition)
{
	LandingAnimation.InAirLandingSequence = LandingSequence;
	LandingAnimation.BlendOutTime = BlendOutTime;
	LandingAnimation.ShouldBlockMovemenUntilBlendOut = BlockMovementOnStart;
	CurrentFeetPosition = NewFeetPosition;
	PlayLandingAnimation = true;
}

// Called from the anim BP once we finish the landing animation.
void UPS_Human_AnimationInstance::LandingAnimationSequenceFinished()
{
	PlayLandingAnimation = false;
}

/* Ragdoll */

void UPS_Human_AnimationInstance::SetRagdollBlendOut(bool IsTouchingGround, bool IsFacingDownwards)
{
	SavePoseSnapshot(TEXT("RagdollSnapshot"));

	BlendingFromRagdollTime = IsTouchingGround ? TimeToBlendFromRagdollGrounded : TimeToBlendFromRagdollNotGrounded;

	// Play montage, only if touching ground.
	if (IsTouchingGround)
	{
		UAnimMontage * RagdollMontage = nullptr;
		if (IsFacingDownwards && GetUpFacingDownwardsMontages.Num() > 0)
			RagdollMontage = GetUpFacingDownwardsMontages[FMath::RandRange(0, GetUpFacingDownwardsMontages.Num() - 1)];
		else if(GetUpFacingUpwardsMontages.Num() > 0)
			RagdollMontage = GetUpFacingUpwardsMontages[FMath::RandRange(0, GetUpFacingUpwardsMontages.Num() - 1)];

		if (!RagdollMontage)
			return;

		if (PlayAnimationMontage(RagdollMontage, 1.0f, 0.0, "None", true))
		{
			FOnMontageBlendingOutDynDelegate BlendingOutMontage;
			BlendingOutMontage.BindDynamic(this, &UPS_Human_AnimationInstance::OnRagdollEndedOrBlendingOut);
			BindOnMontageBlendingOut(RagdollMontage, BlendingOutMontage);

			FOnMontageEndedDynDelegate EndedMontage;
			EndedMontage.BindDynamic(this, &UPS_Human_AnimationInstance::OnRagdollEndedOrBlendingOut);
			BindOnMontageEnded(RagdollMontage, EndedMontage);
		}
	}
	else
	{
		FTimerHandle BlendOutRagdoll;
		UWorld * World = GetWorld();
		if(World)
			World->GetTimerManager().SetTimer(BlendOutRagdoll, this, &UPS_Human_AnimationInstance::OnRagdollEndBlendOut, BlendingFromRagdollTime, false);
	}
}

void UPS_Human_AnimationInstance::OnRagdollEndBlendOut()
{
	Character->SetEnableCharacterMovementAndActions(true);
}

void UPS_Human_AnimationInstance::OnRagdollEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	Character->SetEnableCharacterMovementAndActions(true);
}

/* Climbing */

// Sets a climbing hanging animation to be played inside the AnimBP. Called by the Climb state when we enter the state.
void UPS_Human_AnimationInstance::SetClimbingHangingAnimation(const FHangToLedgeAnimations& NewHangingAnimationToPlay, bool StopMontages)
{
	HangingAnimationToPlay = NewHangingAnimationToPlay;
	HangingAnimationToPlay.HasPostHangingAnimation = NewHangingAnimationToPlay.PostHangingAnimation != nullptr;
	HangingAnimationToPlay.HangingStance = NewHangingAnimationToPlay.HangingStance;
}

// Updates the hanging factor used to control the hanging animations. Factor should go from 0.0f to 1.0f.
void UPS_Human_AnimationInstance::UpdateHangingAnimation(float NewHangingFactor)
{
	HangingFactor = NewHangingFactor;
}

// Called from AnimBP when TryClimbing starts blending out, we return to normal position.
void UPS_Human_AnimationInstance::OnTryClimbingStartBlendOut()
{
	OnTryClimbingStartBlendOutCallback.ExecuteIfBound();
}

// Called from AnimBP when TryClimbing reaches the top. Stops the montage before it blends out completely.
void UPS_Human_AnimationInstance::OnTryClimbingStop()
{
	OnTryClimbingStopCallback.ExecuteIfBound();
}

// Called from AnimBP when the turning montage throws an animation notify. From here on, the rest of the
// movement stops being root motion.
void UPS_Human_AnimationInstance::OnTurnClimbingMontageHalfway()
{
	OnHalfClimbingTurningPerformed.ExecuteIfBound();
}