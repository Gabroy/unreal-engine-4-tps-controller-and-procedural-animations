#pragma once

#include "CoreMinimal.h"
#include "Characters/Common/Components/Carrying/PS_Carrying_Structs_Enums.h"
#include "PS_Human_AnimationsStruct.generated.h"

class UAnimSequence;
class UBlendSpace;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;
class UPS_Human_AnimationInstance;

/* Marks if this animation uses two feet, or the left or right one. */
UENUM(BlueprintType)
enum EAnimationFeetPosition
{
	USES_BOTH_FEET,
	USES_LEFT_FOOT,
	USES_RIGHT_FOOT
};


/* ------------------------ Idle Animations ------------------------ */

/* Random idle animation that can be played while in Idle state. */
USTRUCT(BlueprintType, Blueprintable)
struct FIdleRandomAnimation
{
	GENERATED_BODY()

	// Idle sequence that will be played if this random animation is chosen.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence * IdleSequenceAnimation;

	// Position the feet should be at for playing this animation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EAnimationFeetPosition> FeetPosition;

	// Weight value representing a % probability that this animation might be played at any given time.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float WeightAnimation = 0.0f;

	// Computed on initialization. Used to decide which animation to play. Represents a range in 0.0 to 1.0 where the anim will be played.
	// To check the actual % get the next animation and subtract its FinalWeight from this anim FinalWeight.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float FinalWeight = 0.0f;
};

/* Idle animations container. */
USTRUCT(BlueprintType, Blueprintable)
struct FIdleAnimationsContainer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Standing")
	TArray<FIdleRandomAnimation> StandingUnarmedIdleAnimations;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Standing")
	TArray<FIdleRandomAnimation> StandingTwoHandedGunIdleAnimations;

	void ComputeTotalWeightsForAnims();

	FIdleRandomAnimation GetRandomIdleAnimation(UPS_Human_LocomotionController * LocomotionController, UPS_Human_CarryingController * CarryingController);

private:

	void ComputeTotalWeightsForArray(TArray<FIdleRandomAnimation>& AnimsToCompute);

	FIdleRandomAnimation PickRandomIdleAnimation(const TArray<FIdleRandomAnimation>& IdleAnimations);
};


/* ------------------------ Jumping Animations ------------------------ */

USTRUCT(BlueprintType, Blueprintable)
struct FJumpingMontage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * JumpingMontage = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxSpeedForPlaying = 10000000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinSpeedForPlaying = -1.0f;

	// If true, we will consider the intended feet variable when deciding to play the animation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool ShouldUseIntendedFeetToPlay = false;
	// Necessary feet for montage to play.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EAnimationFeetPosition> IntendedFeetPosition;

	bool IsValidMontage() const { return JumpingMontage != nullptr; }
};

// Jumping animations container.
USTRUCT(BlueprintType, Blueprintable)
struct FJumpingAnimationsContainer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Standing")
	TArray<FJumpingMontage> StandingUnarmedJumpingAnimations;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Standing")
	TArray<FJumpingMontage> StandingTwoHandedGunJumpingAnimations;

	FJumpingMontage GetRandomJumpAnimation(UPS_Human_LocomotionController * LocomotionController, UPS_Human_CarryingController * CarryingController, EAnimationFeetPosition CurrentFeetPosition);

private:

	FJumpingMontage PickRandomJumpAnimation(UPS_Human_LocomotionController * LocomotionController, EAnimationFeetPosition CurrentFeetPosition, const TArray<FJumpingMontage> & JumpAnimations);
};


/* ------------------------ Landing Animations ------------------------ */

// Returned by the PlayLandingAnimation if an animation sequence must be played.
USTRUCT(BlueprintType, Blueprintable)
struct FLandingAnimationData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	UAnimSequence* InAirLandingSequence = nullptr;

	// Time we will start blending out the landing sequence (upper body montages have their own blending out
	// defined by the montage for extra control).
	UPROPERTY(BlueprintReadOnly)
	float BlendOutTime = 0.25f;

	UPROPERTY(BlueprintReadOnly)
	bool ShouldBlockMovemenUntilBlendOut = false;
};

/* ------------------------ Blend Spaces Variations Animations ------------------------ */

// Struct containing a blendspace and the time necessary to blend to it.
// Used by certain animation states that use blend spaces connected to blend nodes.
// For example moving state from Grounded Velocity Facing.
USTRUCT(BlueprintType, Blueprintable)
struct FBlendSpaceVariation
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBlendSpaceBase * BlendSpaceVariation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float BlendTime = 0.2f;
};

// Struct used in Anim FSM to provide animations variations.
// Has two BS that are blended between and provide animation variations even while animation is playing (as long
// as sync markers are added).
// Stored in the animation instance next to the FBlendSpaceVariationsContainer to allow fast path optimizations.
USTRUCT(BlueprintType, Blueprintable)
struct FBlendSpaceVariationPair
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	UBlendSpaceBase * BSVariation1;
	
	UPROPERTY(BlueprintReadOnly)
	UBlendSpaceBase * BSVariation2;
	
	UPROPERTY(BlueprintReadOnly)
	bool UsingVariation1 = true;

	// Time we are taking to blend to new BS, updated based on FBlendSpaceVariation blend time we are blending to.
	UPROPERTY(BlueprintReadOnly)
	float TimeToBlendToBS = 0.4f;
};

// Struct that contains different BlendSpaces that can be swapped between them at any given time.
USTRUCT(BlueprintType, Blueprintable)
struct FBlendSpaceVariationsPool
{
	GENERATED_BODY()

	// Pool with all variations. First entry will be considered base blendspace.
	UPROPERTY(EditAnywhere)
	TArray<FBlendSpaceVariation> BlendSpacesPool;

	// Changes the current active variation. If UpdateToBaseVariation is true, we will update to the base variation.
	// Otherwise, we will update to a random one.
	void ChangeActiveVariation(FBlendSpaceVariationPair& BSVariationToUpdate, bool UpdateToBaseVariation);

private:

	void ChangeToBaseBSVariation(FBlendSpaceVariationPair& BSVariationToUpdate);

	void ChangeToRandomBSVariation(FBlendSpaceVariationPair& BSVariationToUpdate);

	// Gets a random blendspace, excluding the one passed as parameter if one is sent.
	// Prevents returning the same exact blendspace.
	FBlendSpaceVariation & GetRandomBS(UBlendSpaceBase * BlendSpaceToExclude = nullptr);
};

// This container works as a wrapper containing all BS dependent of locomotion or carrying stances.
USTRUCT(BlueprintType, Blueprintable)
struct FBlendSpacesForStancesContainer
{
	GENERATED_BODY()

	/* References */

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_AnimationInstance * AnimationInstance = nullptr;

	/* Blend spaces variables. Each combination has a pool and a pair. Pair is directly used by Anim FSM. Not stored in map due to inability by UE to allow
	storing structs with ptrs. */

	UPROPERTY(EditAnywhere, Category = "Animations - Pools")
	FBlendSpaceVariationsPool StandingUnarmedPool;

	UPROPERTY(EditAnywhere, Category = "Animations - Pools")
	FBlendSpaceVariationsPool StandingTwoHandedPool;

	/* Functions. */

	void Init(UPS_Human_AnimationInstance * NewAnimationInstance);

	void SetupBaseBSValues(UPS_Human_AnimationInstance * NewAnimationInstance);

	// If UpdateToBaseVariation is true, we will set the variation to the base bs.
	void UpdateCurrentActiveVariation(bool UpdateToBaseVariation = false);

private:

	void UpdateStandingBS(bool UpdateToBaseVariation);
	void UpdateCrouchedBS(bool UpdateToBaseVariation);
};