#pragma once

#include "CoreMinimal.h"
#include "Characters/Common/Animations/PS_Extended_AnimInstance.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationsStruct.h"
#include "Characters/Human/Components/Locomotion/PS_Locomotion_Structs_Enums.h"
#include "Characters/Human/Components/Locomotion/States/PS_LocState_Climb_Structs.h"
#include "PS_Human_AnimationInstance.generated.h"

class APS_Character;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;
class UPS_Human_InventoryComponent;
class UCharacterMovementComponent;

DECLARE_DELEGATE(FOnGroundedIdleEntered);
DECLARE_DELEGATE(FOnGroundedIdleExit);
DECLARE_DELEGATE(FOnHalfClimbingTurningPerformed);
DECLARE_DELEGATE(FOnTryClimbingStartBlendOut);
DECLARE_DELEGATE(FOnTryClimbingStop);

/*
* Animation Instance C++ class definition.
* 
* Defines a human character and will be used for storing variables and defining some C++ callable functions
* that will affect the Animation FSM.
* 
* While not being done right now, we could move BP logic here and update logic here based on loc state directly
* to optimize performance. As this is mostly a learning project, I will probably stick to BP for updating most variables.
* Maybe I will move everything once its starting to be finished.
*/
UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_AnimationInstance : public UPS_Extended_AnimInstance
{
	GENERATED_BODY()
	
public:

	/* Callbacks */

	/* Idle callbacks state. */

	FOnGroundedIdleEntered OnGroundedIdleEntered;
	FOnGroundedIdleExit OnGroundedIdleExit;

	/* Climbing Turning animation callbacks */

	FOnTryClimbingStartBlendOut OnTryClimbingStartBlendOutCallback;
	FOnTryClimbingStop OnTryClimbingStopCallback;
	FOnHalfClimbingTurningPerformed OnHalfClimbingTurningPerformed;

protected:

	/* ------------------- Components ------------------- */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Character = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_LocomotionController * LocomotionController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_CarryingController * CarryingController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_InventoryComponent * InventoryComponent = nullptr;

	/* ------------------- Feet position variables. ------------------- */

	UPROPERTY(BlueprintReadWrite)
	TEnumAsByte<EAnimationFeetPosition> CurrentFeetPosition;

	UPROPERTY(BlueprintReadWrite)
	TEnumAsByte<EAnimationFeetPosition> IntendedFeetPosition;

	/* ------------------- Upper Body Variables ------------------- */

	UPROPERTY()
	int UpperBody_FSMOverrideCounter = 0;

	// If set to true, we activate the Upper Body FSM overriding full FSM animations.
	UPROPERTY(BlueprintReadOnly)
	bool UpperBody_FSMOverride = false;

	/* ------------------- Idle animations ------------------- */

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FIdleRandomAnimation IdleAnimationToPlay;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Animations - Idle Variations")
	FIdleAnimationsContainer IdleAnimationsContainer;

	/* ------------------- BS variations animations ------------------- */

	UPROPERTY(BlueprintReadOnly)
	FTimerHandle UpdateVariationsTimerHandle;
	UPROPERTY(BlueprintReadOnly)
	float CurrentMinTimeBetweenVariationsChanges = 0.0f;
	UPROPERTY(BlueprintReadOnly)
	float CurrentMaxTimeBetweenVariationsChanges = 0.0f;

	UPROPERTY(EditAnywhere, Category = "Animations - Stances Variations")
	FBlendSpacesForStancesContainer BlendSpacesPerStancesContainer;

	/* Variation pairs are updated by the FBlendSpacesForStancesContainer struct.
	The container updates each struct automatically if conditions are met.
	If new variations are needed, a new pair should be added here and the
	necessary conditions added to.
	They are keep separated from it and stored directly here to allow fast path
	optimizations. */
	UPROPERTY(BlueprintReadOnly, Category = "Animations")
	FBlendSpaceVariationPair StandingUnarmedPair;
	UPROPERTY(BlueprintReadOnly, Category = "Animations")
	FBlendSpaceVariationPair StandingTwoHandedGunPair;

	/* ------------------- Carrying variables ------------------- */

	// Set to true while aiming.
	UPROPERTY(BlueprintReadOnly)
	bool bIsAiming = false;

	UPROPERTY(BlueprintReadOnly)
	float AimOffsetYaw = 0.0f;

	UPROPERTY(BlueprintReadOnly)
	float AimOffsetPitch = 0.0f;

	/* ------------------- Jumping variables ------------------- */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations - Jumping Variations")
	FJumpingAnimationsContainer JumpingAnimationsContainer;

	/* ------------------- Turning animations variables ------------------- */

	// Used by anim FSM to show turning anims state.
	UPROPERTY(BlueprintReadOnly)
	bool Turning = false;
	UPROPERTY(BlueprintReadOnly)
	UAnimSequence * TurningAnimationToPlay = nullptr;
	// Turning factor used to rule turning animations current frame. Returned by locomotion. 0.0f on start, 1.0f on turning fully done.
	UPROPERTY(BlueprintReadOnly)
	float TurningFactor = 0.0f;

	/* ------------------- In Air Landing variables ------------------- */

	UPROPERTY(BlueprintReadWrite)
	bool PlayLandingAnimation = false;
	UPROPERTY(BlueprintReadOnly)
	FLandingAnimationData LandingAnimation;

	/* ------------------- Ragdoll variables ------------------- */

	UPROPERTY(BlueprintReadOnly, Category = "Ragdoll")
	float BlendingFromRagdollTime = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	float TimeToBlendFromRagdollNotGrounded = 0.8f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	float TimeToBlendFromRagdollGrounded = 0.50f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	TArray <UAnimMontage *> GetUpFacingUpwardsMontages;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	TArray<UAnimMontage *> GetUpFacingDownwardsMontages;

	/* ------------------- Climbing variables ------------------- */
	
	UPROPERTY(BlueprintReadOnly)
	FHangToLedgeAnimations HangingAnimationToPlay;
	UPROPERTY(BlueprintReadOnly)
	float HangingFactor = 0.0f;

	/* Protected Methods */

	// Called when the game starts.
	virtual void NativeBeginPlay() override;

#if WITH_EDITOR
	// Will call PreProcessAnimations to update values when a value is added to any of the two TArrays.
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif

public:

	/* ------------------- Idle ------------------- */

	// Called when we enter the Idle State in the AnimBP.
	UFUNCTION(BlueprintCallable)
	void OnIdleStateEntered();
	// Called when we exit the Idle State in the AnimBP.
	UFUNCTION(BlueprintCallable)
	void OnIdleStateExit();
	// Plays a random Idle animation. Called from AnimBP if we remain for time enough in grounded idle state.
	UFUNCTION(BlueprintCallable)
	void PlayRandomIdleAnimation();
	// Allows setting the intended feet position from code if we want.
	UFUNCTION(BlueprintCallable)
	void SetIntendedFootPosition(TEnumAsByte<EAnimationFeetPosition> NewFeetPosition);

	/* ------------------- BS variations animations ------------------- */

	// Updates BS variation if we are using one. Otherwise, nothing happens.
	// Called from anim FSM.
	UFUNCTION(BlueprintCallable)
	void UpdateCurrentActiveBSVariation();
	// Starts timer to update the currently used BS variation if we are using one.
	// Loops until StopTimerToUpdateCurrentActiveBSVariation is called.
	UFUNCTION(BlueprintCallable)
	void StartTimerToUpdateCurrentActiveBSVariation(float MinTimeBetweenVariations, float MaxTimeBetweenVariations);
	// Stops timer to update the currently used BS variation if we are using one.
	UFUNCTION(BlueprintCallable)
	void StopTimerToUpdateCurrentActiveBSVariation();
	UFUNCTION(BlueprintCallable)
	void OnTimerUpdateCurrentActiveBSVariation();
	UFUNCTION()
	void OnLocomotionStanceChanged(ELocomotionStance NewStance);
	UFUNCTION()
	void OnCarryingStanceChanged(ECarryingStance NewStance);

	/* ------------------- Carrying variables ------------------- */

	UFUNCTION(BlueprintCallable)
	void SetIsAiming(bool NewIsAiming) { bIsAiming = NewIsAiming; }
	UFUNCTION(BlueprintPure, BlueprintCallable)
	bool GetIsAiming() const { return bIsAiming; }

	// Directly sets up aim offset. Normally called on start of states that use aim offsets.
	UFUNCTION(BlueprintCallable)
	void SetAimOffset(float TargetYaw, float TargetPitch);
	UFUNCTION(BlueprintCallable)
	void UpdateAimOffsetVariables(float TargetYaw, float TargetPitch, float DeltaTime, float YawInterpolationSpeed, float PitchInterpolationSpeed);

	/* ------------------- Upper Body Blending ------------------- */

	// Activates or Deactivates Upper Body Blending FSM.
	void SetUpperBodyBlendingStatus(bool ActivateUpperBoddy);

	/* ------------------- Jumping ------------------- */

	UFUNCTION(BlueprintCallable)
	void PlayJumpingAnimation();
	UFUNCTION(BlueprintCallable)
	void OnJumpingMontageEndedOrBlendingOut(UAnimMontage * Montage, bool Interrupted);

	/* ------------------- Turning animations ------------------- */

	// Plays a turning animations.
	UFUNCTION(BlueprintCallable)
	void PlayTurningAnimation(UAnimSequence* AnimTurnToPlay);
	// Updates the turning factor used to rule the turning animation. Goes from 0.0f to 1.0f.
	UFUNCTION(BlueprintCallable)
	void UpdateTurningAnimation(float NewTurningFactor);
	// Returns true if we are turning.
	UFUNCTION(BlueprintCallable)
	bool IsTurning();
	// Stops turning behavior.
	UFUNCTION(BlueprintCallable)
	void StopTurning();
	// Returns turning factor. 0.0f on start, 1.0f on turning fully done.
	UFUNCTION(BlueprintCallable)
	float GetTurningFactor() const { return TurningFactor; }

	/* ------------------- In Air Landing ------------------- */

	// Plays a landing motnage.
	void PlayInAirLandingMontage(UAnimMontage * LandingMontage);
	// Plays a landing sequence.
	void PlayInAirLandingAnimation(UAnimSequence * LandingSequence, float BlendOutTime, bool BlockMovementOnStart, EAnimationFeetPosition NewFeetPosition);
	// Called from the anim BP once we finish the landing animation.
	UFUNCTION(BlueprintCallable)
	void LandingAnimationSequenceFinished();

	/* ------------------- Ragdoll ------------------- */

	UFUNCTION(BlueprintCallable)
	void SetRagdollBlendOut(bool IsTouchingGround, bool IsFacingDownwards);
	UFUNCTION(BlueprintCallable)
	void OnRagdollEndBlendOut();
	UFUNCTION(BlueprintCallable)
	void OnRagdollEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	/* ------------------- Climbing ------------------- */

	// Sets a climbing hanging animation to be played inside the AnimBP. Called by the Climb state when we enter the state.
	void SetClimbingHangingAnimation(const FHangToLedgeAnimations & NewHangingAnimationToPlay, bool StopMontages = true);
	// Updates the hanging factor used to control the hanging animations. Factor should go from 0.0f to 1.0f.
	void UpdateHangingAnimation(float NewHangingFactor);
	// Called from AnimBP when TryClimbing starts blending out, we return to normal position.
	UFUNCTION(BlueprintCallable)
	void OnTryClimbingStartBlendOut();
	// Called from AnimBP when TryClimbing reaches the top. Stops the montage before it blends out completely.
	UFUNCTION(BlueprintCallable)
	void OnTryClimbingStop();
	// Called from AnimBP when the turning montage throws an animation notify. From here on, the rest of the
	// movement stops being root motion.
	UFUNCTION(BlueprintCallable)
	void OnTurnClimbingMontageHalfway();


	friend struct FBlendSpacesForStancesContainer;
};
