#pragma once

#include "PS_Human_InventoryStructs.generated.h"

/*
* Inventory Slots.
* 
* Used as a fast identification to retrieve items in key position.
* Like Primary Item, Secondary Item, ...
*/

UENUM(Blueprintable)
enum EInventoryItemSlot
{
	MARKUP_NONE,
	MARKUP_PRIMARY_SLOT,
	MARKUP_SECONDARY_SLOT
};