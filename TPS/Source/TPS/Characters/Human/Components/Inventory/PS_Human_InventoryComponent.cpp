#include "PS_Human_InventoryComponent.h"
#include "GameFramework/Character.h"
#include "Gameplay/Items/PS_BaseItem.h"
#include "Gameplay/Items/Weapons/PS_WeaponItem.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Characters/Human/Components/Animations/PS_Human_MontageController.h"

UPS_Human_InventoryComponent::UPS_Human_InventoryComponent() : UPS_InventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_Human_InventoryComponent::BeginPlay()
{
	UPS_InventoryComponent::BeginPlay();

	ACharacter * OwnerActor = Cast<ACharacter>(GetOwner());
	if (!OwnerActor)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InventoryComponent::Init OwnerActor Returned Null"));
		return;
	}

	CarryingController = OwnerActor->FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InventoryComponent::Init CarryingController Returned Null"));
		return;
	}

	MontageController = OwnerActor->FindComponentByClass<UPS_Human_MontageController>();
	if (!MontageController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InventoryComponent::Init MontageController Returned Null"));
		return;
	}
}

void UPS_Human_InventoryComponent::StartGrabItem(APS_BaseItemActorInstance* ItemToPick)
{
	if (!ItemToPick || !MontageController || !CanGrabItem(ItemToPick))
	{
		return;
	}

	CurrentlyPickingItem = ItemToPick;
	MontageController->PlayPickupMontage(false, ItemToPick->GetActorLocation());
}

void UPS_Human_InventoryComponent::CancelGrabItem()
{
	if (MontageController)
	{
		MontageController->CancelPickupMontage();
	}
}

void UPS_Human_InventoryComponent::FinishGrabItem(bool Interrupted)
{
	if (!Interrupted && CurrentlyPickingItem)
	{
		GrabItem(CurrentlyPickingItem);
	}
	CurrentlyPickingItem = nullptr;
}

void UPS_Human_InventoryComponent::GrabItem(APS_BaseItemActorInstance * ItemToPick)
{
	if (ItemToPick && CanGrabItem(ItemToPick))
	{
		// Attach picking item to hand.
		AttachItemToSocket(RightHandSocketName, ItemToPick);

		// Set Currently Equipped Item to picking object.
		CurrentlyEquippedItem = ItemToPick;

		// Disable interactions for the object.
		CurrentlyEquippedItem->SetAllowInteractions(false);
		
		// Change carrying stance to the one required by the item by default.
		ECarryingStance CarryingStance = CurrentlyEquippedItem->GetItemCarryingStance();
		CarryingController->ChangeCarryingStance(CarryingStance);

		OnAddedItem(ItemToPick);

		if (OnCarryingEquipedItemChanged.IsBound())
			OnCarryingEquipedItemChanged.Broadcast(ItemToPick);
	}
}

bool UPS_Human_InventoryComponent::CanGrabItem(APS_BaseItemActorInstance * ItemToPick)
{
	bool HasHandsEmpty = CarryingController->GetCurrentStance() == ECarryingStance::Unarmed;
	bool SocketAcceptsItem = CanAttachItemToSocket(RightHandSocketName);
	return ItemToPick && HasHandsEmpty && SocketAcceptsItem;
}

void UPS_Human_InventoryComponent::DropCarryingItem()
{
	if (CanDropCarryingItem())
	{
		// Detach item from hand socket.
		DettachItemFromSocket(RightHandSocketName);

		// Activate physics and interaction.
		CurrentlyEquippedItem->SetSimulatePhysics(true);
		CurrentlyEquippedItem->SetAllowInteractions(true);

		// Launch OnRemovedItem function to perform any necessary cleanup of inventory metadata.
		OnRemovedItem(CurrentlyEquippedItem);

		// Set Carrying stance to unarmed again.
		CarryingController->ChangeCarryingStance(ECarryingStance::Unarmed);

		// Set currently equipped item to nullptr.
		CurrentlyEquippedItem = nullptr;

		// Broadcast on item changed.
		if (OnCarryingEquipedItemChanged.IsBound())
			OnCarryingEquipedItemChanged.Broadcast(nullptr);
	}
}

bool UPS_Human_InventoryComponent::CanDropCarryingItem()
{
	return CurrentlyEquippedItem && !IsSocketEmpty(RightHandSocketName);
}

void UPS_Human_InventoryComponent::ChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave)
{
	if (!CanChangeOrSaveItem(ItemToChangeOrSave))
		return;

	// If null, there is no item with this markup present.
	APS_BaseItemActorInstance * ItemToChangeTo = GetItemInInventorySlot(ItemToChangeOrSave);
	if (!ItemToChangeTo)
		return;

	// If we don't have any item equipped, we simply have to play take animation with the new requested one.
	bool AlreadyEquippedItem = CurrentlyEquippedItem != nullptr;
	if (!AlreadyEquippedItem)
	{
		StartTakeItem(ItemToChangeOrSave);
		return;
	}
	
	// If different item to take, once we save the item, we will start taking the new one.
	// Otherwise, we will simply save the item if it's the same.
	if (ItemToChangeTo != CurrentlyEquippedItem)
	{
		LaunchTakeMontageOnEndedSave = true;
		ObjectToChangeToSlot = ItemToChangeOrSave;
	}

	// Once this montage ends, we will launch the taking one if we were requesting a different item.
	StartSaveCarryingItem();
}

bool UPS_Human_InventoryComponent::CanChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave)
{
	return MontageController
		&& !MontageController->IsWeaponChangeMontagePlaying()
		&& GetItemInInventorySlot(ItemToChangeOrSave) != nullptr;
}

void UPS_Human_InventoryComponent::CancelChangeOrSaveItem()
{
	if (MontageController)
	{
		LaunchTakeMontageOnEndedSave = false;
		MontageController->CancelChangeItemMontage();
	}
}

void UPS_Human_InventoryComponent::StartSaveCarryingItem()
{
	if (!CanSaveCarryingItem())
		return;

	EInventoryItemSlot MarkupToRemove = GetSlotForInventoryItem(CurrentlyEquippedItem);
	MontageController->PlaySaveCarryingItemMontage(false, MarkupToRemove);
}

void UPS_Human_InventoryComponent::DoSaveCarryingItem()
{
	if (CurrentlyEquippedItem)
	{
		// Remove object from hand socket.
		DettachItemFromSocket(RightHandSocketName);

		// Attach item to store socket again.
		FName SocketName = GetItemStoreSocketFromItem(CurrentlyEquippedItem);
		AttachItemToSocket(SocketName, CurrentlyEquippedItem);

		// Change carrying stance to unarmed.
		CarryingController->ChangeCarryingStance(ECarryingStance::Unarmed);

		// Finish save carrying by setting currently equipped to nullptr.
		CurrentlyEquippedItem = nullptr;
	}
}

bool UPS_Human_InventoryComponent::CanSaveCarryingItem() const
{
	return CurrentlyEquippedItem != nullptr && MontageController && !MontageController->IsWeaponChangeMontagePlaying();
}

void UPS_Human_InventoryComponent::OnSaveCarryingItemMontageEnded(bool Interrupted)
{
	// Launch take item on item changed.
	if (!Interrupted && LaunchTakeMontageOnEndedSave)
	{
		StartTakeItem(ObjectToChangeToSlot);
	}
	LaunchTakeMontageOnEndedSave = false;
}

void UPS_Human_InventoryComponent::StartTakeItem(EInventoryItemSlot ItemToTakeSlot)
{
	if (!CanTakeItem())
		return;
	
	// If there is an object with the markup, start taking item.
	if (GetItemInInventorySlot(ItemToTakeSlot))
	{
		ObjectToChangeToSlot = ItemToTakeSlot;
		MontageController->PlayTakeItemItemMontage(false, ObjectToChangeToSlot);
	}
}

void UPS_Human_InventoryComponent::DoTakeItem()
{
	CurrentlyEquippedItem = GetItemInInventorySlot(ObjectToChangeToSlot);
	if (CurrentlyEquippedItem)
	{
		// Remove object from the previously attached socket. If they were attached to any.
		FName SocketName = GetItemStoreSocketFromItem(CurrentlyEquippedItem);
		DettachItemFromSocket(SocketName);

		// Attach object to the hand socket.
		AttachItemToSocket(RightHandSocketName, CurrentlyEquippedItem);

		// Change carrying stance to the one required by the item by default.
		ECarryingStance CarryingStance = CurrentlyEquippedItem->GetItemCarryingStance();
		CarryingController->ChangeCarryingStance(CarryingStance);

		// Set to none as we are not changing to anything anymore.
		ObjectToChangeToSlot = EInventoryItemSlot::MARKUP_NONE;
	}
}

bool UPS_Human_InventoryComponent::CanTakeItem() const
{
	return CurrentlyEquippedItem == nullptr && MontageController && !MontageController->IsWeaponChangeMontagePlaying();
}

void UPS_Human_InventoryComponent::OnAddedItem(APS_BaseItemActorInstance * AddedItem)
{
	if (!AddedItem)
		return;

	EItemCategory ItemCategory = AddedItem->GetItemCategory();
	switch (ItemCategory)
	{
	case EItemCategory::ITEM_Weapon:
		OnAddedWeapon(Cast<APS_WeaponActorInstance>(AddedItem));
		break;
	default:
		break;
	}
}

void UPS_Human_InventoryComponent::OnAddedWeapon(APS_WeaponActorInstance * AddedWeapon)
{
	if (!AddedWeapon)
		return;

	// Add Item to slot based on weapon category.
	EInventoryItemSlot ItemSlot = OnAddedWeaponGetExpectedSlot(AddedWeapon);
	APS_BaseItemActorInstance * ItemInSocket = GetItemInInventorySlot(ItemSlot);
	if (ItemInSocket == nullptr)
	{
		AddItemInInventoryToSlot(ItemInSocket, ItemSlot);
	}

}

EInventoryItemSlot UPS_Human_InventoryComponent::OnAddedWeaponGetExpectedSlot(APS_WeaponActorInstance * AddedWeapon)
{
	UPS_WeaponBaseDefinitionAsset * WeaponDefinition = AddedWeapon->GetWeaponDefinition();
	if (!WeaponDefinition)
		return EInventoryItemSlot::MARKUP_NONE;

	EWeaponCategory WeaponCategory = WeaponDefinition->GetWeaponCategory();
	switch (WeaponCategory)
	{
	case EWeaponCategory::WEAPON_UNDEFINED:
		return EInventoryItemSlot::MARKUP_NONE;
		break;
	case EWeaponCategory::WEAPON_ONE_HANDED_GUN:
		return EInventoryItemSlot::MARKUP_SECONDARY_SLOT;
		break;
	case EWeaponCategory::WEAPON_TWO_HANDED_GUN:
		return EInventoryItemSlot::MARKUP_PRIMARY_SLOT;
		break;
	}
	return EInventoryItemSlot::MARKUP_NONE;
}

void UPS_Human_InventoryComponent::OnRemovedItem(APS_BaseItemActorInstance * RemovedItem)
{
	if (RemovedItem)
	{
		RemoveItemFromSlotInInventory(RemovedItem);
	}
}

void UPS_Human_InventoryComponent::AddItemInInventoryToSlot(APS_BaseItemActorInstance * AddedItem, EInventoryItemSlot SlotToAddTo)
{
	TEnumAsByte<EInventoryItemSlot> & SlotResult = ItemToSlot.FindOrAdd(AddedItem);
	SlotResult = SlotToAddTo;
}

void UPS_Human_InventoryComponent::RemoveItemFromSlotInInventory(APS_BaseItemActorInstance * ItemToRemove)
{
	if (ItemToSlot.Contains(ItemToRemove))
	{
		ItemToSlot.Remove(ItemToRemove);
	}
}


APS_BaseItemActorInstance * UPS_Human_InventoryComponent::GetItemInInventorySlot(EInventoryItemSlot ItemMarkup)
{
	if (SlotToInventorySocket.Contains(ItemMarkup))
	{
		FName SocketName = SlotToInventorySocket[ItemMarkup];
		return GetItemInSocket(SocketName);
	}
	return nullptr;
}

EInventoryItemSlot UPS_Human_InventoryComponent::GetSlotForInventoryItem(APS_BaseItemActorInstance * ItemToCheck)
{
	if (ItemToSlot.Contains(ItemToCheck))
	{
		return ItemToSlot[ItemToCheck];
	}
	return EInventoryItemSlot::MARKUP_NONE;
}

FName UPS_Human_InventoryComponent::GetItemStoreSocketFromItem(APS_BaseItemActorInstance* ItemToCheck)
{
	EInventoryItemSlot ItemMarkup = GetSlotForInventoryItem(ItemToCheck);

	if (ItemMarkup != EInventoryItemSlot::MARKUP_NONE && SlotToInventorySocket.Contains(ItemMarkup))
	{
		return SlotToInventorySocket[ItemMarkup];
	}
	return FName();
}

FName UPS_Human_InventoryComponent::GetItemStoreSocketFromSlot(EInventoryItemSlot ItemMarkup)
{
	if (SlotToInventorySocket.Contains(ItemMarkup))
	{
		return SlotToInventorySocket[ItemMarkup];
	}
	return FName();
}