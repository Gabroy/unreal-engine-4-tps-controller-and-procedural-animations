#pragma once

#include "Characters/Common/Components/Inventory/PS_InventoryComponent.h"
#include "Characters/Human/Components/Inventory/PS_Human_InventoryStructs.h"
#include "PS_Human_InventoryComponent.generated.h"

class APS_WeaponActorInstance;
class UPS_Human_CarryingController;
class UPS_Human_MontageController;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnCarryingEquipedItemChanged, APS_BaseItemActorInstance *)

/*
* Human Inventory component used to implement an inventory like system.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_InventoryComponent : public UPS_InventoryComponent
{

	GENERATED_BODY()

protected:

	/* Components */

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_CarryingController * CarryingController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_MontageController * MontageController = nullptr;

	/* Variables */

	// Controls in which socket item will be stored when saved based on the markup.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<TEnumAsByte<EInventoryItemSlot>, FName> SlotToInventorySocket;

	UPROPERTY(BlueprintReadOnly)
	TMap<APS_BaseItemActorInstance*, TEnumAsByte<EInventoryItemSlot>> ItemToSlot;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName RightHandSocketName = "RHand";

	UPROPERTY(BlueprintReadOnly)
	APS_BaseItemActorInstance * CurrentlyEquippedItem = nullptr;

	/* Picking variables */
	UPROPERTY(BlueprintReadOnly)
	APS_BaseItemActorInstance * CurrentlyPickingItem = nullptr;

	/* Change weapon variables */
	bool LaunchTakeMontageOnEndedSave = false;
	EInventoryItemSlot ObjectToChangeToSlot;

public:

	/* Callbacks */

	FOnCarryingEquipedItemChanged OnCarryingEquipedItemChanged;

	/* Constructor */

	UPS_Human_InventoryComponent();

	/* Methods */

	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void StartGrabItem(APS_BaseItemActorInstance * ItemToPick);
	UFUNCTION(BlueprintCallable)
	void CancelGrabItem();
	UFUNCTION(BlueprintCallable)
	bool CanGrabItem(APS_BaseItemActorInstance * ItemToPick);

	UFUNCTION(BlueprintCallable)
	void DropCarryingItem();
	UFUNCTION(BlueprintCallable)
	bool CanDropCarryingItem();

	// Changes to the item markup (if not equipped), saves the item if equipped.
	UFUNCTION(BlueprintCallable)
	void ChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave);
	UFUNCTION(BlueprintCallable)
	bool CanChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave);
	UFUNCTION(BlueprintCallable)
	void CancelChangeOrSaveItem();

	/* Getters */

	UFUNCTION(BlueprintPure, BlueprintCallable)
	APS_BaseItemActorInstance * GetCurrentEquippedItem() { return CurrentlyEquippedItem; }

private:

	/* Items calls */

	void OnAddedItem(APS_BaseItemActorInstance * AddedItem);
	void OnAddedWeapon(APS_WeaponActorInstance * AddedWeapon);
	EInventoryItemSlot OnAddedWeaponGetExpectedSlot(APS_WeaponActorInstance * AddedWeapon);

	void OnRemovedItem(APS_BaseItemActorInstance * RemovedItem);

	/* Pick Items. */
	
	UFUNCTION(BlueprintCallable)
	void FinishGrabItem(bool Interrupted);
	UFUNCTION(BlueprintCallable)
	void GrabItem(APS_BaseItemActorInstance * ItemToPick);

	/* Item saving and taking. */

	UFUNCTION(BlueprintCallable)
	void StartSaveCarryingItem();
	// Called by AnimBP.
	UFUNCTION(BlueprintCallable)
	void DoSaveCarryingItem();
	UFUNCTION(BlueprintCallable)
	bool CanSaveCarryingItem() const;
	UFUNCTION(BlueprintCallable)
	void OnSaveCarryingItemMontageEnded(bool Interrupted);

	UFUNCTION(BlueprintCallable)
	void StartTakeItem(EInventoryItemSlot ItemToTakeSlot);
	// Called by AnimBP.
	UFUNCTION(BlueprintCallable)
	void DoTakeItem();
	UFUNCTION(BlueprintCallable)
	bool CanTakeItem() const;

	/* Slots */

	UFUNCTION(BlueprintCallable)
	void AddItemInInventoryToSlot(APS_BaseItemActorInstance * AddedItem, EInventoryItemSlot SlotToAddTo);
	UFUNCTION(BlueprintCallable)
	void RemoveItemFromSlotInInventory(APS_BaseItemActorInstance * ItemToRemove);

	UFUNCTION(BlueprintCallable)
	APS_BaseItemActorInstance * GetItemInInventorySlot(EInventoryItemSlot ItemMarkup);
	UFUNCTION(BlueprintCallable)
	EInventoryItemSlot GetSlotForInventoryItem(APS_BaseItemActorInstance* ItemToCheck);

	UFUNCTION(BlueprintCallable)
	FName GetItemStoreSocketFromItem(APS_BaseItemActorInstance * ItemToCheck);
	UFUNCTION(BlueprintCallable)
	FName GetItemStoreSocketFromSlot(EInventoryItemSlot ItemMarkup);


	/* Friend classes. */
	friend class UPS_Human_AnimationInstance;
	friend class UPS_Human_MontageController;
};
