#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_Component.h"
#include "Characters/Common/Components/Carrying/PS_Carrying_Structs_Enums.h"
#include "Characters/Human/Components/Inventory/PS_Human_InventoryStructs.h"
#include "PS_Human_CarryingController.generated.h"

class ACharacter;
class AController;
class UPS_Human_LocomotionController;
class UPS_Human_MontageController;
class UPS_Human_InventoryComponent;
class UPS_Human_UpperTorsoIKController;
class APS_BaseItemActorInstance;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnCarryingStanceChanged, ECarryingStance)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnCarryingStateChanged, FName)

/*
* This controller is responsible of controlling the behaviour of a character based on the currently carrying items on it's hands.
* 
* Depending on the carrying items, certain functionalities may or may not be possible and the stance of the player may change.
* 
* To keep data separated and modular, this controller allows implementing substates for carrying stances or specific items
* which can be used for computing necessary variables or providing specific functionality. For example, when using ranged weapons,
* a substate can be used to compute the current yaw or pitch of the camera controller to make the hands point in that direction.
* 
* Furthermore, the carrying controller is also responsible of handling all indirect events that can affect the carrying stance,
* from falling, getting into water, being stunned, ragdolling, ...
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_CarryingController : public UFSM_Component
{

	GENERATED_BODY()

public:

	/* Callbacks */
	FOnCarryingStateChanged OnCarryingStateChanged;
	FOnCarryingStanceChanged OnCarryingStanceChanged;

protected:

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	ACharacter* Owner = nullptr;

	UPROPERTY(BlueprintReadOnly)
	AController * OwnerController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_LocomotionController * LocomotionController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_AnimationInstance * AnimationInstance = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_InventoryComponent * InventoryComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_UpperTorsoIKController * UpperTorsoIKComponent = nullptr;

	/* Variables */

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ECarryingStance> CurrentStance;

	// Interpolation speed for aim offset.
	UPROPERTY(BlueprintReadOnly)
	float AimOffsetInterpolationSpeed = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D AimOffsetYawStartEndSpeedReduction = FVector2D(45.0f, 75.0f);
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxYawInterpSpeedReduction = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D AimOffsetPitchStartEndSpeedReduction = FVector2D(45.0f, 75.0f);
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxPitchInterpSpeedReduction = 0.5f;

public:

	/* Functions */

	// Initializes the controller, storing all necessary values.
	void Init();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/* Pick items. */

	// Pick the item attaching it to the player hand.
	UFUNCTION(BlueprintCallable)
	void StartPickItem(APS_BaseItemActorInstance * ItemToPick);
	// Called if one wants to cancel picking an object.
	UFUNCTION(BlueprintCallable)
	void CancelPickItem();
	// Returns true if we can pick the item, false otherwise.
	UFUNCTION(BlueprintCallable)
	bool CanPickItem(APS_BaseItemActorInstance * ItemToPick);

	/* Drop item */

	// Drops carried item to the ground.
	UFUNCTION(BlueprintCallable)
	void DropCarryingItem();
	UFUNCTION(BlueprintCallable)
	bool CanDropCarryingItem();

	/* Change Item functions */

	UFUNCTION(BlueprintCallable)
	void ChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave);
	UFUNCTION(BlueprintCallable)
	bool CanChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave);
	UFUNCTION(BlueprintCallable)
	void CancelChangeOrSaveItem();

	/* Gun functions */

	UFUNCTION(BlueprintCallable)
	void SetIsHolsteringWeapon(bool ShouldHolster);
	UFUNCTION(BlueprintCallable)
	bool GetIsHolsteringWeapon() const;

	UFUNCTION(BlueprintCallable)
	void SetAiming(bool ShouldAim);
	UFUNCTION(BlueprintCallable)
	bool GetIsAiming() const;
	UFUNCTION(BlueprintCallable)
	bool CanAim() const;

	// Directly sets up aim offset. Normally called on start of states that use aim offsets.
	UFUNCTION(BlueprintCallable)
	void SetAimOffset(float TargetYaw, float TargetPitch);
	UFUNCTION(BlueprintCallable)
	void UpdateAimOffsetVariables(float TargetYaw, float TargetPitch, float DeltaTime);

	/* IK functions */

	UFUNCTION(BlueprintCallable)
	void SetUpperBodyIKEnabledBasedOnCarryingStance();

	/* C++ Getters and setters */
	
	UFUNCTION(BlueprintPure, BlueprintCallable)
	APS_BaseItemActorInstance * GetCarryingItem() const;

	inline ECarryingStance GetCurrentStance() const { return CurrentStance; }

private:

	/* Init functions */

	void FetchComponents();

	void AddPrerequisiteComponents();

	/* General. */

	void ChangeCarryingStance(ECarryingStance NewStance);
	
	/* Callbacks. */

	void OnLocomotionStateChanged(FName LocomotionNewStateName, int LocomotionStateID);
	inline void OnLocomotionChangedToGroundedMode();
	inline void OnLocomotionChangedToClimbingMode();
	inline void OnLocomotionChangedToInAirMode();
	inline void OnLocomotionChangedToRagdollMode();

	/* Friend classes. */

	friend class UPS_Human_AnimationInstance;
	friend class UPS_Human_InventoryComponent;
};
