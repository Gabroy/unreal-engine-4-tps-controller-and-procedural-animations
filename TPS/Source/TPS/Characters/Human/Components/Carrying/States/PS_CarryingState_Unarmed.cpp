#include "PS_CarryingState_Unarmed.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"

void UPS_CarryingState_Unarmed::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_Unarmed::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_Unarmed::Init_Implementation LocController Returned Null"));
		return;
	}

	CarryingController = Cast<UPS_Human_CarryingController>(Actor->GetComponentByClass(UPS_Human_CarryingController::StaticClass()));
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_Unarmed::Init_Implementation CarryingController Returned Null"));
		return;
	}

	TickByDefault = true;
}

void UPS_CarryingState_Unarmed::OnEnter_Implementation()
{
	if (LocController)
	{
		LocController->SetUpperBodyIKEnabled(EUpperTorsoIKModes::UB_IK_GROUNDED_UNARMED);
	}

	if (CarryingController)
	{
		CarryingController->SetIsHolsteringWeapon(true);
	}
}

void UPS_CarryingState_Unarmed::Tick_Implementation(float DeltaTime)
{
	// Update look at and enable or disable it based on movement.
	UpdateHeadLookAt();
}

void UPS_CarryingState_Unarmed::OnExit_Implementation()
{
	// Perform here any necessary logic when leaving unarmed mode.
}

/* Head Look At*/

void UPS_CarryingState_Unarmed::UpdateHeadLookAt()
{
	if (!LocController)
		return;

	bool EnableHeadLookAt = LocController->IsHeadIKEnabled();

	if (EnableHeadLookAt && !CanDoHeadLookAt())
	{
		LocController->SetHeadIKDisabled();
		return;
	}

	if (!EnableHeadLookAt && CanDoHeadLookAt())
	{
		LocController->SetHeadIKLookAtControllerOrientation();
		return;
	}
}

bool UPS_CarryingState_Unarmed::CanDoHeadLookAt()
{
	return LocController && (LocController->GetSpeed() < MaxSpeedToDisableHeadLookAt);
}