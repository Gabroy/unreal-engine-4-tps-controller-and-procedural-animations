#include "PS_CarryingState_TwoHandedRifle.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "GameFramework/Controller.h"
#include "Kismet/KismetMathLibrary.h"

void UPS_CarryingState_TwoHandedRifle::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_TwoHandedRifle::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	CharacterController = Cast<AController>(CharacterOwner->GetController());
	if (!CharacterController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_TwoHandedRifle::Init_Implementation CharacterController Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_TwoHandedRifle::Init_Implementation LocController Returned Null"));
		return;
	}

	CarryingController = Cast<UPS_Human_CarryingController>(Actor->GetComponentByClass(UPS_Human_CarryingController::StaticClass()));
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_CarryingState_TwoHandedRifle::Init_Implementation CarryingController Returned Null"));
		return;
	}

	TickByDefault = true;
}

void UPS_CarryingState_TwoHandedRifle::OnEnter_Implementation()
{
	FRotator ControllerRotation = CharacterController->GetControlRotation();
	FRotator ActorRotation = CharacterOwner->GetActorRotation();
	FRotator DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(ControllerRotation, ActorRotation);
	if (CarryingController)
	{
		CarryingController->SetAimOffset(DeltaRotation.Yaw, DeltaRotation.Pitch);
	}

	if (LocController)
	{
		LocController->SetHeadIKDisabled();
	}
}

void UPS_CarryingState_TwoHandedRifle::Tick_Implementation(float DeltaTime)
{
	FRotator ControllerRotation = CharacterController->GetControlRotation();
	FRotator ActorRotation = CharacterOwner->GetActorRotation();
	FRotator DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(ControllerRotation, ActorRotation);
	if (CarryingController)
	{
		CarryingController->UpdateAimOffsetVariables(DeltaRotation.Yaw, DeltaRotation.Pitch, DeltaTime);
	}
}

void UPS_CarryingState_TwoHandedRifle::OnExit_Implementation()
{
	// Perform here any necessary logic when leaving carrying mode.
	if (LocController)
	{
		LocController->SetUpperBodyIKDisabled();
	}
}