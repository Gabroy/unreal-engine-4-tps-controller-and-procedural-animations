#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_State.h"
#include "PS_CarryingState_TwoHandedRifle.generated.h"

class APS_Character;
class AController;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_CarryingState_TwoHandedRifle : public UFSM_State
{
protected:

	GENERATED_BODY()

	/* Components */

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;
	UPROPERTY()
	AController * CharacterController = nullptr;
	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;
	UPROPERTY()
	UPS_Human_CarryingController * CarryingController = nullptr;

public:

	/* Methods. */

	virtual void Init_Implementation();
	
	virtual void OnEnter_Implementation();

	virtual void Tick_Implementation(float DeltaTime);

	virtual void OnExit_Implementation();
};
