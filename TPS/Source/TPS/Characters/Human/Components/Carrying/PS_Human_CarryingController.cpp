#include "PS_Human_CarryingController.h"
#include "GameFramework/Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Animations/PS_Human_MontageController.h"
#include "Characters/Human/Components/Inventory/PS_Human_InventoryComponent.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKController.h"
#include "Kismet/KismetMathLibrary.h"

void UPS_Human_CarryingController::Init()
{
	/* Fetch components */
	FetchComponents();

	/* Add Prerequisite components */
	AddPrerequisiteComponents();

	/* Start the FSM. */
	Start();
}

void UPS_Human_CarryingController::FetchComponents()
{
	Owner = Cast<ACharacter>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_CarryingController::Init Owner Returned Null"));
		return;
	}

	OwnerController = Owner->GetController();
	if (!OwnerController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_CarryingController::Init OwnerController Returned Null"));
		return;
	}

	LocomotionController = Owner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_CarryingController::Init LocomotionController Returned Null"));
		return;
	}
	LocomotionController->OnChangedStateMulticast.AddUObject(this, &UPS_Human_CarryingController::OnLocomotionStateChanged);

	UpperTorsoIKComponent = Owner->FindComponentByClass<UPS_Human_UpperTorsoIKController>();
	if (!UpperTorsoIKComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_CarryingController::BeginPlay UpperTorsoIKComponent Returned Null"));
		return;
	}

	InventoryComponent = Owner->FindComponentByClass<UPS_Human_InventoryComponent>();
	if (!InventoryComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_CarryingController::BeginPlay InventoryComponent Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Owner->FindComponentByClass<USkeletalMeshComponent>();
	if (SKMesh)
	{
		AnimationInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimationInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_Human_CarryingController::BeginPlay AnimationInstance Returned Null"));
			return;
		}
	}
}

void UPS_Human_CarryingController::AddPrerequisiteComponents()
{
	// Empty for now.
}

void UPS_Human_CarryingController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	UFSM_Component::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

/* Pick items. */

void UPS_Human_CarryingController::StartPickItem(APS_BaseItemActorInstance * ItemToPick)
{
	if (!ItemToPick || !InventoryComponent)
	{
		return;
	}

	InventoryComponent->StartGrabItem(ItemToPick);
}

void UPS_Human_CarryingController::CancelPickItem()
{
	if (InventoryComponent)
	{
		InventoryComponent->CancelGrabItem();
	}
}

bool UPS_Human_CarryingController::CanPickItem(APS_BaseItemActorInstance * ItemToPick)
{
	return InventoryComponent && InventoryComponent->CanGrabItem(ItemToPick);
}

/* Drop item */

void UPS_Human_CarryingController::DropCarryingItem()
{
	if (CanDropCarryingItem())
	{
		InventoryComponent->DropCarryingItem();
	}
}

bool UPS_Human_CarryingController::CanDropCarryingItem()
{
	return InventoryComponent && InventoryComponent->CanDropCarryingItem();
}

/* Change Item functions */

void UPS_Human_CarryingController::ChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave)
{
	if (InventoryComponent)
	{
		InventoryComponent->ChangeOrSaveItem(ItemToChangeOrSave);
	}
}

bool UPS_Human_CarryingController::CanChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave)
{
	return InventoryComponent && InventoryComponent->CanChangeOrSaveItem(ItemToChangeOrSave);
}

void UPS_Human_CarryingController::CancelChangeOrSaveItem()
{
	if (InventoryComponent)
	{
		InventoryComponent->CancelChangeOrSaveItem();
	}
}

/* Gun functions */

void UPS_Human_CarryingController::SetIsHolsteringWeapon(bool ShouldHolster)
{
	if (!LocomotionController)
		return;

	// Disable IKs for a second or so while we holster weapon.
	UpperTorsoIKComponent->SetUpperTorsoIKDisabled(0.5f);

	LocomotionController->ChangeTargetRotationMode(ShouldHolster ? ELocomotionRotationModes::VelocityFacing : ELocomotionRotationModes::CameraFacing);
}

bool UPS_Human_CarryingController::GetIsHolsteringWeapon() const
{
	return LocomotionController ? (LocomotionController->GetTargetRotationMode() == ELocomotionRotationModes::VelocityFacing) : false;
}

void UPS_Human_CarryingController::SetAiming(bool ShouldAim)
{
	if (!LocomotionController || !AnimationInstance)
		return;

	if (AnimationInstance->GetIsAiming() == ShouldAim)
		return;

	AnimationInstance->SetIsAiming(ShouldAim);
	LocomotionController->SetAiming(ShouldAim);
}

bool UPS_Human_CarryingController::GetIsAiming() const
{
	return AnimationInstance && AnimationInstance->GetIsAiming();
}

bool UPS_Human_CarryingController::CanAim() const
{
	return !LocomotionController->IsSprinting() && !GetIsAiming();
}

void UPS_Human_CarryingController::SetAimOffset(float TargetYaw, float TargetPitch)
{
	if (!AnimationInstance)
		return;

	AnimationInstance->SetAimOffset(TargetYaw, TargetPitch);
}

void UPS_Human_CarryingController::UpdateAimOffsetVariables(float TargetYaw, float TargetPitch, float DeltaTime)
{
	if (!AnimationInstance)
		return;

	float YawInterpSpeedMultiplier = UKismetMathLibrary::MapRangeClamped(FMath::Abs(TargetYaw),
		AimOffsetYawStartEndSpeedReduction.X, AimOffsetYawStartEndSpeedReduction.Y, 1.0f, MaxYawInterpSpeedReduction);
	float PitchInterpSpeedMultiplier = UKismetMathLibrary::MapRangeClamped(FMath::Abs(TargetYaw),
		AimOffsetPitchStartEndSpeedReduction.X, AimOffsetPitchStartEndSpeedReduction.Y, 1.0f, MaxPitchInterpSpeedReduction);
	AnimationInstance->UpdateAimOffsetVariables(TargetYaw, TargetPitch, DeltaTime, AimOffsetInterpolationSpeed * YawInterpSpeedMultiplier, AimOffsetInterpolationSpeed * PitchInterpSpeedMultiplier);
}

void UPS_Human_CarryingController::SetUpperBodyIKEnabledBasedOnCarryingStance()
{
	if (UpperTorsoIKComponent)
	{
		switch (CurrentStance)
		{
		case ECarryingStance::Unarmed:
			UpperTorsoIKComponent->SetUpperTorsoIKEnabled(EUpperTorsoIKModes::UB_IK_GROUNDED_UNARMED);
			break;
		case ECarryingStance::OneHandedGun:
			UpperTorsoIKComponent->SetUpperTorsoIKEnabled(EUpperTorsoIKModes::UB_IK_GROUNDED_GUN);
			break;
		case ECarryingStance::TwoHandedGun:
			UpperTorsoIKComponent->SetUpperTorsoIKEnabled(EUpperTorsoIKModes::UB_IK_GROUNDED_GUN);
			break;
		default:
			break;
		}

	}
}

APS_BaseItemActorInstance * UPS_Human_CarryingController::GetCarryingItem() const
{
	return InventoryComponent ? InventoryComponent->GetCurrentEquippedItem() : nullptr;
}

/* General. */

void UPS_Human_CarryingController::ChangeCarryingStance(ECarryingStance NewStance)
{
	if (CurrentStance == NewStance)
		return;

	CurrentStance = NewStance;

	switch (CurrentStance)
	{
	case ECarryingStance::Unarmed:
		ChangeToStateByName("Unarmed");
		break;
	case ECarryingStance::OneHandedGun:
		ChangeToStateByName("Gun");
		break;
	case ECarryingStance::TwoHandedGun:
		ChangeToStateByName("Gun");
		break;
	default:
		break;
	}

	if (OnCarryingStanceChanged.IsBound())
		OnCarryingStanceChanged.Broadcast(CurrentStance);
}

/* Callbacks. */

void UPS_Human_CarryingController::OnLocomotionStateChanged(FName LocomotionNewStateName, int LocomotionStateID)
{
	ELocomotionMode NewLocomotionMode = (ELocomotionMode)LocomotionStateID;
	switch (NewLocomotionMode)
	{
	case ELocomotionMode::Grounded:
		OnLocomotionChangedToGroundedMode();
		break;

	case ELocomotionMode::Climbing:
		OnLocomotionChangedToClimbingMode();
		break;

	case ELocomotionMode::In_Air:
		OnLocomotionChangedToInAirMode();
		break;

	case ELocomotionMode::Ragdoll:
		OnLocomotionChangedToRagdollMode();
		break;
	}
}

void UPS_Human_CarryingController::OnLocomotionChangedToGroundedMode()
{

}

void UPS_Human_CarryingController::OnLocomotionChangedToClimbingMode()
{
	// Maybe here we would stop aiming or something like that depending on the current state.
}

void UPS_Human_CarryingController::OnLocomotionChangedToInAirMode()
{
	// Maybe here we would stop aiming or something like that depending on the current state.
}

void UPS_Human_CarryingController::OnLocomotionChangedToRagdollMode()
{
	if (CurrentStance == ECarryingStance::Unarmed)
		return;

	// Throw current item to the ground if we have activated ragdolls.
	DropCarryingItem();
}