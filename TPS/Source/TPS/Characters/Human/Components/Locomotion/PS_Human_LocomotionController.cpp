#include "PS_Human_LocomotionController.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKController.h"
#include "Characters/Human/Components/IK/PS_Human_LegIKController.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "States/PS_LocState_Interface.h"
#include "States/PS_LocState_Grounded.h"
#include "States/PS_LocState_Climb.h"
#include "States/PS_LocState_Ragdoll.h"

void UPS_Human_LocomotionController::Init()
{
	/* Fetch components */

	FetchComponents();

	/* Add Prerequisite components */

	AddPrerequisiteComponents();

	/* Init locomotion structures and callbacks */

	// Init moving direction updater that controls computing current moving direction.
	CardinalDirectionUpdater.InitDirections();

	// Target rotation mode.
	ChangeTargetRotationMode(TargetRotationMode);

	// Start the FSM.
	Start();

	/* State References */
	FetchStatesReferences();

	// Launch BegunPlay Callback so those components that were initialized before the locomotion controller but need its information,
	// can fetch them.
	BegunPlayCallback.Broadcast();
	BegunPlayCallback.Clear();
}

void UPS_Human_LocomotionController::FetchComponents()
{
	Owner = Cast<ACharacter>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_LocomotionController::Init Owner Returned Null"));
		return;
	}

	OwnerController = Owner->GetController();
	if (!OwnerController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_LocomotionController::Init OwnerController Returned Null"));
		return;
	}

	MovementComp = Owner->FindComponentByClass<UCharacterMovementComponent>();
	if (!MovementComp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_LocomotionController::Init MovementComp Returned Null"));
		return;
	}

	HeadIKComponent = Owner->FindComponentByClass<UPS_Human_HeadIKController>();
	if (!HeadIKComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_LocomotionController::Init UPS_Human_HeadIKController Returned Null"));
		return;
	}

	UpperTorsoIKComponent = Owner->FindComponentByClass<UPS_Human_UpperTorsoIKController>();
	if (!UpperTorsoIKComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_LocomotionController::Init UpperTorsoIKComponent Returned Null"));
		return;
	}

	LegIKComponent = Owner->FindComponentByClass<UPS_Human_LegIKController>();
	if (!LegIKComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_LocomotionController::Init LegIKComponent Returned Null"));
		return;
	}
}

void UPS_Human_LocomotionController::FetchStatesReferences()
{
	ClimbingState = (UPS_LocState_Climb *)GetStateByID((int)ELocomotionMode::Climbing);
}

void UPS_Human_LocomotionController::AddPrerequisiteComponents()
{
	// USkeletalMeshComponent should be updated after this component has computed new positions, not before.
	USkeletalMeshComponent* SKMesh = Owner->FindComponentByClass<USkeletalMeshComponent>();
	if (SKMesh)
		SKMesh->AddTickPrerequisiteComponent(this);
}

void UPS_Human_LocomotionController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	// General variables, always relevant.
	UpdateGeneralVariables(DeltaTime);

	// Here we update the current locomotion state.
	UFSM_Component::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Update smoother velocity rotation.

	if (IsMoving && !WasMoving)
		SmoothedVelocityRotation = VelocityRotation;
	else
		SmoothedVelocityRotation = UKismetMathLibrary::RInterpTo_Constant(SmoothedVelocityRotation, VelocityRotation, DeltaTime, LastFrameRotationSpeed);
}

/* States */

void UPS_Human_LocomotionController::ChangeToGroundedState()
{
	MovementComp->SetMovementMode(MOVE_Walking);
}

void UPS_Human_LocomotionController::ChangeToFallingState()
{
	MovementComp->SetMovementMode(MOVE_Falling);
}

void UPS_Human_LocomotionController::ChangeToClimbingState(const FVector & PositionToHangTo, const FVector & ObstacleNormal, float DeltaAngleWithSurface)
{
	if (ClimbingState)
	{
		ClimbingState->MoveToHangingPosition(PositionToHangTo, ObstacleNormal, DeltaAngleWithSurface);
		ChangeToStateByName("Climbing");
	}
}

void UPS_Human_LocomotionController::SetRagdollStatus(bool EnableRagdolls)
{
	if (EnableRagdolls)
	{
		if (GetCurrentStateID() != ELocomotionMode::Ragdoll)
			ChangeToStateByID(ELocomotionMode::Ragdoll);
	}
	else
	{
		if (GetCurrentStateID() == ELocomotionMode::Ragdoll)
		{
			UPS_LocState_Ragdoll * RagdollState = (UPS_LocState_Ragdoll*)GetCurrentState();
			if (!RagdollState)
				return;

			// If we are touching ground, we change to grounded state, otherwise to falling.
			if (RagdollState->IsRagdollGrounded())
				ChangeToGroundedState();
			else
				ChangeToFallingState();
		}
	}
}

/* Target rotation functions */

void UPS_Human_LocomotionController::UpdateGeneralVariables(float DeltaTime)
{
	if (!Owner) return;

	// Fetch values used in computations.
	FVector Velocity = Owner->GetVelocity();

	// Set speed values.
	Speed = FVector(Velocity.X, Velocity.Y, 0.0f).Size();
	VerticalSpeed = Velocity.Z;
	WasMoving = IsMoving;
	IsMoving = Speed > 1.0f;

	if (!IsMoving)
		LastFrameRotationSpeed = 0.0f;

	// Set velocity rotation.
	VelocityRotation = Velocity.ToOrientationRotator();

	// Set movement input direction.
	MovementInput = MovementComp->GetLastInputVector();
	HasMovementInput = !MovementInput.Equals(FVector(0.0f), 0.0001f);

	// Update Delta Between Velocity And Desired Direction.
	// (Direction we are moving vs direction we strive to move towards represented by the velocity and movement input vectors).
	if (HasMovementInput) {
		MovementInputRotation = MovementInput.ToOrientationRotator();
		DeltaBetweenVelocityAndDesiredDirection = UKismetMathLibrary::NormalizedDeltaRotator(VelocityRotation, MovementInputRotation).Yaw;
	}

	// Set acceleration vector.
	AccelerationVector = (Velocity - LastUpdateVelocity) / DeltaTime;
	AccelerationValue = (Speed - FVector(LastUpdateVelocity.X, LastUpdateVelocity.Y, 0.0f).Size())/DeltaTime;

	LastUpdateVelocity = Velocity;

	// Update ground friction variables while on ground.
	if(MovementComp->IsMovingOnGround())
		ComputeGroundFriction();
}

void UPS_Human_LocomotionController::UpdateTargetRotationBasedOnRotationMode()
{
	(this->*TargetRotationFunctionPtr)();
}

void UPS_Human_LocomotionController::SetTargetRotation(float NewRotation)
{
	CharacterTargetRotation = FRotator(0.0f, NewRotation, 0.0f);
}

void UPS_Human_LocomotionController::UpdateCharacterMeshRotation(float DeltaTime, float RotationSpeed)
{
	LastFrameRotationSpeed = RotationSpeed;

	// Set the actor rotation.
	FRotator CurrentRotation = Owner->GetActorRotation();
	CurrentRotation = UKismetMathLibrary::RInterpTo_Constant(CurrentRotation, CharacterTargetRotation, DeltaTime, RotationSpeed);
	Owner->SetActorRotation(CurrentRotation);

	// Angle between the desired direction of the character and the direction the character
	// faces. Used by pivoting for CameraFacing movement as it allows to know where we are headed towards.
	DeltaBetweenDesiredDirAndFacingDir = UKismetMathLibrary::NormalizedDeltaRotator(MovementInputRotation, CurrentRotation).Yaw;
	DesiredCardinalDirection = CardinalDirectionUpdater.GetDirectionFromAngle(DeltaBetweenDesiredDirAndFacingDir);

	// Finally, compute the delta between the velocity vector and the facing direction of the character.
	// This delta can be used to blend animations that depend of the delta between the velocity and the current looking dir.
	DeltaBetweenVelocityAndFacingDir = UKismetMathLibrary::NormalizedDeltaRotator(SmoothedVelocityRotation, CurrentRotation).Yaw;

	if (!IsMoving)
		DeltaBetweenVelocityAndFacingDir = 0.0f;
}

void UPS_Human_LocomotionController::UpdateTargetRotationToVelocity()
{
	CharacterTargetRotation = FRotator(0.0f, VelocityRotation.Yaw, 0.0f);
}

void UPS_Human_LocomotionController::UpdateTargetRotationToControllerDirection()
{
	if (!OwnerController) return;
	CharacterTargetRotation = FRotator(0.0f, OwnerController->GetControlRotation().Yaw, 0.0f);
}

void UPS_Human_LocomotionController::ChangeTargetRotationMode(ELocomotionRotationModes NewMode)
{
	ELocomotionRotationModes OldTargetRotation = TargetRotationMode;
	TargetRotationMode = NewMode;

	if (TargetRotationMode == ELocomotionRotationModes::VelocityFacing)
	{
		TargetRotationFunctionPtr = &UPS_Human_LocomotionController::UpdateTargetRotationToVelocity;
	}
	else
	{
		TargetRotationFunctionPtr = &UPS_Human_LocomotionController::UpdateTargetRotationToControllerDirection;
		CardinalDirectionUpdater.UpdateMovementDirection(DeltaBetweenVelocityAndFacingDir);
	}

	IPS_LocState_Interface* LocStateInterface = Cast<IPS_LocState_Interface>(CurrentState);
	if (LocStateInterface)
		LocStateInterface->OnChangedRotationMode(OldTargetRotation, NewMode);
}

bool UPS_Human_LocomotionController::CanChangeLocomotionStance(TEnumAsByte<ELocomotionStance> NewStance)
{
	IPS_LocState_Interface* LocStateInterface = Cast<IPS_LocState_Interface>(CurrentState);
	if (LocStateInterface)
		return LocStateInterface->CanChangeStance(CurrentStance, NewStance);
	return false;
}

void UPS_Human_LocomotionController::ChangeLocomotionStance(TEnumAsByte<ELocomotionStance> NewStance)
{
	// We always update locomotion value first, just to get sure in case states actually queries this value
	// and not the one received from the callback.
	TEnumAsByte<ELocomotionStance> OldStance = CurrentStance;
	CurrentStance = NewStance;

	IPS_LocState_Interface * LocStateInterface = Cast<IPS_LocState_Interface>(CurrentState);
	if (LocStateInterface)
		LocStateInterface->OnChangedStance(OldStance, CurrentStance);

	if(OnChangedStanceMulticast.IsBound())
		OnChangedStanceMulticast.Broadcast(CurrentStance);
}

void UPS_Human_LocomotionController::SetGroundFrictionFactor(float NewGroundFriction)
{
	GroundFrictionFactorInSurface = NewGroundFriction;
}

void UPS_Human_LocomotionController::ComputeGroundFriction()
{
	// Update movement component ground friction and braking deceleration based on the current speed of the character.
	// The faster it moves, the more ground friction the character will have, making it harder to rotate with it.
	// Same thing with braking deceleration which will decrease, making the character lose speed slower if we stop applying any force.
	MovementComp->GroundFriction = GroundFrictionForSpeedCurve->GetFloatValue(Speed) * GroundFrictionFactorInSurface;
	MovementComp->BrakingDecelerationWalking = BrakeFrictionForSpeedCurve->GetFloatValue(Speed);
}

/* Locomotion actions functions. */

void UPS_Human_LocomotionController::DoToggleStandingFoot()
{
	IPS_LocState_Interface* LocStateInterface = Cast<IPS_LocState_Interface>(CurrentState);
	if (LocStateInterface)
		LocStateInterface->DoToggleStandingFoot();
}

void UPS_Human_LocomotionController::SetWalk(bool EnableWalk)
{
	bIsWalking = EnableWalk;
	IPS_LocState_Interface* LocStateInterface = Cast<IPS_LocState_Interface>(CurrentState);
	if (LocStateInterface)
		LocStateInterface->SetWalk(EnableWalk);
}

bool UPS_Human_LocomotionController::GetIsWalking() const
{
	return bIsWalking;
}

void UPS_Human_LocomotionController::SetHeadIKLookAtPosition(const FVector& NewPositionToLookAt)
{
	if (HeadIKComponent)
		HeadIKComponent->LookAtPosition(NewPositionToLookAt);
}

void UPS_Human_LocomotionController::SetHeadIKLookAtComponent(const USceneComponent* NewSceneCompToLookAt)
{
	if (HeadIKComponent)
		HeadIKComponent->LookAtComponent(NewSceneCompToLookAt);
}

void UPS_Human_LocomotionController::SetHeadIKLookAtControllerOrientation()
{
	if (HeadIKComponent)
		HeadIKComponent->LookAtControllerDirection();
}

void UPS_Human_LocomotionController::SetHeadIKLookAtRandomDirection()
{
	if (HeadIKComponent)
		HeadIKComponent->LookAtRandomDirection();
}

void UPS_Human_LocomotionController::SetHeadIKDisabled()
{
	if(HeadIKComponent)
		HeadIKComponent->SetHeadIKDisabled();
}

bool UPS_Human_LocomotionController::IsHeadIKEnabled()
{
	if(HeadIKComponent)
		return HeadIKComponent->GetEnabledHeadIK();
	return false;
}

void UPS_Human_LocomotionController::SetUpperTorsoIKToLastUsedMode()
{
	if (UpperTorsoIKComponent)
		UpperTorsoIKComponent->SetUpperTorsoIKToLastUsedMode();
}

void UPS_Human_LocomotionController::SetUpperBodyIKEnabled(EUpperTorsoIKModes NewModeToUse)
{
	if (UpperTorsoIKComponent)
		UpperTorsoIKComponent->SetUpperTorsoIKEnabled(NewModeToUse);
}

void UPS_Human_LocomotionController::SetUpperBodyIKDisabled()
{
	if (UpperTorsoIKComponent)
		UpperTorsoIKComponent->SetUpperTorsoIKDisabled();
}

void UPS_Human_LocomotionController::SetLegIKEnabledStatus(bool EnableLegIK)
{
	if(LegIKComponent)
		LegIKComponent->SetLegIKEnabledStatus(EnableLegIK);
}

bool UPS_Human_LocomotionController::IsSprinting() const
{
	IPS_LocState_Interface * LocStateInterface = Cast<IPS_LocState_Interface>(CurrentState);
	if (LocStateInterface)
		return LocStateInterface->IsSprinting();
	return false;
}

void UPS_Human_LocomotionController::SetWantsToSprint(bool EnableSprint)
{
	WantsToSprint = EnableSprint;
}

void UPS_Human_LocomotionController::SetRootMotionMovement(bool Enable)
{
	IsInRootMotionMovement = Enable;
}

bool UPS_Human_LocomotionController::IsPlayingRootMotion() const
{
	return (Owner && Owner->IsPlayingRootMotion()) || IsInRootMotionMovement;
}

/* Aiming */

void UPS_Human_LocomotionController::SetAiming(bool IsAiming)
{
	SetWalk(IsAiming);

	if (IsAiming)
		ChangeTargetRotationMode(ELocomotionRotationModes::CameraFacing);
}

/* State control functions */

void UPS_Human_LocomotionController::AllowStateChangeOnMovementChanged(bool AllowStateChange)
{
	AllowChangeStateOnMovementChanged = AllowStateChange;
}

void UPS_Human_LocomotionController::OnMovementComponentStateChanged(EMovementMode PrevMovementMode, EMovementMode NewMovementMode)
{
	if (!AllowChangeStateOnMovementChanged || PrevMovementMode == NewMovementMode) return;

	switch (NewMovementMode)
	{
	case MOVE_NavWalking:
	case MOVE_Walking:
		ChangeToStateByName("Grounded");
		break;

	case MOVE_Falling:
		ChangeToStateByName("InAir");
		break;
	}
}

/* Climbing API */

bool UPS_Human_LocomotionController::TryLedgeGrab()
{
	if(ClimbingState)
		return ClimbingState->TryLedgeGrab();
	return false;
}

void UPS_Human_LocomotionController::Hanging_TryMove(float Axis)
{
	if (ClimbingState)
		ClimbingState->TryMove(Axis);
}

void UPS_Human_LocomotionController::Hanging_TryClimb(float Axis)
{
	if (ClimbingState)
		ClimbingState->TryClimb(Axis);
}

void UPS_Human_LocomotionController::Hanging_PerformJumpingFromHanged()
{
	if (ClimbingState)
		ClimbingState->PerformJumpingFromHanged();
}

void UPS_Human_LocomotionController::Hanging_PerformVerticalJump()
{
	if (ClimbingState)
		ClimbingState->PerformVerticalJump();
}

void UPS_Human_LocomotionController::Hanging_PerformHorizontalJump(bool Right)
{
	if (ClimbingState)
		ClimbingState->PerformHorizontalJump(Right);
}

void UPS_Human_LocomotionController::Hanging_ReleaseHang()
{
	if (ClimbingState)
		ClimbingState->LeaveHanging(GetLocomotionStance());
}

void UPS_Human_LocomotionController::Hanging_Crouch()
{
	if (ClimbingState)
		ClimbingState->Crouch();
}

void UPS_Human_LocomotionController::Hanging_Stand()
{
	if (ClimbingState)
		ClimbingState->Stand();
}

void UPS_Human_LocomotionController::Hanging_Walk()
{
	if (ClimbingState)
		ClimbingState->Walk();
}

void UPS_Human_LocomotionController::Hanging_StopWalk()
{
	if (ClimbingState)
		ClimbingState->StopWalk();
}