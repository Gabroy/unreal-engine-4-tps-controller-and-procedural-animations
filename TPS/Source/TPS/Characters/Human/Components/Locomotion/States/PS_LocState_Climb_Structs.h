#pragma once

#include "CoreMinimal.h"
#include "EnumAsByte.h"
#include "PS_LocState_Climb_Structs.generated.h"

/*	Climbing stances.
	Three stances are supported: standing, crouched and free hanging (no surface to place feet on)). */
UENUM(BlueprintType)
enum EClimbingStance
{
	StandingHang,
	FreeHanging,
	Crouched
};

/* Climbing states */
UENUM(BlueprintType)
enum EClimbingStates
{
	MovingToHang,		// While moving to grab to a ledge (called when entering the state).
	Hanging,			// While hanging from a ledge and moving on it.
	ClimbingMontage,	// Climbing an obstacle we are hanged to.
	TryClimbingMontage, // Trying to climb an obstacle we are hanged to but we can't truly climb.
	OnMontage,			// Playing a montage, normally used for turning or jumping montages.
	Undefined			// Used when we are in undefined state.
};

/*	Climbing movement properties.
	Control movement speed and and walking speed modifier for a climbing stance. */
USTRUCT(Blueprintable, BlueprintType)
struct FClimbingMovementProperties
{
	GENERATED_BODY()
		
	UPROPERTY(EditAnywhere)
	float MovementSpeed = 1.0f;

	UPROPERTY(EditAnywhere)
	float WalkingModifier = 0.50f;
};

/* Hanging To Ledge Animations. Used when grabbing to a ledge. */
USTRUCT(Blueprintable, BlueprintType)
struct FHangToLedgeAnimations
{
	GENERATED_BODY()

	// Hanging to ledge animation. This should be an animation sequence that should play from the moment we detect an edge we move to grab to
	// the moment we get to the hanging position. The playrate of the animation is controlled by how far from the place we are hanging to we are,
	// so, the faster we get to the position, the faster it will play.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence * HangingAnimation;

	// This animation, if not null, will be played after hanging animation is done. This animation sequence can be used to add flavour to hanging animations.
	// For example, if we have an animation where the char hangs to an object and then trembles or loses a bit the hanging, all this part of the animation
	// sequence that comes after reaching the hanging location, should be played here as a different animation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence * PostHangingAnimation;

	UPROPERTY(BlueprintReadOnly)
	bool HasPostHangingAnimation = false;

	// The stance this animation is intended for. Standing and crouched animations can be played independently of the final stance we will be at, but free hanging
	// won't be played if we can place feet on surface and viceversa.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EClimbingStance> HangingStance;
};


/* Jumping from Hang Animation */
USTRUCT(Blueprintable, BlueprintType)
struct FJumpingFromHangAnimation
{
	GENERATED_BODY()
		
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * JumpingFromHangMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FHangToLedgeAnimations EnterAnimationAfterJumpingFromHang;
};

/* Jumping from Hang Animation */
USTRUCT(Blueprintable, BlueprintType)
struct FJumpingFromHangInwardsAnimation
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* JumpingFromHangMontage;

	// Time of the animation when movement starts.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeToStartJumpingMovement = 0.0f;

	// Time of the animation it will take to reach the destination.
	// Should go from TimeToStartJumpingMovement to time it reaches the position.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeToReachDestination = 0.0f;
};