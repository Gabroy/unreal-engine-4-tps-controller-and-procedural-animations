#include "PS_LocState_Climb.h"
#include "GameFramework/Character.h"
#include "../../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../PS_Human_LocomotionController.h"
#include "../../Animations/PS_Human_AnimationInstance.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "../../IK/PS_Human_UpperTorsoIKController.h"

void UPS_LocState_Climb::Init_Implementation()
{
	/* Fetch components */

	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation Actor Returned Null"));
		return;
	}

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation LocController Returned Null"));
		return;
	}

	MovComponent = Cast<UCharacterMovementComponent>(Actor->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!MovComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation MovComponent Returned Null"));
		return;
	}

	CapsuleComponent = Cast<UCapsuleComponent>(Actor->GetComponentByClass(UCapsuleComponent::StaticClass()));
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation CapsuleComponent Returned Null"));
		return;
	}

	SkeletalMeshComp = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SkeletalMeshComp)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SkeletalMeshComp->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation AnimInstance Returned Null"));
			return;
		}

		AnimInstance->OnTryClimbingStartBlendOutCallback.BindUObject(this, &UPS_LocState_Climb::OnTryClimbingStartBlendOut);
		AnimInstance->OnTryClimbingStopCallback.BindUObject(this, &UPS_LocState_Climb::OnTryClimbingStop);
		AnimInstance->OnHalfClimbingTurningPerformed.BindUObject(this, &UPS_LocState_Climb::OnHalfTurnPerformed);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Climb::Init_Implementation SkeletalMeshComp Returned Null"));
		return;
	}

	// Fetch climbing data

	ACharacter * DefaultCharacter = CharacterOwner->GetClass()->GetDefaultObject<ACharacter>();
	if (DefaultCharacter)
	{
		CharacterHalfHeight = DefaultCharacter->GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight();
		HeightDiffBetweenCharHeightAndHeightToPerformCheck = CharacterHalfHeight * 2.0f - HeightToPerformLedgeCheckAt;
	}

	OriginalCapsuleRadius = CapsuleComponent->GetUnscaledCapsuleRadius();
	ClimbingHangInterpolator.Init(CapsuleComponent, SkeletalMeshComp, RootMotionXCoord, RootMotionYCoord, RootMotionZCoord);
	ClimbingTraceQuery = UEngineTypes::ConvertToTraceType(ClimbingChannel);

	LateralCheck_ForwardOffset += CapsuleComponent->GetScaledCapsuleRadius();
	LateralCheck_SideOffset += CapsuleComponent->GetScaledCapsuleRadius();
}

void UPS_LocState_Climb::OnEnter_Implementation()
{
	// Disable leg IKs in climbing mode.
	LocController->SetLegIKEnabledStatus(false);

	// Set movement mode to flying.
	MovComponent->SetMovementMode(MOVE_Flying);

	ChangeStance(LocController->GetLocomotionStance());
}

void UPS_LocState_Climb::OnExit_Implementation()
{
	// Disable hands IK when leaving climbing mode. Certain animations may already have disabled it before but
	// we do it here just to get sure it gets disabled once leaving climbing mode.
	LocController->SetUpperBodyIKDisabled();

	// If leaving state, get sure we return the capsule of the player to the original size.
	CapsuleComponent->SetCapsuleRadius(OriginalCapsuleRadius);
}

void UPS_LocState_Climb::Tick_Implementation(float DeltaTime)
{
	if (!LocController) return;

	// Call the specific stance function we are using.
	if (CurrentSubStateBehaviour)
		(this->*CurrentSubStateBehaviour)(DeltaTime);
}

/* Movement actions for climbing */

void UPS_LocState_Climb::Crouch()
{
	if (CurrentClimbingState == EClimbingStates::Hanging)
		CharacterOwner->DoCrouch();
}

void UPS_LocState_Climb::Stand()
{
	CharacterOwner->DoUncrouch();
}

void UPS_LocState_Climb::Walk()
{
	if (CurrentClimbingState == EClimbingStates::Hanging)
		CharacterOwner->DoWalk();
}

void UPS_LocState_Climb::StopWalk()
{
	if (CurrentClimbingState == EClimbingStates::Hanging)
		CharacterOwner->DoStopWalk();
}

void UPS_LocState_Climb::SetWalk(bool EnableWalk)
{
	CurrentSpeedModifier = EnableWalk ? CurrentMovementProps->WalkingModifier : 1.0f;
}

/* Stance functions */

void UPS_LocState_Climb::SetNewClimbState(EClimbingStates NewClimbState)
{
	EClimbingStates OldState = CurrentClimbingState;
	CurrentClimbingState = NewClimbState;

	switch (CurrentClimbingState)
	{
	
	case EClimbingStates::MovingToHang:
		CharacterOwner->SetEnableCharacterMovementAndActions(false);
		CurrentSubStateBehaviour = &UPS_LocState_Climb::UpdateMovingToHangState;
		break;
	
	case EClimbingStates::Hanging:
		
		// Check that hands are on obstacle, if not, fall. Else, activte normal hanging mode.
		bool RightHandHit, LeftHandHit;
		if (OldState != EClimbingStates::MovingToHang && CheckHandsAreOnObstacle(RightHandHit, LeftHandHit) == false)
			LeaveHanging(LocController->GetLocomotionStance(), false);
		else
		{
			CharacterOwner->SetEnableCharacterMovementAndActions(true);
			CurrentHorMovBehaviour = &UPS_LocState_Climb::TryLateralMovement;
			CurrentSubStateBehaviour = nullptr;
		}
		break;
	
	case EClimbingStates::ClimbingMontage:
		CharacterOwner->SetEnableCharacterMovementAndActions(true);
		CurrentSubStateBehaviour = &UPS_LocState_Climb::UpdateClimbing;
		break;
	
	case EClimbingStates::OnMontage:
		CharacterOwner->SetEnableCharacterMovementAndActions(false);
		CurrentSubStateBehaviour = nullptr;
	
	default:
		CurrentSubStateBehaviour = nullptr;
		break;
	}
}

void UPS_LocState_Climb::OnChangedStance(TEnumAsByte<ELocomotionStance> OldStance, TEnumAsByte<ELocomotionStance> NewStance)
{
	ChangeStance(NewStance);
}

bool UPS_LocState_Climb::CanChangeStance(TEnumAsByte<ELocomotionStance> CurrentStance, TEnumAsByte<ELocomotionStance> StanceToChangeTo)
{
	switch (StanceToChangeTo)
	{
	case ELocomotionStance::Normal:
		if (CurrentStance == ELocomotionStance::Crouch)
			return true;
		break;
	case ELocomotionStance::Crouch:
		if (CurrentStance == ELocomotionStance::Normal && CanPlaceFeetOnSurfaceCheck())
			return true;
		break;
	default:
		break;
	}
	return false;
}

void UPS_LocState_Climb::ChangeStance(TEnumAsByte<ELocomotionStance> NewStance)
{
	// If we change stance, we get sure we stop walking if we were doing so.
	SetWalk(false);

	// Based on the stance, 
	switch (NewStance)
	{
	case ELocomotionStance::Crouch:
		CurrentMovementProps = &CrouchedMovement;
		break;

	case  ELocomotionStance::Normal:
		CurrentMovementProps = &StandingMovement;
		break;

	default:
		break;
	}
}

/* Try Ledge Grab */

bool UPS_LocState_Climb::TryLedgeGrab()
{
	// Do forward check first, we must get sure we hit an obstacle we could try to grab.
	FHitResult ObstacleHitInfo;
	bool ForwardObstacleFound = ForwardCheckCanGrabToLedge(MaxDistanceToGrabToLedge, HeightToPerformLedgeCheckAt, ObstacleHitInfo);
	if (!ForwardObstacleFound)
		return false;

	// We have hit an obstacle, check if we can grab it by performing an height check from the position of the check for climbing.
	FVector HeightHitPosition, LedgePos, LedgeNormal;
	bool HeightCheckSuccessful = HeightCheckCanGrabToLedge(MaxHeightLedgeDifferenceCanClimb, ObstacleHitInfo, HeightHitPosition, LedgePos);
	if (!HeightCheckSuccessful)
		return false;

	// Compute the delta angle with the surface we are trying to climb. If the surface has a step too big, we won't hang to it.
	LedgeNormal = ObstacleHitInfo.ImpactNormal;
	float DeltaAngleWithSurface = FMath::Abs(ComputeSurfaceForwardStep(LedgeNormal));
	if (DeltaAngleWithSurface > MaxSurfaceAngleWeCanHangTo)
		return false;

	// True if we should consider this surface step enough to use the ledge position as final position.
	bool IsSurfaceStepEnoughToUseLedgePosition = DeltaAngleWithSurface >= AngleToConsiderStepSurface;
	if (IsSurfaceStepEnoughToUseLedgePosition)
	{
		bool LedgeCheckSuccessful = LedgeObstacleCheck(ObstacleHitInfo, HeightHitPosition, LedgePos, LedgeNormal);
		if (!LedgeCheckSuccessful)
			return false;
	}
	
	LocController->ChangeToClimbingState(LedgePos, LedgeNormal, DeltaAngleWithSurface);
	return true;
}

bool UPS_LocState_Climb::ForwardCheckCanGrabToLedge(float MaxCheckDistance, float HeightToPerformCheckAt, FHitResult & ObstacleHit)
{
	// Perform forward and thickness check to see if we can hang to ledge.
	bool ThicknessCheckFailed;
	bool FoundObstacle = ForwardAndThicknessCheck(MaxCheckDistance, HeightToPerformCheckAt, 0.0f, ObstacleHit, ThicknessCheckFailed);
	if (!FoundObstacle)
		return false;

	// If we hit obstacle on thickness check, we perform two checks in both right and left directions.
	if (!ThicknessCheckFailed)
		return true;

	FVector RightVectorOfObstacleHit = FVector::CrossProduct(ObstacleHit.ImpactNormal, CapsuleComponent->GetUpVector());		
	FHitResult SideObstacleHit;
	TArray<AActor *> ActorsToIgnore;

	// Right side check.
	FVector SideStartCheck = ObstacleHit.ImpactPoint + ObstacleHit.ImpactNormal * (LedgeHandCheckDepthOffset + SphereCheckRadius);
	FVector SideEndCheck = SideStartCheck + RightVectorOfObstacleHit * CapsuleComponent->GetScaledCapsuleRadius();
	bool RightSideCheck = UKismetSystemLibrary::SphereTraceSingle(this, SideStartCheck, SideEndCheck, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, SideObstacleHit, true);
	if (!RightSideCheck)
	{
		float DistanceToPerformNewCheck = CapsuleComponent->GetScaledCapsuleRadius() - SideObstacleHit.Distance;
		bool FoundObstacleSide = ForwardAndThicknessCheck(MaxCheckDistance, HeightToPerformCheckAt, -DistanceToPerformNewCheck, ObstacleHit, ThicknessCheckFailed);
		if (FoundObstacleSide && !ThicknessCheckFailed)
			return true;
	}

	// Left side check.
	SideEndCheck = SideStartCheck + RightVectorOfObstacleHit * -CapsuleComponent->GetScaledCapsuleRadius();
	bool LeftSideCheck = UKismetSystemLibrary::SphereTraceSingle(this, SideStartCheck, SideEndCheck, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, SideObstacleHit, true);
	if (!LeftSideCheck)
	{
		float DistanceToPerformNewCheck = CapsuleComponent->GetScaledCapsuleRadius() - SideObstacleHit.Distance;
		bool FoundObstacleSide = ForwardAndThicknessCheck(MaxCheckDistance, HeightToPerformCheckAt, DistanceToPerformNewCheck, ObstacleHit, ThicknessCheckFailed);
		if (FoundObstacleSide && !ThicknessCheckFailed)
			return true;
	}

	return false;
}

bool UPS_LocState_Climb::ForwardAndThicknessCheck(float MaxCheckDistance, float HeightToPerformCheckAt, float SideOffset, FHitResult & ObstacleHit, bool & ThicknessCheckFailed)
{
	FVector ActorForwardVector = CapsuleComponent->GetForwardVector();
	const float ThicknessExtraOffsetFromPosition = 15.0f;
	ThicknessCheckFailed = true;

	// Forward check in players facing direction.
	FVector ForwardStartTrace = SkeletalMeshComp->GetComponentLocation() + CapsuleComponent->GetUpVector() * HeightToPerformCheckAt	+ CapsuleComponent->GetRightVector() * SideOffset;
	FVector ForwardEndTrace = ForwardStartTrace + ActorForwardVector * MaxCheckDistance;
	TArray<AActor*> ActorsToIgnore;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, ForwardStartTrace, ForwardEndTrace, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, ObstacleHit, true);
	if (!HitObstacle)
		return false;

	// Compute the up vector we will be checking to see if we can hang to the obstacle. We use the normal of the impact for it.
	FVector UpVector = FVector::CrossProduct(CapsuleComponent->GetRightVector(), ObstacleHit.ImpactNormal);

	// Thickness check to see we fit into that position.
	FHitResult ThicknessObstacleHit;
	FVector ThicknessStartCheck = ObstacleHit.ImpactPoint + ObstacleHit.ImpactNormal * (CapsuleComponent->GetScaledCapsuleRadius() + LedgeHandCheckDepthOffset + ThicknessExtraOffsetFromPosition)
		+ UpVector * (-HeightToPerformLedgeCheckAt * 0.85f);
	FVector ThicknessEndCheck = ThicknessStartCheck + UpVector * (CapsuleComponent->GetScaledCapsuleHalfHeight() * 1.5f);
	ThicknessCheckFailed = UKismetSystemLibrary::SphereTraceSingle(this, ThicknessStartCheck, ThicknessEndCheck, CapsuleComponent->GetScaledCapsuleRadius(), ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, ThicknessObstacleHit, true);

	return true;
}

bool UPS_LocState_Climb::HeightCheckCanGrabToLedge(float HeightToPerformCheck, const FHitResult & ForwardHitInformation, FVector & HeightHitPosition, FVector & LedgePos)
{
	// Compute the up vector we will be checking to see if we can hang to the obstacle. We use the normal of the impact for it.
	FVector UpVector = FVector::CrossProduct(CapsuleComponent->GetRightVector(), ForwardHitInformation.ImpactNormal);
	FVector HeightEndTrace = ForwardHitInformation.ImpactPoint + ForwardHitInformation.ImpactNormal * -LedgeHandCheckDepthOffset;
	FVector HeightStartTrace = HeightEndTrace + UpVector * (HeightDiffBetweenCharHeightAndHeightToPerformCheck + HeightToPerformCheck);

	// Perform Height Check.
	TArray<AActor*> ActorsToIgnore;
	FHitResult HeightHitResult;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, HeightStartTrace, HeightEndTrace, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HeightHitResult, true, FLinearColor::Blue);

	// If we haven't touched any object or the check starts inside the object, we simply leave.
	if (!HitObstacle || HeightHitResult.bStartPenetrating)
		return false;

	// Check the height hit result is bigger than the forward hit, at least by a bit.
	const float MinHeightOffset = 5.0f;
	bool HasEnoughHeightDiff = (HeightHitResult.ImpactPoint.Z - ForwardHitInformation.ImpactPoint.Z) > MinHeightOffset;
	if (!HasEnoughHeightDiff)
		return false;

	// Compute the position we will end at.
	HeightHitPosition = HeightHitResult.ImpactPoint;
	LedgePos = ForwardHitInformation.ImpactPoint + ForwardHitInformation.ImpactNormal * CapsuleComponent->GetScaledCapsuleRadius();
	LedgePos.Z = HeightHitPosition.Z - CharacterHalfHeight;

	return true;
}

bool UPS_LocState_Climb::LedgeObstacleCheck(const FHitResult & ForwardHitInformation, const FVector & HeightHitPosition, FVector & LedgePos, FVector & LedgeNormal)
{
	FVector LedgeStartTrace = CapsuleComponent->GetComponentLocation();
	FVector LedgeEndTrace = LedgeStartTrace + CapsuleComponent->GetForwardVector() * MaxDistanceToGrabToLedge;

	TArray<AActor *> ActorsToIgnore;
	FHitResult LedgeHitPosition;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, LedgeStartTrace, LedgeEndTrace, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, LedgeHitPosition, true, FLinearColor::Blue);
	if (!HitObstacle || LedgeHitPosition.bStartPenetrating)
		return false;

	// Otherwise, we compute the new ledge position and normal that we will use to orient the player.
	FVector UpVector = FVector::CrossProduct(CapsuleComponent->GetRightVector(), ForwardHitInformation.ImpactNormal);
	LedgePos = LedgeHitPosition.ImpactPoint + LedgeHitPosition.ImpactNormal * CapsuleComponent->GetScaledCapsuleRadius()
		+ UpVector * (HeightHitPosition.Z - ForwardHitInformation.ImpactPoint.Z);
	LedgeNormal = LedgeHitPosition.ImpactNormal;

	return true;
}

float UPS_LocState_Climb::ComputeSurfaceForwardStep(const FVector& SurfaceNormal)
{
	FRotator SurfaceVectorInvertedInForwardDir = (SurfaceNormal * -1.0f).Rotation();
	return FMath::FindDeltaAngleDegrees(SurfaceVectorInvertedInForwardDir.Pitch, FVector::ForwardVector.Rotation().Pitch);
}

float UPS_LocState_Climb::ComputeSurfaceHeightStep(const FVector& SurfaceNormal)
{
	return FMath::FindDeltaAngleDegrees(SurfaceNormal.Rotation().Pitch, FVector::UpVector.Rotation().Pitch);
}

/* Moving to Hang */

// Set by the climbing component when it call this state.
void UPS_LocState_Climb::MoveToHangingPosition(const FVector & PositionToHangTo, const FVector & ObstacleNormal, float DeltaAngleWithSurface)
{
	// Set position to reach (hanging pos).
	FVector StartPosition = CapsuleComponent->GetComponentLocation();
	HangingPosDM.Init(StartPosition, PositionToHangTo);

	// Compute rotation target, also set it as target rotation in locomotion.
	HangingStartRotation = CapsuleComponent->GetComponentRotation();
	HangingGoalRotation = (ObstacleNormal * -1.0f).Rotation();

	// When starting, get sure we first stop all movement in all direction as we are now being controlled by the interpolation.
	// This will avoid issues with previous velocity affecting the final position if it isn't 0 by the moment we reach the goal
	// hanging position.
	MovComponent->StopMovementImmediately();

	// Stop previous montages (like jumping montage, before playing the hanging animation.
	AnimInstance->Montage_Stop(0.3f);

	ChooseMoveToHangingAnimation();

	// Set the update method to be the one for moving to hang state.
	SetNewClimbState(EClimbingStates::MovingToHang);
}

// Plays a move to hanging animation.
bool UPS_LocState_Climb::ChooseMoveToHangingAnimation()
{
	// If playing jumping montage, we simply don't pick any animation as we already have one.
	if (CurrentJumpDirAnimation != nullptr)
	{
		return true;
	}
	
	// Play a random animation from hanging ones based on starting position and whether we can place feet on surface or not.
	TArray<int> PossibleCandidatesToPlay;
	if (CanPlaceFeetOnSurfaceMoveToHangingPos())
	{
		for (int i = 0; i < HangingAnimations.Num(); ++i)
		{
			if (HangingAnimations[i].HangingStance != EClimbingStance::FreeHanging)
				PossibleCandidatesToPlay.Add(i);
		}
	}
	else
	{
		for (int i = 0; i < HangingAnimations.Num(); ++i)
		{
			if (HangingAnimations[i].HangingStance == EClimbingStance::FreeHanging)
				PossibleCandidatesToPlay.Add(i);
		}
	}

	if (PossibleCandidatesToPlay.Num() < 1)
		return false;

	int HangingAnimIndx = PossibleCandidatesToPlay[FMath::RandRange(0, PossibleCandidatesToPlay.Num() - 1)];
	AnimInstance->SetClimbingHangingAnimation(HangingAnimations[HangingAnimIndx]);

	return true;
}

// Perform check can place feet in hanging position.
bool UPS_LocState_Climb::CanPlaceFeetOnSurfaceMoveToHangingPos()
{
	return CanPlaceFeetOnSurface(HangingPosDM.GetGoalValue(), HangingGoalRotation.RotateVector(FVector::ForwardVector));
}

// Here we perform the update to get to the hanging position.
void UPS_LocState_Climb::UpdateMovingToHangState(float DeltaTime)
{
	const float VelocityReductionFactor = 0.5f;
	float VelocityToHang = FMath::Max(VelocityToHangingPosition, FMath::Max(LocController->GetSpeed() * VelocityReductionFactor, LocController->GetVerticalSpeed() * VelocityReductionFactor));

	FVector NewPosition = UKismetMathLibrary::VInterpTo_Constant(CapsuleComponent->GetComponentLocation(), HangingPosDM.GetGoalValue(), DeltaTime, VelocityToHang);
	CapsuleComponent->SetWorldLocation(NewPosition);

	float HangingFactor = 0.0f;
	if (AnimInstance)
	{
		HangingFactor = HangingPosDM.GetFactor(CapsuleComponent->GetComponentLocation());
		AnimInstance->UpdateHangingAnimation(HangingFactor);
	}

	CapsuleComponent->SetWorldRotation(UKismetMathLibrary::RLerp(HangingStartRotation, HangingGoalRotation, HangingFactor, true));

	if (FMath::IsNearlyEqual(HangingFactor, 1.0f, 0.005f))
		StartHangingState();
}

/* Hanging */

void UPS_LocState_Climb::StartHangingState()
{
	// Set the object to the goal position in case it's still completely not there.
	CapsuleComponent->SetWorldLocation(HangingPosDM.GetGoalValue());
	CapsuleComponent->SetWorldRotation(HangingGoalRotation);

	// Perform check to see which surface we are at if we are crouching and we must change stance we will do it.
	UpdateCanPlaceFeetOnSurface();

	// Now move to hanging state.
	SetNewClimbState(EClimbingStates::Hanging);
}

void UPS_LocState_Climb::TryMove(float Axis)
{	
	Stride = 0.0f;

	if (!CharacterOwner->GetIsMovementEnabled() || CurrentClimbingState != EClimbingStates::Hanging)
		return;

	// Ignore movement if axis value is too small.
	float MovementAxisAbsValue = FMath::Abs(Axis);
	if (MovementAxisAbsValue <= 0.05f)
		return;

	// Compute horizontal movement behaviour.
	if (CurrentHorMovBehaviour)
		(this->*CurrentHorMovBehaviour)(Axis, MovementAxisAbsValue);
}

void UPS_LocState_Climb::TryClimb(float Axis)
{
	// Ignore movement if axis value is too small.
	float MovementAxisAbsValue = FMath::Abs(Axis);
	if (MovementAxisAbsValue <= 0.05f)
		return;

	if (CurrentClimbingState == EClimbingStates::Hanging)
	{
		if (Axis > 0.0f)
		{
			bool CanClimbObstacle = CanClimbCheck();

			// Call climb montage if we can climb.
			if (CanClimbObstacle)
				StartClimb();
			else
				StartTryClimb();
		}
	}
	else if (CurrentClimbingState == EClimbingStates::ClimbingMontage)
		SetClimbingMontagePlayrate(Axis < 0.0f);
	else if (CurrentClimbingState == EClimbingStates::TryClimbingMontage)
		SetTryClimbingMontagePlayrate(Axis < 0.0f);
}


/* Lateral movement */

void UPS_LocState_Climb::TryLateralMovement(float Axis, float MovementAxisAbsValue)
{
	FVector MovementGoalPosition, TurningEndPosition;
	FRotator MovementGoalRotation, TurningEndRotation;
	if (Axis > 0.0f)
	{
		// Right side movement.
		if (InwardTurnCheck(true, MovementGoalPosition, MovementGoalRotation))
			StartInwardMovement(MovementGoalPosition, MovementGoalRotation, true, Axis, MovementAxisAbsValue);
		else if (LateralMovementCheck(true, MovementGoalPosition, MovementGoalRotation))
			PerformLateralMovement(MovementGoalPosition, MovementGoalRotation, Axis, MovementAxisAbsValue);
		else if (OutwardTurnCheck(true, TurningEndPosition, TurningEndRotation))
			PerformTurnAnimation(true, TurningEndPosition, TurningEndRotation);
	}
	else
	{
		// Left side movement.
		if (InwardTurnCheck(false, MovementGoalPosition, MovementGoalRotation))
			StartInwardMovement(MovementGoalPosition, MovementGoalRotation, false, Axis, MovementAxisAbsValue);
		else if (LateralMovementCheck(false, MovementGoalPosition, MovementGoalRotation))
			PerformLateralMovement(MovementGoalPosition, MovementGoalRotation, Axis, MovementAxisAbsValue);
		else if (OutwardTurnCheck(false, TurningEndPosition, TurningEndRotation))
			PerformTurnAnimation(false, TurningEndPosition, TurningEndRotation);
	}
}

void UPS_LocState_Climb::PerformLateralMovement(const FVector& NewGoalPosition, const FRotator& NewGoalRotation, float Axis, float MovementAxisAbsValue)
{
	if (LocController)
	{
		UWorld* World = LocController->GetWorld();
		if (!World)
			return;

		Stride = Axis * CurrentSpeedModifier;

		float CanMoveToSurface;
		AnimInstance->GetCurveValue(CurveMovementClimbing, CanMoveToSurface);
		if (CanMoveToSurface < 0.05f)
			return;

		float ElapsedDeltaTime = World->GetDeltaSeconds();
		FVector LocationToEndAt = UKismetMathLibrary::VInterpTo(CapsuleComponent->GetComponentLocation(), NewGoalPosition, ElapsedDeltaTime, CurrentMovementProps->MovementSpeed * CurrentSpeedModifier * MovementAxisAbsValue);
		if (CanMoveToPosition(LocationToEndAt))
		{
			CapsuleComponent->SetWorldLocation(LocationToEndAt);
			CapsuleComponent->SetWorldRotation(UKismetMathLibrary::RInterpTo(CapsuleComponent->GetComponentRotation(), NewGoalRotation, ElapsedDeltaTime, HangingRotationSpeed));
			UpdateCanPlaceFeetOnSurface();
		}
	}
}

/* Inward behaviour */

void UPS_LocState_Climb::StartInwardMovement(const FVector& MovementGoalPosition, const FRotator& MovementGoalRotation, bool IsMovingRight, float Axis, float MovementAxisAbsValue)
{
	TurnStartPosition = CapsuleComponent->GetComponentLocation();
	TurnStartRotation = CapsuleComponent->GetComponentRotation();
	TurnGoalPosition = MovementGoalPosition;
	TurnGoalRotation = MovementGoalRotation;
	InwardSideOrientationIsRight = IsMovingRight;

	CurrentHorMovBehaviour = &UPS_LocState_Climb::PerformInwardMovement;
	if (CurrentHorMovBehaviour)
		(this->*CurrentHorMovBehaviour)(Axis, MovementAxisAbsValue);
}

void UPS_LocState_Climb::StopInwardMovement(float Axis, float MovementAxisAbsValue)
{
	CurrentHorMovBehaviour = &UPS_LocState_Climb::TryLateralMovement;
	if (CurrentHorMovBehaviour)
		(this->*CurrentHorMovBehaviour)(Axis, MovementAxisAbsValue);
}

void UPS_LocState_Climb::PerformInwardMovement(float Axis, float MovementAxisAbsValue)
{
	UWorld* World = LocController->GetWorld();
	if (!World)
		return;

	Stride = Axis * CurrentSpeedModifier;

	float CanMoveToSurface;
	AnimInstance->GetCurveValue(CurveMovementClimbing, CanMoveToSurface);
	if (CanMoveToSurface < 0.05f)
		return;

	float AxisSign = FMath::Sign(Axis);
	FVector PositionToMoveTo;
	FRotator RotationToMoveTo;
	if (AxisSign > 0.0f)
	{
		PositionToMoveTo = InwardSideOrientationIsRight ? TurnGoalPosition : TurnStartPosition;
		RotationToMoveTo = InwardSideOrientationIsRight ? TurnGoalRotation : TurnStartRotation;
	}
	else
	{
		PositionToMoveTo = InwardSideOrientationIsRight ? TurnStartPosition : TurnGoalPosition;
		RotationToMoveTo = InwardSideOrientationIsRight ? TurnStartRotation : TurnGoalRotation;
	}

	float ElapsedDeltaTime = World->GetDeltaSeconds();
	FVector InwardPos = UKismetMathLibrary::VInterpTo(CapsuleComponent->GetComponentLocation(), PositionToMoveTo, ElapsedDeltaTime, CurrentMovementProps->MovementSpeed * CurrentSpeedModifier * MovementAxisAbsValue);
	FRotator InwardRot = UKismetMathLibrary::RInterpTo(CapsuleComponent->GetComponentRotation(), RotationToMoveTo, ElapsedDeltaTime, HangingRotationSpeed);

	CapsuleComponent->SetWorldLocation(InwardPos);
	CapsuleComponent->SetWorldRotation(InwardRot);
	UpdateCanPlaceFeetOnSurface();

	// Check if we must end turning.
	const float RotationTolerance = 4.0f;
	if (InwardSideOrientationIsRight)
	{
		if (AxisSign > 0.0f && InwardRot.Equals(TurnGoalRotation, RotationTolerance))
			StopInwardMovement(Axis, MovementAxisAbsValue);
		else if (AxisSign < 0.0f && InwardRot.Equals(TurnStartRotation, RotationTolerance))
			StopInwardMovement(Axis, MovementAxisAbsValue);
	}
	else
	{
		if (AxisSign > 0.0f && InwardRot.Equals(TurnStartRotation, RotationTolerance))
			StopInwardMovement(Axis, MovementAxisAbsValue);
		else if (AxisSign < 0.0f && InwardRot.Equals(TurnGoalRotation, RotationTolerance))
			StopInwardMovement(Axis, MovementAxisAbsValue);
	}
}

/* Jump Montages */

void UPS_LocState_Climb::PerformJumpingFromHanged()
{
	if (CurrentClimbingState != EClimbingStates::Hanging) return;
	if (!JumpFromHanging || !AnimInstance) return;

	// Play leaving montage. This montage will launch an animation notify when it's time to leave the climbing stage and start falling down.
	bool Success = AnimInstance->PlayAnimationMontage(JumpFromHanging, 1.0f, 0.0, "None", true);
	if (!Success) return;

	// Stand if we were crouched.
	Stand();

	// Move to OnMontageState. Montage should get sure to enable movement of player once finished.
	SetNewClimbState(EClimbingStates::OnMontage);

	return;
}

void UPS_LocState_Climb::PerformVerticalJump()
{
	if (CurrentClimbingState != EClimbingStates::Hanging)
		return;

	CurrentJumpDirAnimation = &JumpUpwards;
	if (!CurrentJumpDirAnimation->JumpingFromHangMontage || !AnimInstance)
		return;

	// Play leaving montage. This montage will launch an animation notify when it's time to leave the climbing stage and start falling down.
	bool Success = AnimInstance->PlayAnimationMontage(CurrentJumpDirAnimation->JumpingFromHangMontage, 1.0f, 0.0, "None", true);
	if (!Success) return;

	// Stand if we were crouched.
	Stand();

	// Move to OnMontageState. Montage should get sure to enable movement of player once finished.
	SetNewClimbState(EClimbingStates::OnMontage);

	CapsuleComponent->SetCapsuleRadius(OriginalCapsuleRadius * 0.25f);

	FOnMontageBlendingOutDynDelegate BlendingOutDel;
	BlendingOutDel.BindDynamic(this, &UPS_LocState_Climb::OnJumpingFromHangMontageBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(CurrentJumpDirAnimation->JumpingFromHangMontage, BlendingOutDel);

	FOnMontageEndedDynDelegate EndingDel;
	EndingDel.BindDynamic(this, &UPS_LocState_Climb::OnJumpingFromHangMontageEnded);
	AnimInstance->BindOnMontageEnded(CurrentJumpDirAnimation->JumpingFromHangMontage, EndingDel);

	return;
}

void UPS_LocState_Climb::PerformHorizontalJump(bool Right)
{
	if (CurrentClimbingState != EClimbingStates::Hanging)
		return;
	
	PerformHorizontalJumpCrouched(Right);
}

// I'm leaving this unused as the animation root motion isn't perfect and it moves the player wrongly.
void UPS_LocState_Climb::PerformHorizontalJumpStanding(bool Right)
{
	UAnimMontage* MontageToPlay = Right ? StandingJumpRightSide : StandingJumpLeftSide;
	bool Success = AnimInstance->PlayAnimationMontage(MontageToPlay, 1.0f, 0.0, "None", true);
	if (!Success) return;

	// Move to OnMontageState. Montage should get sure to enable movement of player once finished.
	SetNewClimbState(EClimbingStates::OnMontage);

	// If inward movement, we will override movement of montage. Ideally, this would be a different animation.
	FVector MovementGoalPosition;
	FRotator MovementGoalRotation;
	StartJumpingHorizontalStanding(Right);

	CapsuleComponent->SetCapsuleRadius(OriginalCapsuleRadius * 0.75f);

	FOnMontageBlendingOutDynDelegate StandingBlendingOutDel;
	StandingBlendingOutDel.BindDynamic(this, &UPS_LocState_Climb::OnJumpingFromHangMontageBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(MontageToPlay, StandingBlendingOutDel);

	FOnMontageEndedDynDelegate StandingEndDel;
	StandingEndDel.BindDynamic(this, &UPS_LocState_Climb::OnJumpingFromHangMontageEnded);
	AnimInstance->BindOnMontageEnded(MontageToPlay, StandingEndDel);
}


void UPS_LocState_Climb::StartJumpingHorizontalStanding(bool Right)
{
	StandingMovingRight = Right;
	IsPlayingStandingMontage = true;
	CurrentSubStateBehaviour = &UPS_LocState_Climb::UpdateWhileJumpingHorizontalStanding; // Will check we are not falling.
}

void UPS_LocState_Climb::UpdateWhileJumpingHorizontalStanding(float DeltaTime)
{
	bool RightHandHit, LeftHandHit;
	if (CheckHandsAreOnObstacle(RightHandHit, LeftHandHit) == false)
	{
		CurrentSubStateBehaviour = nullptr;
		LeaveHanging(LocController->GetLocomotionStance(), false);
	}
	else if (StandingMovingRight && !RightHandHit)
	{
		CurrentSubStateBehaviour = nullptr;
		AnimInstance->Montage_Stop(0.3f);
	}
	else if (!StandingMovingRight && !LeftHandHit)
	{
		CurrentSubStateBehaviour = nullptr;
		AnimInstance->Montage_Stop(0.3f);
	}
}

void UPS_LocState_Climb::PerformHorizontalJumpCrouched(bool Right)
{
	// Check position we must end at, either inward position if we hit a corner, or side position otherwise.
	const float InwardTurningRadiusFactorCheck = 4.0f;
	if (InwardTurnCheck(Right, TurnGoalPosition, TurnGoalRotation, InwardTurningRadiusFactorCheck))
		PerformHorizontalJumpCrouchedInward(Right);
	else
		PerformHorizontalJumpCrouchedNormal(Right);
}

void UPS_LocState_Climb::PerformHorizontalJumpCrouchedNormal(bool Right)
{
	CurrentJumpDirAnimation = Right ? &JumpRightSide : &JumpLeftSide;
	if (!CurrentJumpDirAnimation->JumpingFromHangMontage || !AnimInstance) return;

	bool Success = AnimInstance->PlayAnimationMontage(CurrentJumpDirAnimation->JumpingFromHangMontage, 1.0f, 0.0, "None", true);
	if (!Success) return;

	// Move to OnMontageState. Montage should get sure to enable movement of player once finished.
	SetNewClimbState(EClimbingStates::OnMontage);

	// Stand.
	if (LocController->GetLocomotionStance() == ELocomotionStance::Crouch)
		Stand();

	// Set hanging animation in case we hang to an object while playing this montage.
	if (CurrentJumpDirAnimation)
		AnimInstance->SetClimbingHangingAnimation(CurrentJumpDirAnimation->EnterAnimationAfterJumpingFromHang, false);

	CapsuleComponent->SetCapsuleRadius(OriginalCapsuleRadius * 0.75f);

	// Wait for montage to end to reset some final variables.
	FOnMontageBlendingOutDynDelegate BlendingOutDel;
	BlendingOutDel.BindDynamic(this, &UPS_LocState_Climb::OnJumpingFromHangMontageBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(CurrentJumpDirAnimation->JumpingFromHangMontage, BlendingOutDel);

	FOnMontageEndedDynDelegate EndingDel;
	EndingDel.BindDynamic(this, &UPS_LocState_Climb::OnJumpingFromHangMontageEnded);
	AnimInstance->BindOnMontageEnded(CurrentJumpDirAnimation->JumpingFromHangMontage, EndingDel);
}

void UPS_LocState_Climb::OnJumpingFromHangMontageBlendingOut(UAnimMontage* Montage, bool Interrupted)
{	
	if (CurrentClimbingState != EClimbingStates::OnMontage) return;

	// Get sure we stop root motion immediately. if we are were performing a jumping inward.
	if (IsPlayingStandingMontage)
	{
		IsPlayingStandingMontage = false;
		MovComponent->StopMovementImmediately();
	}

	SetNewClimbState(EClimbingStates::Hanging);
}

void UPS_LocState_Climb::OnJumpingFromHangMontageEnded(UAnimMontage* Montage, bool Interrupted)
{
	CurrentJumpDirAnimation = nullptr;
}

void UPS_LocState_Climb::PerformHorizontalJumpCrouchedInward(bool Right)
{
	// Choose inward animation to play.
	CurrentInwardJump = Right ? &InwardsJumpRightSide : &InwardsJumpLeftSide;
	if (!CurrentInwardJump->JumpingFromHangMontage || !AnimInstance) return;

	bool Success = AnimInstance->PlayAnimationMontage(CurrentInwardJump->JumpingFromHangMontage, 1.0f, 0.0, "None", true);
	if (!Success) return;

	// Start inward jump rotation once we start jumping.
	FTimerHandle StartInwardsTimer;
	UWorld * World = GetWorld();
	if (World)
		World->GetTimerManager().SetTimer(StartInwardsTimer, this, &UPS_LocState_Climb::StartInwardsHorizontalJump, CurrentInwardJump->TimeToStartJumpingMovement, false);

	// Move to OnMontageState. Montage should get sure to enable movement of player once finished.
	SetNewClimbState(EClimbingStates::OnMontage);

	// Stand.
	if (LocController->GetLocomotionStance() == ELocomotionStance::Crouch)
		Stand();
}

void UPS_LocState_Climb::StartInwardsHorizontalJump()
{
	FLatentActionInfo LatentInfo = FLatentActionInfo();
	LatentInfo.CallbackTarget = this;
	UKismetSystemLibrary::MoveComponentTo(CapsuleComponent, TurnGoalPosition, TurnGoalRotation, false, false,
		CurrentInwardJump->TimeToReachDestination, true, EMoveComponentAction::Type::Move, LatentInfo);

	FTimerHandle OnInwardsJumpEnds;
	UWorld * World = GetWorld();
	if (World)
		World->GetTimerManager().SetTimer(OnInwardsJumpEnds, this, &UPS_LocState_Climb::OnInwardsHorizontalJumpEnds, CurrentInwardJump->TimeToReachDestination, false);
}

void UPS_LocState_Climb::OnInwardsHorizontalJumpEnds()
{
	SetNewClimbState(EClimbingStates::Hanging);
}

/* Leave Climbing */

void UPS_LocState_Climb::LeaveHanging(ELocomotionStance CurrentStance, bool PlayLeavingMontage)
{
	if (PlayLeavingMontage)
		ChooseAndPlayLeavingMontage(CurrentStance);
	else
		LeaveWithoutMontage();
}

// Leave hanging through a leaving montage.
void UPS_LocState_Climb::ChooseAndPlayLeavingMontage(ELocomotionStance CurrentStance)
{
	int LastIndexAnimations = 0;
	UAnimMontage* MontageToPlay = nullptr;
	switch (CurrentStance)
	{
	case ELocomotionStance::Normal:
		if (CanPlaceFeetOnSurfaceCheck())
		{
			LastIndexAnimations = LeavingHangingFromStanding.Num() - 1;
			if (LastIndexAnimations >= 0)
				MontageToPlay = LeavingHangingFromStanding[FMath::RandRange(0, LastIndexAnimations)];
		}
		else
		{
			LastIndexAnimations = LeavingHangingFromFreeHanging.Num() - 1;
			if (LastIndexAnimations >= 0)
				MontageToPlay = LeavingHangingFromFreeHanging[FMath::RandRange(0, LastIndexAnimations)];
		}

		break;
	case ELocomotionStance::Crouch:
		LastIndexAnimations = LeavingHangingFromCrouched.Num() - 1;
		if (LastIndexAnimations >= 0)
			MontageToPlay = LeavingHangingFromCrouched[FMath::RandRange(0, LastIndexAnimations)];
		break;
	default:
		break;
	}

	if (!MontageToPlay || !AnimInstance)
		return;

	// Play leaving montage. This montage will launch an animation notify when it's time to leave the climbing stage and start falling down.
	bool Success = AnimInstance->PlayAnimationMontage(MontageToPlay, 1.0f, 0.0, "None", true);
	if (!Success)
		return;

	if (CurrentStance == ELocomotionStance::Crouch)
		CharacterOwner->DoUncrouch();

	CharacterOwner->SetEnableCharacterMovementAndActions(false);
	
	LocController->SetUpperBodyIKDisabled();

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_LocState_Climb::OnLeaveClimbingMontageEndedOrBlendingOut);
	AnimInstance->BindOnMontageBlendingOut(MontageToPlay, BlendingOutMontage);
}

// Leave hanging without playing a special leaving montage.
void UPS_LocState_Climb::LeaveWithoutMontage()
{
	CharacterOwner->SetEnableCharacterMovementAndActions(true);
	LocController->ChangeToFallingState();
}

// Called when leave climbing is done.
void UPS_LocState_Climb::OnLeaveClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	// Set falling mode if somehow, the anim notification for setting the falling mode when leaving climbing was not fired.
	if (MovComponent->IsFlying())
	{
		MovComponent->SetMovementMode(MOVE_Falling);
		CharacterOwner->SetEnableCharacterMovementAndActions(true);
	}
}

/* Climbing functions */

// Starts climbing substate and animations if it is possible to climb the obstacle. Character starts climbing object.
void UPS_LocState_Climb::StartClimb()
{
	CharacterOwner->SetEnableCharacterMovementAndActions(false);
	SetNewClimbState(EClimbingStates::ClimbingMontage);

	if (AnimInstance && !ClimbingMontagePlaying)
	{
		PlayClimbingMontage();
		if(ClimbingMontagePlaying)
			ClimbingHangInterpolator.Start(ClimbingMontagePlaying);
	}
}

// Plays a climbing montage depending on the current stance.
bool UPS_LocState_Climb::PlayClimbingMontage()
{
	int LastIndexAnimations;
	UAnimMontage * MontageToPlay = nullptr;

	switch (LocController->GetLocomotionStance())
	{
	case ELocomotionStance::Normal:
		if (CanPlaceFeetOnSurfaceCheck())
		{
			LastIndexAnimations = ClimbingFromStandingHanging.Num() - 1;
			if (LastIndexAnimations >= 0)
				MontageToPlay = ClimbingFromStandingHanging[FMath::RandRange(0, LastIndexAnimations)];
		}
		else
		{
			LastIndexAnimations = ClimbingFromStandingFreeHanging.Num() - 1;
			if (LastIndexAnimations >= 0)
				MontageToPlay = ClimbingFromStandingFreeHanging[FMath::RandRange(0, LastIndexAnimations)];
		}
		break;
	case ELocomotionStance::Crouch:
		LastIndexAnimations = ClimbingFromCrouched.Num() - 1;
		if (LastIndexAnimations >= 0)
			MontageToPlay = ClimbingFromCrouched[FMath::RandRange(0, LastIndexAnimations)];
		break;
	default:
		break;
	}

	if (!MontageToPlay)
		return false;

	// Remove the montage if it's already playing.
	if (AnimInstance->IsAnyMontagePlaying())
		AnimInstance->MontageStop(MontageToPlay, 0.0f);

	// Play the climbing montage.
	bool Success = AnimInstance->PlayAnimationMontage(MontageToPlay, 1.0f, 0.0, "None", true);
	if (!Success)
		return false;

	ClimbingMontagePlaying = MontageToPlay;
	ClimbingUpwards = true;

	// On blending out montage, we call On Climb End.
	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_LocState_Climb::OnClimbBlendingOutMontage);
	AnimInstance->BindOnMontageBlendingOut(MontageToPlay, BlendingOutMontage);

	return true;
}

// Allows modifying the climbing montage playrate so we can reverse the montage and reverse climbing.
void UPS_LocState_Climb::SetClimbingMontagePlayrate(bool Backwards)
{
	if (!ClimbingMontagePlaying)
		return;

	float Playrate = Backwards ? -1.0f : 1.0f;
	ClimbingUpwards = !Backwards;
	AnimInstance->SetMontagePlayRate(ClimbingMontagePlaying, Playrate);
}

// Set while in climbing substate. Controls the climbing end position. Uses some curves with the extracted root motion of the montage
// and gets sure the player ends in the final destination of the root motion. We do this instead of using root motion from the montage
// as collisions with objects may affect the final position we end up while climbing and can make the player appear floating.
void UPS_LocState_Climb::UpdateClimbing(float DeltaTime)
{
	ClimbingHangInterpolator.Update();
}

void UPS_LocState_Climb::OnClimbBlendingOutMontage(UAnimMontage* Montage, bool Interrupted)
{
	// Ignore any on end call if it gets called and this is null. This might happen if, somehow, UE didn't remove
	// the previous montage. The Montage stop on PlayClimb call should already fix this.
	if (!ClimbingMontagePlaying) return;
	ClimbingMontagePlaying = nullptr;
	
	CharacterOwner->SetEnableCharacterMovementAndActions(true);
	if (ClimbingUpwards && LocController)
		LocController->ChangeToGroundedState();
	else
		SetNewClimbState(EClimbingStates::Hanging);
}

/* Try Climbing */

// Starts the try climbing substate.
void UPS_LocState_Climb::StartTryClimb()
{
	if (PlayTryClimbingMontage())
	{
		SetNewClimbState(EClimbingStates::TryClimbingMontage);
	}
}

// Plays a try climbing montage depending on the current stance.
bool UPS_LocState_Climb::PlayTryClimbingMontage()
{
	int LastIndexAnimations;
	UAnimMontage * TryClimbAnimToPlay = nullptr;

	switch (LocController->GetLocomotionStance())
	{
	case ELocomotionStance::Normal:
		if (CanPlaceFeetOnSurfaceCheck())
		{
			LastIndexAnimations = TryClimbingFromStandingHanging.Num() - 1;
			if (LastIndexAnimations >= 0)
				TryClimbAnimToPlay = TryClimbingFromStandingHanging[FMath::RandRange(0, LastIndexAnimations)];
		}
		else
		{
			LastIndexAnimations = TryClimbingFromStandingFreeHanging.Num() - 1;
			if (LastIndexAnimations >= 0)
				TryClimbAnimToPlay = TryClimbingFromStandingFreeHanging[FMath::RandRange(0, LastIndexAnimations)];
		}
		break;
	case ELocomotionStance::Crouch:
		LastIndexAnimations = TryClimbingFromCrouched.Num() - 1;
		if (LastIndexAnimations >= 0)
			TryClimbAnimToPlay = TryClimbingFromCrouched[FMath::RandRange(0, LastIndexAnimations)];
		break;
	default:
		break;
	}

	if (!TryClimbAnimToPlay || !AnimInstance)
		return false;

	// Don't play any montage if we are already playing something.
	if (AnimInstance->IsAnyMontagePlaying())
		return false;

	// Play the climbing montage.
	bool Success = AnimInstance->PlayAnimationMontage(TryClimbAnimToPlay, 1.0f, 0.0, "None", true);
	if (!Success)
		return false;

	TryClimbingMontagePlaying = TryClimbAnimToPlay;
	TryClimbMontagePaused = false;

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_LocState_Climb::OnTryClimbEnded);
	AnimInstance->BindOnMontageBlendingOut(TryClimbAnimToPlay, BlendingOutMontage);
	return true;
}

// Allows modifying the try climbing montage playrate so we can reverse the montage and reverse climbing.
void UPS_LocState_Climb::SetTryClimbingMontagePlayrate(bool Backwards)
{
	if (!TryClimbingMontagePlaying || !AnimInstance)
		return;

	float Playrate = Backwards ? -1.0f : 1.0f;
	AnimInstance->SetMontagePlayRate(TryClimbingMontagePlaying, Playrate);
	if (Backwards)
	{
		if (TryClimbMontagePaused)
		{
			AnimInstance->MontageResume(TryClimbingMontagePlaying);
			TryClimbMontagePaused = false;
		}
	}
}

// Called from AnimBP when TryClimbing starts blending out, we return to normal position.
void UPS_LocState_Climb::OnTryClimbingStartBlendOut()
{
	if (!TryClimbingMontagePlaying || !AnimInstance)
		return;

	bool GoingBackwards = AnimInstance->GetMontagePlayrate(TryClimbingMontagePlaying) < 0.0f;
	if (GoingBackwards)
		AnimInstance->MontageStop(TryClimbingMontagePlaying, TryClimbingMontagePlaying->BlendOut.GetBlendTime());
}

// Called from AnimBP when TryClimbing reaches the top. Stops the montage before it blends out completely.
void UPS_LocState_Climb::OnTryClimbingStop()
{
	if (!TryClimbingMontagePlaying || !AnimInstance)
		return;

	bool GoingForwards = AnimInstance->GetMontagePlayrate(TryClimbingMontagePlaying) > 0.0f;
	if (GoingForwards)
	{
		AnimInstance->MontagePause(TryClimbingMontagePlaying);
		TryClimbMontagePaused = true;
	}
}

// Called when try climbing montage is done. Returns control to player.
void UPS_LocState_Climb::OnTryClimbEnded(UAnimMontage* Montage, bool Interrupted)
{
	CharacterOwner->SetEnableCharacterMovementAndActions(true);
	SetNewClimbState(EClimbingStates::Hanging);
}

/* Turning */

// Performs a turning.
void UPS_LocState_Climb::PerformTurnAnimation(bool IsRightTurn, const FVector& TurningEndPosition, const FRotator& TurningEndRotation)
{
	if (AnimInstance)
	{
		CurrentTurningMontagePlaying = PlayClimbingTurnMontage(IsRightTurn);
		TurnGoalPosition = TurningEndPosition;
		TurnGoalRotation = TurningEndRotation;

		CharacterOwner->SetEnableCharacterMovementAndActions(false);
		SetNewClimbState(EClimbingStates::OnMontage);
	}
}

UAnimMontage * UPS_LocState_Climb::PlayClimbingTurnMontage(bool IsRightTurn)
{
	UAnimMontage* TurnMontage = IsRightTurn ? ClimbingTurnRight : ClimbingTurnLeft;
	if (!AnimInstance || !TurnMontage)
		return TurnMontage;

	bool Success = AnimInstance->PlayAnimationMontage(TurnMontage, 1.0f, 0.0, "None", true);
	if (!Success) return nullptr;

	return TurnMontage;
}

void UPS_LocState_Climb::OnHalfTurnPerformed()
{
	// Set turn movement ready.
	float TimeForOutwardTurning = AnimInstance->GetMontageCurrentTime(CurrentTurningMontagePlaying);

	FLatentActionInfo LatentInfo = FLatentActionInfo();
	LatentInfo.CallbackTarget = this;
	UKismetSystemLibrary::MoveComponentTo(CapsuleComponent, TurnGoalPosition, TurnGoalRotation, false, false,
		TimeForOutwardTurning, true, EMoveComponentAction::Type::Move, LatentInfo);

	FTimerHandle OnTurnEnded;
	UWorld* World = GetWorld();
	if (World)
		World->GetTimerManager().SetTimer(OnTurnEnded, this, &UPS_LocState_Climb::OnTurnMontageEnded, TimeForOutwardTurning, false);

	// Stop movement up until here.
	MovComponent->StopMovementImmediately();
}

// Called when turning is done. Returns control to player.
void UPS_LocState_Climb::OnTurnMontageEnded()
{
	// Stop movement once turning is done. In case we still have some.
	MovComponent->StopMovementImmediately();

	CharacterOwner->SetEnableCharacterMovementAndActions(true);
	SetNewClimbState(EClimbingStates::Hanging);
}

/* Hanging checks */

bool UPS_LocState_Climb::CanClimbCheck()
{
	// Height of the depth check to see if there are no obstacles and we can climb. Checks starts from the top of the player.
	const float SmallHeightOffsetAddedToCheck = 20.0f;
	// Radius of the sphere trace, big enough we get sure it doesn't fail.
	const float SphereRadiusCheck = 15.0f;
	// Distance from the player center forward where no obstacles should be so we can climb. We consider the radius of the capsule.
	const float CanClimbDepthCheckDistance = CapsuleComponent->GetScaledCapsuleRadius() + SphereRadiusCheck;

	FVector StartHeightCheckPosition = CapsuleComponent->GetComponentLocation() + CapsuleComponent->GetForwardVector() * CanClimbDepthCheckDistance
		+ CapsuleComponent->GetUpVector() * (CharacterHalfHeight + SmallHeightOffsetAddedToCheck);
	FVector EndHeightCheckPosition = StartHeightCheckPosition + CapsuleComponent->GetUpVector() * CharacterHalfHeight;

	// Perform height check to see if we have enough space for our player to fit.
	FHitResult HeightHangHit;
	TArray<AActor*> ActorsToIgnore;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartHeightCheckPosition, EndHeightCheckPosition, SphereRadiusCheck, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HeightHangHit, true, FLinearColor::White);
	if (HitObstacle)
		return false;

	return true;
}

bool UPS_LocState_Climb::CanPlaceFeetOnSurface(const FVector& StartPos, const FVector& CheckDirection)
{
	// Radius of the sphere trace, big enough we get sure it doesn't fail not detecting some obstacles.
	const float SphereRadiusCheck = 20.0f;

	// Depth check to see if we can keep moving in that direction.
	FVector StartCheckPosition = StartPos + CapsuleComponent->GetUpVector() * (-HeightToPerformCanPlaceFeetInSurfaceCheck);
	FVector EndCheckPosition = StartCheckPosition + CheckDirection * CapsuleComponent->GetScaledCapsuleRadius();

	FHitResult HangHit;
	TArray<AActor*> ActorsToIgnore;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartCheckPosition, EndCheckPosition, SphereRadiusCheck, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HangHit, true, FLinearColor::Blue);
	if (!HitObstacle)
		return false;

	return true;
}

bool UPS_LocState_Climb::CanPlaceFeetOnSurfaceCheck()
{
	return CanPlaceFeetOnSurface(CapsuleComponent->GetComponentLocation(), CapsuleComponent->GetForwardVector());
}

void UPS_LocState_Climb::UpdateCanPlaceFeetOnSurface()
{
	ELocomotionStance CurrStance = LocController->GetLocomotionStance();
	if (CurrStance == ELocomotionStance::Crouch && !CanPlaceFeetOnSurfaceCheck())
		CharacterOwner->DoUncrouch();
}

// Performs check from player pos to goal pos. If we hit an obstacle, returns false, we can't move there.
bool UPS_LocState_Climb::CanMoveToPosition(const FVector& PosToMoveTo)
{
	const float SphereRadiusCheck = 5.0f;
	FVector StartCheckPos = CapsuleComponent->GetComponentLocation();
	FVector DirectionVector = (PosToMoveTo - StartCheckPos);
	DirectionVector.Normalize();
	StartCheckPos += DirectionVector * CapsuleComponent->GetScaledCapsuleRadius() * 0.5f;
	FVector EndPositionCheck = StartCheckPos + DirectionVector * CapsuleComponent->GetScaledCapsuleRadius() * 0.5f;

	FHitResult HangHit;
	TArray<AActor*> ActorsToIgnore;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartCheckPos, EndPositionCheck, SphereRadiusCheck, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HangHit, true, FLinearColor::Blue);
	if (HitObstacle || HangHit.bStartPenetrating)
		return false;

	return true;
}

// Performs check from hand to see if we find a surface in its position.
bool UPS_LocState_Climb::HandHitCheck(bool RightCheck, FHitResult & HangVerticalHit)
{
	const FName & HandBoneToCheck = RightCheck ? RightHandSocket : LeftHandSocket;
	FVector BoneHandPosition = SkeletalMeshComp->GetSocketLocation(HandBoneToCheck);
	return PerformHandHitTrace(BoneHandPosition, HangVerticalHit);
}

bool UPS_LocState_Climb::PerformHandHitTrace(const FVector & BoneHandPosition, FHitResult & HangVerticalHit)
{
	// Height check from bone hands downward to get the surface we are moving at.
	TArray<FHitResult> HangVerticalHitsArray;
	TArray<AActor*> ActorsToIgnore;

	for (int i = 0; i < MaxHeightChecksIterations; ++i)
	{
		float HeightDiff = MaxHeightLedgeDifferenceCanClimb * (i + 1) / MaxHeightChecksIterations;
		FVector StartCheckPosition = BoneHandPosition + HeightDiff;
		FVector EndCheckPosition = BoneHandPosition - CapsuleComponent->GetUpVector() * (MaxHeightLedgeDifferenceCanClimb * 2.0f);
		bool VerticalHitFound = UKismetSystemLibrary::SphereTraceSingle(this, StartCheckPosition, EndCheckPosition, SphereCheckRadius,
			ClimbingTraceQuery, false, ActorsToIgnore, Debug, HangVerticalHit, true, FLinearColor::Green);
		if (VerticalHitFound && !HangVerticalHit.bStartPenetrating)
			return true;
	}

	return false;
}

#pragma optimize("", off)

bool UPS_LocState_Climb::LateralMovementCheck(bool RightCheck, FVector& HangPosition, FRotator& HangRotation)
{
	bool MovingRight = RightCheck;

	// Perform hand hit to check we are placing hand in surface.
	FHitResult HangVerticalHit;
	bool HandHitFound = HandHitCheck(RightCheck, HangVerticalHit);
	if (!HandHitFound) return false;

	/* Side check to see we can move in given dir. */

	const FName & HandBoneToCheck = RightCheck ? RightHandSocket : LeftHandSocket;
	TArray<AActor*> ActorsToIgnore;
	bool VerticalHitFound = false;

	// Perform maximum 4 side checks to see if we can move sideways. and climb the surface.
	for (int i = 0; i < MaxHeightChecksIterations; ++i)
	{
		float HeightDiff = MaxHeightLedgeDifferenceCanClimb * (i + 1) / MaxHeightChecksIterations;
		FVector StartCheckPosition = CapsuleComponent->GetComponentLocation() + CapsuleComponent->GetForwardVector() * LateralCheck_ForwardOffset + CapsuleComponent->GetRightVector() * (RightCheck ? LateralCheck_SideOffset : -LateralCheck_SideOffset);
		StartCheckPosition.Z = SkeletalMeshComp->GetSocketLocation(HandBoneToCheck).Z + HeightDiff;
		FVector EndCheckPosition = StartCheckPosition + CapsuleComponent->GetUpVector() * -(MaxHeightLedgeDifferenceCanClimb * 2.0f);
		FHitResult CheckCanPutHandHit;
		bool HitFound = UKismetSystemLibrary::SphereTraceSingle(this, StartCheckPosition, EndCheckPosition, SphereCheckRadius,
			ClimbingTraceQuery, false, ActorsToIgnore, Debug, CheckCanPutHandHit, true, FLinearColor::Green);
		if (HitFound && !CheckCanPutHandHit.bStartPenetrating)
		{
			VerticalHitFound = true;
			break;
		}
	}

	if (!VerticalHitFound)
		return false;

	/* Forward check to get the normal of the surface we are moving to. */

	const float SmallHeightOffsetBelowHeightHit = -10.0f;
	FHitResult HangForwardHit;
	FVector StartForwardCheckPosition = HangVerticalHit.ImpactPoint + CapsuleComponent->GetUpVector() * SmallHeightOffsetBelowHeightHit;
	FVector EndForwardCheckPosition = StartForwardCheckPosition + CapsuleComponent->GetForwardVector() * -CapsuleComponent->GetScaledCapsuleRadius();
	bool ForwadHitFound = UKismetSystemLibrary::SphereTraceSingle(this, EndForwardCheckPosition, StartForwardCheckPosition, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HangForwardHit, true, FLinearColor::Yellow);
	if (!ForwadHitFound || HangForwardHit.bStartPenetrating) return false;

	HangPosition = HangForwardHit.ImpactPoint + HangForwardHit.ImpactNormal * CapsuleComponent->GetScaledCapsuleRadius()
		+ HangVerticalHit.ImpactNormal + CapsuleComponent->GetUpVector() * -(CharacterHalfHeight + HangForwardHit.ImpactPoint.Z - HangVerticalHit.ImpactPoint.Z);
	HangRotation = (HangForwardHit.ImpactNormal * -1.0f).Rotation();

	/* Now, we try to perform same check in the other hand to get the final rotation as the result of lerping both. If it fails, we will simply ignore it and use only one hand.*/

	// Other hand height check from bone hands downward to get the surface we are moving at.
	FHitResult OtherHangVerticalHit;
	HandHitFound = HandHitCheck(!RightCheck, OtherHangVerticalHit);
	if (!HandHitFound)
		return true;

	// Forward check to get the normal of the surface we are moving to.
	FHitResult OtherHangForwardHit;
	FVector StartOtherForwardCheckPosition = OtherHangVerticalHit.ImpactPoint + CapsuleComponent->GetUpVector() * SmallHeightOffsetBelowHeightHit;
	FVector EndOtherForwardCheckPosition = StartOtherForwardCheckPosition + CapsuleComponent->GetForwardVector() * -CapsuleComponent->GetScaledCapsuleRadius();
	bool OtherForwadHitFound = UKismetSystemLibrary::SphereTraceSingle(this, EndOtherForwardCheckPosition, StartOtherForwardCheckPosition, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, OtherHangForwardHit, true, FLinearColor::Yellow);
	if (!OtherForwadHitFound)
		return true;

	HangRotation = FMath::Lerp(HangRotation, (OtherHangForwardHit.ImpactNormal * -1.0f).Rotation(), 0.5f);

	return true;
}

#pragma optimize("", on)

bool UPS_LocState_Climb::InwardTurnCheck(bool RightCheck, FVector& HangPosition, FRotator& HangRotation, float DistancePercentBasedOnCapsuleRadius)
{	
	// Get length of the side check to the pertaining side we will perform the test at.
	float SideCheckLength = RightCheck ? (CapsuleComponent->GetScaledCapsuleRadius() * DistancePercentBasedOnCapsuleRadius) : (-CapsuleComponent->GetScaledCapsuleRadius() * DistancePercentBasedOnCapsuleRadius);

	// Side check to see if we can perform an inward turning. Trace starts slightly behind the player to give some extra space for arms.
	FHitResult SideCheckHit;
	TArray<AActor*> ActorsToIgnore;
	FVector StartCheckPosition = SkeletalMeshComp->GetComponentLocation() + CapsuleComponent->GetForwardVector() * -CapsuleComponent->GetScaledCapsuleRadius() * 0.5f
		+ CapsuleComponent->GetUpVector() * HeightToPerformLedgeCheckAt;
	FVector EndCheckPosition = StartCheckPosition + CapsuleComponent->GetRightVector() * SideCheckLength;
	bool SideCheckHitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartCheckPosition, EndCheckPosition, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, SideCheckHit, true, FLinearColor::Green);
	if (!SideCheckHitObstacle || SideCheckHit.bStartPenetrating)
		return false;
	
	// Height check to get sure we can move to the final position and we can hang from it.
	bool SuccessHeightCheck = false;
	const float MaxHeight = HeightDiffBetweenCharHeightAndHeightToPerformCheck + MaxHeightLedgeDifferenceCanClimb;
	FHitResult HangHitHeight;
	for (int i = 0; i < MaxHeightChecksIterations; ++i)
	{
		float HeightDiff = MaxHeight * (i + 1) / MaxHeightChecksIterations;
		FVector EndHeightCheckPosition = SideCheckHit.ImpactPoint + SideCheckHit.ImpactNormal * -LedgeHandCheckDepthOffset;
		FVector StartHeightCheckPosition = EndHeightCheckPosition + CapsuleComponent->GetUpVector() * HeightDiff;
		bool HeightCheckHitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartHeightCheckPosition, EndHeightCheckPosition, SphereCheckRadius, ClimbingTraceQuery,
			false, ActorsToIgnore, Debug, HangHitHeight, true, FLinearColor::Yellow);
		if (HeightCheckHitObstacle && !HangHitHeight.bStartPenetrating)
		{
			SuccessHeightCheck = true;
			break;
		}
	}

	if (!SuccessHeightCheck)
		return false;

	// Get the position and rotation we will inward turn to.
	HangPosition = HangHitHeight.ImpactPoint + SideCheckHit.ImpactNormal * (CapsuleComponent->GetScaledCapsuleRadius() + LedgeHandCheckDepthOffset);
	HangPosition = HangPosition - HangHitHeight.ImpactNormal * CharacterHalfHeight;
	HangRotation = (SideCheckHit.ImpactNormal * -1.0f).Rotation();

	return true;
}

// Performs a check to see if we can perform an outward turn.
bool UPS_LocState_Climb::OutwardTurnCheck(bool RightCheck, FVector & TurningEndPosition, FRotator & TurningEndRotation)
{
	// Get offset to the pertaining side we will perform the test at.
	const float SideCheckOffset = RightCheck ? (CapsuleComponent->GetScaledCapsuleRadius()) : (-CapsuleComponent->GetScaledCapsuleRadius());
	const float HeightToCheckOnSideChecks = (CharacterHalfHeight * 2.25f);
	const float ForwardThicknessCheck = CapsuleComponent->GetScaledCapsuleRadius() * 0.5f;

	FHitResult HitInformation;
	TArray<AActor *> ActorsToIgnore;

	// Perform side checks to see we have no obstacles.
	FVector StartSideCheckPosition = SkeletalMeshComp->GetComponentLocation() + CapsuleComponent->GetRightVector() * SideCheckOffset * 2.0f;
	FVector EndSideCheckPosition = StartSideCheckPosition + CapsuleComponent->GetUpVector() * HeightToCheckOnSideChecks;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartSideCheckPosition, EndSideCheckPosition, CapsuleComponent->GetScaledCapsuleRadius(), ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HitInformation, true, FLinearColor::Gray);
	if (HitObstacle || HitInformation.bStartPenetrating)
			return false;

	// Perform forward check to see we have no obstacles.
	FVector StartForwardCheckPosition = StartSideCheckPosition + CapsuleComponent->GetForwardVector() * CapsuleComponent->GetScaledCapsuleRadius() * 2.5f;
	FVector EndForwardCheckPosition = StartForwardCheckPosition + CapsuleComponent->GetUpVector() * HeightToCheckOnSideChecks;
	HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartForwardCheckPosition, EndForwardCheckPosition, ForwardThicknessCheck, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HitInformation, true, FLinearColor::Blue);
	if (HitObstacle || HitInformation.bStartPenetrating)
		return false;

	// Thickness check from forward check to see there is enough space.
	FHitResult ThicknessHitInformation;
	FVector StartThickCheckPosition = StartForwardCheckPosition + CapsuleComponent->GetUpVector() * HeightToPerformLedgeCheckAt;
	FVector EndThickCheckPosition = StartThickCheckPosition + CapsuleComponent->GetRightVector() * (-SideCheckOffset * 2.0f);
	HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, StartThickCheckPosition, EndThickCheckPosition, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, ThicknessHitInformation, true, FLinearColor::Blue);
	if (!HitObstacle || ThicknessHitInformation.bStartPenetrating)
		return false;

	// Final height check to get sure we can hang to the obstacle.
	bool SuccessHeightCheck = false;
	const float MaxHeight = HeightDiffBetweenCharHeightAndHeightToPerformCheck + MaxHeightLedgeDifferenceCanClimb;
	for (int i = 0; i < MaxHeightChecksIterations; ++i)
	{
		float HeightDiff = MaxHeight * (i + 1) / MaxHeightChecksIterations;
		FVector StartHeightCheckPosition = ThicknessHitInformation.ImpactPoint + ThicknessHitInformation.ImpactNormal * -LedgeHandCheckDepthOffset
			+ CapsuleComponent->GetUpVector() * HeightDiff;
		FVector EndHeightCheckPosition = ThicknessHitInformation.ImpactPoint + ThicknessHitInformation.ImpactNormal * -LedgeHandCheckDepthOffset;
		bool HitObstacleHeight = UKismetSystemLibrary::SphereTraceSingle(this, StartHeightCheckPosition, EndHeightCheckPosition, SphereCheckRadius, ClimbingTraceQuery,
			false, ActorsToIgnore, Debug, HitInformation, true, FLinearColor::Yellow);
		if (HitObstacleHeight && !HitInformation.bStartPenetrating)
		{
			SuccessHeightCheck = true;
			break;
		}
	}

	if (!SuccessHeightCheck)
		return false;
	
	// Compute the turning end position and rotation.
	TurningEndPosition = ThicknessHitInformation.ImpactPoint + ThicknessHitInformation.ImpactNormal * CapsuleComponent->GetScaledCapsuleRadius();
	TurningEndPosition.Z = HitInformation.ImpactPoint.Z - CharacterHalfHeight;
	TurningEndRotation = (ThicknessHitInformation.ImpactNormal * -1.0f).Rotation();

	return true;
}

// Returns false if no hands is on the obstacle.
bool UPS_LocState_Climb::CheckHandsAreOnObstacle(bool & RightHandHitFound, bool & LeftHandHitFound)
{
	FHitResult HangVerticalHit, OtherHangVerticalHit;
	RightHandHitFound = HandHitCheck(true, HangVerticalHit);
	LeftHandHitFound = HandHitCheck(false, OtherHangVerticalHit);

	if (!RightHandHitFound && !LeftHandHitFound)
		return false;
	return true;

}