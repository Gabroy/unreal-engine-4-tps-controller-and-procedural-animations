#include "PS_LocState_Ragdoll.h"
#include "../../../PS_Character.h"
#include "../PS_Human_LocomotionController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"

void UPS_LocState_Ragdoll::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Ragdoll::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Ragdoll::Init_Implementation LocController Returned Null"));
		return;
	}

	MovComponent = Cast<UCharacterMovementComponent>(Actor->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!MovComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Ragdoll::Init_Implementation MovComponent Returned Null"));
		return;
	}

	SkeletalComponent = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SkeletalComponent)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SkeletalComponent->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Ragdoll::Init_Implementation AnimInstance Returned Null"));
			return;
		}
	}

	CapsuleComponent = Cast<UCapsuleComponent>(Actor->GetComponentByClass(UCapsuleComponent::StaticClass()));
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Ragdoll::Init_Implementation CapsuleComponent Returned Null"));
		return;
	}

	VisibilityTraceQuery = UEngineTypes::ConvertToTraceType(ECC_Visibility);
	RelativeTransformSkeleton = SkeletalComponent->GetRelativeTransform();
}

void UPS_LocState_Ragdoll::OnEnter_Implementation()
{
	// Set movement mode to none. We can't move anymore.
	MovComponent->SetMovementMode(MOVE_None);

	// Also disable character movement input and any other actions.
	CharacterOwner->SetEnableCharacterMovementAndActions(false);

	// Disable IKs while ragdolling.
	LocController->SetUpperBodyIKDisabled();
	LocController->SetLegIKEnabledStatus(false);

	// Get sure we also stop walking if we were doing so.
	CharacterOwner->DoStopWalk();

	// If we were crouching when we entered this state, we get sure to uncrouch.
	// If we were sprinting, we also stop.
	ELocomotionStance CurrentStance = LocController->GetLocomotionStance();
	if (CurrentStance == ELocomotionStance::Crouch)
		CharacterOwner->DoUncrouch();
	else if (LocController->GetWantsToSprint())
		CharacterOwner->DoStopSprint();

	// Change collision profiles when entering ragdoll.
	CapsuleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SkeletalComponent->SetCollisionProfileName(TEXT("Ragdoll"));

	// Finally, activate ragdolls. Set simulation for all bones below pelvis, this way
	// skeletam mesh component root position won't move and it won't be detached.
	SkeletalComponent->SetAllBodiesBelowSimulatePhysics(PelvisBone, true, true);

	// Set ragdoll velocity initial value.
	RagdollVelocity = GetRagdollVelocity();

	// Perform grounded test check to see if we are grounded and update capsule transform.
	PerformGroundedTestAndUpdateCapsuleTransform(0.0f);
}

void UPS_LocState_Ragdoll::Tick_Implementation(float DeltaTime)
{
	if (!LocController) return;

	// Protect against high velocity ragdolling issues by disabling gravity.
	// One of these issues is for example clipping through geometry.
	RagdollVelocity = GetRagdollVelocity();
	bool GravityAboveRiskThreshold = RagdollVelocity > 4000;
	if(GravityAboveRiskThreshold && CapsuleComponent->IsGravityEnabled())
		CapsuleComponent->SetEnableGravity(false);
	else if(!CapsuleComponent->IsGravityEnabled() && !GravityAboveRiskThreshold)
		CapsuleComponent->SetEnableGravity(true);

	// Perform grounded test check to see if we are grounded and update capsule transform.
	PerformGroundedTestAndUpdateCapsuleTransform(DeltaTime);
}

void UPS_LocState_Ragdoll::OnExit_Implementation()
{
	// Save snapshot of ragdolls to blend with in upper animations.
	AnimInstance->SetRagdollBlendOut(IsRagdollGrounded(), IsFacingDownwards());

	// Change collision profiles when entering ragdoll.
	CapsuleComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SkeletalComponent->SetCollisionProfileName(TEXT("CharacterMesh"));

	// Deactivate ragdolls.
	SkeletalComponent->SetAllBodiesSimulatePhysics(false);
}


/* Rolling private methods */

void UPS_LocState_Ragdoll::PerformGroundedTestAndUpdateCapsuleTransform(float DeltaTime)
{
	FVector StartPositionRagdoll = SkeletalComponent->GetBoneLocation(PelvisBone);
	FVector EndPositionRagdoll = StartPositionRagdoll + CapsuleComponent->GetUpVector() * (-RagdollIsGroundedCheckHeightDistance);
	FRotator RagdollRotation = SkeletalComponent->GetBoneQuaternion(PelvisBone).Rotator();

	TArray<AActor*> ActorsToIgnore;
	FHitResult HitResultInfo;
	RagdollIsGrounded = UKismetSystemLibrary::LineTraceSingle(this, StartPositionRagdoll, EndPositionRagdoll, VisibilityTraceQuery,
		false, ActorsToIgnore, DebugTrace, HitResultInfo, true);

	// Compute vertical offset.
	float VerticalOffsetCapsule;
	if (RagdollIsGrounded)
		VerticalOffsetCapsule = CapsuleExtraOffsetAppartFromHeight + CapsuleComponent->GetScaledCapsuleHalfHeight() + HitResultInfo.ImpactPoint.Z;
	else
		VerticalOffsetCapsule = SkeletalComponent->GetComponentLocation().Z - RelativeTransformSkeleton.GetLocation().Z;

	// Compute Capsule Location.
	FVector CapsuleFinalPosition = StartPositionRagdoll;
	CapsuleFinalPosition.Z = VerticalOffsetCapsule;

	// Compute Capsule Rotation
	bool IsFacingDown = RagdollRotation.Roll >= 0.0f;
	FRotator RotationToFaceTo = FRotator(0.0f, IsFacingDown ? RagdollRotation.Yaw : RagdollRotation.Yaw - 180.0f, 0.0f);

	// Finally set the world location and rotation.
	CapsuleComponent->SetWorldLocation(UKismetMathLibrary::VInterpTo(CapsuleComponent->GetComponentLocation(), CapsuleFinalPosition, DeltaTime, RagdollLocationInterpolationSpeed));
	LocController->SetTargetRotation(RotationToFaceTo.Yaw);
	LocController->UpdateCharacterMeshRotation(DeltaTime, RagdollRotationInterpolationSpeed);
}

float UPS_LocState_Ragdoll::GetRagdollVelocity() const
{
	return SkeletalComponent->GetPhysicsLinearVelocity(PelvisBone).Size();
}

// Returns true if we are facing upwards.
bool UPS_LocState_Ragdoll::IsFacingDownwards() const
{
	return SkeletalComponent->GetBoneQuaternion(PelvisBone).Rotator().Roll >= 0.0f;
}

// Returns true if we are hitting the ground
bool UPS_LocState_Ragdoll::IsRagdollGrounded() const
{
	return RagdollIsGrounded;
}

// Returns true if we are moving.
bool UPS_LocState_Ragdoll::IsMoving() const
{
	return RagdollVelocity > 1.0f;
}