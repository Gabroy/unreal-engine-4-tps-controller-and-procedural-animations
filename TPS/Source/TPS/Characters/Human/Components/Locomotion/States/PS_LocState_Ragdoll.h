#pragma once

#include "CoreMinimal.h"
#include "../../../../../Base/FSM/FSM_State.h"
#include "Engine/EngineTypes.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_LocState_Ragdoll.generated.h"

class APS_Character;
class UPS_Human_LocomotionController;
class UCharacterMovementComponent;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UPS_Human_AnimationInstance;
class UAnimMontage;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_LocState_Ragdoll : public UFSM_State
{
protected:

	GENERATED_BODY()

	/* Components */

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCapsuleComponent * CapsuleComponent = nullptr;

	UPROPERTY()
	USkeletalMeshComponent * SkeletalComponent = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	/* Variables */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	FName PelvisBone = "Pelvis";

	// You can find this value by placing the char in the world in a plane at 0.0f and seeing at what height
	// it ends at, it should be diff of height minus capsule height.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	float CapsuleExtraOffsetAppartFromHeight = 7.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	float RagdollIsGroundedCheckHeightDistance = 25.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	float RagdollLocationInterpolationSpeed = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ragdoll")
	float RagdollRotationInterpolationSpeed = 35.0f;

	FTransform RelativeTransformSkeleton;

	ETraceTypeQuery VisibilityTraceQuery;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TEnumAsByte<EDrawDebugTrace::Type> DebugTrace = EDrawDebugTrace::None;

	UPROPERTY(BlueprintReadOnly)
	float RagdollVelocity = 0.0f;

	UPROPERTY(BlueprintReadOnly)
	bool RagdollIsGrounded = false;

public:

	/* Methods. */

	virtual void Init_Implementation();
	
	virtual void OnEnter_Implementation();

	virtual void Tick_Implementation(float DeltaTime);

	virtual void OnExit_Implementation();

	/* Getters */
	float GetRagdollVelocity() const;

	// Returns true if we are facing down.
	bool IsFacingDownwards() const;

	// Returns true if we have not been moving for a while.
	bool IsMoving() const;

	// Returns true if we are hitting the ground
	bool IsRagdollGrounded() const;

private:

	/* Rolling private methods */

	void PerformGroundedTestAndUpdateCapsuleTransform(float DeltaTime);
};
