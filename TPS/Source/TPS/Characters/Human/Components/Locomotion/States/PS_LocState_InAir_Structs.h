#pragma once

#include "CoreMinimal.h"
#include "EnumAsByte.h"
#include "../../Animations/PS_Human_AnimationsStruct.h"
#include "PS_LocState_InAir_Structs.generated.h"

/* In Air Landing Animation */
USTRUCT(BlueprintType, Blueprintable)
struct FInAirLandingAnimationSequence
{
	GENERATED_BODY()

	// The animation sequence that will be played when the character lands if all necessary conditions
	// (flail and speed values) are met.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence* InAirLandingSequence = nullptr;

	// Montage with the same sequence set to play on the upper layer slot only.
	// Will be played if the player is not idle when falling and won't block the character movement.
	// Prevents foot sliding while keeping the character dynamic.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * UpperBodyMontage = nullptr;

	// Feet position the animation is at while playing.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EAnimationFeetPosition> FeetPositionOfAnim;

	// If true, movement will be blocked while landing if animation is not played as UpperBody and
	// and will be unblocked once we reach blending out time.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool BlockMovementOnStart = true;

	// Time we will start blending out the landing sequence (upper body montages have their own blending out
	// defined by the montage for extra control).
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float BlendOutTime = 0.25f;

	// From 0.0f to 1.0f, controls when this anim sequence can be played based on the
	// "time" it has been falling.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float MinFlailValueToPlay = 0.0f;

	// From 0.0f to 1.0f, controls when this anim sequence can be played based on the
	// "time" it has been falling.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float MaxFlailValueToPlay = 1.0f;

	// Minimum speed at which this animation will be played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float MinSpeedToPlay = 0.0f;

	// Maximum speed at which this animation will be played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float MaxSpeedToPlay = 1000000.0f;
};