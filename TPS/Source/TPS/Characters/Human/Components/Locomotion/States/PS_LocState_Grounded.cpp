#include "PS_LocState_Grounded.h"
#include "GameFramework/Character.h"
#include "../PS_Human_LocomotionController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../../../PS_Character.h"
#include "../../Animations/PS_Human_AnimationInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"

void UPS_LocState_Grounded::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
		return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Grounded::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Grounded::Init_Implementation LocController Returned Null"));
		return;
	}

	MovComponent = Cast<UCharacterMovementComponent>(Actor->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!MovComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Grounded::Init_Implementation MovComponent Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SKMesh)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_LocState_Grounded::Init_Implementation AnimInstance Returned Null"));
			return;
		}
	}

	MinSpeedForPivotingAnimationToPlay = NormalizedStandingSpeedForPivotingAnimToPlay * StandingMovProps.MaximumSpeed;
}

void UPS_LocState_Grounded::OnEnter_Implementation()
{
	if (!LocController || !CharacterOwner)
		return;

	SetStance(LocController->GetLocomotionStance());

	SetRotationMode(LocController->GetTargetRotationMode());

	// Enable IKs.
	LocController->SetLegIKEnabledStatus(true);
}

void UPS_LocState_Grounded::OnExit_Implementation()
{
	if (!CharacterOwner)
		return;

	LocController->SetHeadIKDisabled();
	LocController->SetUpperBodyIKDisabled();
}

void UPS_LocState_Grounded::Tick_Implementation(float DeltaTime)
{
	if (!LocController)
		return;

	bool IsMoving = LocController->GetIsMoving();
	bool WasMoving = LocController->GetWasMoving();
	float CurrentSpeed = LocController->GetSpeed();

	// Check if we are baking or Turning.
	IsBraking = CheckIsBraking();
	IsPivoting = CheckIsPivoting(MinSpeedForPivotingAnimationToPlay);

	// Compute stance specific variables.
	if (PtrToStanceFunctionToUse)
		(this->*PtrToStanceFunctionToUse)(CurrentSpeed);

	// Compute rotations specific variables.
	if (PtrForRotationModeFunctionToUse)
		(this->*PtrForRotationModeFunctionToUse)(DeltaTime);

	// If we are playing a root motion animation, we don't want to directly modify the character rotation as 
	// such job is already being done by the animation.
	if (LocController->IsPlayingRootMotion())
		return;

	// If moving we will rotate based on the current state.
	if (LocController->GetIsMoving())
		OnMovingUpdateRotation(DeltaTime);
}

void UPS_LocState_Grounded::OnMovingUpdateRotation(float DeltaTime)
{
	if (!LocController)
		return;

	// Get rotation mesh speed.
	float VelDesirDirAbsDelt = FMath::Abs(LocController->GetDeltaBetweenVelocityAndFacingDir());
	float CharacterMeshRotationSpeed = GroundedCharacterRotationBySpeedCurve->GetFloatValue(LocController->GetSpeed())
		* GroundedCharacterRotationModByDeltaMeshRotationAndVelocity->GetFloatValue(VelDesirDirAbsDelt);
	LocController->UpdateTargetRotationBasedOnRotationMode();
	LocController->UpdateCharacterMeshRotation(DeltaTime, CharacterMeshRotationSpeed);
}

void UPS_LocState_Grounded::DoToggleStandingFoot()
{
	IsUsingRightFoot = !IsUsingRightFoot;
}

/* Walk functions */

void UPS_LocState_Grounded::SetWalk(bool EnableWalk)
{
	UpdateWalkingValues();
}

void UPS_LocState_Grounded::UpdateWalkingValues()
{
	if (!CurrentMovementProps || !LocController || !CharacterOwner || !AnimInstance)
		return;
	
	if (LocController->GetIsWalking())
	{
		CharacterOwner->SetSpeedModifier(AnimInstance->GetIsAiming() ? CurrentMovementProps->AimingModeSpeedModifier : CurrentMovementProps->WalkingModeSpeedModifier);
	}
	else
	{
		CharacterOwner->SetSpeedModifier(1.0f);
	}
}

/* Sprinting functions */

bool UPS_LocState_Grounded::CanSprint() const
{
	return LocController && LocController->GetWantsToSprint() && LocController->GetIsMoving()
		&& !LocController->GetIsWalking() && FMath::Abs(LocController->GetDeltaBetweenVelocityAndFacingDir()) <= MaxDeltaCanSprint;
}

bool UPS_LocState_Grounded::IsSprinting() const
{
	return CanSprint();
}

void UPS_LocState_Grounded::UpdateSprintingValues()
{
	if (CanSprint())
	{
		MovComponent->MaxWalkSpeed = SprintingMaximumSpeed;
		MovComponent->MaxAcceleration = SpritingMaximumAcceleration;
	}
	else
	{
		MovComponent->MaxWalkSpeed = CurrentMovementProps->MaximumSpeed;
		MovComponent->MaxAcceleration = CurrentMovementProps->MaximumAcceleration;
	}
}

/* Turning functions */

// Apply turning behavior if the state allows so.
bool UPS_LocState_Grounded::CanTurn()
{
	// We can only turn if not moving and standing.
	return LocController && !(LocController->GetIsMoving() || (LocController->GetLocomotionStance() != ELocomotionStance::Normal));
}

bool UPS_LocState_Grounded::CheckIsBraking()
{
	return LocController && LocController->GetIsMoving() && (!LocController->GetHasMovementInput());
}

bool UPS_LocState_Grounded::CheckIsPivoting(float MinSpeedToConsiderPivoting)
{
	return LocController && (LocController->GetSpeed() >= MinSpeedToConsiderPivoting)
		&& LocController->GetHasMovementInput()
		&& (FMath::Abs(LocController->GetDeltaBetweenVelocityAndDesiredDirection()) >= DegreesToConsiderIsPivoting);
}

/* Stance functions */

void UPS_LocState_Grounded::OnChangedStance(TEnumAsByte<ELocomotionStance> OldStance, TEnumAsByte<ELocomotionStance> NewStance)
{
	SetStance(NewStance);
}

void UPS_LocState_Grounded::SetStance(TEnumAsByte<ELocomotionStance> NewStance)
{
	// If we change stance, we get sure we stop walking if we were doing so.
	SetWalk(false);

	SetStanceSpeedValues(NewStance);

	// Here we choose, based on the stance we are using now, which stance function to use.
	// This function will be used to compute stance specific behavior and vars.
	switch (NewStance)
	{
	case ELocomotionStance::Crouch:
		PtrToStanceFunctionToUse = &UPS_LocState_Grounded::ComputeCrouchStanceVariables;
		UpdateWalkingValues();
		break;

	case  ELocomotionStance::Normal:
		PtrToStanceFunctionToUse = &UPS_LocState_Grounded::ComputeNormalStanceVariables;
		UpdateWalkingValues();
		break;

	default:
		PtrToStanceFunctionToUse = nullptr;
		break;
	}
}

void UPS_LocState_Grounded::SetStanceSpeedValues(TEnumAsByte<ELocomotionStance> NewStance)
{
	switch (NewStance)
	{
	case ELocomotionStance::Crouch:

		// Set the movement stance basic movement properties.
		CurrentMovementProps = &CrouchedMovProps;
		MovComponent->MaxWalkSpeedCrouched = CurrentMovementProps->MaximumSpeed;
		MovComponent->MaxAcceleration = CurrentMovementProps->MaximumAcceleration;

		break;

	case  ELocomotionStance::Normal:

		// Set the movement stance basic movement properties.
		CurrentMovementProps = &StandingMovProps;
		MovComponent->MaxWalkSpeed = CurrentMovementProps->MaximumSpeed;
		MovComponent->MaxAcceleration = CurrentMovementProps->MaximumAcceleration;

		break;

	default:
		CurrentMovementProps = nullptr;
		break;
	}
}

void UPS_LocState_Grounded::ComputeCrouchStanceVariables(float CurrentSpeed)
{
	Stride = FMath::GetMappedRangeValueClamped(FVector2D(0.0, CurrentMovementProps->MaximumSpeed), FVector2D(0.0f, 1.0f), CurrentSpeed);
}

void UPS_LocState_Grounded::ComputeNormalStanceVariables(float CurrentSpeed)
{
	// Stride. If first check fails, we are sprinting.
	if(CurrentSpeed <= StandingMovProps.MaximumSpeed)
		Stride = FMath::GetMappedRangeValueClamped(FVector2D(0.0, CurrentMovementProps->MaximumSpeed), FVector2D(0.0f, 0.50f), CurrentSpeed);
	else
		Stride = FMath::GetMappedRangeValueClamped(FVector2D(CurrentMovementProps->MaximumSpeed, SprintingMaximumSpeed), FVector2D(0.50f, 1.0f), CurrentSpeed);

	// If sprinting, we check direction we are moving and set variables accordingly.
	UpdateSprintingValues();

	if (!CurrentMovementProps || !CurrentMovementProps->ModifierVelocityReductionOnTurningBasedOnRotationDelta)
		return;

	// Acceleration.
	float VelDesirDirAbsDelt = FMath::Abs(LocController->GetDeltaBetweenVelocityAndFacingDir());
	float GroundFrictionModifier = CurrentMovementProps->ModifierVelocityReductionOnTurningBasedOnRotationDelta->GetFloatValue(VelDesirDirAbsDelt);
	float AccelerationModifier = CurrentMovementProps->ModifierAccelerationReductionOnTurningBasedOnRotationDelta->GetFloatValue(VelDesirDirAbsDelt);
	MovComponent->GroundFriction = MovComponent->GroundFriction * GroundFrictionModifier;
	MovComponent->MaxAcceleration = MovComponent->MaxAcceleration * AccelerationModifier;
}

// Called when rotation mode changes.
void UPS_LocState_Grounded::OnChangedRotationMode(TEnumAsByte<ELocomotionRotationModes> OldRotationMode, TEnumAsByte<ELocomotionRotationModes> NewRotationMode)
{
	SetRotationMode(NewRotationMode);
}

void UPS_LocState_Grounded::SetRotationMode(TEnumAsByte<ELocomotionRotationModes> NewRotationMode)
{
	switch (NewRotationMode)
	{
	case ELocomotionRotationModes::CameraFacing:
		PtrForRotationModeFunctionToUse = &UPS_LocState_Grounded::UpdateCameraFacingVariables;
		break;
	case ELocomotionRotationModes::VelocityFacing:
		AnimInstance->SetIntendedFootPosition(EAnimationFeetPosition::USES_BOTH_FEET);
		PtrForRotationModeFunctionToUse = nullptr;
		break;

	default:
		break;
	}
}

void UPS_LocState_Grounded::UpdateCameraFacingVariables(float DeltaTime)
{
	// Update cardinal direction if we are using ELocomotionRotationModes::CameraFacing
	LocController->GetCardinalDirectionUpdater().UpdateMovementDirection(LocController->GetDeltaBetweenVelocityAndFacingDir());
}