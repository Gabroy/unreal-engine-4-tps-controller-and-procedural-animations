#include "PS_LocState_InAir.h"
#include "../../../PS_Character.h"
#include "../PS_Human_LocomotionController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PS_LocState_Interface.h"
#include "../../Animations/PS_Human_AnimationInstance.h"
#include "Components/SkeletalMeshComponent.h"

void UPS_LocState_InAir::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_InAir::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_InAir::Init_Implementation LocController Returned Null"));
		return;
	}

	MovComponent = Cast<UCharacterMovementComponent>(Actor->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!MovComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_LocState_InAir::Init_Implementation MovComponent Returned Null"));
		return;
	}

	USkeletalMeshComponent* SKMesh = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SKMesh)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_LocState_InAir::Init_Implementation AnimInstance Returned Null"));
			return;
		}
	}
}

void UPS_LocState_InAir::OnEnter_Implementation()
{
	// Disable leg IK while on air.
	LocController->SetLegIKEnabledStatus(false);

	// Get sure we also stop walking if we were doing so.
	CharacterOwner->DoStopWalk();

	// If we were crouching when we entered this state, we get sure to uncrouch.
	// If we were sprinting, we stop. We also have this last check in grounded state
	// as, normally, we will stop sprinting in all states.
	ELocomotionStance CurrentStance = LocController->GetLocomotionStance();
	if (CurrentStance == ELocomotionStance::Crouch)
		CharacterOwner->DoUncrouch();
	else if (LocController->GetWantsToSprint())
		CharacterOwner->DoStopSprint();
}

void UPS_LocState_InAir::Tick_Implementation(float DeltaTime)
{
	if (!LocController) return;

	FlailingFactor = FlailingArmsCurve->GetFloatValue(LocController->GetVerticalSpeed());

	// Updates the maximum acceleration, if we are jumping, this only controls how fast we rotate in air.
	MovComponent->MaxAcceleration = InAirControlFactorCurve->GetFloatValue(LocController->GetSpeed());

	// Ignore rotation if we are doing some kind of root motion.
	if (LocController->IsPlayingRootMotion())
		return;

	// If moving, allow rotation in air.
	if (LocController->GetIsMoving())
		OnMovingUpdateRotation(DeltaTime);
}

void UPS_LocState_InAir::OnExit_Implementation()
{
	if(MovComponent->IsMovingOnGround())
		OnLanding(LocController->GetSpeed());
}

/* In Air Landing Animations */

void UPS_LocState_InAir::OnLanding(float CurrentSpeed)
{
	FlailingFactor = FlailingArmsCurve->GetFloatValue(LocController->GetVerticalSpeed());

	// Check if we can do rolling montage.
	bool HasFlailForRolling = FlailingFactor >= InAirMinFlailForRolling && FlailingFactor <= InAirMaxFlailForRolling;
	if (HasFlailForRolling)
	{
		bool HasSpeedForRolling = CurrentSpeed >= InAirMinSpeedForRolling;
		if (HasSpeedForRolling)
		{
			CharacterOwner->DoRolling();
			return;
		}
	}

	// Otherwise, check if we can do a landing animation. Any one.

	// There are no landing animations. Return.
	if (LandingSequences.Num() == 0)
		return;

	CandidateLandingAnims.Empty(CandidateLandingAnims.Max());

	for (int i = 0; i < LandingSequences.Num(); ++i)
	{
		const FInAirLandingAnimationSequence& LandingAnim = LandingSequences[i];
		bool MeetsFlailLimits = FlailingFactor <= LandingAnim.MaxFlailValueToPlay && FlailingFactor >= LandingAnim.MinFlailValueToPlay;
		if (!MeetsFlailLimits)
			continue;
		bool MeetsSpeedLimits = CurrentSpeed <= LandingAnim.MaxSpeedToPlay && CurrentSpeed >= LandingAnim.MinSpeedToPlay;
		if (!MeetsSpeedLimits)
			continue;
		CandidateLandingAnims.EmplaceAt(CandidateLandingAnims.Num(), i);
	}

	// Get a random climbing animation from the possible ones.
	int AnimToPlayIndex = -1;
	if (CandidateLandingAnims.Num() > 0)
	{
		AnimToPlayIndex = FMath::RandRange(0, CandidateLandingAnims.Num() - 1);
		AnimToPlayIndex = CandidateLandingAnims[AnimToPlayIndex];
	}
	else
		return;

	// If we are moving and we have an upper body montage, we will play it instead of the upper boddy one.
	// Will prevent foot sliding while moving.
	const FInAirLandingAnimationSequence& LandingAnim = LandingSequences[AnimToPlayIndex];
	if (CurrentSpeed >= 1.0f && LandingAnim.UpperBodyMontage)
	{
		AnimInstance->PlayInAirLandingMontage(LandingAnim.UpperBodyMontage);
		return;
	}

	// Otherwise, we play the normal animation as a sequence.
	AnimInstance->PlayInAirLandingAnimation(LandingAnim.InAirLandingSequence, LandingAnim.BlendOutTime, LandingAnim.BlockMovementOnStart, LandingAnim.FeetPositionOfAnim);
}

void UPS_LocState_InAir::OnMovingUpdateRotation(float DeltaTime)
{
	// Get rotation mesh speed.
	float CharacterMeshRotationSpeed = CharacterMeshRotationSpeedCurve->GetFloatValue(LocController->GetSpeed());
	LocController->UpdateTargetRotationBasedOnRotationMode();
	LocController->UpdateCharacterMeshRotation(DeltaTime, CharacterMeshRotationSpeed);
}