// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_State.h"
#include "PS_LocState_Interface.h"
#include "PS_LocState_Grounded.generated.h"

class APS_Character;
class UPS_Human_LocomotionController;
class UCharacterMovementComponent;
class UPS_Human_AnimationInstance;

UCLASS()
class TPS_API UPS_LocState_Grounded : public UFSM_State, public IPS_LocState_Interface
{
protected:

	GENERATED_BODY()

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	/* Character Stance Properties. */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FGroundedLocomotionMovementProperties CrouchedMovProps;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FGroundedLocomotionMovementProperties StandingMovProps;

	FGroundedLocomotionMovementProperties * CurrentMovementProps = nullptr;

	// Rotation speed.
	// Curve used to control  how fast the character mesh rotates to face the
	// direction it should face (velocity or camera) while on ground.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	UCurveFloat* GroundedCharacterRotationBySpeedCurve = nullptr;

	// Modifier from 1.0f upwards to increase rotation speed when difference is higher.
	// Should be combine with speed curve. While speed curve reduces velocity, we increase
	// the rotation speed.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	UCurveFloat* GroundedCharacterRotationModByDeltaMeshRotationAndVelocity = nullptr;

	// Sprinting variables.
	bool WantsToSprint = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float SprintingMaximumSpeed = 600.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	float SpritingMaximumAcceleration = 1200.0f;
	
	// Max delta forward that allows the player to sprint.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float MaxDeltaCanSprint = 30.0f;

	// Standing foot.

	// Foot being used right now. If false, we are using the left foot.
	// Used by the CameraFacing movement mostly.
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool IsUsingRightFoot = false;

	/* Stride. */
	
	// Used in ABP to interpolate between animations.
	// Strides represent how far away the feets move from the center. Faster a player goes,
	// bigger this value becomes.
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	float Stride = 0.0f;

	/* Braking and Pivot Turning variables */

	// Returns true if the character is no receiving any movement input anymore.
	// This mean it's stopping.
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
	bool IsBraking = false;

	// Set to true or false in the Tick function, used by ABP to control if it should play pivoting
	// animations or not.
	UPROPERTY(BlueprintReadOnly, Category = "Pivoting")
	bool IsPivoting = false;

	// Necessary degrees to consider we are pivoting and should play the necessary animation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pivoting")
	float DegreesToConsiderIsPivoting = 130.0f;

	// From 0.0f to 1.0f, how much speed is necessary for pivoting animation to play.
	// The speed is calculated from the max speed in standing stance.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pivoting")
	float NormalizedStandingSpeedForPivotingAnimToPlay = 0.5f;

	// Speed at which we allow turning animations to start playing, allows avoiding
	// playing pivoting animations when moving very slow, which doesn't make much sense.
	float MinSpeedForPivotingAnimationToPlay = 0.0f;

	/* State variables */

	/* Ptr to function that is executed based on the current stance. */
	typedef void (UPS_LocState_Grounded::* StanceFunctionPtr)(float);
	StanceFunctionPtr PtrToStanceFunctionToUse = nullptr;

	/* Rotation mode variables */

	/* Ptr to function that is executed based on the current stance. */
	typedef void (UPS_LocState_Grounded::* RotationModeFunctionPtr)(float);
	RotationModeFunctionPtr PtrForRotationModeFunctionToUse = nullptr;

public:

	/* Methods. */

	virtual void Init_Implementation();

	virtual void OnEnter_Implementation();

	virtual void OnExit_Implementation();

	virtual void Tick_Implementation(float DeltaTime);

	// Interface function. Called by the FSM locomotion machine  when locomotions stance changes.
	void OnChangedStance(TEnumAsByte<ELocomotionStance> OldStance, TEnumAsByte<ELocomotionStance> NewStance) override;

	void DoToggleStandingFoot() override;

	/* Walk functions */

	// Interface function. Activate walking function.
	void SetWalk(bool EnableWalk) override;

	// Called when changing stances. If walking is active, it will change the maximum velocity.
	void UpdateWalkingValues();

	/* Aim function */

	/* Sprint function */

	// Returns true if we are actually sprinting. Override of interface.
	bool IsSprinting() const override;

	void UpdateSprintingValues();

	/* Turning functions */

	// Returns true if we can perform turning behaviour.
	virtual bool CanTurn();

protected:

	/* General */

	// Called to update rotation while moving.
	inline void OnMovingUpdateRotation(float DeltaTime);

	// Returns true if we can sprint.
	inline bool CanSprint() const;

	inline bool CheckIsBraking();

	inline bool CheckIsPivoting(float MinSpeedToConsiderPivoting);

	/* Stance functions */

	void SetStance(TEnumAsByte<ELocomotionStance> NewStance);

	void SetStanceSpeedValues(TEnumAsByte<ELocomotionStance> NewStance);

	void ComputeCrouchStanceVariables(float CurrentSpeed);

	void ComputeNormalStanceVariables(float CurrentSpeed);

	/* Rotation specific functionality */

	void OnChangedRotationMode(TEnumAsByte<ELocomotionRotationModes> OldRotationMode, TEnumAsByte<ELocomotionRotationModes> NewRotationMode) override;

	void SetRotationMode(TEnumAsByte<ELocomotionRotationModes> NewRotationMode);

	void UpdateCameraFacingVariables(float DeltaTime);
};
