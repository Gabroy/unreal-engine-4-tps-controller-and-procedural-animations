#pragma once

#include "CoreMinimal.h"
#include "../../../../../Base/FSM/FSM_State.h"
#include "PS_LocState_Interface.h"
#include "../PS_Locomotion_Structs_DistanceMatching.h"
#include "../../Mantling/PS_Human_ClimbingStruct.h"
#include "PS_LocState_Climb_Structs.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_LocState_Climb.generated.h"

class APS_Character;
class UPS_Human_LocomotionController;
class UCharacterMovementComponent;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UPS_Human_AnimationInstance;
class UPS_Human_UpperTorsoIKController;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_LocState_Climb : public UFSM_State, public IPS_LocState_Interface
{

protected:

	GENERATED_BODY()

	/* Components */

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY()
	UCapsuleComponent * CapsuleComponent = nullptr;

	UPROPERTY()
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	/* Climbing - Checks */

	// Maximum distance we can be from an edge to grab to it.
	UPROPERTY(EditAnywhere, Category = "Checks")
	float MaxDistanceToGrabToLedge = 95.0f;

	// Height offset from the base of the player from where the ledge check (both while in air or hanging), is performed.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float HeightToPerformLedgeCheckAt = 168.0f;

	// Maximum height difference between current and goal surface we can climb. Bigger differences can't be climbed.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float MaxHeightLedgeDifferenceCanClimb = 10.0f;
	const int MaxHeightChecksIterations = 4;

	// Depth offset to check can hang to ledge. Should be a small value, no bigger than the part of the hand that should be hanging to a ledge.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float LedgeHandCheckDepthOffset = 5.0f;

	// Bigger values will allow hanging to more curved surfaces.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float MaxSurfaceAngleWeCanHangTo = 25.0f;

	// Angle at which we should start considering we are hanging to step surface.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float AngleToConsiderStepSurface = 5.0f;

	// Height starting from the feet of the player, from where the check to see if we can place feet on surface.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float HeightToPerformCanPlaceFeetInSurfaceCheck = 25.0f;

	// Used by LateralMovementCheck function to control where we perform test to see if there is an object to our side.
	// If 0.0f, we use the capsule radius as value. Negative values will substract from capsule value.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float LateralCheck_ForwardOffset = 0.0f;
	// Used by LateralMovementCheck function to control where we perform test to see if there is an object to our side.
	// If 0.0f, we use the capsule radius as value. Negative values will substract from capsule value.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Checks")
	float LateralCheck_SideOffset = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	TEnumAsByte<ECollisionChannel> ClimbingChannel = ECollisionChannel::ECC_GameTraceChannel2;
	ETraceTypeQuery ClimbingTraceQuery;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Debug")
	TEnumAsByte<EDrawDebugTrace::Type> Debug;

	// Half height of the character when not crouched. Fetched at the beginning.
	float CharacterHalfHeight = 0.0f;

	// Set up at the beginning. Diff between total height of capsule and HeightToPerformLedgeCheckAt.
	float HeightDiffBetweenCharHeightAndHeightToPerformCheck = 0.0f;

	// Size of the radius of the sphere trace checks launched in different parts of the code.
	const float SphereCheckRadius = 5.0f;

	/* Climbing - General */

	/* Ptr to function that is executed based on the current climbing substate we are at. */
	typedef void (UPS_LocState_Climb::* ClimbinFunctionPtr)(float);
	ClimbinFunctionPtr CurrentSubStateBehaviour = nullptr;

	// Current climbing state we are at.
	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<EClimbingStates> CurrentClimbingState = EClimbingStates::MovingToHang;

	/* Moving To Hang Substate variables */

	// Minimum velocity at which we will move to the hanging position.
	// Velocity may be bigger if the player speed is bigger.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MovingToHang")
	float VelocityToHangingPosition = 100.0f;

	// Hanging animations that can be played when grabbing to a ledge. One is picked up
	// when we start MoveToHang substate.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "MovingToHang")
	TArray<FHangToLedgeAnimations> HangingAnimations;

	// Start and Goal rotation we will end at after finishing hanging to the object.
	FRotator HangingStartRotation, HangingGoalRotation;

	// Distant matching struct used to control the moving to hang animation playing.
	FDistanceMatchingVectorValues HangingPosDM;

	/* Hanging Variables */

	UPROPERTY(EditAnywhere, Category = "Hanging")
	FName RightHandSocket = "Hand_RSocket";

	UPROPERTY(EditAnywhere, Category = "Hanging")
	FName LeftHandSocket = "Hand_LSocket";

	UPROPERTY(EditAnywhere, Category = "Hanging")
	FName CurveMovementClimbing = "HangingCanMove";

	UPROPERTY(EditAnywhere, Category = "Hanging")
	FClimbingMovementProperties StandingMovement;

	UPROPERTY(EditAnywhere, Category = "Hanging")
	FClimbingMovementProperties CrouchedMovement;

	UPROPERTY(EditAnywhere, Category = "Hanging")
	float HangingRotationSpeed = 2.0f;

	// Current movement.
	FClimbingMovementProperties * CurrentMovementProps = nullptr;

	// Controls if the player is walking.
	float CurrentSpeedModifier = 1.0f;

	// Original capsule radius. Jumping montage modify capsule to avoid collision issues.
	// We return to it once we leave the state or finish the montage.
	float OriginalCapsuleRadius = 0.0f;

	UPROPERTY(BlueprintReadOnly)
	float Stride = 0.0f;

	// Ptr to function that is executed while TryMove
	typedef void (UPS_LocState_Climb::* HorizontalMovementFunctionPtr)(float, float);
	HorizontalMovementFunctionPtr CurrentHorMovBehaviour = nullptr;

	/* Climbing variables */

	UAnimMontage * ClimbingMontagePlaying = nullptr;
	bool ClimbingUpwards = true;

	// Interpolator used for climbing hanging.
	FHangingClimbingInterpolation ClimbingHangInterpolator;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing")
	FName RootMotionXCoord = "RootMotionX";
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing")
	FName RootMotionYCoord = "RootMotionY";
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing")
	FName RootMotionZCoord = "RootMotionZ";

	UPROPERTY(EditAnywhere, Category = "Climbing")
	TArray<UAnimMontage *> ClimbingFromStandingHanging;
	UPROPERTY(EditAnywhere, Category = "Climbing")
	TArray<UAnimMontage *> ClimbingFromStandingFreeHanging;
	UPROPERTY(EditAnywhere, Category = "Climbing")
	TArray<UAnimMontage *> ClimbingFromCrouched;

	/* Try Climbing variables */

	UPROPERTY(EditAnywhere, Category = "Try Climbing")
	TArray<UAnimMontage *> TryClimbingFromStandingHanging;
	UPROPERTY(EditAnywhere, Category = "Try Climbing")
	TArray<UAnimMontage *> TryClimbingFromStandingFreeHanging;
	UPROPERTY(EditAnywhere, Category = "Try Climbing")
	TArray<UAnimMontage *> TryClimbingFromCrouched;

	UAnimMontage * TryClimbingMontagePlaying = nullptr;
	bool TryClimbMontagePaused = true;

	/* Inward turning and outward turning variables. */

	FVector TurnStartPosition, TurnGoalPosition;
	FRotator TurnStartRotation, TurnGoalRotation;
	bool InwardSideOrientationIsRight = false;

	UAnimMontage * CurrentTurningMontagePlaying = nullptr;
	UPROPERTY(EditAnywhere, Category = "Turning")
	UAnimMontage * ClimbingTurnLeft;
	UPROPERTY(EditAnywhere, Category = "Turning")
	UAnimMontage * ClimbingTurnRight;

	/* Jump Montages */

	FJumpingFromHangAnimation * CurrentJumpDirAnimation = nullptr;

	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	FJumpingFromHangAnimation JumpUpwards;
	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	UAnimMontage * JumpFromHanging;

	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	FJumpingFromHangAnimation JumpLeftSide;
	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	FJumpingFromHangAnimation JumpRightSide;

	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	UAnimMontage* StandingJumpLeftSide;
	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	UAnimMontage* StandingJumpRightSide;
	bool IsPlayingStandingMontage = false;
	bool StandingMovingRight = false;

	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	FJumpingFromHangInwardsAnimation InwardsJumpLeftSide;
	UPROPERTY(EditAnywhere, Category = "Jump Montages")
	FJumpingFromHangInwardsAnimation InwardsJumpRightSide;
	FJumpingFromHangInwardsAnimation * CurrentInwardJump = nullptr;

	/* Leaving */

	UPROPERTY(EditAnywhere, Category = "Leaving")
	TArray<UAnimMontage *> LeavingHangingFromStanding;
	UPROPERTY(EditAnywhere, Category = "Leaving")
	TArray<UAnimMontage *> LeavingHangingFromFreeHanging;
	UPROPERTY(EditAnywhere, Category = "Leaving")
	TArray<UAnimMontage *> LeavingHangingFromCrouched;

public:

	/* Methods. */

	virtual void Init_Implementation();

	virtual void OnEnter_Implementation();

	virtual void OnExit_Implementation();

	virtual void Tick_Implementation(float DeltaTime);

	/* In Air Grab To Ledge */

	// Performs the ledge grab checks to see if we can grab to a ledge.
	bool TryLedgeGrab();

	/* Moving to Hang */

	// Called from Locomotion Controller when we enter this state.
	void MoveToHangingPosition(const FVector& PositionToHangTo, const FVector& ObstacleNormal, float DeltaAngleWithSurface);

	/* Hanging */

	void TryMove(float Axis);

	void TryClimb(float Axis);

	/* Jump Montages */

	void PerformJumpingFromHanged();

	void PerformVerticalJump();

	void PerformHorizontalJump(bool Right);

	/* Movement actions for climbing */

	void Crouch();

	void Stand();

	void Walk();

	void StopWalk();

	/* Leave Climbing */

	// Can be called from outside whenever we wish to leave climbing.
	// Montage throws a notify when it's time to leave the climbing stage and start falling, therefore changing the locomotion state.
	void LeaveHanging(ELocomotionStance CurrentStance, bool PlayLeavingMontage = true);

	/* Walk functions */

	// Interface function. Activate walking function.
	void SetWalk(bool EnableWalk) override;

	/* Stance functions */

	// Interface function. Called by the FSM locomotion machine  when locomotions stance changes.
	void OnChangedStance(TEnumAsByte<ELocomotionStance> OldStance, TEnumAsByte<ELocomotionStance> NewStance) override;

	/* Getters */

	float GetLedgeHandCheckDepthOffset() const { return LedgeHandCheckDepthOffset; }
	float GetHeightToPerformLedgeCheckAt() const { return HeightToPerformLedgeCheckAt; }
	float GetMaxDistanceToGrabToLedge() const { return MaxDistanceToGrabToLedge; }
	const TArray<FHangToLedgeAnimations> & GetHangingAnimations() const { return HangingAnimations; }
	const TArray<UAnimMontage*> & GetClimbingFromStandingHanging() const { return ClimbingFromStandingHanging; }
	const TArray<UAnimMontage*> & GetClimbingFromStandingFreeHanging() const { return ClimbingFromStandingFreeHanging; }
	const TArray<UAnimMontage*> & GetClimbingFromCrouched() const { return ClimbingFromCrouched; }
	const TArray<UAnimMontage*> & GetTryClimbingFromStandingHanging() const { return TryClimbingFromStandingHanging; }
	const TArray<UAnimMontage*> & GetTryClimbingFromStandingFreeHanging() const { return TryClimbingFromStandingFreeHanging; }
	const TArray<UAnimMontage*> & GetTryClimbingFromCrouched() const { return TryClimbingFromCrouched; }
	const TArray<UAnimMontage*> & GetLeavingHangingFromFreeHanging() const { return LeavingHangingFromFreeHanging; }
	const TArray<UAnimMontage*> & GetLeavingHangingFromCrouched() const { return LeavingHangingFromCrouched; }
	const UAnimMontage * GetClimbingTurnLeft() const { return ClimbingTurnLeft; }
	const UAnimMontage * GetClimbingTurnRight() const { return ClimbingTurnRight; }

protected:

	/* Try Ledge Grab */

	bool ForwardCheckCanGrabToLedge(float MaxCheckDistance, float HeightToPerformCheckAt, FHitResult & ObstacleHit);

	bool ForwardAndThicknessCheck(float MaxCheckDistance, float HeightToPerformCheckAt, float SideOffset, FHitResult& ObstacleHit, bool& ThicknessCheckFailed);

	bool HeightCheckCanGrabToLedge(float HeightToPerformCheck, const FHitResult & ForwardHitInformation, FVector & HeightHitPosition, FVector & LedgePos);

	bool LedgeObstacleCheck(const FHitResult & ForwardHitInformation, const FVector & HeightHitPosition, FVector & LedgePos, FVector & LedgeNormal);

	/* Moving to Hang */

	// Selects a move to hanging animation and passes it to the anim instance to be played. Called by climbing when MoveToHanging is started.
	bool ChooseMoveToHangingAnimation();

	// Here we perform the update to get to the hanging position.
	void UpdateMovingToHangState(float DeltaTime);

	/* Hanging */

	void StartHangingState();

	/* Lateral movement */

	void TryLateralMovement(float Axis, float MovementAxisAbsValue);
	// Moves player to the given goal through interpolation.
	void PerformLateralMovement(const FVector& NewGoalPosition, const FRotator& NewGoalRotation, float Axis, float MovementAxisAbsValue);

	/* Inward behaviour */

	// Performs inward movement behaviour. Ideally this would be the same as perform turn animation. We would simply play an animation with root motion and let it
	// rule the movement. But I'm doing it programatically as I lack one.
	void PerformInwardMovement(float Axis, float MovementAxisAbsValue);
	void StartInwardMovement(const FVector& MovementGoalPosition, const FRotator& MovementGoalRotation, bool IsMovingRight, float Axis, float MovementAxisAbsValue);
	void StopInwardMovement(float Axis, float MovementAxisAbsValue);

	/* Turning behavior */

	// Performs a turning.
	void PerformTurnAnimation(bool IsRightTurn, const FVector & TurningEndPosition, const FRotator & TurningEndRotation);
	// Choses a turning animation and plays it. Calls anim instance.
	UAnimMontage * PlayClimbingTurnMontage(bool IsRightTurn);
	// Called by the animation instance class once the turning animation throws a notify indicating half a turn has been performed.
	// Sets UpdateTurning to be called each tick. Movement is now ruled by this function and not root motion.
	UFUNCTION()
	void OnHalfTurnPerformed();
	// Called when turning is done. Returns control to player.
	UFUNCTION()
	void OnTurnMontageEnded();

	/* Climbing */

	// Starts climbing substate and animations if it is possible to climb the obstacle. Character starts climbing object.
	void StartClimb();
	// Plays a climbing montage depending on the current stance.
	bool PlayClimbingMontage();
	// Allows modifying the climbing montage playrate so we can reverse the montage and reverse climbing.
	void SetClimbingMontagePlayrate(bool Backwards);
	// Set while in climbing substate. Controls the climbing end position. Uses some curves with the extracted root motion of the montage
	// and gets sure the player ends in the final destination of the root motion. We do this instead of using root motion from the montage
	// as collisions with objects may affect the final position we end up while climbing and can make the player appear floating.
	void UpdateClimbing(float DeltaTime);
	// Called when climbing montage blends out.
	UFUNCTION()
	void OnClimbBlendingOutMontage(UAnimMontage* Montage, bool Interrupted);

	/* Try Climbing */

	// Starts the try climbing substate.
	void StartTryClimb();
	// Plays a try climbing montage depending on the current stance.
	bool PlayTryClimbingMontage();
	// Allows modifying the try climbing montage playrate so we can reverse the montage and reverse climbing.
	void SetTryClimbingMontagePlayrate(bool Backwards);
	// Called from AnimBP when TryClimbing starts blending out, we return to normal position.
	UFUNCTION()
	void OnTryClimbingStartBlendOut();
	// Called from AnimBP when TryClimbing reaches the top. Stops the montage before it blends out completely.
	UFUNCTION()
	void OnTryClimbingStop();
	// Called when try climbing montage is done. Returns control to player.
	UFUNCTION()
	void OnTryClimbEnded(UAnimMontage* Montage, bool Interrupted);

	/* Jumping from hang */

	void PerformHorizontalJumpStanding(bool Right);
	void PerformHorizontalJumpCrouched(bool Right);

	void PerformHorizontalJumpCrouchedNormal(bool Right);

	void StartJumpingHorizontalStanding(bool Right);
	void UpdateWhileJumpingHorizontalStanding(float DeltaTime);
	
	void PerformHorizontalJumpCrouchedInward(bool Right);
	UFUNCTION()
	void StartInwardsHorizontalJump();
	UFUNCTION()
	void OnInwardsHorizontalJumpEnds();

	UFUNCTION()
	void OnJumpingFromHangMontageBlendingOut(UAnimMontage* Montage, bool Interrupted);
	UFUNCTION()
	void OnJumpingFromHangMontageEnded(UAnimMontage* Montage, bool Interrupted);

	/* Leaving climbing */

	// Leave hanging through a leaving montage.
	void ChooseAndPlayLeavingMontage(ELocomotionStance CurrentStance);

	// Leave hanging without playing a special leaving montage.
	void LeaveWithoutMontage();

	// Called when leave climbing is done.
	UFUNCTION()
	void OnLeaveClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	/* Climbing states helper functions */

	float ComputeSurfaceForwardStep(const FVector& SurfaceNormal);
	float ComputeSurfaceHeightStep(const FVector& SurfaceNormal);

	/* Hanging checks */

	// Performed to check if we can climb a surface. Returns true if possible.
	bool CanClimbCheck();

	// Can place feet on surface check.
	bool CanPlaceFeetOnSurface(const FVector& StartPos, const FVector& CheckDirection);
	// Can place feet check from goal hanging pos.
	bool CanPlaceFeetOnSurfaceMoveToHangingPos();
	// Can place feet on surface from character pos.
	bool CanPlaceFeetOnSurfaceCheck();

	// Performs check from player pos to goal pos. If we hit an obstacle, returns false, we can't move there.
	bool CanMoveToPosition(const FVector & PosToMoveTo);

	// Performs check from hand to see if we find a place we can climb to.
	bool HandHitCheck(bool RightCheck, FHitResult & HangVerticalHit);
	bool PerformHandHitTrace(const FVector & BoneHandPosition, FHitResult & HangVerticalHit);

	// Performs a side check to one of the sides (left or right).
	bool LateralMovementCheck(bool RightCheck, FVector& HangPosition, FRotator& HangRotation);

	// Performs a check to see if we can inward turn.
	bool InwardTurnCheck(bool RightCheck, FVector& HangPosition, FRotator& HangRotation, float DistancePercentBasedOnCapsuleRadius = 1.25f);

	// Performs a check to see if we can perform a turn.
	bool OutwardTurnCheck(bool RightCheck, FVector& TurningEndPosition, FRotator& TurningEndRotation);

	// Returns false if no hands is on the obstacle.
	bool CheckHandsAreOnObstacle(bool & RightHandHitFound, bool& LeftHandHitFound);

	/* State and Stance functions */

	void UpdateCanPlaceFeetOnSurface();

	void SetNewClimbState(EClimbingStates NewClimbState);

	bool CanChangeStance(TEnumAsByte<ELocomotionStance> CurrentStance, TEnumAsByte<ELocomotionStance> StanceToChangeTo) override;

	void ChangeStance(TEnumAsByte<ELocomotionStance> NewStance);
};
