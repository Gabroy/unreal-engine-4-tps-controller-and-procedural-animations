#pragma once

#include "CoreMinimal.h"
#include "../PS_Locomotion_Structs_Enums.h"
#include "PS_LocState_Interface.generated.h"

/*
	Interface class that allow locomotion states to implement a serie of callbacks that are common in most
	states. If any of the behaviours are needed for the state, add this interface. Otherwise, you are free
	to ignore it completely.
*/

UINTERFACE(MinimalAPI, BlueprintType, meta = (CannotImplementInterfaceInBlueprint))
class UPS_LocState_Interface : public UInterface
{
	GENERATED_BODY()
};

class IPS_LocState_Interface
{
	GENERATED_BODY()

public:

	// Used to check if we can change an stance. Can be overriden by other states.
	UFUNCTION(BlueprintCallable)
	virtual bool CanChangeStance(TEnumAsByte<ELocomotionStance> CurrentStance, TEnumAsByte<ELocomotionStance> StanceToChangeTo)
	{
		switch (StanceToChangeTo)
		{
		case ELocomotionStance::Normal:
			if (CurrentStance == ELocomotionStance::Crouch)
				return true;
			break;
		case ELocomotionStance::Crouch:
			if (CurrentStance == ELocomotionStance::Normal)
				return true;
			break;
		default:
			break;
		}
		return false;
	}

	 // Called when locomotions stance changes.
	UFUNCTION(BlueprintCallable)
	virtual void OnChangedStance(TEnumAsByte<ELocomotionStance> OldStance, TEnumAsByte<ELocomotionStance> NewStance){}

	// Called when rotation mode changes.
	UFUNCTION(BlueprintCallable)
	virtual void OnChangedRotationMode(TEnumAsByte<ELocomotionRotationModes> OldRotationMode, TEnumAsByte<ELocomotionRotationModes> NewRotationMode) {}

	// Called when we enable or disable walking mode. Implement this function if you allow walking mode.
	UFUNCTION(BlueprintCallable)
	virtual void SetWalk(bool EnableWalk){}

	// Implement this function and set any necessary variables if you allow changing feet and need to use some variables for the
	// anim bp or any other functionality.
	UFUNCTION(BlueprintCallable)
	virtual void DoToggleStandingFoot(){}

	// Returns true if the state allows sprinting and the necessary conditions are met such that sprinting is actually happening.
	// This conditions might change depending on the locomotion state.
	UFUNCTION(BlueprintCallable)
	virtual bool IsSprinting() const { return false; }

	// Returns true if we can perform turning behaviour.
	UFUNCTION(BlueprintCallable)
	virtual bool CanTurn() { return false; }
};