#pragma once

#include "CoreMinimal.h"
#include "../../../../../Base/FSM/FSM_State.h"
#include "PS_LocState_InAir_Structs.h"
#include "PS_LocState_InAir.generated.h"

class APS_Character;
class UPS_Human_LocomotionController;
class UCharacterMovementComponent;
class UPS_Human_AnimationInstance;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_LocState_InAir : public UFSM_State
{
protected:

	GENERATED_BODY()

	/* Components */

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	/* Variables */

	// Controls the velocity at which the player velocity changes while in air.
	// Indexed by the horizontal speed of the character. From 0.0f onwards...
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "In Air")
	UCurveFloat * InAirControlFactorCurve;

	// Controls the rotation speed value that makes the character rotate to face the
	// targeting direction. This Curve is indexed based on the horizontal speed of the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "In Air")
	UCurveFloat * CharacterMeshRotationSpeedCurve = nullptr;

	// Controls the flailing animation weight.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "In Air")
	UCurveFloat * FlailingArmsCurve = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "In Air")
	float InAirMinFlailForRolling = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "In Air")
	float InAirMaxFlailForRolling = 0.65f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "In Air")
	float InAirMinSpeedForRolling = 20.0f;

	/* Jump state animator variables. */

	// Controls the flailing animation that gets played while in Falling state.
	UPROPERTY(BlueprintReadWrite)
	float FlailingFactor = 0.0f;

	/* In Air Landing Montages */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "In Air")
	TArray<FInAirLandingAnimationSequence> LandingSequences;

	TArray<int> CandidateLandingAnims;

public:

	/* Methods. */

	virtual void Init_Implementation();
	
	virtual void OnEnter_Implementation();

	virtual void Tick_Implementation(float DeltaTime);

	virtual void OnExit_Implementation();

private:

	/* In Air Landing Animations */

	void OnLanding(float CurrentSpeed);

	void OnMovingUpdateRotation(float DeltaTime);
};
