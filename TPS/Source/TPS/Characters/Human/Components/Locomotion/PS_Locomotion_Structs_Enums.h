#pragma once

#include "PS_Locomotion_Structs_Enums.generated.h"

/*
 * Locomotion Stances.
 *
 * Locomotion stances represent the position of the character.
 * These are not actual states and might be used in more than one state.
 *
 */
UENUM(BlueprintType)
enum ELocomotionStance
{
	Normal, // Either running or walking, normal stance behavior.
	Crouch // Crouched.
};

/*
 * Locomotion Modes.
 *
 * Locomotion modes are the Locomotion FSM states responsible of implementing different locomotion behaviors.
 * These enums are mostly used by the Animation FSM to help it fetch only the necessary values it needs for its current frame.
 *
 */
UENUM(BlueprintType)
enum ELocomotionMode
{
	Grounded, // Characters rotates to face the direction of it's velocity.
	In_Air, // While character is in air.
	Climbing, // Climbing mode.
	Ragdoll
};

/*
 * Locomotion Rotation Methods.
 *
 * Locomotion rotation methods define how player rotation should be handled. This means the player should rotate to face
 * velocity (Velocity Facing) or the camera direction (camera facing).
 *
 */
UENUM(BlueprintType)
enum ELocomotionRotationModes
{
	VelocityFacing, // Character will rotate to face the velocity is moving to.
	CameraFacing    // Character will rotate to face the camera direction.
};

/*
 * Locomotion Movement Directions enum
 *
 * Locomotion movement directions, used by Camera Facing.
 *
 */
UENUM(BlueprintType)
enum ELocomotionMovementDirections
{
	M_Forward,
	M_Backwards,
	M_Leftwards,
	M_Rightwards
};

/*
 * Locomotion Cardinal Directions struct.
 *
 * Locomotion movement directions. Store the necessary values for FMovementDirectionUpdater to
 * correctly compute the current direction.
 *
 */
USTRUCT(Blueprintable, BlueprintType)
struct FMovementDirection
{
	GENERATED_BODY()

	// Delta angle of the direction. Holds the delta angle of a given direction
	// so it can be used to blend animations in the ABP.
	UPROPERTY(BlueprintReadOnly)
	float DirectionDeltaAngle = 0.0f;

	// Angle to consider we are moving in a given direction.
	// Feel free to change it to better suit your animations but try to keep it a smaller bigger
	// than it's skip counterpart to avoid flickering in animation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float InsideDirectionAngle = 45.0f;

	// Angle to consider we are outside the current direction and should fetch another one.
	// Feel free to change it to better suit your animations but try to keep it a bit bigger
	// than it's inside counterpart to avoid flickering in animation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SkipDirectionAngle = 60.0f;

	/* Functions */

	bool IsAngleInsideDirection(float AngleBetweenFrontAndCurrentDir);

	bool IsAngleOutsideDirection(float AngleBetweenFrontAndCurrentDir);

private:

	friend struct FMovementDirectionUpdater;

	// Private values that are precomputed.
	float MaxInsideAngle = 0.0f;
	float MinInsideAngle = 0.0f;
	float MaxSkipAngle = 0.0f;
	float MinSkipAngle = 0.0f;
	bool IsBackwardsDirection = false;
};

USTRUCT(Blueprintable, BlueprintType)
struct FMovementDirectionUpdater
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ELocomotionMovementDirections> CurrentMovDir = ELocomotionMovementDirections::M_Forward;
	
	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ELocomotionMovementDirections> PreviousMovDir = ELocomotionMovementDirections::M_Forward;

	FMovementDirection * CurrentDirProps = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FMovementDirection ForwardDir;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FMovementDirection BackwardsDir;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FMovementDirection RightwardsDir;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FMovementDirection LeftwardsDir;

	void InitDirections();

	void UpdateMovementDirection(float DeltaBetweenVelocityAndFacingDir);

	TEnumAsByte<ELocomotionMovementDirections> GetDirectionFromAngle(float AngleBetweenFrontAndCurrentDir);

private:

	// Returns true if opposite direction.
	bool IsOppositeDirectionOfCurrentOne(float AngleBetweenFrontAndCurrentDir);

	// Called by UpdateMovementDirection when we need to get a new direction.
	void FetchNewDirection(float DeltaBetweenVelocityAndFacingDir);
};

/*
* Locomotion Grounded Movement.
* 
* Includes variables used for implementing grounded movement behavior. Each stance usually uses a different
* version of this struct.
* 
*/
USTRUCT(Blueprintable, BlueprintType)
struct FGroundedLocomotionMovementProperties
{
	GENERATED_BODY()

	// Maximum speed we can have in this stance.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaximumSpeed;
	
	// The maximum acceleration we have in this stance.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaximumAcceleration;

	// If we can walk in this stance, speed modifier that will be used when in walking mode.
	// If MaximumSpeed is 1000.0, and walking is 0.25, we will have 250.0 max speed while walking.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float WalkingModeSpeedModifier = 0.25f;

	// Modifier while in aiming mode.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AimingModeSpeedModifier = 0.6f;

	// Controls ground friction based on delta between current velocity and facing dir.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	UCurveFloat * ModifierVelocityReductionOnTurningBasedOnRotationDelta;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	UCurveFloat* ModifierAccelerationReductionOnTurningBasedOnRotationDelta;
};