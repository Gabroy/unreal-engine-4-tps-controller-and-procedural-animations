#include "PS_Locomotion_Structs_DistanceMatching.h"
#include "UnrealMathUtility.h"

void FDistanceMatchingFloatValues::Init(float NewStartValue, float NewGoalValue)
{
	StartValue = NewStartValue;
	GoalValue = NewGoalValue;
}

float FDistanceMatchingFloatValues::GetFactor(float CurrentValue) const
{
	return FMath::GetMappedRangeValueClamped(FVector2D(StartValue, GoalValue), FVector2D(0.0f, 1.0f), CurrentValue);
}

void FDistanceMatchingVectorValues::Init(const FVector& NewStartValue, const FVector& NewGoalValue)
{
	GoalValue = NewGoalValue;
	StartDistanceToGoal = FVector::DistSquared(NewStartValue, GoalValue);
}

float FDistanceMatchingVectorValues::GetFactor(const FVector & CurrentValue) const
{
	float CurrentDistance = FVector::DistSquared(CurrentValue, GoalValue);
	return FMath::GetMappedRangeValueClamped(FVector2D(StartDistanceToGoal, 0.0f), FVector2D(0.0f, 1.0f), CurrentDistance);
}