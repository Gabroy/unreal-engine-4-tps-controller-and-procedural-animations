#pragma once

/*
* 
* A Distance matching struct used to compute a float value factor from two other floats
* a start and goal one.
* 
* This factor is mainly used by Distance Matched animations to decide which frame to play.
* Nevertheless could be also useful for other logic.
* 
* It basically returns a value between 0.0f and 1.0f based on how close to the goal value we are currently.
* 
*/
struct FDistanceMatchingFloatValues
{

private:

	float StartValue;

	float GoalValue;
	
public:

	void Init(float NewStartValue, float NewGoalValue);

	float GetFactor(float CurrentValue) const;

	float GetStartValue() const { return StartValue; }

	float GetGoalValue() const { return GoalValue; }
};

/*
* 
* Similar to previous FDistanceMatchingFloatValues struct but implemented for DM that uses FVector
* values to compute the final factor. This could be used for example to control DM animations through
* the use of a start and ending positions and passing the current position to get the actual factor.
* Right now it's unused though.
* 
*/
struct FDistanceMatchingVectorValues
{

private:

	FVector GoalValue;
	float StartDistanceToGoal;

public:

	void Init(const FVector& NewStartValue, const FVector& NewGoalValue);

	float GetFactor(const FVector & CurrentValue) const;

	const FVector & GetGoalValue() const { return GoalValue; }
};