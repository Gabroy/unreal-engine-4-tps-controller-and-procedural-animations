#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_Component.h"
#include "PS_Locomotion_Structs_Enums.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKStructs.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKController.h"
#include "PS_Human_LocomotionController.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnLocomotionBegunPlay)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnLocomotionChangedStance, ELocomotionStance)

class ACharacter;
class UCharacterMovementComponent;
class UPS_Human_HeadIKController;
class UPS_Human_UpperTorsoIKController;
class UPS_Human_LegIKController;
class UPS_LocState_Climb;

/*
* FSM responsible of controlling the locomotion behavior of human characters.
* 
* States inside the FSM implement different behaviors to simulate locomotion in different situations
* like movement while grounded, in air, or other possible states like swimming, in cover, ...
* 
* The logic inside this states might modify different components present in the character, like
* the character movement component variables or variables present in this FSM that are then used
* by the animation FSM to play the necessary animations. It might also call certain actions from the
* PS_Character for certain actions (like uncrouching if we start falling).
* 
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_LocomotionController : public UFSM_Component
{

	GENERATED_BODY()

public:

	FOnLocomotionBegunPlay BegunPlayCallback;
	FOnLocomotionChangedStance OnChangedStanceMulticast;

protected:

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	ACharacter * Owner;

	UPROPERTY(BlueprintReadOnly)
	AController * OwnerController;

	UPROPERTY(BlueprintReadOnly)
	UCharacterMovementComponent * MovementComp;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_HeadIKController * HeadIKComponent;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_UpperTorsoIKController * UpperTorsoIKComponent;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_LegIKController * LegIKComponent;

	/* Locomotion State Ptrs */

	UPROPERTY()
	UPS_LocState_Climb * ClimbingState = nullptr;

	/* State management */

	// If set to false, states won't be changed when movement mode from character component changes.
	// Can be used by states that desire to avoid this behavior. For now, unused.
	bool AllowChangeStateOnMovementChanged = true;

	/* General variables. */

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ELocomotionStance> CurrentStance;

	// Horizontal speed of the character.
	UPROPERTY(BlueprintReadOnly)
	float Speed;

	// Vertical speed of the character (when falling for example).
	UPROPERTY(BlueprintReadOnly)
	float VerticalSpeed;

	// True if the character is moving.
	UPROPERTY(BlueprintReadOnly)
	bool IsMoving;

	// True if the character was moving last frame.
	UPROPERTY(BlueprintReadOnly)
	bool WasMoving;

	// Rotation the character should rotate to.
	UPROPERTY(BlueprintReadOnly)
	FRotator CharacterTargetRotation;

	// Movement input vector. Direction character wants to go to.
	UPROPERTY(BlueprintReadOnly)
	FVector MovementInput;

	// Movement input rotation.
	UPROPERTY(BlueprintReadOnly)
	FRotator MovementInputRotation;

	// Last frame rotation speed. Update in Update Character Mesh Rotation. When Idle, 0.0f.
	UPROPERTY(BlueprintReadOnly)
	float LastFrameRotationSpeed = 0.0f;

	// Smoothed velocity vector, used for camera facing movement.
	UPROPERTY(BlueprintReadOnly)
	FRotator SmoothedVelocityRotation;

	// True if movement input length is > 0.0f.
	UPROPERTY(BlueprintReadOnly)
	bool HasMovementInput;

	// Last update velocity.
	UPROPERTY(BlueprintReadOnly)
	FVector LastUpdateVelocity;

	// Velocity vector rotation. Represent the angle in world space
	// character is moving to. Not necessary the same direction it should face to!
	UPROPERTY(BlueprintReadOnly)
	FRotator VelocityRotation;

	// Difference between current velocity vector and movement input vector in degrees.
	// Used for checking if player is turning and for controlling transitions in this animations.
	UPROPERTY(BlueprintReadOnly)
	float DeltaBetweenVelocityAndDesiredDirection;

	// Angle between the velocity of the character and the direction the character
	// faces. Used to control which animations get played (for example for strafing).
	UPROPERTY(BlueprintReadOnly)
	float DeltaBetweenVelocityAndFacingDir;

	// Angle between the desired direction of the character and the facing dir.
	UPROPERTY(BlueprintReadOnly)
	float DeltaBetweenDesiredDirAndFacingDir;

	// Actual acceleration vector of the character (character component is not real acceleration).
	// Computed from last frame velocity.
	UPROPERTY(BlueprintReadOnly)
	FVector AccelerationVector;

	// Value of the acceleration.
	UPROPERTY(BlueprintReadOnly)
	float AccelerationValue;

	UPROPERTY(BlueprintReadOnly)
	bool bIsWalking = false;

	// Set to true when we intend to sprint (normally the player has pressed the sprint key
	// or the AI has decided to sprint). This doesn't mean we are necessarily sprinting.
	// To check that use the function IsSprinting.
	UPROPERTY(BlueprintReadOnly)
	bool WantsToSprint = false;

	/* Target Rotation variables */
	
	// If true, we start player facing movement velocity.
	// If false, player will face the controller direction.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	TEnumAsByte<ELocomotionRotationModes> TargetRotationMode = ELocomotionRotationModes::VelocityFacing;

	/* Ptr to function that is executed based on the rotation mode. */
	typedef void (UPS_Human_LocomotionController::* RotationModeFunctionPtr)();
	RotationModeFunctionPtr TargetRotationFunctionPtr = nullptr;

	// Used by ELocomotionRotationsModes::CameraFacing. Should be updated in states that allow this type of movement to avoid wasting cycles.
	// Returns in which cardinal direction the player is currently moving. To do so we use the facing dir and velocity delta
	// and the config values provided by this struct.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	FMovementDirectionUpdater CardinalDirectionUpdater;

	// Enum with the desired direction the character wants to move to.
	// Computed from the input vector and calculated with FMovementDirectionUpdater.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	TEnumAsByte<ELocomotionMovementDirections> DesiredCardinalDirection = ELocomotionMovementDirections::M_Forward;

	/* Ground and brake friction variables */

	// Curve that controls default ground friction based on current character speed.
	// Used to simulate momentum so it gets more difficult changing directions when speed increases.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	UCurveFloat * GroundFrictionForSpeedCurve;

	// Ground friction factor, difference surfaces may have different friction factors.
	// The higher, the more friction we have. Unused for now.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float GroundFrictionFactorInSurface = 1.0f;

	// Difficulty of braking with speed increases.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	UCurveFloat * BrakeFrictionForSpeedCurve;

	/* Root motion */

	// Can be set to true while doing guided movements like climbing or vaulting.
	bool IsInRootMotionMovement = false;

public:

	// Initializes the controller, storing all necessary values.
	void Init();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/* States */

	void ChangeToGroundedState();

	void ChangeToFallingState();

	void ChangeToClimbingState(const FVector& PositionToHangTo, const FVector& ObstacleNormal, float DeltaAngleWithSurface);

	void SetRagdollStatus(bool EnableRagdolls);

	/* Target rotation functions */

	// Calls the current target rotation function based on the current rotation mode so the target rotation gets updated with it. Use this function
	// if you want to support multiple rotations modes that can be setup by ChangeTargetRotationMode. Otherwise, it will be better to directly use
	// set target rotation or any of the other specific update target rotation functions.
	UFUNCTION(BlueprintCallable)
	void UpdateTargetRotationBasedOnRotationMode();

	// Sets directly the new rotation for the character. This function is normally used by other states that might implement their own rotation
	// logic apart from the default one. For example, while turning.
	UFUNCTION(BlueprintCallable)
	void SetTargetRotation(float NewRotation);

	// Makes the character rotate to the direction of its movement.
	// Prefer to use UpdateTargetRotationBasedOnRotationMode if you want to give support to different target rotation modes.
	UFUNCTION(BlueprintCallable)
	void UpdateTargetRotationToVelocity();

	// Makes the character rotate to the direction the controller is facing.
	// Prefer to use UpdateTargetRotationBasedOnRotationMode if you want to give support to different target rotation modes.
	UFUNCTION(BlueprintCallable)
	void UpdateTargetRotationToControllerDirection();

	// Updates the character actor rotation so it faces the target rotation. Furthermore, it also computes delta between
	// velocity of character and facing direction. can implement their own rotation logic.
	UFUNCTION(BlueprintCallable)
	void UpdateCharacterMeshRotation(float DeltaTime, float RotationSpeed);

	// Changes the target rotation mode.
	// Velocity facing will make the player orient to the direction of the velocity.
	// Camera facing will make the player orient to the direction of the camera.
	UFUNCTION(BlueprintCallable)
	void ChangeTargetRotationMode(ELocomotionRotationModes NewMode);

	/* Stances */

	UFUNCTION(BlueprintCallable)
	bool CanChangeLocomotionStance(TEnumAsByte<ELocomotionStance> NewStance);

	// Sets the new locomotion stance for the locomotion controller. If the controller is currently
	// running a state that inherits from UPS_LocState_Interface, it will also call the ChangeStance
	// function for the state so it updates it's stance behavior.
	UFUNCTION(BlueprintCallable)
	void ChangeLocomotionStance(TEnumAsByte<ELocomotionStance> NewStance);

	/* Movement */

	UFUNCTION(BlueprintCallable)
	void SetGroundFrictionFactor(float NewGroundFriction);

	/* Feet. */

	// Changes the standing foot (changes from right to left foot or viceversa).
	UFUNCTION(BlueprintCallable)
	void DoToggleStandingFoot();

	/* Walking */

	// Makes character walk if we are in a state that allows walking.
	UFUNCTION(BlueprintCallable)
	void SetWalk(bool EnableWalk);
	UFUNCTION(BlueprintCallable)
	bool GetIsWalking() const;

	/* Sprinting */

	// Set boolean to indicate we want to start sprinting. Doesn't necessarily trigger it inmediately.
	// Different states that implement sprinting may decide when we can actually sprint.
	// Use IsSprinting to check when sprinting is actually happening.
	UFUNCTION(BlueprintCallable)
	void SetWantsToSprint(bool EnableSprint);
	// Returns true if the character is actually sprinting.
	UFUNCTION(BlueprintCallable)
	bool IsSprinting() const;

	/* Aiming */

	UFUNCTION(BlueprintCallable)
	void SetAiming(bool IsAiming);

	/* IK Functions */

	// Enable Head IK Look at position.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKLookAtPosition(const FVector& NewPositionToLookAt);
	// Enable Head IK Look at component.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKLookAtComponent(const USceneComponent* NewSceneCompToLookAt);
	// Enable Head IK Look in controller orientation.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKLookAtControllerOrientation();
	// Enable Head IK Look in Random direction.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKLookAtRandomDirection();
	// Disable head IK.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKDisabled();
	// Returns true if enabled, false otherwise.
	UFUNCTION(BlueprintCallable)
	bool IsHeadIKEnabled();

	// Enables Hand IKs reusing previous UpperTorsoIkMode.
	UFUNCTION(BlueprintCallable)
	void SetUpperTorsoIKToLastUsedMode();
	// Enables Hand IKs with the given mode as IK behaviour.
	UFUNCTION(BlueprintCallable)
	void SetUpperBodyIKEnabled(EUpperTorsoIKModes ModeToUse);
	// Disables Hand IKs
	UFUNCTION(BlueprintCallable)
	void SetUpperBodyIKDisabled();

	// Enables or disables Leg IKs.
	UFUNCTION(BlueprintCallable)
	void SetLegIKEnabledStatus(bool EnableLegIK);

	/* Root motion */

	// Set to true by movements like climbing or vaulting.
	UFUNCTION(BlueprintCallable)
	void SetRootMotionMovement(bool Enable);

	// Returns true if we are playing root motion animations
	UFUNCTION(BlueprintCallable)
	bool IsPlayingRootMotion() const;

	/* State control functions */

	UFUNCTION(BlueprintCallable)
	void AllowStateChangeOnMovementChanged(bool AllowStateChange);

	/* Climbing API */

	/* Hanging actions  */

	bool TryLedgeGrab();
	void Hanging_TryMove(float Axis);
	void Hanging_TryClimb(float Axis);
	void Hanging_PerformJumpingFromHanged();
	void Hanging_PerformVerticalJump();
	void Hanging_PerformHorizontalJump(bool Right);
	void Hanging_ReleaseHang();
	void Hanging_Crouch();
	void Hanging_Stand();
	void Hanging_Walk();
	void Hanging_StopWalk();

private:

	/* Init functions */

	void FetchComponents();
	void FetchStatesReferences();
	void AddPrerequisiteComponents();
	
	/* General functions */

	// Updates general variables that are common to most, if not all, movement
	// states. Used for always relevant variables.
	void UpdateGeneralVariables(float DeltaTime);

	/* Ground friction computation */

	void ComputeGroundFriction();

	friend class APS_Character;

	/* Change state events. */
	
	// Called by PS Character when the movement component changes states. Push states like falling, flying or
	// swimming will be received here. Use this function to control the transition logic in each case.
	void OnMovementComponentStateChanged(EMovementMode PrevMovementMode, EMovementMode NewMovementMode);

public:

	/* C++ Getters and setters */

	inline float GetSpeed() const { return Speed; }
	inline float GetVerticalSpeed() const { return VerticalSpeed; }
	inline float GetDeltaBetweenVelocityAndFacingDir() const { return DeltaBetweenVelocityAndFacingDir; }
	inline float GetDeltaBetweenVelocityAndDesiredDirection() const { return DeltaBetweenVelocityAndDesiredDirection; }
	inline float GetDeltaBetweenDesiredDirAndFacingDir() const { return DeltaBetweenDesiredDirAndFacingDir;	}
	inline bool GetIsMoving() const { return IsMoving; }
	inline bool GetWasMoving() const { return WasMoving; }
	inline bool GetHasMovementInput() const { return HasMovementInput; }
	inline bool GetWantsToSprint() const { return WantsToSprint; }
	inline FRotator GetActorCurrentRotation() const { return GetOwner() != nullptr ? GetOwner()->GetActorRotation() : FRotator(); }
	inline FRotator GetActorTargetRotation() const { return GetOwner() != nullptr ? CharacterTargetRotation : FRotator(); }
	inline float GetCharacterCurrentAngle() const { return GetOwner() != nullptr ? GetOwner()->GetActorRotation().Yaw : 0.0f;	}
	inline FMovementDirectionUpdater & GetCardinalDirectionUpdater() { return CardinalDirectionUpdater; }
	inline TEnumAsByte<ELocomotionStance> GetLocomotionStance() const { return CurrentStance; }
	inline ELocomotionRotationModes GetTargetRotationMode() const { return TargetRotationMode; }
};
