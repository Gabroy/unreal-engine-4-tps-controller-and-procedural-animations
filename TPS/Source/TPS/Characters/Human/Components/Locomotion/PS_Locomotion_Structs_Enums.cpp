#include "PS_Locomotion_Structs_Enums.h"

bool FMovementDirection::IsAngleInsideDirection(float AngleBetweenFrontAndCurrentDir)
{
	return AngleBetweenFrontAndCurrentDir >= MinInsideAngle && AngleBetweenFrontAndCurrentDir <= MaxInsideAngle;
}

bool FMovementDirection::IsAngleOutsideDirection(float AngleBetweenFrontAndCurrentDir)
{
	if (IsBackwardsDirection)
		return FMath::Abs(AngleBetweenFrontAndCurrentDir) <= MaxSkipAngle;
	else
		return AngleBetweenFrontAndCurrentDir >= MaxSkipAngle || AngleBetweenFrontAndCurrentDir <= MinSkipAngle;
}

void FMovementDirectionUpdater::InitDirections()
{
	ForwardDir.MaxInsideAngle = ForwardDir.InsideDirectionAngle;
	ForwardDir.MinInsideAngle = -ForwardDir.InsideDirectionAngle;
	ForwardDir.MaxSkipAngle = ForwardDir.SkipDirectionAngle;
	ForwardDir.MinSkipAngle = -ForwardDir.SkipDirectionAngle;

	RightwardsDir.MaxInsideAngle = 90.0f + RightwardsDir.InsideDirectionAngle;
	RightwardsDir.MinInsideAngle = 90.0f - RightwardsDir.InsideDirectionAngle;
	RightwardsDir.MaxSkipAngle = 90.0f + RightwardsDir.SkipDirectionAngle;
	RightwardsDir.MinSkipAngle = 90.0f - RightwardsDir.SkipDirectionAngle;

	LeftwardsDir.MaxInsideAngle = -90.0f + LeftwardsDir.InsideDirectionAngle;
	LeftwardsDir.MinInsideAngle = -90.0f - LeftwardsDir.InsideDirectionAngle;
	LeftwardsDir.MaxSkipAngle = -90.0f + LeftwardsDir.SkipDirectionAngle;
	LeftwardsDir.MinSkipAngle = -90.0f - LeftwardsDir.SkipDirectionAngle;

	BackwardsDir.MaxInsideAngle = 180.0f - BackwardsDir.InsideDirectionAngle;
	BackwardsDir.MinInsideAngle = -180.0f + BackwardsDir.InsideDirectionAngle;
	BackwardsDir.MaxSkipAngle = 180.0f - BackwardsDir.SkipDirectionAngle;
	BackwardsDir.MinSkipAngle = -180.0f + BackwardsDir.SkipDirectionAngle;
	BackwardsDir.IsBackwardsDirection = true;

	FetchNewDirection(0.0f);
}

void FMovementDirectionUpdater::UpdateMovementDirection(float DeltaBetweenVelocityAndFacingDir)
{
	bool IsStillInsideDirection = !CurrentDirProps->IsAngleOutsideDirection(DeltaBetweenVelocityAndFacingDir);

	// If we are still in the direction we keep updating current direction.
	if (IsStillInsideDirection)
	{
		CurrentDirProps->DirectionDeltaAngle = DeltaBetweenVelocityAndFacingDir;
	}
	else
	{
		FetchNewDirection(DeltaBetweenVelocityAndFacingDir);
	}
}

TEnumAsByte<ELocomotionMovementDirections> FMovementDirectionUpdater::GetDirectionFromAngle(float DeltaBetweenVelocityAndFacingDir)
{
	if (ForwardDir.IsAngleInsideDirection(DeltaBetweenVelocityAndFacingDir))
	{
		return ELocomotionMovementDirections::M_Forward;
	}
	else if (RightwardsDir.IsAngleInsideDirection(DeltaBetweenVelocityAndFacingDir))
	{
		return ELocomotionMovementDirections::M_Rightwards;
	}
	else if (LeftwardsDir.IsAngleInsideDirection(DeltaBetweenVelocityAndFacingDir))
	{
		return ELocomotionMovementDirections::M_Leftwards;
	}
	else
	{
		return ELocomotionMovementDirections::M_Backwards;
	}
}

bool FMovementDirectionUpdater::IsOppositeDirectionOfCurrentOne(float AngleBetweenFrontAndCurrentDir)
{
	if (CurrentMovDir == ELocomotionMovementDirections::M_Forward)
	{
		return BackwardsDir.IsAngleInsideDirection(AngleBetweenFrontAndCurrentDir);
	}
	else if (CurrentMovDir == ELocomotionMovementDirections::M_Backwards)
	{
		return ForwardDir.IsAngleInsideDirection(AngleBetweenFrontAndCurrentDir);
	}
	else if (CurrentMovDir == ELocomotionMovementDirections::M_Leftwards)
	{
		return RightwardsDir.IsAngleInsideDirection(AngleBetweenFrontAndCurrentDir);
	}
	else if (CurrentMovDir == ELocomotionMovementDirections::M_Rightwards)
	{
		return LeftwardsDir.IsAngleInsideDirection(AngleBetweenFrontAndCurrentDir);
	}
	return false;
}

void FMovementDirectionUpdater::FetchNewDirection(float DeltaBetweenVelocityAndFacingDir)
{
	PreviousMovDir = CurrentMovDir;

	if (ForwardDir.IsAngleInsideDirection(DeltaBetweenVelocityAndFacingDir))
	{
		CurrentMovDir = ELocomotionMovementDirections::M_Forward;
		CurrentDirProps = &ForwardDir;
		ForwardDir.DirectionDeltaAngle = DeltaBetweenVelocityAndFacingDir;
		return;
	}
	else if (RightwardsDir.IsAngleInsideDirection(DeltaBetweenVelocityAndFacingDir))
	{
		CurrentMovDir = ELocomotionMovementDirections::M_Rightwards;
		CurrentDirProps = &RightwardsDir;
		RightwardsDir.DirectionDeltaAngle = DeltaBetweenVelocityAndFacingDir;
	}
	else if (LeftwardsDir.IsAngleInsideDirection(DeltaBetweenVelocityAndFacingDir))
	{
		CurrentMovDir = ELocomotionMovementDirections::M_Leftwards;
		CurrentDirProps = &LeftwardsDir;
		LeftwardsDir.DirectionDeltaAngle = DeltaBetweenVelocityAndFacingDir;
	}
	else
	{
		CurrentMovDir = ELocomotionMovementDirections::M_Backwards;
		CurrentDirProps = &BackwardsDir;
		BackwardsDir.DirectionDeltaAngle = DeltaBetweenVelocityAndFacingDir;
	}
}