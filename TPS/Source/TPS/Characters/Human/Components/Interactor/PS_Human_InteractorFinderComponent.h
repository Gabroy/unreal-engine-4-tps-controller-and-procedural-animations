#pragma once

#include "CoreMinimal.h"
#include "Gameplay/Interactuable/PS_InteractuableStructs.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_Human_InteractorFinderComponent.generated.h"

class APS_Character;
class APlayerCameraManager;
class IPS_InteractuableInterface;
class UPS_Human_InteractorComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnFoundInteractuableChangedStatus, TScriptInterface<IPS_InteractuableInterface>, InteractuableActor, EInteractor_CanBeInteractedResult, CanBeInteractedResult, FText, ReasonInteraction);

/*
* Player Interactor Finder Component used to find interactors for any player controller.
* 
* The component works by throwing rays from the player camera component and checking if they collide
* with interactive objects. Then, it communicates with the interactor component of the player controlled
* actor to start interactions.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_InteractorFinderComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Component variables */

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	APlayerCameraManager * CameraManager = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_InteractorComponent * InteractorComponent = nullptr;

	/* Variables */

	// Time between checks for finding interactors we are pointing at.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "InteractorDetection")
	float IntervalBetweenFindInteractorChecks = 0.1f;

	// Length of the ray that is used to detect interactors. Not the same as interactuable distance which can vary per object.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "InteractorDetection")
	float MaxDistanceForInteractorDetection = 1000.0f;

	// Trace Type Query Used By Interactor objects.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "InteractorDetection")
	TEnumAsByte<ETraceTypeQuery> InteractorTypeQuery;

	// Debug flag for tracing.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "InteractorDetection")
	TEnumAsByte<EDrawDebugTrace::Type> DebugInteractorFindQuery;

	// Current actor that is being checked for interactions.
	UPROPERTY(BlueprintReadOnly, Category = "InteractorDetection")
	AActor * CurrentActorInteracting = nullptr;

	// IPS_InteractuableInterface of the current actor we are interacting with.
	UPROPERTY(BlueprintReadOnly, Category = "InteractorDetection")
	TScriptInterface<IPS_InteractuableInterface> CurrentActorInterface;

	// Handle for the find interactors timer loop.
	UPROPERTY(BlueprintReadOnly, Category = "InteractorDetection")
	FTimerHandle FindInteractorsTimerHandle;

	// While pointing to an interactor, how much time we wait before doing a new check
	// on the interactor we are pointing to see if we met the conditions again.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "InteractorConditionChecks")
	float IntervalBetweenInteractorCheckConditionsUpdates = 1.0f;

	// Current value of the actor check condition.
	UPROPERTY(BlueprintReadOnly, Category = "InteractorConditionChecks")
	TEnumAsByte<EInteractor_CanBeInteractedResult> CurrentActorCheckCondition;

	// Turns true on first PerformConditionCheckOnInteractor after interactor gets performed first condition check.
	UPROPERTY(BlueprintReadOnly, Category = "InteractorConditionChecks")
	bool FirstConditionCheckForInteractor = false;

	UPROPERTY(BlueprintReadOnly, Category = "InteractorConditionChecks")
	FTimerHandle InteractorsConditionsChecksTimerHandle;

	/* Protected functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UPROPERTY(BlueprintReadOnly)
	FOnFoundInteractuableChangedStatus OnFoundInteractuableCallback;

	// Constructor.
	UPS_Human_InteractorFinderComponent();

	// Call this to interact with the object we are detecting as interactuable, if there is one.
	UFUNCTION(BlueprintCallable)
	void StartInteraction();

	/* Getters and setters. */

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AActor * GetCurrentActorInteracting();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TScriptInterface<IPS_InteractuableInterface> & GetCurrentActorInterface();

private:

	// Sets timer to perform raycast for findinf interactors.
	void SetFindInteractorChecks(bool EnabledChecks);

	// Sets timer for evaluating the conditions for the interactor we are pointing at.
	void SetCheckConditionsInteractorChecks(bool EnabledChecks);

	// Performs raycast to find interactors.
	void PerformTestToFindInteractor();

	inline void SelectPotentialInteractiveActor(AActor * HitActor);
	inline void UnselectPotentialInteractiveActor();

	// Performs conditions checking on the interactor we are pointing to.
	void PerformConditionCheckOnInteractor();

	// Sets the check result, called by InteractWithRelevantObject and PerformConditionCheckOnInteractor. Stores wether we can interact
	// with the interactuable and fires callback if necessary.
	// ForceCallbackLaunch if true, callbacks are always launched no matter if ResultInteraction is always the same.
	void SetCheckResultForInteractuable(EInteractor_CanBeInteractedResult ResultInteraction, const FText & ReasonInteraction, bool ForceCallbackLaunch = false);

	// Called when the interactor component is enabled or disabled.
	void OnInteractorComponentEnabled(bool EnabledInteractor);
};
