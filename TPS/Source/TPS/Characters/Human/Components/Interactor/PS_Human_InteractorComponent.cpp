#include "PS_Human_InteractorComponent.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Gameplay/Interactuable/PS_InteractorInterface.h"
#include "Gameplay/Interactuable/PS_InteractuableInterface.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Kismet/KismetMathLibrary.h"

UPS_Human_InteractorComponent::UPS_Human_InteractorComponent() : UPS_Base_InteractorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_Human_InteractorComponent::BeginPlay()
{
	Super::BeginPlay();

	CharacterOwner = Cast<APS_Character>(GetOwner());
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorComponent::BeginPlay GetOwner Returned Null"));
		return;
	}

	LocomotionController = CharacterOwner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorComponent::Init LocomotionController Returned Null"));
		return;
	}
	LocomotionController->OnChangedStateMulticast.AddUObject(this, &UPS_Human_InteractorComponent::OnLocomotionControllerChangedState);

	CarryingController = CharacterOwner->FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorComponent::Init CarryingController Returned Null"));
		return;
	}
}

void UPS_Human_InteractorComponent::OnStartInteractionBehaviour()
{
	if (CharacterOwner)
	{
		// Perform rotation to face object, on finish
		UObject * CurrentlyInteractingObject = CurrentInteractingObjectInterface.GetObject();
		if (CurrentlyInteractingObject)
		{
			AActor * ActorInteractingWith = Cast<AActor>(CurrentlyInteractingObject);
			if (ActorInteractingWith)
			{
				TurningFinishedCallback FuncPtr = (TurningFinishedCallback)&UPS_Human_InteractorComponent::OnTurningFinished;
				CharacterOwner->BindToTurningFinishedEvent(this, FuncPtr);
				FVector ItemDirection = ActorInteractingWith->GetActorLocation() - CharacterOwner->GetActorLocation();
				CharacterOwner->StartTurning(ItemDirection.Rotation().Yaw, 180.0f);
			}
		}
	}
}

void UPS_Human_InteractorComponent::OnCancelInteractionBehaviour()
{
	UObject* InteractorObject = CurrentInteractingObjectInterface.GetObject();
	EInteractuableTypes InteractType = IPS_InteractuableInterface::Execute_GetInteractuableType(InteractorObject);
	FinishInteraction();
}

EInteractor_CanBeInteractedResult UPS_Human_InteractorComponent::CheckActorCanInteractWithInteractuable(TScriptInterface<IPS_InteractuableInterface>& InteractuableActorInterface, FText& ReasonInteraction)
{
	if (!CharacterOwner || !CharacterOwner->CanTurn())
	{
		return EInteractor_CanBeInteractedResult::ICBIR_Disabled;
	}

	UObject * CurrentlyInteractingObject = InteractuableActorInterface.GetObject();
	if (!CurrentlyInteractingObject)
	{
		return EInteractor_CanBeInteractedResult::ICBIR_Disabled;
	}
	
	// Based on type of interactuable, perform necessary checks.
	EInteractuableTypes InteractType = IPS_InteractuableInterface::Execute_GetInteractuableType(CurrentlyInteractingObject);
	switch (InteractType)
	{
	case EInteractuableTypes::IT_Object:
		return CheckCanPickItem(CurrentlyInteractingObject);
		break;
	default:
		break;
	}

	return EInteractor_CanBeInteractedResult::ICBIR_Yes;
}

void UPS_Human_InteractorComponent::OnLocomotionControllerChangedState(FName NewStateName, int NewStateID)
{
	bool PrevEnabledInteractionsValue = CheckInteractionsAreEnabled();

	ELocomotionMode LocomotionMode = (ELocomotionMode)NewStateID;
	switch (LocomotionMode)
	{
	case ELocomotionMode::Climbing:
		InteractionsAreEnabled = InteractionsAreEnabled - 1;
		break;
	case ELocomotionMode::Ragdoll:
		InteractionsAreEnabled = InteractionsAreEnabled - 1;
		break;
	default:
		InteractionsAreEnabled = InteractionsAreEnabled + 1;
		break;
	}
	InteractionsAreEnabled = FMath::Max(InteractionsAreEnabled, 0);

	bool EnabledInteractionsValue = CheckInteractionsAreEnabled();
	if (PrevEnabledInteractionsValue != EnabledInteractionsValue)
	{
		OnInteractorEnabledCallback.Broadcast(EnabledInteractionsValue);
		if (!EnabledInteractionsValue && CurrentInteractingObjectInterface.GetObject() != nullptr)
		{
			FinishInteraction();
		}
	}
}

void UPS_Human_InteractorComponent::OnTurningFinished(bool FinishedTurningCompletely)
{
	// Was interrupted.
	if (!FinishedTurningCompletely)
	{
		FinishInteraction();
		return;
	}

	UObject * CurrentlyInteractingObject = CurrentInteractingObjectInterface.GetObject();
	if (!CurrentlyInteractingObject)
	{
		FinishInteraction();
		return;
	}

	// Based on the type of interactuable, start the necessary interaction behaviour.
	EInteractuableTypes InteractType = IPS_InteractuableInterface::Execute_GetInteractuableType(CurrentlyInteractingObject);
	switch (InteractType)
	{
	case EInteractuableTypes::IT_Object:
		StartPickItemBehaviour(CurrentlyInteractingObject);
		break;
	default:
		break;
	}
}

void UPS_Human_InteractorComponent::StartPickItemBehaviour(UObject * InteractedObject)
{
	APS_BaseItemActorInstance * ItemActor = Cast<APS_BaseItemActorInstance>(InteractedObject);
	if (!ItemActor)
	{
		return;
	}

	// In the future we could check here the context of the object (if it's in the hands of an npc or on the ground or ...).
	// For now we assume it's on the ground and we want to pick it.
	CarryingController->StartPickItem(ItemActor);

	FinishInteraction();
}

EInteractor_CanBeInteractedResult UPS_Human_InteractorComponent::CheckCanPickItem(UObject * InteractedObject)
{
	APS_BaseItemActorInstance * ItemActor = Cast<APS_BaseItemActorInstance>(InteractedObject);
	return (ItemActor && CarryingController && CarryingController->CanPickItem(ItemActor)) ? EInteractor_CanBeInteractedResult::ICBIR_Yes : EInteractor_CanBeInteractedResult::ICBIR_Disabled;
}