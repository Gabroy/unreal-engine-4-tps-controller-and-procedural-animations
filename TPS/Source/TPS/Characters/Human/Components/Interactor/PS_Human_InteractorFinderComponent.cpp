#include "PS_Human_InteractorFinderComponent.h"
#include "PS_Human_InteractorComponent.h"
#include "Characters/Human/PS_Character.h"

UPS_Human_InteractorFinderComponent::UPS_Human_InteractorFinderComponent() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UPS_Human_InteractorFinderComponent::BeginPlay()
{
	Super::BeginPlay();

	CharacterOwner = Cast<APS_Character>(GetOwner());
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorFinderComponent::BeginPlay GetOwner Returned Null"));
		return;
	}

	APlayerController * PlayerController = Cast<APlayerController>(CharacterOwner->GetController());
	if (!PlayerController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorFinderComponent::BeginPlay PlayerController Returned Null"));
		return;
	}

	CameraManager = PlayerController->PlayerCameraManager;
	if (!CameraManager)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorFinderComponent::Init CameraManager Returned Null"));
		return;
	}

	InteractorComponent = CharacterOwner->FindComponentByClass<UPS_Human_InteractorComponent>();
	if (!InteractorComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InteractorFinderComponent::Init InteractorComponent Returned Null"));
		return;
	}
	InteractorComponent->OnInteractorEnabledCallback.AddUObject(this, &UPS_Human_InteractorFinderComponent::OnInteractorComponentEnabled);

	SetFindInteractorChecks(InteractorComponent->CheckInteractionsAreEnabled());
}

void UPS_Human_InteractorFinderComponent::StartInteraction()
{
	if (CurrentActorInteracting != nullptr)
	{
		FText ReasonInteraction;
		EInteractor_CanBeInteractedResult ResultInteraction = InteractorComponent->CanInteractWithInteractuable(CurrentActorInterface, ReasonInteraction);
		if(ResultInteraction == EInteractor_CanBeInteractedResult::ICBIR_Yes)
			InteractorComponent->StartInteraction(CurrentActorInterface);
		
		SetCheckResultForInteractuable(ResultInteraction, ReasonInteraction, true);
	}
}

void UPS_Human_InteractorFinderComponent::SetFindInteractorChecks(bool EnabledChecks)
{
	UWorld * WorldPtr = GetWorld();
	if (!WorldPtr)
		return;

	FTimerManager & TimerManager = WorldPtr->GetTimerManager();

	if (EnabledChecks)
		TimerManager.SetTimer(FindInteractorsTimerHandle, this, &UPS_Human_InteractorFinderComponent::PerformTestToFindInteractor, IntervalBetweenFindInteractorChecks, true);
	else
	{
		TimerManager.ClearTimer(FindInteractorsTimerHandle);
		TimerManager.ClearTimer(InteractorsConditionsChecksTimerHandle);
	}
}

void UPS_Human_InteractorFinderComponent::SetCheckConditionsInteractorChecks(bool EnabledChecks)
{
	UWorld* WorldPtr = GetWorld();
	if (!WorldPtr)
		return;

	FTimerManager & TimerManager = WorldPtr->GetTimerManager();

	if (EnabledChecks)
		TimerManager.SetTimer(InteractorsConditionsChecksTimerHandle, this, &UPS_Human_InteractorFinderComponent::PerformConditionCheckOnInteractor, IntervalBetweenInteractorCheckConditionsUpdates, true);
	else
		TimerManager.ClearTimer(InteractorsConditionsChecksTimerHandle);
}

void UPS_Human_InteractorFinderComponent::PerformTestToFindInteractor()
{
	FVector StartCheckPosition = CameraManager->GetCameraLocation();
	FVector EndCheckPosition = StartCheckPosition + CameraManager->GetCameraRotation().RotateVector(FVector::ForwardVector) * MaxDistanceForInteractorDetection;

	FHitResult OutHit;
	TArray<AActor*> ActorsToIgnore;
	bool HitFound = UKismetSystemLibrary::LineTraceSingle(this, StartCheckPosition, EndCheckPosition, InteractorTypeQuery,
		false, ActorsToIgnore, DebugInteractorFindQuery, OutHit, true);


	if (HitFound)
	{
		AActor * HitActor = OutHit.Actor.Get();
		bool InteractuableActor = HitActor && CurrentActorInteracting != HitActor;
		if (InteractuableActor && HitActor->Implements<UPS_InteractuableInterface>())
		{
			UnselectPotentialInteractiveActor();
			SelectPotentialInteractiveActor(HitActor);
		}
		else if (InteractuableActor && CurrentActorInteracting)
		{
			UnselectPotentialInteractiveActor();
		}
	}
	else if (CurrentActorInteracting)
	{
		UnselectPotentialInteractiveActor();
	}
}

void UPS_Human_InteractorFinderComponent::SelectPotentialInteractiveActor(AActor * HitActor)
{
	CurrentActorInteracting = HitActor;
	CurrentActorInterface.SetInterface(Cast<IPS_InteractuableInterface>(CurrentActorInteracting));
	CurrentActorInterface.SetObject(CurrentActorInteracting);
	FirstConditionCheckForInteractor = false;

	// Perform condition check for the new found actor.
	PerformConditionCheckOnInteractor();

	// Enable condition check timer if it was disabled.
	if (!InteractorsConditionsChecksTimerHandle.IsValid())
		SetCheckConditionsInteractorChecks(true);
}

void UPS_Human_InteractorFinderComponent::UnselectPotentialInteractiveActor()
{
	if (!CurrentActorInteracting)
		return;

	// Disable interaction feedback.
	IPS_InteractuableInterface::Execute_SetInteractionFeedback(CurrentActorInteracting, EInteractor_CanBeInteractedResult::ICBIR_Disabled);

	// Set to null as we are not pointing to any actor now.
	CurrentActorInteracting = nullptr;
	CurrentActorInterface.SetInterface(nullptr);
	CurrentActorInterface.SetObject(nullptr);

	// Disable checks conditions.
	if (InteractorsConditionsChecksTimerHandle.IsValid())
		SetCheckConditionsInteractorChecks(false);
}

void UPS_Human_InteractorFinderComponent::PerformConditionCheckOnInteractor()
{
	if (!CurrentActorInteracting)
		return;

	// Perform check conditions.
	FText ReasonInteraction;
	EInteractor_CanBeInteractedResult ResultInteraction = InteractorComponent->CanInteractWithInteractuable(CurrentActorInterface, ReasonInteraction);
	SetCheckResultForInteractuable(ResultInteraction, ReasonInteraction);
}

void UPS_Human_InteractorFinderComponent::SetCheckResultForInteractuable(EInteractor_CanBeInteractedResult ResultInteraction, const FText & ReasonInteraction, bool ForceCallbackLaunch)
{
	bool DifferentInteractorResult = ResultInteraction != CurrentActorCheckCondition || FirstConditionCheckForInteractor == false;

	// Set the new check conditions.
	CurrentActorCheckCondition = ResultInteraction;
	FirstConditionCheckForInteractor = true;

	// Broadcast results of currently checking interact actor so components or UI might display information.
	if (OnFoundInteractuableCallback.IsBound() && (ForceCallbackLaunch || DifferentInteractorResult))
		OnFoundInteractuableCallback.Broadcast(CurrentActorInterface, ResultInteraction, ReasonInteraction);

	// Set the interaction feedback for the user.
	if (DifferentInteractorResult)
		IPS_InteractuableInterface::Execute_SetInteractionFeedback(CurrentActorInteracting, ResultInteraction);
}

void UPS_Human_InteractorFinderComponent::OnInteractorComponentEnabled(bool EnabledInteractor)
{
	SetFindInteractorChecks(EnabledInteractor);
}

/* Getters and setters. */

AActor* UPS_Human_InteractorFinderComponent::GetCurrentActorInteracting()
{
	return CurrentActorInteracting;
}

const TScriptInterface<IPS_InteractuableInterface> & UPS_Human_InteractorFinderComponent::GetCurrentActorInterface()
{
	return CurrentActorInterface;
}
