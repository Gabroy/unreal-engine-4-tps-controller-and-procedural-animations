#pragma once

#include "CoreMinimal.h"
#include "Characters/Common/Components/Interactor/PS_Base_InteractorComponent.h"
#include "PS_Human_InteractorComponent.generated.h"

class APS_Character;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;

/*
* Interactor component responsible of implementing the necessary logic for allowing a human character to correctly
* interact with different types of interactuables.
*/

DECLARE_MULTICAST_DELEGATE_OneParam(FInteractorComponentEnabledStatus, bool)

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_InteractorComponent : public UPS_Base_InteractorComponent
{
	GENERATED_BODY()

protected:

	/* Component variables */

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_LocomotionController * LocomotionController = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UPS_Human_CarryingController * CarryingController = nullptr;

	/* Protected functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UPS_Human_InteractorComponent();

	/* Callbacks and others. */

	// Received when locomotion controller changes state. May enable or disable this component depending on the state support for interactions.
	UFUNCTION(BlueprintCallable)
	void OnLocomotionControllerChangedState(FName NewStateName, int NewStateID);

private:

	// Implement here any start interaction based on the type of interactuable we are interacting with.
	// This interactuable is stored in CurrentInteractingObjectInterface.
	virtual void OnStartInteractionBehaviour() override;

	// Implement here any stop interaction based on the type of interactuable we are interacting with.
	// This interactuable is stored in CurrentInteractingObjectInterface.
	virtual void OnCancelInteractionBehaviour() override;

	// Called when turning ends. Launches interaction behaviour.
	void OnTurningFinished(bool FinishedTurningCompletely);

	// Implement here any checks related to the actor with this component that should be performed to decide
	// if we can interact with the interactuable. For example, if the actor has hands free for example or is in the correct mode to interact
	// with other interactuables.
	virtual EInteractor_CanBeInteractedResult CheckActorCanInteractWithInteractuable(TScriptInterface<IPS_InteractuableInterface>& InteractuableActorInterface, FText& ReasonInteraction);

	/* Pick Item Behaviour */
	void StartPickItemBehaviour(UObject * InteractedObject);
	EInteractor_CanBeInteractedResult CheckCanPickItem(UObject * InteractedObject);
};
