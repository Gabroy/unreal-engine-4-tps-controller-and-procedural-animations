#pragma once

#include "CoreMinimal.h"
#include "PS_Human_VaultingStruct.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/EngineTypes.h"
#include "PS_Human_VaultingComponent.generated.h"

class APS_Character;
class UCharacterMovementComponent;
class USkeletalMeshComponent;
class UCapsuleComponent;
class UPS_Human_LocomotionController;
class UPS_Extended_AnimInstance;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_VaultingComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Components. */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Character = nullptr;

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCharacterMovementComponent * CharacterMovementComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCapsuleComponent * CapsuleComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Extended_AnimInstance * AnimInstance = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_LocomotionController* LocController = nullptr;

	/* Vaulting variables. */

	// Collision channel that should be present in blocked behaviour by objects that can be vaulted.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting")
	TEnumAsByte<ECollisionChannel> VaultingChannel = ECollisionChannel::ECC_GameTraceChannel1;
	ETraceTypeQuery VaultingTraceQuery;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vaulting")
	TEnumAsByte<EDrawDebugTrace::Type> DebugTrace = EDrawDebugTrace::None;

	// Conditions.

	// Maximum distance we can check for vault according to vaulting animations possible.
	// This doesn't necessarily mean we can necessarily vault the object as the animations that can
	// be played at that distance might not fulfill all height or speed requirements.
	// It's designers choice to see how far they want to vault and what animations they want to allow or not.
	// Distance checks provide some extra control to avoid some vaulting animations overlapping too much
	// if played from too close or looking fake when being done from too far away.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vaulting - Checks")
	float MaxDistanceToAllowVaulting = 0.0f;

	// Same as maximum distance but for minimum vaulting.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vaulting - Checks")
	float MinDistanceToAllowVaulting = 0.0f;

	// Max vaulting height. Computed from the maximum vaulting height of animations.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vaulting - Checks")
	float MaxVaultingHeight = 300.0f;

	// Min vaulting height. Computed from the minimum vaulting height of animations.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Vaulting - Checks")
	float MinVaultingHeight = 30.0f;

	// Minimum height difference between point we are vaulting at and point we are ending at
	// for vaulting to be considered. Avoids having weird issues like ending in a position that is
	// the same exact height as the point of vaulting. In such cases, a climbing animation should play.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Checks")
	float MinHeightDiffFromVaultPointToGroundToAllowVault = 50.0f;

	// Displacement.

	// Default distance that we will move after applying a vaulting animation if speed is 0.
	// This controls how far from the vaulted object we will end. Animations can have their own
	// curve adding or removing from this base modifier.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Displacement")
	float BaseVaultingDisplacementDistance = 120.0f;

	// Curve float adding an offset to the base vaulting displacement distance based on the current speed of the player.
	// This way, when we are sprinting, we can vault further away than when walking.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Displacement")
	UCurveFloat * VaultingDistanceModifierBasedOnSpeed = nullptr;

	/* Vaulting animations */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Animations")
	TArray<FVaultingAnimationData> VaultingAnimations;

	// Name of curve animation used to adjust the skeletal mesh while playing animations so the mesh changes height independent of the
	// capsule it's attached to so we avoid flying animations and they appear grounded in the object being vaulted. In general, animations
	// should already consider this but this allows modifying the behaviour if they don't.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Animations")
	FName RootHeightOffsetAnimationCurveName = "RootHeightOffset";

	// Name of curve animation used to adjust the interpolation of the vertical position of the player while vaulting or climbing.
	// Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting or end of climbing) and return to 0.0f at the end in the case of vaulting.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Animations")
	FName PositionVerticalInterpolationCurveName = "MantlingVerticalTranslationInterp";

	// Name of curve animation used to adjust the interpolation of the horizontal position of the player while vaulting or climbing.
	// Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting or end of climbing) and return to 0.0f at the end in the case of vaulting.
	// Allows controlling the horizontal lerping of the capsule allowing certain animations to better adapt to the obstacles being vaulted over.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Animations")
	FName PositionHorizontalInterpolationCurveName = "MantlingHorizontalTranslationInterp";

	// Name of curve animation used to adjust the interpolation of the rotation of the player while vaulting or climbing.
	// Should go from 0.0f to 1.0f (where 1.0f is the part of the animation where we want to be already fully rotated in the correct direction.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Vaulting - Animations")
	FName RotationInterpolationCurveName = "MantlingRotationInterp";

	/* Protected variables. */

	// True if we are vaulting.
	bool IsCurrentlyVaulting = false;

	// Object we are vaulting. We store a reference to it to allow modifying its collision response to the camera in parts of the animation
	// to avoid issues with camera collision.
	UPROPERTY(BlueprintReadOnly)
	FHitResult VaultingObject;

	// Used to check which vaulting anims meet all conditions and could be played. A random one is picked.
	TArray<int> CandidateVaultingAnims;

	// Controls position displacement while vaulting montages are being played.
	FVaultingInterpolation VaultingInterpolationHandler;

	/* Protected functions. */

	UPS_Human_VaultingComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	void FetchComponents();

	/* Editor only functions. */

#if WITH_EDITOR
	// Will call PreProcessAnimations to update values when a value is added to any of the two TArrays.
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent & PropertyChangedEvent) override;

	// Reads the arrays and fetches the correct values.
	void PreProcessVaultingVars();
#endif


public:

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction);

	// Check if the player can currently vault an obstacle. If so returns true and start vaulting. Otherwise, return false and don't vault.
	UFUNCTION(BlueprintCallable)
	bool TryVaulting();

	UFUNCTION(BlueprintCallable)
	bool IsVaulting() const { return IsCurrentlyVaulting; }

protected:

	/* Protected functions. */

	// Forward obstacle check.
	bool ForwardObstacleCheck(FHitResult & ObstacleHit);

	// Height obstacle check.
	bool HeightObstacleCheck(FHitResult & ObstacleHit, FVector & HeightHitPosition, float & ObstacleHeight);

	// Vault obstacle check. Returns true if it hit ground, false otherwise.
	bool VaultObstacleCheck(FHitResult& ObstacleHit, const FVector & HeightHitPosition, FVector & GroundPositionToEndVaultingAt, bool & WillHitGroundWhenVaulting);

	// Tries vaulting with the passed parameters. Returns false if none of the vaulting
	// anims meet the speed or height requirements.
	bool TryChooseVault(const FHitResult& Obstacle, const FVector & HeightHitPosition, float ObstacleHeight, const FVector & GroundPositionToEndVaultingAt, bool WillHitGroundWhenVaulting);

	void PlayVaultingAnimation(const FVaultingAnimationData & VaultAnim, const FHitResult& Obstacle, const FVector& HeightHitPosition, const FVector& GroundPositionToEndVaultingAt, bool HitGround, const FRotator& RotationToFaceTo);

	friend struct FVaultingInterpolation;

	// Called when we are falling to the ground after vaulting an obstacle but we didn't hit any object.
	// We set the player to falling mode. Allows correct blending of the vaulting anim with falling animations.
	void OnVaultingFallStart();

	// Sets the vaulting blending out that gets called when we are falling to the ground but we didn't hit any object.
	// Allows smoother blending of vaulting with falling animations.
	void StartVaultingBlending(UAnimMontage* AnimMontage, float BlendingOut);

	// Ends vaulting, called by Update once vaulting animations are finished.
	void EndVaulting();
};
