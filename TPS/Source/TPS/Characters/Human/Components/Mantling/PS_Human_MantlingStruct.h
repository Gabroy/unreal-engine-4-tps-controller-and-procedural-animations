#pragma once

#include "CoreMinimal.h"
#include "PS_Human_MantlingStruct.generated.h"

class UAnimMontage;
class UCurveFloat;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UPS_Human_MantlingComponent;

USTRUCT(BlueprintType, Blueprintable)
struct FVaultingAnimationData
{
	GENERATED_BODY()

	// Montage to play when vaulting over an obstacle.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * VaultingMontage;

	// Max speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxCharacterSpeedCanPlay = -1.0f;

	// Min speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinCharacterSpeedCanPlay = -1.0f;

	// Max height this animation can be played for.
	// Height starts from the feet of the player and not the center of mass.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxVaultingHeight;

	// Min height this animation can be played for.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinVaultingHeight = 0.0f;

	// Offset added to the top position over the obstacle being vaulted. Allows advancing the capsule
	// top pivoting point being vaulted either forward or backwards so certain animations look correct
	// while vaulting.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ForwardOffsetTopObstaclePosition = 0.0f;

	// Time in seconds to reach the top of the obstacle.
	// Controls how long for the player's capsule to reach the middle of the vaulted object
	// trajectory. This the point the player is vaulting over in the obstacle and starts going down.
	// Should be sync with the values of the curves MantlingVerticalTranslationInterp and
	// MantlingHorizontalTranslationInterp so all of them have value (1.0f) by the time
	// TimeReachTopObstacle is set to.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeReachTopObstacle = 0.0f;

	// Curve that controls how much distance is vaulted when applying this animation based
	// on the height of the object being vaulted. If the animation is used to vault over
	// objects with a lot of size variance, you should set the curve to taller objects have higher
	// distances so clipping issues are prevented.
	UCurveFloat * DistanceToVaultBasedOnHeight;
};

USTRUCT(BlueprintType, Blueprintable)
struct FClimbingAnimationData
{
	GENERATED_BODY()

	// Montage to play when climbing an obstacle.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * ClimbingMontage;

	// Max speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxCharacterSpeedCanPlay = -1.0f;

	// Min speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinCharacterSpeedCanPlay = -1.0f;

	// Max height this animation can be played for.
	// Height starts from the feet of the player and not the center of mass.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxClimbingHeight;

	// Min height this animation can be played for.
	// Negative values, means no min height.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinClimbingHeight;
};

/*
	Interpolation struct used by mantling component for vaulting animations.
	Interpolation works through three different animation curves that control:

	HeightOffsetCurve: Controls the height of the mesh component during the vaulting animation.
	Helps making animations correctly adapt to the obstacle so, due to the capsule movement, they
	don't look as the player is vaulting over air.

	PositionInterpolationCurve: Controls the position interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting) and return to 0.0f at the end.

	RotationInterpolationCurve: Controls the rotation interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting).

	If necessary two other implementations could be done:

	Dispense of PositionInterpolationCurve and RotationInterpolationCurve and use instead simple interpolations
	with TimeToReachTopOfObstacle and TotalTime - TimeToReachTopOfObstacle. We lose a bit of control of the interpolation
	but it might be slightly faster. Although this doesn't seem an issue.

	Use three (or two) PositionInterpolationCurves to control the interpolation horizontally and vertically of the position
	while applying the animation. Could give even more control of the interpolation to reduce issues with the character
	overlapping. Even then, it doesn't seem to be necessary for animations, so I have kept the simpler one curve as it's easy
	to setup and probably a bit faster.
*/

USTRUCT(BlueprintType, Blueprintable)
struct FVaultingInterpolation
{
	GENERATED_BODY()

private:
	// Components.
	UCapsuleComponent* CapsuleComponent = nullptr;
	USkeletalMeshComponent* SkeletalMeshComp = nullptr;
	UPS_Human_MantlingComponent * MantlingComp = nullptr;

	float SkeletalMeshRelativeLocHeight;

	// Curves for controlling mesh component position and interpolating movement while vaulting.
	FName HeightOffsetCurveName = "RootHeightOffset";
	FName PositionVerticalInterpolationCurveName = "MantlingVerticalTranslationInterp";
	FName PositionHorizontalInterpolationCurveName = "MantlingHorizontalTranslationInterp";
	FName RotationInterpolationCurveName = "MantlingRotationInterp";

	UAnimMontage * AnimMontage = nullptr;

	// Capsule current position and rotation and target positions and rotation.
	FVector CurrentCapsulePosition;
	FRotator CurrentRotationToFaceTo;
	FVector TargetHeightObstaclePosition;
	FVector TargetGroundPositionToEndVaultingAt;
	bool IsFallingAfterVault;
	FRotator TargetRotationToFaceTo;

	// Time for each part of the vaulting.
	float CurrentInterpolationTime = 0.0f;
	float TimeToReachTopOfObstacle = 0.0f;
	float TotalAnimationTime = 0.0f;

	// Set to true when we are passing the obstacle.
	bool HasGoneOverObstacle = false;

	// Controls the blending out of the animation when falling.
	UPROPERTY(EditAnywhere)
	float OnFallingAfterVaultBlendingOut = 0.5f;

public:

	// Initializes the interpolation struct.
	void Init(UCapsuleComponent* CapsuleComp, USkeletalMeshComponent* SkeletalMesh, UPS_Human_MantlingComponent * NewMantlingComp,
		const FName& NewHeightOffsetCurveName, const FName & NewPositionVerticalInterpolationCurveName,
		const FName& NewPositionHorizontalInterpolationCurveName, const FName& NewRotationInterpolationCurveName);

	// Starts the the interpolation struct setting all data from the vaulting animation data so
	// it's ready to start being interpolated.
	void Start(const FVaultingAnimationData& VaultData, const FVector& NewTargetHeightObstaclePosition,
		const FVector& NewTargetGroundPositionToEndVaultingAt, bool NewWillHitGroundWhenVaulting, const FRotator& TargetRotator);

	// Updates player position during vaulting animations.
	// Returns true once interpolation is done. False otherwise.
	bool Update(float dt);
};

/*
	Interpolation struct used by mantling component for climbing animations.
	Interpolation works through three different animation curves that control:

	HeightOffsetCurve: Controls the height of the mesh component during the vaulting animation.
	Helps making animations correctly adapt to the obstacle so, due to the capsule movement, they
	don't look as the player is vaulting over air.

	PositionVerticalInterpolationCurve and PositionHorizontalInterpolationCurve: Controls the position
	interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting) and return to 0.0f at the end.

	RotationInterpolationCurve: Controls the rotation interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting).

	If necessary two implementation could be done:

	Dispense of PositionInterpolationCurve and RotationInterpolationCurve and use instead simple interpolations
	with TimeToReachTopOfObstacle and TotalTime - TimeToReachTopOfObstacle. We lose a bit of control of the interpolation
	but it might be slightly faster. Although this doesn't seem an issue.

	Use three PositionInterpolationCurves to control the interpolation horizontally and vertically of the position
	while applying the animation. Could give even more control of the interpolation to reduce issues with the character
	overlapping. Even then, it doesn't seem to be necessary for animations, so I have kept the simpler one curve as it's easy
	to setup and probably a bit faster.
*/

struct FClimbingInterpolation
{
private:

	UCapsuleComponent * CapsuleComponent = nullptr;

	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	float SkeletalMeshRelativeLocHeight;

	// Curves for controlling mesh component position and interpolating movement while climbing.
	FName HeightOffsetCurveName = "RootHeightOffset";
	FName PositionVerticalInterpolationCurveName = "MantlingVerticalTranslationInterp";
	FName PositionHorizontalInterpolationCurveName = "MantlingHorizontalTranslationInterp";
	FName RotationInterpolationCurveName = "MantlingRotationInterp";

	// Capsule current position and rotation and target positions and rotation.
	FVector CurrentCapsulePosition;
	FRotator CurrentRotationToFaceTo;
	FVector TargetHeightObstaclePosition;
	FRotator TargetRotationToFaceTo;

	// Time for each part of the vaulting.
	float CurrentInterpolationTime = 0.0f;
	float TotalAnimationTime = 0.0f;

public:

	// Initializes the interpolation struct.
	void Init(UCapsuleComponent* CapsuleComp, USkeletalMeshComponent* SkeletalMesh,
		const FName& NewHeightOffsetCurveName, const FName& NewPositionVerticalInterpolationCurveName,
		const FName& NewPositionHorizontalInterpolationCurveName, const FName& NewRotationInterpolationCurveName);

	// Starts the the interpolation struct setting all data from the vaulting animation data so
	// it's ready to start being interpolated.
	void Start(const FClimbingAnimationData& ClimbData, const FVector& NewTargetHeightObstaclePosition, const FRotator& TargetRotator);

	// Updates player position during vaulting animations.
	// Returns true once interpolation is done. False otherwise.
	bool Update(float dt);
};