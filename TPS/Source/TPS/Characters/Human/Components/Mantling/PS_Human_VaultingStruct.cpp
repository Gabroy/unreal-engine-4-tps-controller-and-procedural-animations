#pragma once

#include "PS_Human_VaultingStruct.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "PS_Human_VaultingComponent.h"

// Initializes the interpolation struct.
void FVaultingInterpolation::Init(UCapsuleComponent * CapsuleComp, USkeletalMeshComponent * SkeletalMesh, UPS_Human_VaultingComponent * NewVaultingComp,
	const FName& NewHeightOffsetCurveName, const FName& NewPositionVerticalInterpolationCurveName,
	const FName& NewPositionHorizontalInterpolationCurveName, const FName & NewRotationInterpolationCurveName)
{
	// Fetch components.
	CapsuleComponent = CapsuleComp;
	VaultingComp = NewVaultingComp;
	SkeletalMeshComp = SkeletalMesh;
	if (SkeletalMeshComp)
		SkeletalMeshRelativeLocHeight = SkeletalMeshComp->GetRelativeLocation().Z;

	// Fetch animation curve names responsible of position, rotation and mesh relative position interpolations.
	HeightOffsetCurveName = NewHeightOffsetCurveName;
	PositionVerticalInterpolationCurveName = NewPositionVerticalInterpolationCurveName;
	PositionHorizontalInterpolationCurveName = NewPositionHorizontalInterpolationCurveName;
	RotationInterpolationCurveName = NewRotationInterpolationCurveName;
}

// Starts the the interpolation struct setting all data from the vaulting animation data so
// it's ready to start being interpolated.
void FVaultingInterpolation::Start(const FVaultingAnimationData& VaultData, const FVector& NewTargetHeightObstaclePosition,
	const FVector& NewTargetGroundPositionToEndVaultingAt, bool IsLandingOnGroundAfterVaulting, const FRotator& TargetRotator)
{
	// Get the animation montage.
	AnimMontage = VaultData.VaultingMontage;

	// Check if we will land on ground or falling. If falling we will do some extra logic to improve blending out.
	IsFallingAfterVault = !IsLandingOnGroundAfterVaulting;

	// Time for each part of the vaulting.
	CurrentInterpolationTime = 0.0f;
	TimeToReachTopOfObstacle = VaultData.TimeReachTopObstacle;
	TimeForBlendingOut = AnimMontage->GetPlayLength() - AnimMontage->GetDefaultBlendOutTime();
	TimeForBlendingOut -= IsFallingAfterVault ? AnimMontage->GetDefaultBlendOutTime() * OnFallingAfterVaultAddedBlendingPercent : 0.0f;

	// Get the current position and rotation.
	FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
	CurrentCapsulePosition = CapsuleTransform.GetLocation();
	CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();

	// Fetch the target position.
	TargetHeightObstaclePosition = NewTargetHeightObstaclePosition + VaultData.ForwardOffsetVaultingPos * TargetRotator.Vector();
	TargetGroundPositionToEndVaultingAt = NewTargetGroundPositionToEndVaultingAt;
	TargetHeightObstaclePosition.Z += CapsuleComponent->GetScaledCapsuleHalfHeight();
	TargetGroundPositionToEndVaultingAt.Z += CapsuleComponent->GetScaledCapsuleHalfHeight();
	TargetRotationToFaceTo = TargetRotator;

	// Finally, set the function ptr to the start vaulting step.
	VaultingFunctionPtr = &FVaultingInterpolation::VaultingStep;
}

// Updates player position during vaulting animations.
// Returns true once interpolation is done. False otherwise.
bool FVaultingInterpolation::Update(float dt)
{
	if (!CapsuleComponent || !SkeletalMeshComp || !VaultingFunctionPtr) return false;

	Instance = SkeletalMeshComp->GetAnimInstance();
	if (!Instance)
		return false;

	CurrentInterpolationTime += dt;

	bool FinishVaulting = (this->*VaultingFunctionPtr)(dt);

	// Mesh adjustment to adapt animations to obstacle so they are more grounded in obstacles.
	UpdateCharacterMeshLocation(FinishVaulting);

	return FinishVaulting;
}

// Function for the first part of the vaulting process. Rising until vaulting the object.
bool FVaultingInterpolation::VaultingStep(float dt)
{
	// Fetch factors.
	float PositionVerticalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalInterpolationCurveName), 1.0f);
	float PositionHorizontalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalInterpolationCurveName), 1.0f);
	float RotationInterpFactor = FMath::Min(Instance->GetCurveValue(RotationInterpolationCurveName), 1.0f);

	// Position Interpolation.
	float NewCoordX = FMath::Lerp(CurrentCapsulePosition.X, TargetHeightObstaclePosition.X, PositionHorizontalInterpFactor);
	float NewCoordY = FMath::Lerp(CurrentCapsulePosition.Y, TargetHeightObstaclePosition.Y, PositionHorizontalInterpFactor);
	float NewCoordZ = FMath::Lerp(CurrentCapsulePosition.Z, TargetHeightObstaclePosition.Z, PositionVerticalInterpFactor);
	FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
	CapsuleComponent->SetWorldLocation(NewCapsulePosition);

	// Rotation of the mesh to face obstacle vaulting direction.
	FRotator NewCapsuleRotation = FMath::Lerp(CurrentRotationToFaceTo, TargetRotationToFaceTo, RotationInterpFactor);
	CapsuleComponent->SetWorldRotation(NewCapsuleRotation);

	// We have reached the top of the obstacle. Now we start moving to the end.
	if (CurrentInterpolationTime >= TimeToReachTopOfObstacle)
	{
		VaultingFunctionPtr = &FVaultingInterpolation::LandingStep;

		// If we are falling after vault, we call this function to already change to falling mode of the character.
		// This allows correct blending of the montage with the falling state animations when we get to the end of the vaulting.
		if (IsFallingAfterVault)
			VaultingComp->OnVaultingFallStart();

		// Update current values for next part of vaulting.
		FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
		CurrentCapsulePosition = CapsuleTransform.GetLocation();
		CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();
	}

	return false;
}

// Function for second part of the process. Landing after going over the object.
bool FVaultingInterpolation::LandingStep(float dt)
{
	float PositionVerticalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalInterpolationCurveName), 1.0f);
	float PositionHorizontalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalInterpolationCurveName), 1.0f);

	// Position Interpolation.
	float NewCoordX = FMath::Lerp(TargetGroundPositionToEndVaultingAt.X, CurrentCapsulePosition.X, PositionHorizontalInterpFactor);
	float NewCoordY = FMath::Lerp(TargetGroundPositionToEndVaultingAt.Y, CurrentCapsulePosition.Y, PositionHorizontalInterpFactor);
	float NewCoordZ = FMath::Lerp(TargetGroundPositionToEndVaultingAt.Z, CurrentCapsulePosition.Z, PositionVerticalInterpFactor);
	FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
	CapsuleComponent->SetWorldLocation(NewCapsulePosition);

	// Check if we should finish interpolation normally or if we should blend out vaulting animation in case we are falling after vaulting.
	if (CurrentInterpolationTime >= TimeForBlendingOut)
	{
		if (IsFallingAfterVault)
		{
			float BlendingOut = AnimMontage->GetDefaultBlendOutTime();
			BlendingOut += BlendingOut * OnFallingAfterVaultAddedBlendingPercent;
			VaultingComp->StartVaultingBlending(AnimMontage, BlendingOut);
		}
		return true;
	}

	return false;
}

void FVaultingInterpolation::UpdateCharacterMeshLocation(bool IsFinishedVaulting)
{
	// Mesh adjustment to adapt animations to obstacle so they are more grounded in obstacles.
	float HeightOffsetForRootBone = Instance->GetCurveValue(HeightOffsetCurveName);
	HeightOffsetForRootBone = HeightOffsetForRootBone * CapsuleComponent->GetScaledCapsuleHalfHeight();
	float NewSkeletalHeight = SkeletalMeshRelativeLocHeight + HeightOffsetForRootBone;

	FVector NewRelativeLocation = SkeletalMeshComp->GetRelativeLocation();
	NewRelativeLocation.Z = NewSkeletalHeight;
	SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);

	// Get sure we return the mesh to the original relative location after vaulting. This might fix
	// small imprecisions with interpolation process.
	if (IsFinishedVaulting)
	{
		NewRelativeLocation.Z = SkeletalMeshRelativeLocHeight;
		SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);
	}
}