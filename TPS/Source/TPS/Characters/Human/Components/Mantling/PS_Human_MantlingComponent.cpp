#include "PS_Human_MantlingComponent.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "../Montages/PS_Human_MontageComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"

UPS_Human_MantlingComponent::UPS_Human_MantlingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UPS_Human_MantlingComponent::BeginPlay()
{
	Super::BeginPlay();

	FetchComponents();

	// Reserve memory for two private arrays.
	CandidateVaultingAnims.Reserve(VaultingAnimations.Num());
	CandidateClimbingAnims.Reserve(ClimbingAnimations.Num());

	// Interpolators.
	VaultingInterpolationHandler.Init(CapsuleComponent, SkeletalMeshComp, this,
		RootHeightOffsetAnimationCurveName, PositionVerticalInterpolationCurveName, PositionHorizontalInterpolationCurveName,
		RotationInterpolationCurveName);
	ClimbingInterpolationHandler.Init(CapsuleComponent, SkeletalMeshComp,
		RootHeightOffsetAnimationCurveName, PositionVerticalInterpolationCurveName, PositionHorizontalInterpolationCurveName,
		RotationInterpolationCurveName);

	// Disable ticking while we don't need it.
	SetComponentTickEnabled(false);
}

void UPS_Human_MantlingComponent::FetchComponents()
{
	Character = Cast<APS_Character>(GetOwner());
	if (!Character)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MantlingComponent::BeginPlay Character Returned Null"));
		return;
	}

	SkeletalMeshComp = Character->FindComponentByClass<USkeletalMeshComponent>();
	if (!SkeletalMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MantlingComponent::BeginPlay SkeletalMeshComponent Returned Null"));
		return;
	}

	CharacterMovementComponent = Character->FindComponentByClass<UCharacterMovementComponent>();
	if (!CharacterMovementComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MantlingComponent::BeginPlay UCharacterMovementComponent Returned Null"));
		return;
	}

	CapsuleComponent = Character->FindComponentByClass<UCapsuleComponent>();
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MantlingComponent::BeginPlay UCapsuleComponent Returned Null"));
		return;
	}

	MontageController = Character->FindComponentByClass<UPS_Human_MontageComponent>();
	if (!MontageController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MantlingComponent::BeginPlay UPS_Human_MontageComponent Returned Null"));
		return;
	}

	VaultingTraceQuery = UEngineTypes::ConvertToTraceType(VaultingChannel);
}

/* Editor only functions. */

void UPS_Human_MantlingComponent::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);

	PreProcessAnimations();
}

void UPS_Human_MantlingComponent::PreProcessAnimations()
{
	MaxMantlingHeight = 0.0f;
	MinMantlingHeight = 0.0f;

	for (int i = 0; i < VaultingAnimations.Num(); ++i)
	{
		FVaultingAnimationData & VaultingAnimation = VaultingAnimations[i];
		MaxMantlingHeight = FMath::Max(VaultingAnimation.MaxVaultingHeight, MaxMantlingHeight);
		MinMantlingHeight = FMath::Max(VaultingAnimation.MinVaultingHeight, MinMantlingHeight);

		if (VaultingAnimation.MaxCharacterSpeedCanPlay <= 0.0f)
			VaultingAnimation.MaxCharacterSpeedCanPlay = 100000.0f;

		if (VaultingAnimation.MinCharacterSpeedCanPlay < 0.0f)
			VaultingAnimation.MinCharacterSpeedCanPlay = 0.0f;
	}

	for (int i = 0; i < ClimbingAnimations.Num(); ++i)
	{
		FClimbingAnimationData & ClimbingAnimation = ClimbingAnimations[i];
		MaxClimbingHeight = FMath::Max(ClimbingAnimation.MaxClimbingHeight, MaxClimbingHeight);

		if (ClimbingAnimation.MaxCharacterSpeedCanPlay <= 0.0f)
			ClimbingAnimation.MaxCharacterSpeedCanPlay = 100000.0f;

		if (ClimbingAnimation.MinCharacterSpeedCanPlay < 0.0f)
			ClimbingAnimation.MinCharacterSpeedCanPlay = 0.0f;
	}
}

void UPS_Human_MantlingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool EndedInterpolation = false;

	if (IsPlayingVaultingMontage)
		EndedInterpolation = VaultingInterpolationHandler.Update(DeltaTime);
	else
		EndedInterpolation = ClimbingInterpolationHandler.Update(DeltaTime);

	if (EndedInterpolation)
	{
		EndMantling();
	}
}

bool UPS_Human_MantlingComponent::TryVaulting()
{
	if (CurrentlyMantling) return false;

	// Do ground check fist.
	TWeakObjectPtr<class AActor> ActorHit;
	FVector ObstacleForwardHitPos, ObstacleForwardHitNormal;
	bool ForwardCheck = ForwardObstacleCheck(ActorHit, ObstacleForwardHitPos, ObstacleForwardHitNormal, MinMantlingHeight);
	if (!ForwardCheck)
		return false;

	// Check the obstacle height. Returns false if too tall.
	FVector HeightHitPosition;
	float ObstacleHeight;
	bool HeightCheck = HeightObstacleCheck(ObstacleForwardHitPos, ObstacleForwardHitNormal, MaxMantlingHeight,
		MinMantlingHeight, HeightHitPosition, ObstacleHeight);
	if (!HeightCheck)
		return false;

	// Check the obstacle thickness.
	// Try Vaulting. If not possible due to anims not meeting speed or height conditions, returns false.
	FVector GroundPositionToEndVaultingAt;
	bool HitGround;
	bool CanVault = VaultObstacleCheck(ActorHit, ObstacleForwardHitPos, HeightHitPosition, GroundPositionToEndVaultingAt, HitGround);
	
	if(CanVault)
		return TryVault(ObstacleForwardHitNormal, HeightHitPosition, ObstacleHeight, GroundPositionToEndVaultingAt, HitGround);

	return false;
}

bool UPS_Human_MantlingComponent::TryClimbing()
{
	if (CurrentlyMantling) return false;

	// Do ground check fist.
	TWeakObjectPtr<class AActor> ActorHit;
	FVector ObstacleForwardHitPos, ObstacleForwardHitNormal;
	bool ForwardCheck = ForwardObstacleCheck(ActorHit, ObstacleForwardHitPos, ObstacleForwardHitNormal, MaxClimbingHeight);
	if (!ForwardCheck)
		return false;

	// Check the obstacle height. Returns false if too tall.
	FVector HeightHitPosition;
	float ObstacleHeight;
	bool HeightCheck = HeightObstacleCheck(ObstacleForwardHitPos, ObstacleForwardHitNormal,
		MaxMantlingHeight, MinMantlingHeight, HeightHitPosition, ObstacleHeight);
	if (!HeightCheck)
		return false;

	// Try Climbing if vaulting didn't succeed or couldn't be done. If not possible due to animations not meeting speed or height
	// conditions, returns false.
	return TryClimb(ObstacleForwardHitNormal, HeightHitPosition, ObstacleHeight);
}

bool UPS_Human_MantlingComponent::ForwardObstacleCheck(TWeakObjectPtr<class AActor> & ActorHit, FVector& ObstacleForwardHitPosition, FVector & ObstacleForwardHitNormal, float HeightToPerformCheckAt)
{
	FVector ActorLocation = SkeletalMeshComp->GetComponentLocation();
	FVector ActorForwardVector = CapsuleComponent->GetForwardVector(); // We use capsule due to skeletal mesh being rotated 90�.
	FVector MinHeightForMantlingPosition = FVector(ActorLocation.X, ActorLocation.Y, ActorLocation.Z + HeightToPerformCheckAt);

	FVector ForwardStartTrace = MinHeightForMantlingPosition + ActorForwardVector;
	FVector ForwardEndTrace = MinHeightForMantlingPosition + ActorForwardVector * MaxDistanceForMantling;

	TArray<AActor*> ActorsToIgnore;
	FHitResult ForwardObstacleHitResult;
	bool HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, ForwardStartTrace, ForwardEndTrace, VaultingTraceQuery,
		false, ActorsToIgnore, DebugTrace, ForwardObstacleHitResult, true);

	if (!HitObstacle)
		return false;

	ActorHit = ForwardObstacleHitResult.Actor;
	ObstacleForwardHitPosition = ForwardObstacleHitResult.ImpactPoint;
	ObstacleForwardHitNormal = ForwardObstacleHitResult.ImpactNormal;

	return true;
}

bool UPS_Human_MantlingComponent::HeightObstacleCheck(const FVector & ObstacleForwardHitPosition, const FVector & ObstacleForwardHitNormal,
	float MaxHeight, float MinHeight, FVector & HeightHitPosition, float & ObstacleHeight)
{
	FVector ActorLocation = SkeletalMeshComp->GetComponentLocation();
	FVector CheckHeightPosition = ObstacleForwardHitPosition + ObstacleForwardHitNormal * -15.0f;

	FVector HeightStartTrace = FVector(CheckHeightPosition.X, CheckHeightPosition.Y, ActorLocation.Z + MaxHeight);
	FVector HeightEndTrace = FVector(CheckHeightPosition.X, CheckHeightPosition.Y, ActorLocation.Z + MinHeight);

	TArray<AActor *> ActorsToIgnore;
	FHitResult HeightHitResult;
	bool HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, HeightStartTrace, HeightEndTrace, VaultingTraceQuery,
		false, ActorsToIgnore, DebugTrace, HeightHitResult, true);


	bool HasEnoughHeightDiff = (HeightHitResult.ImpactPoint.Z - ObstacleForwardHitPosition.Z) > 0.0f;
	if (!HitObstacle || !HeightHitResult.IsValidBlockingHit() || !HasEnoughHeightDiff)
		return false;

	HeightHitPosition = HeightHitResult.ImpactPoint;
	ObstacleHeight = HeightHitPosition.Z - ActorLocation.Z;

	return true;
}

bool UPS_Human_MantlingComponent::VaultObstacleCheck(const TWeakObjectPtr<class AActor> & ActorToVault, const FVector & ObstacleForwardHitPosition, const FVector & HeightHitPosition,
	FVector & GroundPositionToEndVaultingAt, bool & WillHitGroundWhenVaulting)
{
	FVector ActorLocation = SkeletalMeshComp->GetComponentLocation();
	FVector ActorForwardVector = CapsuleComponent->GetForwardVector(); // We use capsule due to skeletal mesh being rotated 90�.

	float FinalVaultingDistance = IdleVaultingDistance;
	if (VaultingDistanceModifierBasedOnSpeed)
		FinalVaultingDistance += VaultingDistanceModifierBasedOnSpeed->GetFloatValue(Character->GetSpeed());
	
	const float SmallOfssetToAvoidOverlappingHits = 20.0f;
	FVector CheckVaultingPositionForward = ObstacleForwardHitPosition + ActorForwardVector * FinalVaultingDistance;
	FVector StartCheckTrace = FVector(CheckVaultingPositionForward.X, CheckVaultingPositionForward.Y, HeightHitPosition.Z + SmallOfssetToAvoidOverlappingHits);
	FVector EndCheckTrace = FVector(CheckVaultingPositionForward.X, CheckVaultingPositionForward.Y, ActorLocation.Z - SmallOfssetToAvoidOverlappingHits);

	// Check position we will vault to.
	TArray<AActor*> ActorsToIgnore;
	FHitResult VaultHitResult;
	bool HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, StartCheckTrace, EndCheckTrace, ETraceTypeQuery::TraceTypeQuery1,
		false, ActorsToIgnore, DebugTrace, VaultHitResult, true);

	WillHitGroundWhenVaulting = HitObstacle;
	if (HitObstacle)
		GroundPositionToEndVaultingAt = VaultHitResult.ImpactPoint;
	else
	{
		EndCheckTrace.Z = EndCheckTrace.Z - CapsuleComponent->GetScaledCapsuleHalfHeight();
		GroundPositionToEndVaultingAt = EndCheckTrace;
	}

	// We need a last check to get sure we can move from origin to ground position. We throw a ray from player location to ground vault location, both
	// with the height of the vault point + a small offset.
	float HeightForHorizontalCanVaultThereCheck = HeightHitPosition.Z + SmallOfssetToAvoidOverlappingHits;
	FVector StartCanMoveCheckTrace = ActorLocation + ActorForwardVector;
	StartCanMoveCheckTrace.Z = HeightForHorizontalCanVaultThereCheck;
	FVector EndCanMoveCheckTrace = FVector(GroundPositionToEndVaultingAt.X, GroundPositionToEndVaultingAt.Y, HeightForHorizontalCanVaultThereCheck);
	FHitResult CanMoveToVaultPosHit;
	HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, StartCanMoveCheckTrace, EndCanMoveCheckTrace, ETraceTypeQuery::TraceTypeQuery1,
		false, ActorsToIgnore, DebugTrace, CanMoveToVaultPosHit, true);

	if (HitObstacle)
		return false;

	return true;
}

bool UPS_Human_MantlingComponent::TryVault(const FVector & ObstacleForwardNormal, const FVector & HeightHitPosition, float ObstacleHeight, const FVector & GroundPositionToEndVaultingAt, bool WillHitGroundWhenVaulting)
{
	float CurrentPlayerSpeed = Character->GetSpeed();
	CandidateVaultingAnims.Empty(CandidateVaultingAnims.Max());

	for (int i = 0; i < VaultingAnimations.Num(); ++i)
	{
		const FVaultingAnimationData & VaultAnim = VaultingAnimations[i];

		// Check speed.
		bool MeetsSpeedLimits = CurrentPlayerSpeed <= VaultAnim.MaxCharacterSpeedCanPlay && CurrentPlayerSpeed >= VaultAnim.MinCharacterSpeedCanPlay;
		if (MeetsSpeedLimits)
		{
			bool MeetsHeightLimits = ObstacleHeight <= VaultAnim.MaxVaultingHeight && ObstacleHeight >= VaultAnim.MinVaultingHeight;
			if (MeetsHeightLimits)
			{
				CandidateVaultingAnims.EmplaceAt(CandidateVaultingAnims.Num(), i);

			}
		}
	}

	// Not a single vaulting animation passed the two tests.
	if (CandidateVaultingAnims.Num() == 0)
		return false;

	// Get a random vaulting animation from the possible ones.
	int RandomIndex = FMath::RandRange(0, CandidateVaultingAnims.Num() - 1);

	// Play the vaulting animation.
	FRotator RotationToFaceTo = (ObstacleForwardNormal * -1.0f).Rotation();
	PlayVaultingAnimation(VaultingAnimations[RandomIndex], HeightHitPosition, GroundPositionToEndVaultingAt, WillHitGroundWhenVaulting, RotationToFaceTo);

	return true;
}

void UPS_Human_MantlingComponent::PlayVaultingAnimation(const FVaultingAnimationData & VaultAnim, const FVector& HeightHitPosition, const FVector& GroundPositionToEndVaultingAt, bool WillHitGroundWhenVaulting, const FRotator & RotationToFaceTo)
{
	CurrentlyMantling = true;

	IsPlayingVaultingMontage = true;

	VaultingInterpolationHandler.Start(VaultAnim, HeightHitPosition, GroundPositionToEndVaultingAt, WillHitGroundWhenVaulting, RotationToFaceTo);

	Character->SetEnableCharacterMovementAndActions(false);

	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Flying);

	MontageController->PlayAnimationMontage(VaultAnim.VaultingMontage, 1.0f, 0.0f, "", true);

	SetComponentTickEnabled(true);
}

bool UPS_Human_MantlingComponent::TryClimb(const FVector & ObstacleForwardNormal, const FVector & HeightHitPosition, float ObstacleHeight)
{
	float CurrentPlayerSpeed = Character->GetSpeed();
	CandidateClimbingAnims.Empty(CandidateClimbingAnims.Max());

	for (int i = 0; i < ClimbingAnimations.Num(); ++i)
	{
		const FClimbingAnimationData & ClimbAnim = ClimbingAnimations[i];

		// Check speed.
		bool MeetsSpeedLimits = CurrentPlayerSpeed <= ClimbAnim.MaxCharacterSpeedCanPlay && CurrentPlayerSpeed >= ClimbAnim.MinCharacterSpeedCanPlay;
		if (MeetsSpeedLimits)
		{
			bool MeetsHeightLimits = ObstacleHeight <= ClimbAnim.MaxClimbingHeight && ObstacleHeight >= ClimbAnim.MinClimbingHeight;
			if (MeetsHeightLimits)
			{
				CandidateClimbingAnims.EmplaceAt(CandidateClimbingAnims.Num(), i);

			}
		}
	}

	// Not a single climbing animation passed the two tests.
	if (CandidateClimbingAnims.Num() == 0)
		return false;

	// Get a random climbing animation from the possible ones.
	int RandomIndex = FMath::RandRange(0, CandidateClimbingAnims.Num() - 1);

	// Play the climbing animation.
	FRotator RotationToFaceTo = (ObstacleForwardNormal * -1.0f).Rotation();
	PlayClimbingAnimation(ClimbingAnimations[RandomIndex], HeightHitPosition, RotationToFaceTo);

	return true;
}

void UPS_Human_MantlingComponent::PlayClimbingAnimation(const FClimbingAnimationData & ClimbAnim, const FVector& HeightHitPosition, const FRotator& RotationToFaceTo)
{
	CurrentlyMantling = true;

	IsPlayingVaultingMontage = false;

	ClimbingInterpolationHandler.Start(ClimbAnim, HeightHitPosition, RotationToFaceTo);

	Character->SetEnableCharacterMovementAndActions(false);

	Character->SetLegIKEnabled(false);

	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Flying);

	MontageController->PlayAnimationMontage(ClimbAnim.ClimbingMontage, 1.0f, 0.0f, "", true);

	SetComponentTickEnabled(true);
}

// Called when we are falling to the ground after vaulting an obstacle but we didn't hit any object.
// We set the player to falling mode. Allows correct blending of the vaulting anim with falling animations.
void UPS_Human_MantlingComponent::OnVaultingFallStart()
{
	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Falling);
}

void UPS_Human_MantlingComponent::StartVaultingBlending(UAnimMontage* AnimMontage, float BlendingOut)
{
	if(MontageController)
		MontageController->StopAnimationMontage(AnimMontage, BlendingOut);
}

// Ends mantling, called by Update once mantling animations are finished.
void UPS_Human_MantlingComponent::EndMantling()
{
	CurrentlyMantling = false;

	Character->SetEnableCharacterMovementAndActions(true);

	Character->SetLegIKEnabled(true);

	if(CharacterMovementComponent->IsMovingOnGround())
		CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Walking);
	else if (CharacterMovementComponent->IsFlying())
		CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Falling);
	else if(CharacterMovementComponent->IsSwimming())
		CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Swimming);

	SetComponentTickEnabled(false);
}