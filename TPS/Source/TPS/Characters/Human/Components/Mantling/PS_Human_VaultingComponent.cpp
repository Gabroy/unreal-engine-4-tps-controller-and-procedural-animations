#include "PS_Human_VaultingComponent.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "../../../Common/Animations/PS_Extended_AnimInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../Locomotion/PS_Human_LocomotionController.h"

UPS_Human_VaultingComponent::UPS_Human_VaultingComponent() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UPS_Human_VaultingComponent::BeginPlay()
{
	Super::BeginPlay();

	FetchComponents();

	// Reserve memory for the candidate vaulting array.
	CandidateVaultingAnims.Reserve(VaultingAnimations.Num());

	// Init the vaulting interpolator.
	VaultingInterpolationHandler.Init(CapsuleComponent, SkeletalMeshComp, this,
		RootHeightOffsetAnimationCurveName, PositionVerticalInterpolationCurveName, PositionHorizontalInterpolationCurveName,
		RotationInterpolationCurveName);

	// Disable ticking while we don't need it.
	SetComponentTickEnabled(false);
}

void UPS_Human_VaultingComponent::FetchComponents()
{
	Character = Cast<APS_Character>(GetOwner());
	if (!Character)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_VaultingComponent::BeginPlay Character Returned Null"));
		return;
	}

	SkeletalMeshComp = Cast<USkeletalMeshComponent>(Character->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (!SkeletalMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_VaultingComponent::BeginPlay SkeletalMeshComponent Returned Null"));
		return;
	}

	CharacterMovementComponent = Cast<UCharacterMovementComponent>(Character->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!CharacterMovementComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_VaultingComponent::BeginPlay UCharacterMovementComponent Returned Null"));
		return;
	}

	CapsuleComponent = Cast<UCapsuleComponent>(Character->GetComponentByClass(UCapsuleComponent::StaticClass()));
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_VaultingComponent::BeginPlay UCapsuleComponent Returned Null"));
		return;
	}

	AnimInstance = (UPS_Extended_AnimInstance*)SkeletalMeshComp->GetAnimInstance();
	if (!AnimInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_VaultingComponent::BeginPlay UPS_Extended_AnimInstance Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Character->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_VaultingComponent::BeginPlay LocController Returned Null"));
		return;
	}

	VaultingTraceQuery = UEngineTypes::ConvertToTraceType(VaultingChannel);
}

/* Editor only functions. */

void UPS_Human_VaultingComponent::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);

	PreProcessVaultingVars();
}

void UPS_Human_VaultingComponent::PreProcessVaultingVars()
{
	const float VeryHighNumber = 100000.0f;

	MaxDistanceToAllowVaulting = 0.0f;
	MinDistanceToAllowVaulting = VeryHighNumber;
	MaxVaultingHeight = 0.0f;
	MinVaultingHeight = VeryHighNumber;

	for (int i = 0; i < VaultingAnimations.Num(); ++i)
	{
		FVaultingAnimationData & VaultingAnimation = VaultingAnimations[i];

		if (VaultingAnimation.MaxCharacterSpeedCanPlay <= 0.0f)
			VaultingAnimation.MaxCharacterSpeedCanPlay = VeryHighNumber;

		if (VaultingAnimation.MinCharacterSpeedCanPlay < 0.0f)
			VaultingAnimation.MinCharacterSpeedCanPlay = 0.0f;

		MaxDistanceToAllowVaulting = FMath::Max(VaultingAnimation.MaxDistanceCanPlay, MaxDistanceToAllowVaulting);
		MinDistanceToAllowVaulting = FMath::Min(VaultingAnimation.MinDistanceCanPlay, MinDistanceToAllowVaulting);
		MaxVaultingHeight = FMath::Max(VaultingAnimation.MaxVaultingHeight, MaxVaultingHeight);
		MinVaultingHeight = FMath::Min(VaultingAnimation.MinVaultingHeight, MinVaultingHeight);
	}
}

/* Public functions */

// Ticking behaviour is enabled only while we are vaulting and gets disabled once it ends.
void UPS_Human_VaultingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool EndedInterpolation = VaultingInterpolationHandler.Update(DeltaTime);
	if (EndedInterpolation)
		EndVaulting();
}

// Check if the player can currently vault an obstacle. If so returns true and start vaulting. Otherwise, return false and don't vault.
bool UPS_Human_VaultingComponent::TryVaulting()
{
	if (IsCurrentlyVaulting) return false;

	// Do ground check fist.
	FHitResult ObstacleHit;
	bool ForwardCheck = ForwardObstacleCheck(ObstacleHit);
	if (!ForwardCheck)
		return false;

	// Check the obstacle height. Returns false if too tall.
	FVector HeightHitPosition;
	float ObstacleHeight;
	bool HeightCheck = HeightObstacleCheck(ObstacleHit, HeightHitPosition, ObstacleHeight);
	if (!HeightCheck)
		return false;

	// Check the obstacle thickness.
	// Try Vaulting. If not possible due to anims not meeting speed or height conditions, returns false.
	FVector GroundPositionToEndVaultingAt;
	bool HitGround;
	bool CanVault = VaultObstacleCheck(ObstacleHit, HeightHitPosition, GroundPositionToEndVaultingAt, HitGround);
	if(CanVault)
		return TryChooseVault(ObstacleHit, HeightHitPosition, ObstacleHeight, GroundPositionToEndVaultingAt, HitGround);

	return false;
}

/* Protected functions. */

bool UPS_Human_VaultingComponent::ForwardObstacleCheck(FHitResult & ObstacleHit)
{
	FVector ActorLocation = SkeletalMeshComp->GetComponentLocation();
	FVector ActorForwardVector = CapsuleComponent->GetForwardVector(); // We use capsule due to skeletal mesh being rotated 90�.
	FVector MinHeightForMantlingPosition = FVector(ActorLocation.X, ActorLocation.Y, ActorLocation.Z + MinVaultingHeight);

	FVector ForwardStartTrace = MinHeightForMantlingPosition + ActorForwardVector * MinDistanceToAllowVaulting;
	FVector ForwardEndTrace = MinHeightForMantlingPosition + ActorForwardVector * MaxDistanceToAllowVaulting;

	TArray<AActor *> ActorsToIgnore;
	bool HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, ForwardStartTrace, ForwardEndTrace, VaultingTraceQuery,
		false, ActorsToIgnore, DebugTrace, ObstacleHit, true);

	if (!HitObstacle)
		return false;

	return true;
}

bool UPS_Human_VaultingComponent::HeightObstacleCheck(FHitResult & ObstacleHit, FVector & HeightHitPosition, float & ObstacleHeight)
{
	FVector ActorLocation = SkeletalMeshComp->GetComponentLocation();
	const float SmallOffsetToCheckHeight = -15.0f;
	FVector CheckHeightPosition = ObstacleHit.ImpactPoint + ObstacleHit.ImpactNormal * SmallOffsetToCheckHeight; // We do the check a bit after the obstacle forward position.

	FVector HeightStartTrace = FVector(CheckHeightPosition.X, CheckHeightPosition.Y, ActorLocation.Z + MaxVaultingHeight);
	FVector HeightEndTrace = FVector(CheckHeightPosition.X, CheckHeightPosition.Y, ActorLocation.Z + MinVaultingHeight);

	TArray<AActor *> ActorsToIgnore;
	FHitResult HeightHitResult;
	bool HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, HeightStartTrace, HeightEndTrace, VaultingTraceQuery,
		true, ActorsToIgnore, DebugTrace, HeightHitResult, true);

	// We ignore vaulting if the height ending position is somehow smaller than the the starting position. This might happen
	// if ray started inside object for example.
	bool HasEnoughHeightDiff = (HeightHitResult.ImpactPoint.Z - ObstacleHit.ImpactPoint.Z) > 0.0f;
	if (!HitObstacle || HeightHitResult.bStartPenetrating || !HeightHitResult.IsValidBlockingHit() || !HasEnoughHeightDiff)
		return false;

	HeightHitPosition = HeightHitResult.ImpactPoint;
	ObstacleHeight = HeightHitPosition.Z - ActorLocation.Z;

	return true;
}

bool UPS_Human_VaultingComponent::VaultObstacleCheck(FHitResult& ObstacleHit, const FVector & HeightHitPosition, FVector & GroundPositionToEndVaultingAt, bool & WillHitGroundWhenVaulting)
{
	FVector ActorLocation = SkeletalMeshComp->GetComponentLocation();
	FVector ActorForwardVector = CapsuleComponent->GetForwardVector(); // We use capsule due to skeletal mesh being rotated 90�.

	// Compute the distance we will end at after vaulting the object. This is based on the Base vault distance and the vaulting
	// curve controlled by the speed.
	float FinalVaultingDistance = BaseVaultingDisplacementDistance;
	if (VaultingDistanceModifierBasedOnSpeed)
		FinalVaultingDistance += VaultingDistanceModifierBasedOnSpeed->GetFloatValue(Character->GetSpeed());
	
	// Get the start and end check. We throw a vertical ray pushed forward that goes from the height hit height to the ground (with a small offset in both sides
	// to avoid possible issues with precision and rays not actually hitting).
	const float SmallOfssetToAvoidOverlappingHits = 20.0f;
	FVector CheckVaultingPositionForward = ObstacleHit.ImpactPoint + ActorForwardVector * FinalVaultingDistance;
	FVector StartCheckTrace = FVector(CheckVaultingPositionForward.X, CheckVaultingPositionForward.Y, HeightHitPosition.Z + SmallOfssetToAvoidOverlappingHits);
	FVector EndCheckTrace = FVector(CheckVaultingPositionForward.X, CheckVaultingPositionForward.Y, ActorLocation.Z - SmallOfssetToAvoidOverlappingHits);

	// We throw the ray and check the position we will end up vaulting to.
	TArray<AActor*> ActorsToIgnore;
	FHitResult VaultHitResult;
	bool HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, StartCheckTrace, EndCheckTrace, ETraceTypeQuery::TraceTypeQuery1,
		false, ActorsToIgnore, DebugTrace, VaultHitResult, true);

	WillHitGroundWhenVaulting = HitObstacle;
	if (HitObstacle)
	{
		// We don't vault if the height difference with the ending vault point is at least a quarter of the height of the character.
		// In such cases vaulting animations would look weird so we avoid them. If the object is thick enough, we could still climb it.
		const float MaxHeightToAllowVaulting = HeightHitPosition.Z - MinHeightDiffFromVaultPointToGroundToAllowVault;
		if (VaultHitResult.ImpactPoint.Z >= MaxHeightToAllowVaulting)
			return false;

		GroundPositionToEndVaultingAt = VaultHitResult.ImpactPoint;
	}
	else
	{
		// If we are not hitting the obstacle, we will displace ourselves to the end check traze heigt minus the capsule height.
		// This means we are vaulting and starting to fall.
		EndCheckTrace.Z = EndCheckTrace.Z - CapsuleComponent->GetScaledCapsuleHalfHeight();
		GroundPositionToEndVaultingAt = EndCheckTrace;
	}

	// We need a last check to get sure we can move from origin to ground position. We throw a ray from player location to ground vault location, both
	// with the height of the vault point + a small offset.
	float HeightForHorizontalCanVaultThereCheck = HeightHitPosition.Z + SmallOfssetToAvoidOverlappingHits;
	FVector StartCanMoveCheckTrace = ActorLocation + ActorForwardVector;
	StartCanMoveCheckTrace.Z = HeightForHorizontalCanVaultThereCheck;
	FVector EndCanMoveCheckTrace = FVector(GroundPositionToEndVaultingAt.X, GroundPositionToEndVaultingAt.Y, HeightForHorizontalCanVaultThereCheck);
	
	FHitResult CanMoveToVaultPosHit;
	HitObstacle = UKismetSystemLibrary::LineTraceSingle(this, StartCanMoveCheckTrace, EndCanMoveCheckTrace, ETraceTypeQuery::TraceTypeQuery1,
		false, ActorsToIgnore, DebugTrace, CanMoveToVaultPosHit, true);
	if (HitObstacle)
		return false;

	return true;
}

bool UPS_Human_VaultingComponent::TryChooseVault(const FHitResult& Obstacle, const FVector & HeightHitPosition, float ObstacleHeight,
	const FVector & GroundPositionToEndVaultingAt, bool WillHitGroundWhenVaulting)
{
	// Pick vaulting animation to play.
	float CurrentPlayerSpeed = Character->GetSpeed();
	CandidateVaultingAnims.Empty(CandidateVaultingAnims.Max());
	for (int i = 0; i < VaultingAnimations.Num(); ++i)
	{
		const FVaultingAnimationData & VaultAnim = VaultingAnimations[i];

		// For vaulting animations we must check we meet distance, height and speed limits.
		bool MeetsDistanceLimits = Obstacle.Distance <= VaultAnim.MaxDistanceCanPlay && Obstacle.Distance >= VaultAnim.MinDistanceCanPlay;
		if (MeetsDistanceLimits)
		{
			bool MeetsSpeedLimits = CurrentPlayerSpeed <= VaultAnim.MaxCharacterSpeedCanPlay && CurrentPlayerSpeed >= VaultAnim.MinCharacterSpeedCanPlay;
			if (MeetsSpeedLimits)
			{
				bool MeetsHeightLimits = ObstacleHeight <= VaultAnim.MaxVaultingHeight && ObstacleHeight >= VaultAnim.MinVaultingHeight;
				if (MeetsHeightLimits)
					CandidateVaultingAnims.EmplaceAt(CandidateVaultingAnims.Num(), i);
			}
		}
	}

	// Not a single vaulting animation passed the two tests. We can't vault. Return false.
	if (CandidateVaultingAnims.Num() == 0)
		return false;

	// Get a random vaulting animation from the possible ones.
	int RandomIndex = FMath::RandRange(0, CandidateVaultingAnims.Num() - 1);

	// Play the vaulting animation.
	FRotator RotationToFaceTo = (Obstacle.ImpactNormal * -1.0f).Rotation();
	PlayVaultingAnimation(VaultingAnimations[RandomIndex], Obstacle, HeightHitPosition, GroundPositionToEndVaultingAt, WillHitGroundWhenVaulting, RotationToFaceTo);

	return true;
}

void UPS_Human_VaultingComponent::PlayVaultingAnimation(const FVaultingAnimationData & VaultAnim, const FHitResult& Obstacle, const FVector& HeightHitPosition, const FVector& GroundPositionToEndVaultingAt, bool WillHitGroundWhenVaulting, const FRotator & RotationToFaceTo)
{
	IsCurrentlyVaulting = true;

	// Store the info of the hit, we will use the info of the actor to disable collision in parts of the animation to prevent camera issues.
	// This is done through the use of anim notifies.
	VaultingObject = Obstacle;

	VaultingInterpolationHandler.Start(VaultAnim, HeightHitPosition, GroundPositionToEndVaultingAt, WillHitGroundWhenVaulting, RotationToFaceTo);

	Character->SetEnableCharacterMovementAndActions(false);

	LocController->SetLegIKEnabledStatus(false);

	LocController->SetRootMotionMovement(true);

	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Flying);

	AnimInstance->PlayAnimationMontage(VaultAnim.VaultingMontage, 1.0f, 0.0f, "", true);

	if (AnimInstance)
		SetComponentTickEnabled(true);
}

// Called when we are falling to the ground after vaulting an obstacle but we didn't hit any object.
// We set the player to falling mode. Allows correct blending of the vaulting anim with falling animations.
void UPS_Human_VaultingComponent::OnVaultingFallStart()
{
	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Falling);
}

// Sets the vaulting blending out that gets called when we are falling to the ground but we didn't hit any object.
// Allows smoother blending of vaulting with falling animations.
void UPS_Human_VaultingComponent::StartVaultingBlending(UAnimMontage* AnimMontage, float BlendingOut)
{
	if(AnimInstance)
		AnimInstance->MontageStop(AnimMontage, BlendingOut);
}

// Ends mantling, called by Update once mantling animations are finished.
void UPS_Human_VaultingComponent::EndVaulting()
{
	IsCurrentlyVaulting = false;

	Character->SetEnableCharacterMovementAndActions(true);

	LocController->SetLegIKEnabledStatus(true);

	LocController->SetRootMotionMovement(false);

	if(CharacterMovementComponent->IsMovingOnGround())
		CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Walking);
	else if (CharacterMovementComponent->IsFlying())
		CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Falling);
	else if(CharacterMovementComponent->IsSwimming())
		CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Swimming);

	SetComponentTickEnabled(false);
}