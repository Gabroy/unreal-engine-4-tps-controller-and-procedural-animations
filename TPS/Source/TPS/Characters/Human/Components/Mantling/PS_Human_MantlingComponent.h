#pragma once

#include "CoreMinimal.h"
#include "PS_Human_MantlingStruct.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_Human_MantlingComponent.generated.h"

class APS_Character;
class USkeletalMeshComponent;
class UCharacterMovementComponent;
class UCapsuleComponent;
class UPS_Human_MontageComponent;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_MantlingComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Components. */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Character = nullptr;

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCharacterMovementComponent * CharacterMovementComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCapsuleComponent * CapsuleComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_MontageComponent * MontageController = nullptr;

	/* Mantling general variables. */

	// How far from an object we can be to be able to mantle (vault or climb) the object.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - General")
	float MaxDistanceForMantling = 0.0f;

	// Height from the base of the player from where the first check (going from the chest of the actor)
	// will be done for mantling.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - General")
	float ChestCheckHeight = 160.0f;

	// Name of curve animation used to adjust the skeletal mesh while playing animations so the mesh changes height independent of the
	// capsule it's attached to so we avoid flying animations and they appear grounded in the object being vaulted.
	// Values go between -1.0f and 1.0f where 1.0f is the half height of the capsule component.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - General")
	FName RootHeightOffsetAnimationCurveName = "RootHeightOffset";

	// Name of curve animation used to adjust the interpolation of the vertical position of the player while vaulting or climbing.
	// Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting or end of climbing) and return to 0.0f at the end in the case of vaulting.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - General")
	FName PositionVerticalInterpolationCurveName = "MantlingVerticalTranslationInterp";

	// Name of curve animation used to adjust the interpolation of the horizontal position of the player while vaulting or climbing.
	// Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting or end of climbing) and return to 0.0f at the end in the case of vaulting.
	// Allows controlling the horizontal lerping of the capsule allowing certain animations to better adapt to the obstacles being vaulted over.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - General")
	FName PositionHorizontalInterpolationCurveName = "MantlingHorizontalTranslationInterp";

	// Name of curve animation used to adjust the interpolation of the rotation of the player while vaulting or climbing.
	// Should go from 0.0f to 1.0f (where 1.0f is the part of the animation where we want to be already fully rotated in the correct direction.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - General")
	FName RotationInterpolationCurveName = "MantlingRotationInterp";

	/* Vaulting variables */

	// Distance that can be vaulted by vaulting animations by default from an idle stance.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Vaulting")
	float IdleVaultingDistance = 120.0f;

	// Curve float adding an offset to the idle vaulting distance based on the current speed of the player.
	// Allows vaulting further or closer based on the speed of the character.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Vaulting")
	UCurveFloat * VaultingDistanceModifierBasedOnSpeed = nullptr;

	// Max mantling height. Computed from the maximum height of animations (climbing and vaulting).
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Vaulting")
	float MaxMantlingHeight = 300.0f;

	// Min mantling height. Computed from the maximum height of animations (climbing and vaulting).
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Vaulting")
	float MinMantlingHeight = 30.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Vaulting")
	TArray<FVaultingAnimationData> VaultingAnimations;

	ECollisionChannel VaultingChannel = ECollisionChannel::ECC_GameTraceChannel1;
	ETraceTypeQuery VaultingTraceQuery;

	/* Climbing animations */

	// Max climbing height
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Climbing")
	float MaxClimbingHeight = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Mantling - Climbing")
	TArray<FClimbingAnimationData> ClimbingAnimations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TEnumAsByte<EDrawDebugTrace::Type> DebugTrace = EDrawDebugTrace::None;

	/* Protected functions. */

	UPS_Human_MantlingComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	void FetchComponents();

	/* Editor only functions. */

#if WITH_EDITOR
	// Will call PreProcessAnimations to update values when a value is added to any of the two TArrays.
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent & PropertyChangedEvent) override;

	// Reads the arrays and fetches the correct values.
	void PreProcessAnimations();
#endif


public:

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction);

	UFUNCTION(BlueprintCallable)
	bool TryVaulting();

	UFUNCTION(BlueprintCallable)
	bool TryClimbing();

protected:

	/* Protected variables. */

	bool CurrentlyMantling = false;

	// Used to check which vaulting anims meet all conditions and could be played. A random one is picked.
	TArray<int> CandidateVaultingAnims;

	// Used to check which climbing anims meet all conditions and could be played. A random one is picked.
	TArray<int> CandidateClimbingAnims;

	/* Interpolation variables. */

	bool IsPlayingVaultingMontage = false;

	FVaultingInterpolation VaultingInterpolationHandler;

	FClimbingInterpolation ClimbingInterpolationHandler;


	/* Protected functions. */

	// Forward obstacle check.
	bool ForwardObstacleCheck(TWeakObjectPtr<class AActor> & ActorHit, FVector & ObstacleForwardHitPosition, FVector& ObstacleForwardHitNormal, float HeightToPerformCheckAt);

	// Height obstacle check.
	bool HeightObstacleCheck(const FVector & ObstacleForwardHitPosition, const FVector & ObstacleForwardHitNormal,
		float MaxHeight, float MinHeight, FVector & HeightHitPosition, float & ObstacleHeight);

	// Vault obstacle check. Returns true if it hit ground, false otherwise.
	bool VaultObstacleCheck(const TWeakObjectPtr<class AActor> & ActorToVault, const FVector & ObstacleForwardHitPosition, const FVector & HeightHitPosition,
		FVector & GroundPositionToEndVaultingAt, bool & WillHitGroundWhenVaulting);

	// Tries vaulting with given the passed parameters. Returns false if none of the vaulting
	// anims meet the speed or height requirements.
	bool TryVault(const FVector & ObstacleForwardNormal, const FVector & HeightHitPosition, float ObstacleHeight, const FVector & GroundPositionToEndVaultingAt, bool WillHitGroundWhenVaulting);

	void PlayVaultingAnimation(const FVaultingAnimationData & VaultAnim, const FVector& HeightHitPosition, const FVector& GroundPositionToEndVaultingAt, bool HitGround, const FRotator& RotationToFaceTo);

	// Tries climbing with given the passed parameters. Returns false if none of the climbing
	// anims meet the speed or height requirements.
	bool TryClimb(const FVector& ObstacleForwardNormal, const FVector& HeightHitPosition, float ObstacleHeight);

	void PlayClimbingAnimation(const FClimbingAnimationData & ClimbAnim, const FVector& HeightHitPosition, const FRotator& RotationToFaceTo);

	friend struct FVaultingInterpolation;

	// Called when we are falling to the ground after vaulting an obstacle but we didn't hit any object.
	// We set the player to falling mode. Allows correct blending of the vaulting anim with falling animations.
	void OnVaultingFallStart();

	// Sets the vaulting blending out that gets called when we are falling to the ground but we didn't hit any object.
	// Allows smoother blending of vaulting with falling animations.
	void StartVaultingBlending(UAnimMontage* AnimMontage, float BlendingOut);

	// Ends mantling, called by Update once mantling animations are finished.
	void EndMantling();
};
