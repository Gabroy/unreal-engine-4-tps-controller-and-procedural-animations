#pragma once

#include "CoreMinimal.h"
#include "PS_Human_ClimbingStruct.h"
#include "Engine/EngineTypes.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_Human_ClimbingComponent.generated.h"

class APS_Character;
class USkeletalMeshComponent;
class UCharacterMovementComponent;
class UCapsuleComponent;
class UPS_Human_AnimationInstance;
class UPS_Human_LocomotionController;
class UPS_LocState_Climb;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_ClimbingComponent : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Components. */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Character = nullptr;

	UPROPERTY(BlueprintReadOnly)
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCharacterMovementComponent * CharacterMovementComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCapsuleComponent * CapsuleComponent = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_LocomotionController * LocController = nullptr;

	/* Climbing variables. */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing")
	TEnumAsByte<ECollisionChannel> ClimbingChannel = ECollisionChannel::ECC_GameTraceChannel2;
	ETraceTypeQuery ClimbingTraceQuery;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climbing")
	TEnumAsByte<EDrawDebugTrace::Type> DebugTrace = EDrawDebugTrace::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Climbing")
	TEnumAsByte<EDrawDebugTrace::Type> HangingDebugTrace = EDrawDebugTrace::None;
	
	// Half height of the character when not crouched. Fetched at the beginning.
	float CharacterHalfHeight = 0.0f;

	// Checks.

	// Offset added to control the position where the player will end at after climbing an obstacle.
	// It's added to the hit position depth and will calculate the point where the player ends after ending
	// the climbing montage.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing - Checks")
	float ClimbingEndPositionOffset = 40.0f;

	// Grounded check variables.

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Climbing - Grounded")
	float Grounded_MaxClimbingDistance = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Climbing - Grounded")
	float Grounded_MinClimbingDistance = 0.0f;

	// Max climbing height while grounded. 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Climbing - Grounded")
	float Grounded_MaxClimbingHeight = 300.0f;

	// Min climbing height while grounded. Smaller obstacles, we won't be able to climb them.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Climbing - Grounded")
	float Grounded_MinClimbingHeight = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing - Grounded")
	TArray<FClimbingAnimationData> GroundedClimbingAnimations;

	/* Climb State pointer. Used to perform the can hang from ledge checks. */

	float LedgeHandCheckDepthOffset;
	float HeightToPerformLedgeCheckAt;
	float MaxDistanceToGrabToLedge;

	/* Climbing animations */

	// Controls extra height offset applied to animation. Positive values raise mesh and negative lower it.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing - Curves")
	FName RootHeightOffsetAnimationCurveName = "RootHeightOffset";

	// Controls horizontal movement. Anim modifier RootMotion_Climbing will create them. Go from 0.0f to 1.0f and back to 0.0f.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing - Curves")
	FName PositionHorizontalCoord = "RootMotionHorizontal";

	// Controls vertical movement. Anim modifier RootMotion_Climbing will create them. Go from 0.0f to 1.0f and back to 0.0f.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Climbing - Curves")
	FName PositionVerticalCoord = "RootMotionVertical";

	/* Protected variables. */

	bool IsCurrentlyClimbing = false;

	// Object we are climbing. We store a reference to it to allow modifying its collision response to the camera in parts of the animation
	// to avoid issues with camera collision.
	UPROPERTY(BlueprintReadOnly)
	FHitResult ClimbingObject;

	// Size of the radius of the sphere trace checks launched in different parts of the code.
	const float SphereCheckRadius = 5.0f;

	// Used to check which climbing anims meet all conditions and could be played. A random one is picked.
	TArray<int> CandidateClimbingAnims;

	FClimbingInterpolation ClimbingInterpolationHandler;

	/* Protected functions. */

	UPS_Human_ClimbingComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	void FetchComponents();

	/* Editor only functions. */

#if WITH_EDITOR
	// Will call PreProcessAnimations to update values when a value is added to any of the two TArrays.
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent & PropertyChangedEvent) override;

	// Reads the arrays and fetches the correct values.
	void PreProcessAnimations();
#endif


public:

	/* Public functions */

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction);

	// Should be called after TryVaulting if we want to be able to vault and climb obstacles.
	// Only to be used when we want to directly climb an obstacle and not grab it.
	// If called after vaulting, obstacles that can be vaulted, will be vaulted, and those that can't
	// but can be climbed, while be climbed instead.
	UFUNCTION(BlueprintCallable)
	bool TryClimb();

	UFUNCTION(BlueprintCallable)
	bool GetIsCurrentlyClimbing() const;

protected:

	/* Protected functions. */

	/* Init functions */

	UFUNCTION()
	void OnLocomotionCompInitialized();

	/* Check functions */

	// Forward obstacle check.
	bool ForwardObstacleCheck(float MinCheckDistance, float MaxCheckDistance, float HeightToPerformCheckAt, FHitResult & ObstacleHit, EDrawDebugTrace::Type Debug);

	bool ForwardAndThicknessCheck(float MinCheckDistance, float MaxCheckDistance, float HeightToPerformCheckAt, float SideOffset, FHitResult& ObstacleHit, bool& ThicknessSuccess, EDrawDebugTrace::Type Debug);

	// Height obstacle check.
	bool HeightObstacleCheck(float MinHeight, float MaxHeight, float ForwardOffset, const FHitResult & ForwardHitInformation,
		FVector & HeightHitPosition, float & ObstacleHeight, FVector& LedgePos, EDrawDebugTrace::Type Debug);

	// Ledge obstacle check.
	bool LedgeObstacleCheck(FHitResult & ForwardHitInformation, FVector & HeightHitPosition, bool ComputeNewLedgePos, FVector & LedgePos, FVector& LedgeNormal, EDrawDebugTrace::Type Debug);

	float ComputeDeltaAngleWithSurface(const FVector & SurfaceNormal);

	/* Grounded animations. */

	// Tries climbing with given the passed parameters. Returns false if none of the climbing
	// anims meet the speed or height requirements.
	bool ChooseClimbFromGrounded(const FHitResult & Obstacle, const FVector& HeightHitPosition, float ObstacleHeight, const FVector & LedgePos);

	void PlayClimbingAnimation(const FClimbingAnimationData & ClimbAnim, const FHitResult & Obstacle, const FVector& LedgePos, const FVector& HeightHitPosition, const FRotator& RotationToFaceTo);

	// Ends climbing, called by Update once climbing animations are finished.
	void EndClimbing();
};
