#pragma once

#include "PS_Human_ClimbingStruct.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"

/* Interpolation struct used for mantling component climbing animations. */

// Initializes the interpolation struct.
void FClimbingInterpolation::Init(UCapsuleComponent * CapsuleComp, USkeletalMeshComponent * SkeletalMesh,
	const FName & NewHeightOffsetCurveName, const FName & NewPositionHorizontalCoord, const FName & NewPositionVerticalCoord)
{
	CapsuleComponent = CapsuleComp;
	SkeletalMeshComp = SkeletalMesh;
	if (SkeletalMeshComp)
		SkeletalMeshRelativeLocHeight = SkeletalMeshComp->GetRelativeLocation().Z;

	HeightOffsetCurveName = NewHeightOffsetCurveName;
	PositionHorizontalCoord = NewPositionHorizontalCoord;
	PositionVerticalCoord = NewPositionVerticalCoord;

	ClimbingPtr = nullptr;
}

// Starts the the interpolation struct setting all data from the vaulting animation data so
// it's ready to start being interpolated.
void FClimbingInterpolation::Start(const FClimbingAnimationData & ClimbData, const FVector& NewLedgePos, const FVector & NewTargetHeightObstaclePosition, const FRotator & TargetRotator)
{
	CurrentInterpolationTime = 0.0f;
	TimeToReachLedge = ClimbData.TimeReachLedge;
	TotalAnimationTime = ClimbData.ClimbingMontage->GetPlayLength();

	// Fetch current positions.
	FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
	CurrentCapsulePosition = CapsuleTransform.GetLocation();
	CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();

	// Targets.
	TargetLedgePosition = NewLedgePos;
	TargetLedgePosition.Z += CapsuleComponent->GetScaledCapsuleHalfHeight() * ClimbData.LedgeHeightOffsetFactor;
	TargetHeightObstaclePosition = NewTargetHeightObstaclePosition + CapsuleComponent->GetForwardVector() * ClimbData.FinalPositionForwardOffset;
	TargetRotationToFaceTo = TargetRotator;

	// Finally, set the function ptr to the start climbing step.
	ClimbingPtr = &FClimbingInterpolation::ToHangStep;
}

// Updates player position during vaulting animations.
// Returns true once interpolation is done. False otherwise.
bool FClimbingInterpolation::Update(float dt)
{
	if (!CapsuleComponent || !SkeletalMeshComp || !ClimbingPtr)
		return false;

	CurrentInterpolationTime += dt;

	bool FinishedClimbing = (this->*ClimbingPtr)(dt);
	UpdateMeshRelativeLocation(FinishedClimbing);
	return FinishedClimbing;
}

// Function for the first part of the climbing process.
bool FClimbingInterpolation::ToHangStep(float dt)
{
	UAnimInstance* Instance = SkeletalMeshComp->GetAnimInstance();
	if (!Instance)
		return false;

	float PosHorizontalFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalCoord), 1.0f);
	float PosVerticalFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalCoord), 1.0f);

	// Position Interpolation. Blending is inverted to avoid issue with blending out montages returning 0.0f values while blending out.
	// 1.0f means start position, and 0.0f means ends.
	float NewCoordX = FMath::Lerp(CurrentCapsulePosition.X, TargetLedgePosition.X, PosHorizontalFactor);
	float NewCoordY = FMath::Lerp(CurrentCapsulePosition.Y, TargetLedgePosition.Y, PosHorizontalFactor);
	float NewCoordZ = FMath::Lerp(CurrentCapsulePosition.Z, TargetLedgePosition.Z, PosVerticalFactor);
	FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
	CapsuleComponent->SetWorldLocation(NewCapsulePosition);

	// We have reached the ledge of the obstacle. Now we start climbing
	if (CurrentInterpolationTime >= TimeToReachLedge)
	{
		ClimbingPtr = &FClimbingInterpolation::ClimbStep;

		// Update current values for next part of vaulting.
		FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
		CurrentCapsulePosition = CapsuleTransform.GetLocation();
		CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();
	}

	return false;
}

// Function for second part of the process. Landing after going over the object.
bool FClimbingInterpolation::ClimbStep(float dt)
{
	UAnimInstance* Instance = SkeletalMeshComp->GetAnimInstance();
	if (!Instance)
		return false;

	float PosHorizontalFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalCoord), 1.0f);
	float PosVerticalFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalCoord), 1.0f);

	// Position Interpolation. Blending is inverted to avoid issue with blending out montages returning 0.0f values while blending out.
	// 1.0f means start position, and 0.0f means ends.
	float NewCoordX = FMath::Lerp(TargetHeightObstaclePosition.X, CurrentCapsulePosition.X, PosHorizontalFactor);
	float NewCoordY = FMath::Lerp(TargetHeightObstaclePosition.Y, CurrentCapsulePosition.Y, PosHorizontalFactor);
	float NewCoordZ = FMath::Lerp(TargetHeightObstaclePosition.Z, CurrentCapsulePosition.Z, PosVerticalFactor);
	FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
	CapsuleComponent->SetWorldLocation(NewCapsulePosition);
	
	return CurrentInterpolationTime >= TotalAnimationTime;
}

void FClimbingInterpolation::UpdateMeshRelativeLocation(bool FinishedClimbing)
{
	UAnimInstance* Instance = SkeletalMeshComp->GetAnimInstance();
	if (!Instance)
		return;

	// Mesh adjustment to adapt animations to obstacle so they are more grounded in obstacles.
	float HeightOffsetForRootBone = Instance->GetCurveValue(HeightOffsetCurveName);
	HeightOffsetForRootBone = -(HeightOffsetForRootBone * CapsuleComponent->GetScaledCapsuleHalfHeight());
	float NewSkeletalHeight = SkeletalMeshRelativeLocHeight + HeightOffsetForRootBone;

	FVector NewRelativeLocation = SkeletalMeshComp->GetRelativeLocation();
	NewRelativeLocation.Z = NewSkeletalHeight;
	SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);

	// Get sure we return to original relative location.
	if (FinishedClimbing)
	{
		SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);
		ClimbingPtr = nullptr;
	}
}

/* Climbing struct used for climbing animations while hanging. */

// Initializes the interpolation struct.
void FHangingClimbingInterpolation::Init(UCapsuleComponent* CapsuleComp, USkeletalMeshComponent* SkeletalMesh,
	const FName & RootMotionXCurveName, const FName & RootMotionYCurveName, const FName & RootMotionZCurveName)
{
	// Fetch components.
	CapsuleComponent = CapsuleComp;
	SkeletalMeshComp = SkeletalMesh;
	if (!SkeletalMeshComp)
		return;
	AnimationInstance = SkeletalMeshComp->GetAnimInstance();
		
	USkeletalMesh * SKMesh = SkeletalMeshComp->SkeletalMesh;
	if (!SKMesh)
		return;

	USkeleton * Skeleton = SKMesh->Skeleton;
	if (!Skeleton)
		return;

	const FSmartNameMapping * NameMapping = Skeleton->GetSmartNameContainer(USkeleton::AnimCurveMappingName);
	if (!NameMapping)
		return;

	RootMotionXCurve = NameMapping->FindUID(RootMotionXCurveName);
	RootMotionYCurve = NameMapping->FindUID(RootMotionYCurveName);
	RootMotionZCurve = NameMapping->FindUID(RootMotionZCurveName);
}

// Starts the the interpolation struct setting all data necessary.
void FHangingClimbingInterpolation::Start(UAnimMontage * ClimbingMontageBeingPlayed)
{
	// Fetch current and target position.
	FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
	StartPosition = CapsuleTransform.GetLocation();

	PlayingMontage = ClimbingMontageBeingPlayed;

	TArray<UAnimationAsset*> AnimationAssets;
	ClimbingMontageBeingPlayed->GetAllAnimationSequencesReferred(AnimationAssets);

	// Get the animation asset inside the montage from which we will query the curves.
	if (AnimationAssets.Num() > 0)
	{
		UAnimationAsset * AnimAsset = AnimationAssets[0];
		if (AnimAsset)
			AnimationSequenceInsideMontage = (UAnimSequence*)AnimAsset;
	}
}

// Updates player position during climbing animations.
void FHangingClimbingInterpolation::Update()
{
	const FRawCurveTracks & CurveTracksData = AnimationSequenceInsideMontage->GetCurveData();
	const FFloatCurve * RootMotionXCurvePtr = static_cast<const FFloatCurve*>(CurveTracksData.GetCurveData(RootMotionXCurve));
	const FFloatCurve * RootMotionYCurvePtr = static_cast<const FFloatCurve*>(CurveTracksData.GetCurveData(RootMotionYCurve));
	const FFloatCurve * RootMotionZCurvePtr = static_cast<const FFloatCurve*>(CurveTracksData.GetCurveData(RootMotionZCurve));
	if (!RootMotionXCurvePtr || !RootMotionYCurvePtr || !RootMotionZCurvePtr)
		return;

	float MontageTime = AnimationInstance->Montage_GetPosition(PlayingMontage);

	float XDeltaRoot = RootMotionXCurvePtr->Evaluate(MontageTime);
	float YDeltaRoot = RootMotionYCurvePtr->Evaluate(MontageTime);
	float ZDeltaRoot = RootMotionZCurvePtr->Evaluate(MontageTime);

	// Position Interpolation.
	FVector TranslatedDeltas = CapsuleComponent->GetRightVector() * XDeltaRoot
		+ CapsuleComponent->GetForwardVector() * YDeltaRoot
		+ CapsuleComponent->GetUpVector() * ZDeltaRoot;
	float NewCoordX = StartPosition.X + TranslatedDeltas.X;
	float NewCoordY = StartPosition.Y + TranslatedDeltas.Y;
	float NewCoordZ = StartPosition.Z + TranslatedDeltas.Z;
	FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
	CapsuleComponent->SetWorldLocation(NewCapsulePosition);
}