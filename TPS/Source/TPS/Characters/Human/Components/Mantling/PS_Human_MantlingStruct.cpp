#pragma once

#include "PS_Human_MantlingStruct.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "PS_Human_MantlingComponent.h"

// Initializes the interpolation struct.
void FVaultingInterpolation::Init(UCapsuleComponent * CapsuleComp, USkeletalMeshComponent * SkeletalMesh, UPS_Human_MantlingComponent * NewMantlingComp,
	const FName& NewHeightOffsetCurveName, const FName& NewPositionVerticalInterpolationCurveName,
	const FName& NewPositionHorizontalInterpolationCurveName, const FName & NewRotationInterpolationCurveName)
{
	CapsuleComponent = CapsuleComp;
	MantlingComp = NewMantlingComp;
	SkeletalMeshComp = SkeletalMesh;
	if (SkeletalMeshComp)
		SkeletalMeshRelativeLocHeight = SkeletalMeshComp->GetRelativeLocation().Z;

	HeightOffsetCurveName = NewHeightOffsetCurveName;
	PositionVerticalInterpolationCurveName = NewPositionVerticalInterpolationCurveName;
	PositionHorizontalInterpolationCurveName = NewPositionHorizontalInterpolationCurveName;
	RotationInterpolationCurveName = NewRotationInterpolationCurveName;
}

// Starts the the interpolation struct setting all data from the vaulting animation data so
// it's ready to start being interpolated.
void FVaultingInterpolation::Start(const FVaultingAnimationData& VaultData, const FVector& NewTargetHeightObstaclePosition,
	const FVector& NewTargetGroundPositionToEndVaultingAt, bool NewWillHitGroundWhenVaulting, const FRotator& TargetRotator)
{
	CurrentInterpolationTime = 0.0f;
	TimeToReachTopOfObstacle = VaultData.TimeReachTopObstacle;
	AnimMontage = VaultData.VaultingMontage;
	TotalAnimationTime = VaultData.VaultingMontage->GetPlayLength();
	HasGoneOverObstacle = false;

	FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
	CurrentCapsulePosition = CapsuleTransform.GetLocation();
	CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();

	TargetHeightObstaclePosition = NewTargetHeightObstaclePosition + VaultData.ForwardOffsetTopObstaclePosition * TargetRotator.Vector();
	TargetGroundPositionToEndVaultingAt = NewTargetGroundPositionToEndVaultingAt;
	IsFallingAfterVault = !NewWillHitGroundWhenVaulting;
	TargetHeightObstaclePosition.Z += CapsuleComponent->GetScaledCapsuleHalfHeight();
	TargetGroundPositionToEndVaultingAt.Z += CapsuleComponent->GetScaledCapsuleHalfHeight();
	TargetRotationToFaceTo = TargetRotator;
}

// Updates player position during vaulting animations.
// Returns true once interpolation is done. False otherwise.
bool FVaultingInterpolation::Update(float dt)
{
	if (!CapsuleComponent || !SkeletalMeshComp) return false;

	UAnimInstance * Instance = SkeletalMeshComp->GetAnimInstance();
	if (!Instance)
		return false;

	CurrentInterpolationTime += dt;

	bool FinishVaulting = false;
	if (HasGoneOverObstacle)
	{
		float PositionVerticalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalInterpolationCurveName), 1.0f);
		float PositionHorizontalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalInterpolationCurveName), 1.0f);

		// Position Interpolation.
		float NewCoordX = FMath::Lerp(TargetGroundPositionToEndVaultingAt.X, CurrentCapsulePosition.X, PositionHorizontalInterpFactor);
		float NewCoordY = FMath::Lerp(TargetGroundPositionToEndVaultingAt.Y, CurrentCapsulePosition.Y, PositionHorizontalInterpFactor);
		float NewCoordZ = FMath::Lerp(TargetGroundPositionToEndVaultingAt.Z, CurrentCapsulePosition.Z, PositionVerticalInterpFactor);
		FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
		CapsuleComponent->SetWorldLocation(NewCapsulePosition);

		// Check if we should finish interpolation normally or if we should blend out vaulting animation in case we are falling after vaulting.
		bool IsFinishingInterpolation = CurrentInterpolationTime >= TotalAnimationTime;
		bool ShouldStartBlendingOutVaulting = IsFallingAfterVault && ((TotalAnimationTime - OnFallingAfterVaultBlendingOut) >= CurrentInterpolationTime);
		if (IsFinishingInterpolation)
		{
			FinishVaulting = true;
		}
		else if (ShouldStartBlendingOutVaulting)
		{
			MantlingComp->StartVaultingBlending(AnimMontage, OnFallingAfterVaultBlendingOut);
			FinishVaulting = true;
		}
	}
	else
	{
		float PositionVerticalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalInterpolationCurveName), 1.0f);
		float PositionHorizontalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalInterpolationCurveName), 1.0f);
		float RotationInterpFactor = FMath::Min(Instance->GetCurveValue(RotationInterpolationCurveName), 1.0f);

		// Position Interpolation.
		float NewCoordX = FMath::Lerp(CurrentCapsulePosition.X, TargetHeightObstaclePosition.X, PositionHorizontalInterpFactor);
		float NewCoordY = FMath::Lerp(CurrentCapsulePosition.Y, TargetHeightObstaclePosition.Y, PositionHorizontalInterpFactor);
		float NewCoordZ = FMath::Lerp(CurrentCapsulePosition.Z, TargetHeightObstaclePosition.Z, PositionVerticalInterpFactor);
		FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
		CapsuleComponent->SetWorldLocation(NewCapsulePosition);

		// Rotation
		FRotator NewCapsuleRotation = FMath::Lerp(CurrentRotationToFaceTo, TargetRotationToFaceTo, RotationInterpFactor);
		CapsuleComponent->SetWorldRotation(NewCapsuleRotation);

		// We have reached the top of the obstacle. Now we start moving to the end.
		if (CurrentInterpolationTime >= TimeToReachTopOfObstacle)
		{
			HasGoneOverObstacle = true;

			// If we are falling after vault, we call this function to already change to falling mode of the character.
			// This allows correct blending of the montage with the falling state animations when we get to the end of the vaulting.
			if (IsFallingAfterVault)
			{
				MantlingComp->OnVaultingFallStart();
			}

			// Update current values for next part of vaulting.
			FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
			CurrentCapsulePosition = CapsuleTransform.GetLocation();
			CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();
		}
	}

	// Mesh adjustment to adapt animations to obstacle so they are more grounded in obstacles.
	float HeightOffsetForRootBone = Instance->GetCurveValue(HeightOffsetCurveName);
	HeightOffsetForRootBone = HeightOffsetForRootBone * CapsuleComponent->GetScaledCapsuleHalfHeight();
	float NewSkeletalHeight = SkeletalMeshRelativeLocHeight + HeightOffsetForRootBone;

	FVector NewRelativeLocation = SkeletalMeshComp->GetRelativeLocation();
	NewRelativeLocation.Z = NewSkeletalHeight;
	SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);

	// Get sure we return to original relative location.
	if (FinishVaulting)
	{
		NewRelativeLocation.Z = SkeletalMeshRelativeLocHeight;
		SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);
	}

	return FinishVaulting;
}

/* Interpolation struct used for mantling component climbing animations. */

// Initializes the interpolation struct.
void FClimbingInterpolation::Init(UCapsuleComponent* CapsuleComp, USkeletalMeshComponent* SkeletalMesh,
	const FName& NewHeightOffsetCurveName, const FName& NewPositionVerticalInterpolationCurveName,
	const FName& NewPositionHorizontalInterpolationCurveName, const FName& NewRotationInterpolationCurveName)
{
	CapsuleComponent = CapsuleComp;
	SkeletalMeshComp = SkeletalMesh;
	if (SkeletalMeshComp)
		SkeletalMeshRelativeLocHeight = SkeletalMeshComp->GetRelativeLocation().Z;

	HeightOffsetCurveName = NewHeightOffsetCurveName;
	PositionVerticalInterpolationCurveName = NewPositionVerticalInterpolationCurveName;
	PositionHorizontalInterpolationCurveName = NewPositionHorizontalInterpolationCurveName;
	RotationInterpolationCurveName = NewRotationInterpolationCurveName;
}

// Starts the the interpolation struct setting all data from the vaulting animation data so
// it's ready to start being interpolated.
void FClimbingInterpolation::Start(const FClimbingAnimationData & ClimbData, const FVector & NewTargetHeightObstaclePosition, const FRotator & TargetRotator)
{
	CurrentInterpolationTime = 0.0f;
	TotalAnimationTime = ClimbData.ClimbingMontage->GetPlayLength();

	FTransform CapsuleTransform = CapsuleComponent->GetComponentTransform();
	CurrentCapsulePosition = CapsuleTransform.GetLocation();
	CurrentRotationToFaceTo = CapsuleTransform.GetRotation().Rotator();

	TargetHeightObstaclePosition = NewTargetHeightObstaclePosition;
	TargetHeightObstaclePosition.Z += CapsuleComponent->GetScaledCapsuleHalfHeight();
	TargetRotationToFaceTo = TargetRotator;
}

// Updates player position during vaulting animations.
// Returns true once interpolation is done. False otherwise.
bool FClimbingInterpolation::Update(float dt)
{
	if (!CapsuleComponent || !SkeletalMeshComp)
		return false;

	UAnimInstance* Instance = SkeletalMeshComp->GetAnimInstance();
	if (!Instance)
		return false;

	bool FinishClimbing = false;

	CurrentInterpolationTime += dt;
	
	float PositionVerticalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionVerticalInterpolationCurveName), 1.0f);
	float PositionHorizontalInterpFactor = FMath::Min(Instance->GetCurveValue(PositionHorizontalInterpolationCurveName), 1.0f);

	float RotationInterpFactor = FMath::Min(Instance->GetCurveValue(RotationInterpolationCurveName), 1.0f);

	// Position Interpolation.
	float NewCoordX = FMath::Lerp(CurrentCapsulePosition.X, TargetHeightObstaclePosition.X, PositionHorizontalInterpFactor);
	float NewCoordY = FMath::Lerp(CurrentCapsulePosition.Y, TargetHeightObstaclePosition.Y, PositionHorizontalInterpFactor);
	float NewCoordZ = FMath::Lerp(CurrentCapsulePosition.Z, TargetHeightObstaclePosition.Z, PositionVerticalInterpFactor);
	FVector NewCapsulePosition = FVector(NewCoordX, NewCoordY, NewCoordZ);
	CapsuleComponent->SetWorldLocation(NewCapsulePosition);

	// Rotation
	FRotator NewCapsuleRotation = FMath::Lerp(CurrentRotationToFaceTo, TargetRotationToFaceTo, RotationInterpFactor);
	CapsuleComponent->SetWorldRotation(NewCapsuleRotation);

	// We have reached the top of the obstacle. Now we start moving to the end.
	if (CurrentInterpolationTime >= TotalAnimationTime)
	{
		// Get sure we are in the target.
		CapsuleComponent->SetWorldLocation(TargetHeightObstaclePosition);
		CapsuleComponent->SetWorldRotation(TargetRotationToFaceTo);

		FinishClimbing = true;
	}

	// Mesh adjustment to adapt animations to obstacle so they are more grounded in obstacles.
	float HeightOffsetForRootBone = Instance->GetCurveValue(HeightOffsetCurveName);
	HeightOffsetForRootBone = -(HeightOffsetForRootBone * CapsuleComponent->GetScaledCapsuleHalfHeight());
	float NewSkeletalHeight = SkeletalMeshRelativeLocHeight + HeightOffsetForRootBone;

	FVector NewRelativeLocation = SkeletalMeshComp->GetRelativeLocation();
	NewRelativeLocation.Z = NewSkeletalHeight;
	SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);

	// Get sure we return to original relative location.
	if (FinishClimbing)
		SkeletalMeshComp->SetRelativeLocation(NewRelativeLocation);

	return FinishClimbing;
}