#include "PS_Human_ClimbingComponent.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "../../../Common/Animations/PS_Extended_AnimInstance.h"
#include "../Locomotion/PS_Human_LocomotionController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"

UPS_Human_ClimbingComponent::UPS_Human_ClimbingComponent() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UPS_Human_ClimbingComponent::BeginPlay()
{
	Super::BeginPlay();

	FetchComponents();

	// Reserve memory for climbing animations
	CandidateClimbingAnims.Reserve(GroundedClimbingAnimations.Num());

	// Interpolators.
	ClimbingInterpolationHandler.Init(CapsuleComponent, SkeletalMeshComp, RootHeightOffsetAnimationCurveName,
		PositionHorizontalCoord, PositionVerticalCoord);

	// Disable ticking while we don't need it.
	SetComponentTickEnabled(false);
}

void UPS_Human_ClimbingComponent::FetchComponents()
{
	Character = Cast<APS_Character>(GetOwner());
	if (!Character)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay Character Returned Null"));
		return;
	}

	SkeletalMeshComp = Cast<USkeletalMeshComponent>(Character->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (!SkeletalMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay SkeletalMeshComponent Returned Null"));
		return;
	}

	CharacterMovementComponent = Cast<UCharacterMovementComponent>(Character->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!CharacterMovementComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay UCharacterMovementComponent Returned Null"));
		return;
	}

	CapsuleComponent = Cast<UCapsuleComponent>(Character->GetComponentByClass(UCapsuleComponent::StaticClass()));
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay UCapsuleComponent Returned Null"));
		return;
	}

	AnimInstance = (UPS_Human_AnimationInstance *)SkeletalMeshComp->GetAnimInstance();
	if (!AnimInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay UPS_Human_AnimationInstance Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Character->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay LocController Returned Null"));
		return;
	}
	if (LocController->HasBegunPlay())
		OnLocomotionCompInitialized();
	else
		LocController->BegunPlayCallback.AddUObject(this, &UPS_Human_ClimbingComponent::OnLocomotionCompInitialized);

	ACharacter* DefaultCharacter = Character->GetClass()->GetDefaultObject<ACharacter>();
	if (DefaultCharacter)
	{
		CharacterHalfHeight = DefaultCharacter->GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight();
	}

	ClimbingTraceQuery = UEngineTypes::ConvertToTraceType(ClimbingChannel);
}

/* Editor only functions. */

void UPS_Human_ClimbingComponent::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);

	PreProcessAnimations();
}

void UPS_Human_ClimbingComponent::PreProcessAnimations()
{
	const float VeryHighNumber = 100000.0f;
	Grounded_MaxClimbingDistance = 0.0f;
	Grounded_MinClimbingDistance = VeryHighNumber;
	Grounded_MaxClimbingHeight = 0.0f;
	Grounded_MinClimbingHeight = VeryHighNumber;

	for (int i = 0; i < GroundedClimbingAnimations.Num(); ++i)
	{
		FClimbingAnimationData & ClimbingAnimation = GroundedClimbingAnimations[i];

		if (ClimbingAnimation.MaxCharacterSpeedCanPlay <= 0.0f)
			ClimbingAnimation.MaxCharacterSpeedCanPlay = VeryHighNumber;

		if (ClimbingAnimation.MinCharacterSpeedCanPlay < 0.0f)
			ClimbingAnimation.MinCharacterSpeedCanPlay = 0.0f;

		Grounded_MaxClimbingDistance = FMath::Max(ClimbingAnimation.MaxDistanceCanPlay, Grounded_MaxClimbingDistance);
		Grounded_MinClimbingDistance = FMath::Min(ClimbingAnimation.MinDistanceCanPlay, Grounded_MinClimbingDistance);
		Grounded_MaxClimbingHeight = FMath::Max(ClimbingAnimation.MaxClimbingHeight, Grounded_MaxClimbingHeight);
		Grounded_MinClimbingHeight = FMath::Min(ClimbingAnimation.MinClimbingHeight, Grounded_MinClimbingHeight);
	}
}


/* Public functions */

void UPS_Human_ClimbingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool EndedInterpolation = ClimbingInterpolationHandler.Update(DeltaTime);
	if (EndedInterpolation)
		EndClimbing();
}

bool UPS_Human_ClimbingComponent::TryClimb()
{
	if (IsCurrentlyClimbing)
		return false;

	// Do forward check first.
	FHitResult ObstacleHitInfo;
	bool ForwardObstacleFound = ForwardObstacleCheck(Grounded_MinClimbingDistance, Grounded_MaxClimbingDistance, Grounded_MinClimbingHeight, ObstacleHitInfo, DebugTrace);
	if (!ForwardObstacleFound)
		return false;

	// Check the obstacle height. Returns false if too tall.
	FVector HeightHitPosition;
	float ObstacleHeight;
	FVector LedgePos;
	bool HeightCheck = HeightObstacleCheck(0.0f, Grounded_MaxClimbingHeight - ObstacleHitInfo.ImpactPoint.Z, ClimbingEndPositionOffset,
		ObstacleHitInfo, HeightHitPosition, ObstacleHeight, LedgePos, DebugTrace);
	if (!HeightCheck)
		return false;

	// Add the scaled capsule half height to the height hit position so the player doesn't get inside the geometry.
	HeightHitPosition.Z += CharacterHalfHeight;

	// Try to climb the obstacle by checking on climb from grounded animations.
	return ChooseClimbFromGrounded(ObstacleHitInfo, HeightHitPosition, ObstacleHeight, LedgePos);
}

bool UPS_Human_ClimbingComponent::GetIsCurrentlyClimbing() const
{
	return IsCurrentlyClimbing;
}

void UPS_Human_ClimbingComponent::OnLocomotionCompInitialized()
{
	UPS_LocState_Climb * ClimbState = (UPS_LocState_Climb*)LocController->GetState("Climbing");
	if (ClimbState)
	{
		LedgeHandCheckDepthOffset = ClimbState->GetLedgeHandCheckDepthOffset();
		HeightToPerformLedgeCheckAt = ClimbState->GetHeightToPerformLedgeCheckAt();
		MaxDistanceToGrabToLedge = ClimbState->GetMaxDistanceToGrabToLedge();
	}
	else
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_ClimbingComponent::BeginPlay ClimbState Returned Null"));
}

bool UPS_Human_ClimbingComponent::ForwardObstacleCheck(float MinCheckDistance, float MaxCheckDistance, float HeightToPerformCheckAt, FHitResult & ObstacleHit, EDrawDebugTrace::Type Debug)
{
	bool ThicknessHitObstacle;
	bool FoundObstacle = ForwardAndThicknessCheck(MinCheckDistance, MaxCheckDistance, HeightToPerformCheckAt, 0.0f, ObstacleHit, ThicknessHitObstacle, Debug);
	if (!FoundObstacle)
		return false;
	
	// If we hit obstacle on thickness check, we perform two checks in both right and left directions.
	if (ThicknessHitObstacle)
	{
		const float SideCheckRadius = 5.0f;
		FVector RightObstacleVector = FVector::CrossProduct(ObstacleHit.ImpactNormal, CapsuleComponent->GetUpVector());
		FHitResult SideObstacleHit;
		TArray<AActor*> ActorsToIgnore;

		FVector SideStartCheck = ObstacleHit.ImpactPoint + ObstacleHit.ImpactNormal * (LedgeHandCheckDepthOffset + SideCheckRadius);
		FVector SideEndCheck = SideStartCheck + RightObstacleVector * CapsuleComponent->GetScaledCapsuleRadius();
		bool SideCheck = UKismetSystemLibrary::SphereTraceSingle(this, SideStartCheck, SideEndCheck, SideCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, SideObstacleHit, true);
		if (SideCheck)
		{
			float DistanceToPerformNewCheck = CapsuleComponent->GetScaledCapsuleRadius() - SideObstacleHit.Distance;
			bool FoundObstacleSide = ForwardAndThicknessCheck(MinCheckDistance, MaxCheckDistance, HeightToPerformCheckAt, -DistanceToPerformNewCheck, ObstacleHit, ThicknessHitObstacle, Debug);
			if (!FoundObstacleSide || ThicknessHitObstacle)
				return false;
		}

		SideEndCheck = SideStartCheck + RightObstacleVector * -CapsuleComponent->GetScaledCapsuleRadius();
		SideCheck = UKismetSystemLibrary::SphereTraceSingle(this, SideStartCheck, SideEndCheck, SideCheckRadius, ClimbingTraceQuery,
			false, ActorsToIgnore, Debug, SideObstacleHit, true);
		if (SideCheck)
		{
			float DistanceToPerformNewCheck = CapsuleComponent->GetScaledCapsuleRadius() - SideObstacleHit.Distance;
			bool FoundObstacleSide = ForwardAndThicknessCheck(MinCheckDistance, MaxCheckDistance, HeightToPerformCheckAt, DistanceToPerformNewCheck, ObstacleHit, ThicknessHitObstacle, Debug);
			if (!FoundObstacleSide || ThicknessHitObstacle)
				return false;
		}
	}

	return true;
}

bool UPS_Human_ClimbingComponent::ForwardAndThicknessCheck(float MinCheckDistance, float MaxCheckDistance, float HeightToPerformCheckAt, float SideOffset, FHitResult& ObstacleHit, bool & ThicknessSuccess, EDrawDebugTrace::Type Debug)
{
	FVector ActorForwardVector = CapsuleComponent->GetForwardVector(); // We use capsule due to skeletal mesh being rotated 90�.
	FVector PositionToCheckForObstacleHit = SkeletalMeshComp->GetComponentLocation() + CapsuleComponent->GetUpVector() * HeightToPerformCheckAt;

	// Forward check in players facing direction.
	FVector ForwardStartTrace = SkeletalMeshComp->GetComponentLocation() + ActorForwardVector * MinCheckDistance + CapsuleComponent->GetUpVector() * HeightToPerformCheckAt
		+ CapsuleComponent->GetRightVector() * SideOffset;
	FVector ForwardEndTrace = ForwardStartTrace + ActorForwardVector * MaxCheckDistance;
	TArray<AActor*> ActorsToIgnore;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, ForwardStartTrace, ForwardEndTrace, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, ObstacleHit, true);
	if (!HitObstacle)
		return false;

	// Thickness check to see we fit into that position.
	FHitResult ThicknessObstacleHit;
	FVector ThicknessStartCheck = ObstacleHit.ImpactPoint + ObstacleHit.ImpactNormal * (CapsuleComponent->GetScaledCapsuleRadius() + LedgeHandCheckDepthOffset)
		+ CapsuleComponent->GetUpVector() * -HeightToPerformLedgeCheckAt;
	FVector ThicknessEndCheck = ThicknessStartCheck + CapsuleComponent->GetUpVector() * (CapsuleComponent->GetScaledCapsuleHalfHeight() * 2.0f);
	ThicknessSuccess = UKismetSystemLibrary::SphereTraceSingle(this, ThicknessStartCheck, ThicknessEndCheck, CapsuleComponent->GetScaledCapsuleRadius(), ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, ThicknessObstacleHit, true);

	return true;
}

bool UPS_Human_ClimbingComponent::HeightObstacleCheck(float MinHeight, float MaxHeight,	float ForwardOffset,
	const FHitResult & ForwardHitInformation, FVector & HeightHitPosition, float & ObstacleHeight, FVector & LedgePos, EDrawDebugTrace::Type Debug)
{
	// Compute the up vector we will be checking to see if we can hang to the obstacle. We use the normal of the impact for it.
	FVector UpVector = FVector::CrossProduct(CapsuleComponent->GetRightVector(), ForwardHitInformation.ImpactNormal);
	FVector HeightEndTrace = ForwardHitInformation.ImpactPoint + ForwardHitInformation.ImpactNormal * -LedgeHandCheckDepthOffset + UpVector * MinHeight;
	FVector HeightStartTrace = HeightEndTrace + UpVector * MaxHeight;

	// Perform Height Check.
	TArray<AActor *> ActorsToIgnore;
	FHitResult HeightHitResult;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, HeightStartTrace, HeightEndTrace, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, HeightHitResult, true, FLinearColor::Blue);

	// Get sure height result is, at least, bigger or equal than the minimum height we have considered when compute the  ForwardHitInformation.ImpactPoint height.
	const float MinHeightOffset = 5.0f;
	bool HasEnoughHeightDiff = (HeightHitResult.ImpactPoint.Z - ForwardHitInformation.ImpactPoint.Z) > MinHeightOffset;
	if (!HitObstacle || HeightHitResult.bStartPenetrating || !HasEnoughHeightDiff)
		return false;

	// Compute the position we will end at.
	HeightHitPosition = HeightHitResult.ImpactPoint;
	ObstacleHeight = HeightHitPosition.Z - SkeletalMeshComp->GetComponentLocation().Z;
	LedgePos = ForwardHitInformation.ImpactPoint + ForwardHitInformation.ImpactNormal * CapsuleComponent->GetScaledCapsuleRadius();
	LedgePos.Z = HeightHitPosition.Z - CharacterHalfHeight;

	return true;
}

bool UPS_Human_ClimbingComponent::LedgeObstacleCheck(FHitResult & ForwardHitInformation, FVector & HeightHitPosition, bool ComputeNewLedgePos, FVector & LedgePos, FVector& LedgeNormal, EDrawDebugTrace::Type Debug)
{
	FVector LedgeStartTrace = CapsuleComponent->GetComponentLocation();
	FVector LedgeEndTrace = LedgeStartTrace + CapsuleComponent->GetForwardVector() * MaxDistanceToGrabToLedge;

	TArray<AActor*> ActorsToIgnore;
	FHitResult LedgeHitPosition;
	bool HitObstacle = UKismetSystemLibrary::SphereTraceSingle(this, LedgeStartTrace, LedgeEndTrace, SphereCheckRadius, ClimbingTraceQuery,
		false, ActorsToIgnore, Debug, LedgeHitPosition, true, FLinearColor::Blue);
	if (!HitObstacle || LedgeHitPosition.bStartPenetrating)
		return false;

	// Otherwise, we compute the new ledge position and normal that we will use to orient the player.
	FVector UpVector = FVector::CrossProduct(CapsuleComponent->GetRightVector(), ForwardHitInformation.ImpactNormal);
	LedgePos = LedgeHitPosition.ImpactPoint + LedgeHitPosition.ImpactNormal * CapsuleComponent->GetScaledCapsuleRadius()
		+ UpVector * (HeightHitPosition.Z - ForwardHitInformation.ImpactPoint.Z);
	LedgeNormal = LedgeHitPosition.ImpactNormal;

	return true;
}

float UPS_Human_ClimbingComponent::ComputeDeltaAngleWithSurface(const FVector & SurfaceNormal)
{
	FRotator SurfaceRotationLookingInPlayersDir = (SurfaceNormal * -1.0f).Rotation();
	FRotator PlayerRotation = CapsuleComponent->GetForwardVector().Rotation();
	return FMath::FindDeltaAngleDegrees(SurfaceRotationLookingInPlayersDir.Pitch, PlayerRotation.Pitch);
}

bool UPS_Human_ClimbingComponent::ChooseClimbFromGrounded(const FHitResult& Obstacle, const FVector & HeightHitPosition, float ObstacleHeight, const FVector & LedgePos)
{
	float CurrentPlayerSpeed = Character->GetSpeed();
	CandidateClimbingAnims.Empty(CandidateClimbingAnims.Max());

	for (int i = 0; i < GroundedClimbingAnimations.Num(); ++i)
	{
		const FClimbingAnimationData & ClimbAnim = GroundedClimbingAnimations[i];

		bool MeetsDistanceLimits = Obstacle.Distance <= ClimbAnim.MaxDistanceCanPlay && Obstacle.Distance >= ClimbAnim.MinDistanceCanPlay;
		if (MeetsDistanceLimits)
		{
			// Check speed.
			bool MeetsSpeedLimits = CurrentPlayerSpeed <= ClimbAnim.MaxCharacterSpeedCanPlay && CurrentPlayerSpeed >= ClimbAnim.MinCharacterSpeedCanPlay;
			if (MeetsSpeedLimits)
			{
				// Check height.
				bool MeetsHeightLimits = ObstacleHeight <= ClimbAnim.MaxClimbingHeight && ObstacleHeight >= ClimbAnim.MinClimbingHeight;
				if (MeetsHeightLimits)
				{
					CandidateClimbingAnims.EmplaceAt(CandidateClimbingAnims.Num(), i);
				}
			}
		}
	}

	// Not a single climbing animation passed the two tests.
	if (CandidateClimbingAnims.Num() == 0)
		return false;

	// Get a random climbing animation from the possible ones.
	int RandomIndex = FMath::RandRange(0, CandidateClimbingAnims.Num() - 1);

	// Play the climbing animation.
	FRotator RotationToFaceTo = (Obstacle.Normal * -1.0f).Rotation();

	PlayClimbingAnimation(GroundedClimbingAnimations[RandomIndex], Obstacle, LedgePos, HeightHitPosition, RotationToFaceTo);

	return true;
}

void UPS_Human_ClimbingComponent::PlayClimbingAnimation(const FClimbingAnimationData & ClimbAnim, const FHitResult& Obstacle, const FVector& LedgePos, const FVector& HeightHitPosition, const FRotator& RotationToFaceTo)
{
	IsCurrentlyClimbing = true;

	// Store the info of the hit, we will use the info of the actor to disable collision in parts of the animation to prevent camera issues.
	// This is done through the use of anim notifies.
	ClimbingObject = Obstacle;

	ClimbingInterpolationHandler.Start(ClimbAnim, LedgePos, HeightHitPosition, RotationToFaceTo);

	Character->SetEnableCharacterMovementAndActions(false);

	LocController->SetLegIKEnabledStatus(false);

	LocController->SetRootMotionMovement(true);

	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Flying);

	AnimInstance->PlayAnimationMontage(ClimbAnim.ClimbingMontage, 1.0f, 0.0f, "", true);

	SetComponentTickEnabled(true);
}

// Ends mantling, called by Update once mantling animations are finished.
void UPS_Human_ClimbingComponent::EndClimbing()
{
	IsCurrentlyClimbing = false;

	Character->SetEnableCharacterMovementAndActions(true);

	LocController->SetLegIKEnabledStatus(true);

	LocController->SetRootMotionMovement(false);

	CharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Walking);

	SetComponentTickEnabled(false);
}