#pragma once

#include "CoreMinimal.h"
#include "PS_Human_VaultingStruct.generated.h"

class UAnimMontage;
class UCurveFloat;
class UAnimInstance;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UPS_Human_VaultingComponent;

USTRUCT(BlueprintType, Blueprintable)
struct FVaultingAnimationData
{
	GENERATED_BODY()

	// Montage to play when vaulting over an obstacle.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * VaultingMontage;

	// Max distance at which the anim can be played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxDistanceCanPlay = 0.0f;

	// Min distance at which the anim can be played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinDistanceCanPlay = 0.0f;

	// Max speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxCharacterSpeedCanPlay = -1.0f;

	// Min speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinCharacterSpeedCanPlay = -1.0f;

	// Max height this animation can be played for.
	// Height starts from the feet of the player and not the center of mass.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxVaultingHeight;

	// Min height this animation can be played for.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinVaultingHeight = 0.0f;

	// Offset added to the top position over the obstacle being vaulted. Allows advancing the capsule
	// top pivoting point being vaulted either forward or backwards so certain animations look correct
	// while vaulting.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ForwardOffsetVaultingPos = 0.0f;

	// Time in seconds to reach the top of the obstacle.
	// Controls how long for the player's capsule to reach the middle of the vaulted object
	// trajectory. This the point the player is vaulting over in the obstacle and starts going down.
	// Should be sync with the values of the curves MantlingVerticalTranslationInterp and
	// MantlingHorizontalTranslationInterp so all of them have value (1.0f) by the time
	// TimeReachTopObstacle is set to.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeReachTopObstacle = 0.0f;
};

/*
	Interpolation struct used by vaulting component for vaulting animations.
	Interpolation works through three different animation curves that control:

	HeightOffsetCurve: Controls the height of the mesh component during the vaulting animation.
	Helps making animations correctly adapt to the obstacle so, due to the capsule movement, they
	don't look as the player is vaulting over air.

	PositionInterpolationCurve: Controls the position interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting) and return to 0.0f at the end.

	RotationInterpolationCurve: Controls the rotation interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting).

	If necessary two other implementations could be done:

	Dispense of PositionInterpolationCurve and RotationInterpolationCurve and use instead simple interpolations
	with TimeToReachTopOfObstacle and TotalTime - TimeToReachTopOfObstacle. We lose a bit of control of the interpolation
	but it might be slightly faster. Although this doesn't seem an issue.

	Use three PositionInterpolationCurves to control the interpolation of each coordinate while applying the animation.
	Could give even more control of the interpolation to reduce issues with the character overlapping.
	It doesn't seem to be necessary for animations, so I have decided to use one for horizontal and one for vertical movement.
*/

USTRUCT(BlueprintType, Blueprintable)
struct FVaultingInterpolation
{
	GENERATED_BODY()

private:
	
	// Components.
	UAnimInstance * Instance = nullptr;
	UCapsuleComponent * CapsuleComponent = nullptr;
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;
	UPS_Human_VaultingComponent * VaultingComp = nullptr;
	
	/* Variables */

	UAnimMontage * AnimMontage = nullptr;

	typedef bool (FVaultingInterpolation::* VaultingStepFunctionPtr)(float);
	VaultingStepFunctionPtr VaultingFunctionPtr = nullptr;
	
	// Time for each part of the vaulting.
	float CurrentInterpolationTime = 0.0f;
	float TimeToReachTopOfObstacle = 0.0f;
	float TimeForBlendingOut = 0.0f;

	// Curves for controlling mesh component position and interpolating movement while vaulting.
	FName HeightOffsetCurveName = "RootHeightOffset";
	FName PositionVerticalInterpolationCurveName = "MantlingVerticalTranslationInterp";
	FName PositionHorizontalInterpolationCurveName = "MantlingHorizontalTranslationInterp";
	FName RotationInterpolationCurveName = "MantlingRotationInterp";

	// Relative position of the mesh component. As we modify it during animations, we store the original one
	// to return to it once vaulting ends.
	float SkeletalMeshRelativeLocHeight = 0.0f;

	// Capsule current position and rotation.
	FVector CurrentCapsulePosition;
	FRotator CurrentRotationToFaceTo;

	// Target positions and rotation.
	FVector TargetHeightObstaclePosition;
	FVector TargetGroundPositionToEndVaultingAt;
	FRotator TargetRotationToFaceTo;

	// Boolean to control if we are falling after vaulting or not. If so, different behaviour is applied.
	bool IsFallingAfterVault;

	// Extra added percent of normal blending added if we are falling after vaulting. Allows a slightly more natural
	// blending out.
 	UPROPERTY(EditAnywhere)
	float OnFallingAfterVaultAddedBlendingPercent = 0.2f;

public:

	// Initializes the interpolation struct.
	void Init(UCapsuleComponent* CapsuleComp, USkeletalMeshComponent* SkeletalMesh, UPS_Human_VaultingComponent * NewVaultingComp,
		const FName& NewHeightOffsetCurveName, const FName & NewPositionVerticalInterpolationCurveName,
		const FName& NewPositionHorizontalInterpolationCurveName, const FName& NewRotationInterpolationCurveName);

	// Starts the the interpolation struct setting all data from the vaulting animation data so
	// it's ready to start being interpolated.
	void Start(const FVaultingAnimationData& VaultData, const FVector& NewTargetHeightObstaclePosition,
		const FVector& NewTargetGroundPositionToEndVaultingAt, bool IsLandingOnGroundAfterVaulting, const FRotator& TargetRotator);

	// Updates player position during vaulting animations.
	// Returns true once interpolation is done. False otherwise.
	bool Update(float dt);

protected:

	// Function for the first part of the vaulting process. Rising until vaulting the object.
	bool VaultingStep(float dt);

	// Function for second part of the process. Landing after going over the object.
	bool LandingStep(float dt);

	void UpdateCharacterMeshLocation(bool IsFinishedVaulting);
};