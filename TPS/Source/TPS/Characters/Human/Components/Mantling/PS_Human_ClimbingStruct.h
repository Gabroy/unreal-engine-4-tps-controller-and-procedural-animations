#pragma once

#include "CoreMinimal.h"
#include "PS_Human_ClimbingStruct.generated.h"

class UAnimSequence;
class UAnimMontage;
class UCurveFloat;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UAnimInstance;
class UPS_Human_ClimbingComponent;

USTRUCT(BlueprintType, Blueprintable)
struct FClimbingAnimationData
{
	GENERATED_BODY()

	// Montage to play when climbing an obstacle.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage * ClimbingMontage;

	// Max distance at which the anim can be played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxDistanceCanPlay = 0.0f;

	// Min distance at which the anim can be played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinDistanceCanPlay = 0.0f;

	// Max speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxCharacterSpeedCanPlay = -1.0f;

	// Min speed at which the anim can be played. Negative values means no limit.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinCharacterSpeedCanPlay = -1.0f;

	// Max height this animation can be played for.
	// Height starts from the feet of the player and not the center of mass.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxClimbingHeight;

	// Min height this animation can be played for.
	// Negative values, means no min height.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MinClimbingHeight;

	// Factor used to raise the ledge position (position where we grab the obstacle and are about to climb over it).
	// Used to raise animations that look bad with default ledge pos. A factor of 1.0f will put half of the capsule above
	// the height of the final climb position and, the other half, below.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float LedgeHeightOffsetFactor = 0.0f;

	// Time in seconds to reach the ledge of the obstacle. Set it to the time when RootHorizontal and RootVertical are set to 1.0f.
	// Time when we have reached the ledge position (hands are on obstacle and about to climb).
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeReachLedge = 0.0f;

	// Extra distance that can be added to the final position. Allows pushing the default final position the character ends at after climbing.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float FinalPositionForwardOffset = 0.0f;
};

/*
	Interpolation struct used by climbing component for climbing animations.
	Interpolation works through three different animation curves that control:

	HeightOffsetCurve: Controls the height of the mesh component during the vaulting animation.
	Helps making animations correctly adapt to the obstacle so, due to the capsule movement, they
	don't look as the player is vaulting over air.

	PositionVerticalInterpolationCurve and PositionHorizontalInterpolationCurve: Controls the position
	interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting) and return to 0.0f at the end.

	RotationInterpolationCurve: Controls the rotation interpolation of the player while vaulting.
	Should go from 0.0f to 1.0f (where 1.0f is the peak of the vaulting).

	If necessary two implementation could be done:

	Dispense of PositionInterpolationCurve and RotationInterpolationCurve and use instead simple interpolations
	with TimeToReachTopOfObstacle and TotalTime - TimeToReachTopOfObstacle. We lose a bit of control of the interpolation
	but it might be slightly faster. Although this doesn't seem an issue.

	Use three PositionInterpolationCurves to control the interpolation of each coordinate while applying the animation.
	Could give even more control of the interpolation to reduce issues with the character overlapping.
	It doesn't seem to be necessary for animations, so I have decided to use one for horizontal and one for vertical movement.
*/
struct FClimbingInterpolation
{
private:

	UCapsuleComponent * CapsuleComponent = nullptr;

	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	typedef bool (FClimbingInterpolation::* ClimbingFunctionPtr)(float);
	ClimbingFunctionPtr ClimbingPtr = nullptr;

	float SkeletalMeshRelativeLocHeight;

	// Curves for controlling mesh component position and interpolating movement while climbing.
	FName HeightOffsetCurveName = "RootHeightOffset";
	FName PositionHorizontalCoord = "RootMotionHorizontal";
	FName PositionVerticalCoord = "RootMotionVertical";

	// Capsule current position and rotation and target positions and rotation.
	FVector CurrentCapsulePosition;
	FRotator CurrentRotationToFaceTo;
	FVector TargetLedgePosition;
	FVector TargetHeightObstaclePosition;
	FRotator TargetRotationToFaceTo;

	// Time for each part of the vaulting.
	float CurrentInterpolationTime = 0.0f;
	float TimeToReachLedge = 0.0f;
	float TotalAnimationTime = 0.0f;

public:

	// Initializes the interpolation struct.
	void Init(UCapsuleComponent* CapsuleComp, USkeletalMeshComponent* SkeletalMesh,
		const FName& NewHeightOffsetCurveName, const FName& NewPositionHorizontalCoord, const FName& NewPositionVerticalCoord);

	// Starts the the interpolation struct setting all data from the vaulting animation data so
	// it's ready to start being interpolated.
	void Start(const FClimbingAnimationData& ClimbData, const FVector & NewLedgePos, const FVector& NewTargetHeightObstaclePosition, const FRotator& TargetRotator);

	// Updates player position during vaulting animations.
	// Returns true once interpolation is done. False otherwise.
	bool Update(float dt);

protected:

	// Function for the first part of the climbing process.
	bool ToHangStep(float dt);

	// Function for second part of the process. Landing after going over the object.
	bool ClimbStep(float dt);

	void UpdateMeshRelativeLocation(bool FinishedClimbing);
};


/* Climbing struct used for climbing animations while hanging. */

struct FHangingClimbingInterpolation
{
private:

	 /* Components */

	UCapsuleComponent * CapsuleComponent = nullptr;

	USkeletalMeshComponent * SkeletalMeshComp = nullptr;

	UAnimInstance * AnimationInstance = nullptr;

	/* Variables */

	// Curves for controlling mesh component position.
	USkeleton::AnimCurveUID RootMotionXCurve;
	USkeleton::AnimCurveUID RootMotionYCurve;
	USkeleton::AnimCurveUID RootMotionZCurve;

	FVector StartPosition;

	UAnimMontage * PlayingMontage = nullptr;
	UAnimSequence * AnimationSequenceInsideMontage = nullptr;

public:

	// Initializes the interpolation struct.
	void Init(UCapsuleComponent * CapsuleComp, USkeletalMeshComponent * SkeletalMesh,
		const FName & RootMotionXCurveName, const FName & RootMotionYCurveName,
		const FName & RootMotionZCurveName);

	// Starts the the interpolation struct setting all data necessary.
	void Start(UAnimMontage * ClimbingMontage);

	// Updates player position during climbing animations.
	void Update();
};