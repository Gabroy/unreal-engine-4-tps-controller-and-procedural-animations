#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_State.h"
#include "Components/InputComponent.h"
#include "PS_Human_InputState_Climb.generated.h"

class APS_Character;
class UPS_Human_Input_GeneralController;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_Human_InputState_Climb : public UFSM_State
{
protected:

	GENERATED_BODY()
	
	/* Components */
	
	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_Input_GeneralController * InputComponent = nullptr;

	/* Input binding. */

	FInputAxisBinding MoveForwardActionBind;
	FInputAxisBinding MoveSidewaysActionBind;

	FInputActionBinding WalkAction;
	FInputActionBinding StopWalkAction;
	FInputActionBinding CrouchAction;
	FInputActionBinding UncrouchAction;
	FInputActionBinding JumpAction;
	FInputActionBinding ReleaseAction;

public:

	/* Methods. */

	void Init_Implementation() override;
	void OnEnter_Implementation() override;
	void OnExit_Implementation() override;

	/* Actions */

	void MoveForwards(float AxisValue);
	void MoveSideways(float AxisValue);

private:

	/* Actions. */

	void Crouch();
	void Stand();
	void Walk();
	void StopWalk();
	void Jump();
	void Release();
};
