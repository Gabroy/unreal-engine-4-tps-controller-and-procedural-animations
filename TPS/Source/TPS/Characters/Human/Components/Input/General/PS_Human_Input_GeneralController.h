#pragma once

#include "CoreMinimal.h"
#include "Characters/Common/Components/Input/PS_Base_InputController.h"
#include "PS_Human_Input_GeneralController.generated.h"

class APS_Character;
class AController;
class UPS_Human_CameraController;

/*
* Human Input controller FSM.
* 
* Controls enabling and disabling inputs bindings for a character to its actions based on the current locomotion
* state the character is in: Grounded, Falling, Climbing, Blocked, ...
* 
* The class is not responsible of checking if such actions can actually be performed, that belongs to the APS character
* and subclasses.
* 
* This controller is responsible of the locomotion aspect of the character while Human Input Carrying Controller controls
* inputs based on carrying object.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_Input_GeneralController : public UPS_Base_InputController
{

protected:

	GENERATED_BODY()

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Owner;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_CameraController * CameraController;

	/* Variables */

	/* General actions. */

	FInputAxisBinding MoveForwardActionBind;
	FInputAxisBinding MoveSidewaysActionBind;
	FInputAxisBinding LookupActionBind;
	FInputAxisBinding TurnActionBind;

	FInputActionBinding FlipViewModeHandle;

	FInputActionBinding UpwardsAction;
	FInputActionBinding ReleasedUpwardsAction;
	FInputActionBinding DownwardsAction;
	FInputActionBinding ReleasedDownwardsAction;
	FInputActionBinding RightwardsAction;
	FInputActionBinding ReleasedRightwardsAction;
	FInputActionBinding LeftwardsAction;
	FInputActionBinding ReleasedLeftwardsAction;

	bool UpwardsKeyPressed = false;
	bool DownwardsKeyPressed = false;
	bool LeftwardsKeyPressed = false;
	bool RightwardsKeyPressed = false;

public:

	// Initializes the controller, storing all necessary values.
	virtual void Init() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	/* Changing state functions */

	void OnLocomotionControllerChangedState(FName NewStateName, int NewStateID);

	/* Input functions */

	// Enable movement based on the controller rotator.
	// Used by default by locomotion modes like grounded or in air.
	void SetEnabledMovementBasedOnController(bool EnableMov);

	void RemoveAxis(const FInputAxisBinding & AxisBind);

	/* Getters */

	APS_Character * GetCharacter() const;
	UInputComponent * GetInputComponent() const;
	bool GetUpwardsKeyPressed() const { return UpwardsKeyPressed; }
	bool GetDownwardsKeyPressed() const { return DownwardsKeyPressed; }
	bool GetLeftwardsKeyPressed() const { return LeftwardsKeyPressed; }
	bool GetRightwardsKeyPressed() const { return RightwardsKeyPressed; }

protected:

	/* Init functions. */

	// Bind basic inputs.
	void BindBasicInputs();

	/* General Actions. */

	// Change the camera view mode.
	void FlipViewMode();

	// Check keys pressed.
	void Upwards();
	void ReleasedUpwards();
	void Downwards();
	void ReleasedDownwards();
	void Rightwards();
	void ReleasedRightwards();
	void Leftwards();
	void ReleasedLeftwards();
};
