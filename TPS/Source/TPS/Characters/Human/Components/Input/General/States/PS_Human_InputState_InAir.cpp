#include "PS_Human_InputState_InAir.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Input/General/PS_Human_Input_GeneralController.h"

void UPS_Human_InputState_InAir::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
	{
		return;
	}

	CharacterOwner = Cast<APS_Character>(Actor);
	if(!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_InAir::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	InputComponent = Cast<UPS_Human_Input_GeneralController>(GetFSMOwner().GetObject());
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_InAir::Init_Implementation InputComponent Returned Null"));
		return;
	}

	TickByDefault = true;
}

void UPS_Human_InputState_InAir::OnEnter_Implementation()
{
	if (!InputComponent)
	{
		return;
	}
	
	AimBtnPressed = InputComponent->BindAction("Aim", IE_Pressed, this, &UPS_Human_InputState_InAir::Aim);
	AimBtnReleased = InputComponent->BindAction("Aim", IE_Released, this, &UPS_Human_InputState_InAir::StopAim);
}

void UPS_Human_InputState_InAir::OnExit_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	InputComponent->RemoveActionBindingForHandle(AimBtnPressed.GetHandle());
	InputComponent->RemoveActionBindingForHandle(AimBtnReleased.GetHandle());
}

void UPS_Human_InputState_InAir::Tick_Implementation(float DeltaTime)
{
	if(CharacterOwner)
		CharacterOwner->TryLedgeGrab();
}

void UPS_Human_InputState_InAir::Aim()
{
	if (CharacterOwner)
		CharacterOwner->DoAim();
}

void UPS_Human_InputState_InAir::StopAim()
{
	if (CharacterOwner)
		CharacterOwner->DoUnAim();
}