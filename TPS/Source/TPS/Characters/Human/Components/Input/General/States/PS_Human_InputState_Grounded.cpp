#include "PS_Human_InputState_Grounded.h"
#include "Components/SkeletalMeshComponent.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Input/General/PS_Human_Input_GeneralController.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationInstance.h"
#include "Kismet/KismetMathLibrary.h"
#include "Gameplay/Camera/Controllers/PS_Human_CameraController.h"

void UPS_Human_InputState_Grounded::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
	{
		return;
	}

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	InputComponent = Cast<UPS_Human_Input_GeneralController>(GetFSMOwner().GetObject());
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation InputComponent Returned Null"));
		return;
	}

	CameraController = Cast<UPS_Human_CameraController>(Actor->GetComponentByClass(UPS_Human_CameraController::StaticClass()));
	if (!CameraController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation CameraController Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation LocController Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SKMesh)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation AnimInstance Returned Null"));
			return;
		}
	}

	FDelegateAction ForwardActionDelegate;
	ForwardActionDelegate.BindUObject(this, &UPS_Human_InputState_Grounded::PerformRolling);
	ForwardActionHandler.Init(GetWorld(), ForwardActionDelegate);

	TickByDefault = false;
}

void UPS_Human_InputState_Grounded::OnEnter_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	// Forward action.
	ForwardAction = InputComponent->BindAction("Forwards", IE_Pressed, this, &UPS_Human_InputState_Grounded::Forward);

	// Walk.
	WalkAction = InputComponent->BindAction("ToggleWalk", IE_Pressed, this, &UPS_Human_InputState_Grounded::Walk);
	StopWalkAction = InputComponent->BindAction("ToggleWalk", IE_Released, this, &UPS_Human_InputState_Grounded::StopWalk);

	// Crouch.
	CrouchAction = InputComponent->BindAction("Crouch", IE_Pressed, this, &UPS_Human_InputState_Grounded::Crouch);
	UncrouchAction = InputComponent->BindAction("Crouch", IE_Released, this, &UPS_Human_InputState_Grounded::Stand);

	// Sprint.
	SprintAction = InputComponent->BindAction("Sprint", IE_Pressed, this, &UPS_Human_InputState_Grounded::Sprint);
	UnSprintAction = InputComponent->BindAction("Sprint", IE_Released, this, &UPS_Human_InputState_Grounded::StopSprint);

	// Jump.
	JumpAction = InputComponent->BindAction("Jump", IE_Pressed, this, &UPS_Human_InputState_Grounded::Jump);

	// Aim.
	AimBtnPressed = InputComponent->BindAction("Aim", IE_Pressed, this, &UPS_Human_InputState_Grounded::Aim);
	AimBtnReleased = InputComponent->BindAction("Aim", IE_Released, this, &UPS_Human_InputState_Grounded::StopAim);

	// Toggle standing foot.
	ToggleStandingFootBtnPressed = InputComponent->BindAction("ChangeFoot", IE_Pressed, this, &UPS_Human_InputState_Grounded::ChangeFoot);

	if (AnimInstance)
	{
		AnimInstance->OnGroundedIdleEntered.BindUObject(this, &UPS_Human_InputState_Grounded::OnIdleEntered);
		AnimInstance->OnGroundedIdleExit.BindUObject(this, &UPS_Human_InputState_Grounded::OnIdleExit);
	}
}

void UPS_Human_InputState_Grounded::OnExit_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	InputComponent->RemoveActionBindingForHandle(ForwardAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(WalkAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(StopWalkAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(CrouchAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(UncrouchAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(SprintAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(UnSprintAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(JumpAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(AimBtnPressed.GetHandle());
	InputComponent->RemoveActionBindingForHandle(AimBtnReleased.GetHandle());
	InputComponent->RemoveActionBindingForHandle(ToggleStandingFootBtnPressed.GetHandle());

	if (AnimInstance)
	{
		AnimInstance->OnGroundedIdleEntered.Unbind();
		AnimInstance->OnGroundedIdleExit.Unbind();
	}
}

void UPS_Human_InputState_Grounded::Tick_Implementation(float DeltaTime)
{
	if(TickBehaviorPtr)
		(this->*TickBehaviorPtr)();
}

/* Actions. */

void UPS_Human_InputState_Grounded::Forward()
{
	if (!CharacterOwner || !CharacterOwner->CanDoRolling())
		return;
	ForwardActionHandler.PressAction();
}

void UPS_Human_InputState_Grounded::PerformRolling()
{
	if (!CharacterOwner)
		return;

	CharacterOwner->DoRolling();
	StopForward();
}

void UPS_Human_InputState_Grounded::StopForward()
{

}

void UPS_Human_InputState_Grounded::Crouch()
{
	if (!CharacterOwner || !LocController)
		return;

	bool CanPerformSlide = LocController->IsSprinting() && CharacterOwner->CanDoSlide();
	if (CanPerformSlide)
	{
		StopSprint();
		CharacterOwner->DoSlide();
	}
	else
	{
		CharacterOwner->DoCrouch();
	}
}

void UPS_Human_InputState_Grounded::Stand()
{
	if (CharacterOwner)
	{
		CharacterOwner->DoUncrouch();
	}
}

void UPS_Human_InputState_Grounded::Walk()
{
	if (!CharacterOwner || !LocController)
		return;

	bool CanWalk = LocController->GetLocomotionStance() == ELocomotionStance::Normal;
	if (CanWalk)
	{
		CharacterOwner->DoWalk();
	}
}

void UPS_Human_InputState_Grounded::StopWalk()
{
	if (CharacterOwner)
	{
		CharacterOwner->DoStopWalk();
	}
}

void UPS_Human_InputState_Grounded::Sprint()
{
	if (CharacterOwner && CharacterOwner->CanDoSprint())
	{
		TickBehaviorPtr = &UPS_Human_InputState_Grounded::SprintTickBehavior;
		EnableStateTick(true);
		CharacterOwner->DoSprint();
	}
}

void UPS_Human_InputState_Grounded::StopSprint()
{
	if (!CharacterOwner || !LocController || !CameraController)
		return;

	if (LocController->IsSprinting())
	{
		CameraController->ChangeToDefaultCamera();
		EnableStateTick(false);
		CharacterOwner->DoStopSprint();
	}
}

void UPS_Human_InputState_Grounded::SprintTickBehavior()
{
	if (!CharacterOwner || !LocController || !CameraController)
		return;

	// While sprinting, we change cameras to sprint if necessary.
	// We have to check this boolean each tick because we might have
	// Stopped sprinting while falling or hitting a wall.
	// This checks that all conditions are correct.
	if (LocController->IsSprinting())
	{
		CameraController->ChangeToSprintCamera();
		if (!CharacterOwner->TryVaulting())
			CharacterOwner->TryClimbing();
	}
	else
	{
		CameraController->ChangeToDefaultCamera();
	}
}

void UPS_Human_InputState_Grounded::Jump()
{
	if (!CharacterOwner || !LocController)
		return;

	if (LocController->GetLocomotionStance() == ELocomotionStance::Crouch)
	{
		return;
	}

	if (!CharacterOwner->TryVaulting())
	{
		if (!CharacterOwner->TryClimbing())
		{
			CharacterOwner->DoIntendJump();
		}
	}
}

void UPS_Human_InputState_Grounded::Aim()
{
	if (CharacterOwner)
		CharacterOwner->DoAim();
}

void UPS_Human_InputState_Grounded::StopAim()
{
	if (CharacterOwner)
		CharacterOwner->DoUnAim();
}

void UPS_Human_InputState_Grounded::ChangeFoot()
{
	if (CharacterOwner)
		CharacterOwner->DoToggleStandingFoot();
}

/* On Idle Turning */

void UPS_Human_InputState_Grounded::OnIdleEntered()
{
	TickBehaviorPtr = &UPS_Human_InputState_Grounded::TurningTickBehavior;
	EnableStateTick(true);
}

void UPS_Human_InputState_Grounded::OnIdleExit()
{
	StopTurningCheck();
	EnableStateTick(false);
}

void UPS_Human_InputState_Grounded::StartTurningCheck()
{
	if (TurningCheck.IsValid())
	{
		return;
	}

	UWorld * WorldPtr = LocController->GetWorld();
	if (!WorldPtr)
	{
		return;
	}

	WorldPtr->GetTimerManager().SetTimer(TurningCheck, this, &UPS_Human_InputState_Grounded::DoTurning, OnIdleTimeForTurningToHappen, false);
}

void UPS_Human_InputState_Grounded::StopTurningCheck()
{
	if (!TurningCheck.IsValid())
	{
		return;
	}

	UWorld * WorldPtr = LocController->GetWorld();
	if (!WorldPtr)
	{
		return;
	}

	WorldPtr->GetTimerManager().ClearTimer(TurningCheck);
}

float UPS_Human_InputState_Grounded::GetLookingAngle()
{
	return UKismetMathLibrary::NormalizedDeltaRotator(LocController->GetActorCurrentRotation(), CharacterOwner->GetController()->GetControlRotation()).Yaw;
}

void UPS_Human_InputState_Grounded::TurningTickBehavior()
{
	float LookingDelta = GetLookingAngle();
	if (FMath::Abs(LookingDelta) >= OnIdleDeltaToStartTurning)
		StartTurningCheck();
	else
		StopTurningCheck();
}

void UPS_Human_InputState_Grounded::DoTurning()
{
	if (CharacterOwner)
	{
		CharacterOwner->StartTurning(CharacterOwner->GetController()->GetControlRotation().Yaw);
	}
}