#include "PS_Human_Input_GeneralController.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Gameplay/Camera/Controllers/PS_Human_CameraController.h"

void UPS_Human_Input_GeneralController::Init()
{
	Owner = Cast<APS_Character>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputController::Init Owner Returned Null"));
		return;
	}

	CameraController = Owner->FindComponentByClass<UPS_Human_CameraController>();
	if (!CameraController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputController::Init CameraController Returned Null"));
		return;
	}

	UPS_Human_LocomotionController * LocomotionController = Owner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputController::Init LocomotionController Returned Null"));
		return;
	}
	LocomotionController->OnChangedStateMulticast.AddUObject(this, &UPS_Human_Input_GeneralController::OnLocomotionControllerChangedState);
	LocomotionController->AddTickPrerequisiteComponent(this);

	BindBasicInputs();

	Super::Init();
}

void UPS_Human_Input_GeneralController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	// Here we update the current locomotion state.
	UFSM_Component::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UPS_Human_Input_GeneralController::BindBasicInputs()
{
	UInputComponent * InputCmp = Owner->InputComponent;

	if (!InputCmp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputController::BindBasicInputs InputComponent Returned Null"));
		return;
	}

	// Movement.
	SetEnabledMovementBasedOnController(true);

	// Camera rotation.
	LookupActionBind = InputCmp->BindAxis("Lookup", Owner, &APS_Character::AddControllerPitchInput);
	TurnActionBind = InputCmp->BindAxis("Turn", Owner, &APS_Character::AddControllerYawInput);

	// FPS and TPS
	FlipViewModeHandle = InputCmp->BindAction("FlipViewMode", IE_Pressed, this, &UPS_Human_Input_GeneralController::FlipViewMode);

	// Upwards action.
	UpwardsAction = InputCmp->BindAction("Upwards", IE_Pressed, this, &UPS_Human_Input_GeneralController::Upwards);
	ReleasedUpwardsAction = InputCmp->BindAction("Upwards", IE_Released, this, &UPS_Human_Input_GeneralController::ReleasedUpwards);
	// Backwards action.
	DownwardsAction = InputCmp->BindAction("Backwards", IE_Pressed, this, &UPS_Human_Input_GeneralController::Downwards);
	ReleasedDownwardsAction = InputCmp->BindAction("Backwards", IE_Released, this, &UPS_Human_Input_GeneralController::ReleasedDownwards);
	// Rightwards action.
	RightwardsAction = InputCmp->BindAction("Rightwards", IE_Pressed, this, &UPS_Human_Input_GeneralController::Rightwards);
	ReleasedRightwardsAction = InputCmp->BindAction("Rightwards", IE_Released, this, &UPS_Human_Input_GeneralController::ReleasedRightwards);
	// Leftwards action.
	LeftwardsAction = InputCmp->BindAction("Leftwards", IE_Pressed, this, &UPS_Human_Input_GeneralController::Leftwards);
	ReleasedLeftwardsAction = InputCmp->BindAction("Leftwards", IE_Released, this, &UPS_Human_Input_GeneralController::ReleasedLeftwards);
}

void UPS_Human_Input_GeneralController::OnLocomotionControllerChangedState(FName NewStateName, int NewStateID)
{
	ELocomotionMode LocomotionMode = (ELocomotionMode)NewStateID;
	switch (LocomotionMode)
	{
	case ELocomotionMode::Grounded:
		ChangeToStateByName("Grounded");
		break;
	case ELocomotionMode::In_Air:
		ChangeToStateByName("InAir");
		break;
	case ELocomotionMode::Climbing:
		ChangeToStateByName("Climbing");
		break;
	default:
		ChangeToStateByName("Grounded");
		break;
	}
}

/* Input functions */

void UPS_Human_Input_GeneralController::SetEnabledMovementBasedOnController(bool EnableMov)
{
	if (EnableMov)
	{
		MoveForwardActionBind = Owner->InputComponent->BindAxis("MoveForward", Owner, &APS_Character::MoveForward);
		MoveSidewaysActionBind = Owner->InputComponent->BindAxis("MoveSideways", Owner, &APS_Character::MoveSideways);
	}
	else
	{
		RemoveAxis(MoveForwardActionBind);
		RemoveAxis(MoveSidewaysActionBind);
	}
}

void UPS_Human_Input_GeneralController::RemoveAxis(const FInputAxisBinding& AxisBind)
{
	for (int i = 0; i < Owner->InputComponent->AxisBindings.Num(); ++i)
	{
		FInputAxisBinding& AxisToCheckToRemove = Owner->InputComponent->AxisBindings[i];

		if (AxisToCheckToRemove.AxisName == AxisBind.AxisName) {
			Owner->InputComponent->AxisBindings.RemoveAt(i);
			break;
		}
	}
}

APS_Character * UPS_Human_Input_GeneralController::GetCharacter() const
{
	return Owner;
}

UInputComponent * UPS_Human_Input_GeneralController::GetInputComponent() const
{
	return Owner ? Owner->InputComponent : nullptr;
}

// Change the camera view mode.
void UPS_Human_Input_GeneralController::FlipViewMode()
{
	if (!CameraController)
	{
		return;
	}

	CameraController->ToogleViewMode();
}

void UPS_Human_Input_GeneralController::Upwards()
{
	UpwardsKeyPressed = true;
}

void UPS_Human_Input_GeneralController::ReleasedUpwards()
{
	UpwardsKeyPressed = false;
}

void UPS_Human_Input_GeneralController::Downwards()
{
	DownwardsKeyPressed = true;
}

void UPS_Human_Input_GeneralController::ReleasedDownwards()
{
	DownwardsKeyPressed = false;
}

void UPS_Human_Input_GeneralController::Rightwards()
{
	RightwardsKeyPressed = true;
}

void UPS_Human_Input_GeneralController::ReleasedRightwards()
{
	RightwardsKeyPressed = false;
}

void UPS_Human_Input_GeneralController::Leftwards()
{
	LeftwardsKeyPressed = true;
}

void UPS_Human_Input_GeneralController::ReleasedLeftwards()
{
	LeftwardsKeyPressed = false;
}