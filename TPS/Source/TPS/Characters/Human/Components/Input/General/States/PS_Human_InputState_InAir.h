#pragma once

#include "Base/FSM/FSM_State.h"
#include "CoreMinimal.h"
#include "Components/InputComponent.h"
#include "PS_Human_InputState_InAir.generated.h"

class APS_Character;
class UPS_Human_Input_GeneralController;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_Human_InputState_InAir : public UFSM_State
{
protected:

	GENERATED_BODY()

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_Input_GeneralController * InputComponent = nullptr;

	/* Input binding. */

	FInputActionBinding AimBtnPressed;
	FInputActionBinding AimBtnReleased;

public:

	/* Methods. */

	void Init_Implementation() override;
	
	void OnEnter_Implementation() override;

	void OnExit_Implementation() override;

	void Tick_Implementation(float DeltaTime) override;

private:

	void Aim();
	void StopAim();
};
