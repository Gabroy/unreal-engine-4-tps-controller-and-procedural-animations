#include "PS_Human_InputState_Climb.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Input/General/PS_Human_Input_GeneralController.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationInstance.h"

void UPS_Human_InputState_Climb::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
	{
		return;
	}

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	InputComponent = Cast<UPS_Human_Input_GeneralController>(GetFSMOwner().GetObject());
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation InputComponent Returned Null"));
		return;
	}

	TickByDefault = false;
}

void UPS_Human_InputState_Climb::OnEnter_Implementation()
{
	if (!InputComponent) return;

	// Disabled movement based on controller rotation.
	InputComponent->SetEnabledMovementBasedOnController(false);
	
	// Movement.
	MoveForwardActionBind = InputComponent->BindAxis("MoveForward", this, &UPS_Human_InputState_Climb::MoveForwards);
	MoveSidewaysActionBind = InputComponent->BindAxis("MoveSideways", this, &UPS_Human_InputState_Climb::MoveSideways);
	// Walk.
	WalkAction = InputComponent->BindAction("ToggleWalk", IE_Pressed, this, &UPS_Human_InputState_Climb::Walk);
	StopWalkAction = InputComponent->BindAction("ToggleWalk", IE_Released, this, &UPS_Human_InputState_Climb::StopWalk);
	// Crouch.
	CrouchAction = InputComponent->BindAction("Crouch", IE_Pressed, this, &UPS_Human_InputState_Climb::Crouch);
	UncrouchAction = InputComponent->BindAction("Crouch", IE_Released, this, &UPS_Human_InputState_Climb::Stand);
	// Jump.
	JumpAction = InputComponent->BindAction("Jump", IE_Pressed, this, &UPS_Human_InputState_Climb::Jump);
	// Release
	ReleaseAction = InputComponent->BindAction("Action", IE_Pressed, this, &UPS_Human_InputState_Climb::Release);
}

void UPS_Human_InputState_Climb::OnExit_Implementation()
{
	if (InputComponent)
	{
		// Enable movement based on controller rotation.
		InputComponent->SetEnabledMovementBasedOnController(true);

		// Unbind actions.
		InputComponent->RemoveAxis(MoveForwardActionBind);
		InputComponent->RemoveAxis(MoveSidewaysActionBind);
		InputComponent->RemoveActionBindingForHandle(WalkAction.GetHandle());
		InputComponent->RemoveActionBindingForHandle(StopWalkAction.GetHandle());
		InputComponent->RemoveActionBindingForHandle(CrouchAction.GetHandle());
		InputComponent->RemoveActionBindingForHandle(UncrouchAction.GetHandle());
		InputComponent->RemoveActionBindingForHandle(JumpAction.GetHandle());
		InputComponent->RemoveActionBindingForHandle(ReleaseAction.GetHandle());

	}
}

void UPS_Human_InputState_Climb::MoveForwards(float AxisValue)
{
	if(CharacterOwner)
		CharacterOwner->Hanging_TryClimb(AxisValue);
}

void UPS_Human_InputState_Climb::MoveSideways(float AxisValue)
{
	if (CharacterOwner)
		CharacterOwner->Hanging_TryMove(AxisValue);
}

/* Actions. */

void UPS_Human_InputState_Climb::Crouch()
{
	if (CharacterOwner)
		CharacterOwner->Hanging_Crouch();
}

void UPS_Human_InputState_Climb::Stand()
{
	if (CharacterOwner)
		CharacterOwner->Hanging_Stand();
}

void UPS_Human_InputState_Climb::Walk()
{
	if (CharacterOwner)
		CharacterOwner->Hanging_Walk();
}

void UPS_Human_InputState_Climb::StopWalk()
{
	if (CharacterOwner)
		CharacterOwner->Hanging_StopWalk();
}

void UPS_Human_InputState_Climb::Jump()
{
	if (!CharacterOwner || !InputComponent)
		return;

	if (InputComponent->GetDownwardsKeyPressed())
		CharacterOwner->Hanging_PerformJumpingFromHanged();
	else if (InputComponent->GetRightwardsKeyPressed())
		CharacterOwner->Hanging_PerformHorizontalJump(true);
	else if(InputComponent->GetLeftwardsKeyPressed())
		CharacterOwner->Hanging_PerformHorizontalJump(false);
	else
		CharacterOwner->Hanging_PerformVerticalJump();
}

void UPS_Human_InputState_Climb::Release()
{
	if (CharacterOwner)
		CharacterOwner->Hanging_ReleaseHang();
}