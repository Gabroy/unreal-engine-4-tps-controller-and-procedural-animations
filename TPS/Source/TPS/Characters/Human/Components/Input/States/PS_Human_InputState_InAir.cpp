#include "PS_Human_InputState_InAir.h"
#include "../../../PS_Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../../../../../Camera/Controllers/PS_Human_CameraController.h"


void UPS_Human_InputState_InAir::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if(!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_InAir::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	CameraController = Cast<UPS_Human_CameraController>(Actor->GetComponentByClass(UPS_Human_CameraController::StaticClass()));
	if(!CameraController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_InAir::Init_Implementation CameraController Returned Null"));
		return;
	}

	MovComponent = Cast<UCharacterMovementComponent>(Actor->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if(!MovComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_InAir::Init_Implementation MovComponent Returned Null"));
		return;
	}
	
	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if(!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_InAir::Init_Implementation LocController Returned Null"));
		return;
	}

	TickByDefault = true;
}

void UPS_Human_InputState_InAir::OnEnter_Implementation()
{
	if (!CharacterOwner || !CharacterOwner->InputComponent) return;

	UInputComponent* InputComp = CharacterOwner->InputComponent;

	// Aim.
	AimBtnPressed = InputComp->BindAction("Aim", IE_Pressed, this, &UPS_Human_InputState_InAir::Aim);
	AimBtnReleased = InputComp->BindAction("Aim", IE_Released, this, &UPS_Human_InputState_InAir::StopAim);
}

void UPS_Human_InputState_InAir::OnExit_Implementation()
{
	if (!CharacterOwner || !CharacterOwner->InputComponent) return;

	UInputComponent* InputComp = CharacterOwner->InputComponent;

	InputComp->RemoveActionBindingForHandle(AimBtnPressed.GetHandle());
	InputComp->RemoveActionBindingForHandle(AimBtnReleased.GetHandle());
}

void UPS_Human_InputState_InAir::Tick_Implementation(float DeltaTime)
{
	CharacterOwner->TryLedgeGrab();
}

void UPS_Human_InputState_InAir::Aim()
{

	CharacterOwner->DoZoom();
}

void UPS_Human_InputState_InAir::StopAim()
{
	CharacterOwner->DoUnZoom();
}