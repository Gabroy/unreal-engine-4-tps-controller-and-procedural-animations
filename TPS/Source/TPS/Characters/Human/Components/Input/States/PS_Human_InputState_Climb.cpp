#include "PS_Human_InputState_Climb.h"
#include "../../../PS_Character.h"
#include "../../../../../Camera/Controllers/PS_Human_CameraController.h"
#include "../../Locomotion/PS_Human_LocomotionController.h"
#include "Components/SkeletalMeshComponent.h"
#include "../../Animations/PS_Human_AnimationInstance.h"
#include "GameFramework/PlayerController.h"

void UPS_Human_InputState_Climb::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	PlayerController = Cast<APlayerController>(CharacterOwner->GetController());
	if (!PlayerController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation PlayerController Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation LocController Returned Null"));
		return;
	}

	CapsuleComponent = Cast<UCapsuleComponent>(Actor->GetComponentByClass(UCapsuleComponent::StaticClass()));
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation CapsuleComponent Returned Null"));
		return;
	}

	InputComponent = Cast<UPS_Human_InputController>(Actor->GetComponentByClass(UPS_Human_InputController::StaticClass()));
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation InputComponent Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SKMesh)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Climb::Init_Implementation AnimInstance Returned Null"));
			return;
		}
	}

	TickByDefault = false;
}

void UPS_Human_InputState_Climb::OnEnter_Implementation()
{
	if (!CharacterOwner || !CharacterOwner->InputComponent) return;

	// Disabled movement based on controller rotation.
	InputComponent->SetEnabledMovementBasedOnController(false);

	UInputComponent* UEInputComp = CharacterOwner->InputComponent;

	// Movement.
	MoveForwardActionBind = UEInputComp->BindAxis("MoveForward", this, &UPS_Human_InputState_Climb::MoveForwards);
	MoveSidewaysActionBind = UEInputComp->BindAxis("MoveSideways", this, &UPS_Human_InputState_Climb::MoveSideways);
	// Walk.
	WalkAction = UEInputComp->BindAction("ToggleWalk", IE_Pressed, this, &UPS_Human_InputState_Climb::Walk);
	StopWalkAction = UEInputComp->BindAction("ToggleWalk", IE_Released, this, &UPS_Human_InputState_Climb::StopWalk);
	// Crouch.
	CrouchAction = UEInputComp->BindAction("Crouch", IE_Pressed, this, &UPS_Human_InputState_Climb::Crouch);
	UncrouchAction = UEInputComp->BindAction("Crouch", IE_Released, this, &UPS_Human_InputState_Climb::Stand);
	// Jump.
	JumpAction = UEInputComp->BindAction("Jump", IE_Pressed, this, &UPS_Human_InputState_Climb::Jump);
	// Release
	ReleaseAction = UEInputComp->BindAction("Action", IE_Pressed, this, &UPS_Human_InputState_Climb::Release);
}

void UPS_Human_InputState_Climb::OnExit_Implementation()
{
	if (!CharacterOwner || !CharacterOwner->InputComponent) return;

	InputComponent->RemoveAxis(MoveForwardActionBind);
	InputComponent->RemoveAxis(MoveSidewaysActionBind);

	// Enable movement based on controller rotation.
	InputComponent->SetEnabledMovementBasedOnController(true);

	UInputComponent* UEInputComp = CharacterOwner->InputComponent;

	UEInputComp->RemoveActionBindingForHandle(WalkAction.GetHandle());
	UEInputComp->RemoveActionBindingForHandle(StopWalkAction.GetHandle());
	UEInputComp->RemoveActionBindingForHandle(CrouchAction.GetHandle());
	UEInputComp->RemoveActionBindingForHandle(UncrouchAction.GetHandle());
	UEInputComp->RemoveActionBindingForHandle(JumpAction.GetHandle());
	UEInputComp->RemoveActionBindingForHandle(ReleaseAction.GetHandle());
}

void UPS_Human_InputState_Climb::Tick_Implementation(float DeltaTime)
{

}

void UPS_Human_InputState_Climb::MoveForwards(float AxisValue)
{
	CharacterOwner->Hanging_TryClimb(AxisValue);
}

void UPS_Human_InputState_Climb::MoveSideways(float AxisValue)
{
	CharacterOwner->Hanging_TryMove(AxisValue);
}

/* Actions. */

void UPS_Human_InputState_Climb::Crouch()
{
	CharacterOwner->Hanging_Crouch();
}

void UPS_Human_InputState_Climb::Stand()
{
	CharacterOwner->Hanging_Stand();
}

void UPS_Human_InputState_Climb::Walk()
{
	CharacterOwner->Hanging_Walk();
}

void UPS_Human_InputState_Climb::StopWalk()
{
	CharacterOwner->Hanging_StopWalk();
}

void UPS_Human_InputState_Climb::Jump()
{
	if (InputComponent->GetDownwardsKeyPressed())
		CharacterOwner->Hanging_PerformJumpingFromHanged();
	else if (InputComponent->GetRightwardsKeyPressed())
		CharacterOwner->Hanging_PerformHorizontalJump(true);
	else if(InputComponent->GetLeftwardsKeyPressed())
		CharacterOwner->Hanging_PerformHorizontalJump(false);
	else
		CharacterOwner->Hanging_PerformVerticalJump();
}

void UPS_Human_InputState_Climb::Release()
{
	CharacterOwner->Hanging_ReleaseHang();
}