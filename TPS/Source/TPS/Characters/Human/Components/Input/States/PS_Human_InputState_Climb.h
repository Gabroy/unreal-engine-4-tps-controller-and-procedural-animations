#pragma once

#include "CoreMinimal.h"
#include "TPS/FSM/FSM_State.h"
#include "Components/InputComponent.h"
#include "../../Locomotion/States/PS_LocState_Climb.h"
#include "../../../../Common/Structs/PS_ActionFlags.h"
#include "PS_Human_InputState_Climb.generated.h"

class APS_Character;
class APlayerController;
class UCharacterMovementComponent;
class UCapsuleComponent;
class UPS_Human_LocomotionController;
class UPS_Human_InputController;
class UPS_Human_AnimationInstance;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_Human_InputState_Climb : public UFSM_State
{
protected:

	GENERATED_BODY()
	
	/* Components */
	
	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	APlayerController * PlayerController = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY()
	UCapsuleComponent * CapsuleComponent = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UPS_Human_InputController * InputComponent = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance * AnimInstance = nullptr;

	/* Input binding. */

	FInputAxisBinding MoveForwardActionBind;
	FInputAxisBinding MoveSidewaysActionBind;

	FInputActionBinding WalkAction;
	FInputActionBinding StopWalkAction;
	FInputActionBinding CrouchAction;
	FInputActionBinding UncrouchAction;
	FInputActionBinding JumpAction;
	FInputActionBinding ReleaseAction;

public:

	/* Methods. */

	void Init_Implementation() override;
	
	void OnEnter_Implementation() override;

	void OnExit_Implementation() override;

	void Tick_Implementation(float DeltaTime) override;

	/* Actions */

	void MoveForwards(float AxisValue);

	void MoveSideways(float AxisValue);

private:

	/* Actions. */

	void Crouch();
	void Stand();
	void Walk();
	void StopWalk();
	void Jump();
	void Release();
};
