#pragma once

#include "CoreMinimal.h"
#include "TPS/FSM/FSM_State.h"
#include "../../Locomotion/PS_Human_LocomotionController.h"
#include "Components/InputComponent.h"
#include "PS_Human_InputState_InAir.generated.h"

class APS_Character;
class UCharacterMovementComponent;
class UPS_Human_LocomotionController;
class UPS_Human_CameraController;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_Human_InputState_InAir : public UFSM_State
{
protected:

	GENERATED_BODY()

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UPS_Human_CameraController * CameraController = nullptr;

	/* Input binding. */

	FInputActionBinding AimBtnPressed;
	FInputActionBinding AimBtnReleased;

public:

	/* Methods. */

	void Init_Implementation() override;
	
	void OnEnter_Implementation() override;

	void OnExit_Implementation() override;

	void Tick_Implementation(float DeltaTime) override;

private:

	void Aim();

	void StopAim();
};
