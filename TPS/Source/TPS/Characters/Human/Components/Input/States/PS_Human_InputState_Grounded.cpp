#include "PS_Human_InputState_Grounded.h"
#include "../../../PS_Character.h"
#include "TPS/Camera/Controllers/PS_Human_CameraController.h"
#include "Components/SkeletalMeshComponent.h"
#include "../../Locomotion/PS_Human_LocomotionController.h"
#include "Kismet/KismetMathLibrary.h"
#include "../../Animations/PS_Human_AnimationInstance.h"

void UPS_Human_InputState_Grounded::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor) return;

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	CameraController = Cast<UPS_Human_CameraController>(Actor->GetComponentByClass(UPS_Human_CameraController::StaticClass()));
	if (!CameraController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation CameraController Returned Null"));
		return;
	}

	LocController = Cast<UPS_Human_LocomotionController>(Actor->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputState_Grounded::Init_Implementation LocController Returned Null"));
		return;
	}

	USkeletalMeshComponent * SKMesh = Cast<USkeletalMeshComponent>(Actor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if (SKMesh)
	{
		AnimInstance = Cast<UPS_Human_AnimationInstance>(SKMesh->GetAnimInstance());
		if (!AnimInstance)
		{
			UE_LOG(LogTemp, Error, TEXT("UPS_Human_AnimationInstance::Init_Implementation AnimInstance Returned Null"));
			return;
		}
	}

	FDelegateAction ForwardActionDelegate;
	ForwardActionDelegate.BindUObject(this, &UPS_Human_InputState_Grounded::PerformRolling);
	ForwardActionHandler.Init(GetWorld(), ForwardActionDelegate);

	TickByDefault = false;
}

void UPS_Human_InputState_Grounded::OnEnter_Implementation()
{
	if (!CharacterOwner || !CharacterOwner->InputComponent) return;

	UInputComponent * InputComp = CharacterOwner->InputComponent;

	// Forward action.
	ForwardAction = InputComp->BindAction("Forwards", IE_Pressed, this, &UPS_Human_InputState_Grounded::Forward);

	// Walk.
	WalkAction = InputComp->BindAction("ToggleWalk", IE_Pressed, this, &UPS_Human_InputState_Grounded::Walk);
	StopWalkAction = InputComp->BindAction("ToggleWalk", IE_Released, this, &UPS_Human_InputState_Grounded::StopWalk);

	// Crouch.
	CrouchAction = InputComp->BindAction("Crouch", IE_Pressed, this, &UPS_Human_InputState_Grounded::Crouch);
	UncrouchAction = InputComp->BindAction("Crouch", IE_Released, this, &UPS_Human_InputState_Grounded::Stand);

	// Sprint.
	SprintAction = InputComp->BindAction("Sprint", IE_Pressed, this, &UPS_Human_InputState_Grounded::Sprint);
	UnSprintAction = InputComp->BindAction("Sprint", IE_Released, this, &UPS_Human_InputState_Grounded::StopSprint);

	// Jump.
	JumpAction = InputComp->BindAction("Jump", IE_Pressed, this, &UPS_Human_InputState_Grounded::Jump);

	// Aim.
	AimBtnPressed = InputComp->BindAction("Aim", IE_Pressed, this, &UPS_Human_InputState_Grounded::Aim);
	AimBtnReleased = InputComp->BindAction("Aim", IE_Released, this, &UPS_Human_InputState_Grounded::StopAim);

	// Toggle standing foor.
	ToggleStandingFootBtnPressed = InputComp->BindAction("ChangeFoot", IE_Pressed, this, &UPS_Human_InputState_Grounded::ChangeFoot);

	if (AnimInstance)
	{
		AnimInstance->OnGroundedIdleEntered.BindUObject(this, &UPS_Human_InputState_Grounded::OnIdleEntered);
		AnimInstance->OnGroundedIdleExit.BindUObject(this, &UPS_Human_InputState_Grounded::OnIdleExit);
	}
}

void UPS_Human_InputState_Grounded::OnExit_Implementation()
{
	if (!CharacterOwner || !CharacterOwner->InputComponent) return;

	UInputComponent * InputComp = CharacterOwner->InputComponent;

	InputComp->RemoveActionBindingForHandle(ForwardAction.GetHandle());

	InputComp->RemoveActionBindingForHandle(WalkAction.GetHandle());
	InputComp->RemoveActionBindingForHandle(StopWalkAction.GetHandle());

	InputComp->RemoveActionBindingForHandle(CrouchAction.GetHandle());
	InputComp->RemoveActionBindingForHandle(UncrouchAction.GetHandle());

	InputComp->RemoveActionBindingForHandle(SprintAction.GetHandle());
	InputComp->RemoveActionBindingForHandle(UnSprintAction.GetHandle());

	InputComp->RemoveActionBindingForHandle(JumpAction.GetHandle());

	InputComp->RemoveActionBindingForHandle(AimBtnPressed.GetHandle());
	InputComp->RemoveActionBindingForHandle(AimBtnReleased.GetHandle());

	InputComp->RemoveActionBindingForHandle(ToggleStandingFootBtnPressed.GetHandle());

	if (AnimInstance)
	{
		AnimInstance->OnGroundedIdleEntered.Unbind();
		AnimInstance->OnGroundedIdleExit.Unbind();
	}
}

void UPS_Human_InputState_Grounded::Tick_Implementation(float DeltaTime)
{
	if(TickBehaviorPtr)
		(this->*TickBehaviorPtr)();
}

/* Actions. */

void UPS_Human_InputState_Grounded::Forward()
{
	if (!CharacterOwner->CanDoRolling()) return;

	ForwardActionHandler.PressAction();
}

void UPS_Human_InputState_Grounded::PerformRolling()
{
	// Do roll if pressed more than once.
	CharacterOwner->DoRolling();
	StopForward();
}

void UPS_Human_InputState_Grounded::StopForward(){}

void UPS_Human_InputState_Grounded::Crouch()
{
	if (LocController->IsSprinting() && CharacterOwner->CanDoSlide())
	{
		StopSprint();
		CharacterOwner->DoSlide();
	}
	else
		CharacterOwner->DoCrouch();
}

void UPS_Human_InputState_Grounded::Stand()
{
	CharacterOwner->DoUncrouch();
}

void UPS_Human_InputState_Grounded::Walk()
{
	if (LocController->GetLocomotionStance() == ELocomotionStance::Normal)
		CharacterOwner->DoWalk();
}

void UPS_Human_InputState_Grounded::StopWalk()
{
	CharacterOwner->DoStopWalk();
}

void UPS_Human_InputState_Grounded::Sprint()
{
	if (CharacterOwner->CanDoSprint())
	{
		TickBehaviorPtr = &UPS_Human_InputState_Grounded::SprintTickBehavior;
		EnableStateTick(true);
		CharacterOwner->DoSprint();
	}
}

void UPS_Human_InputState_Grounded::StopSprint()
{
	if (LocController->IsSprinting())
	{
		CameraController->ChangeToDefaultCamera();
		EnableStateTick(false);
		CharacterOwner->DoStopSprint();
	}
}

void UPS_Human_InputState_Grounded::SprintTickBehavior()
{
	// While sprinting, we change cameras to sprint if necessary.
	// We have to check this boolean each tick because we might have
	// Stopped sprinting while falling or hitting a wall.
	// This checks that all conditions are correct.
	if (LocController->IsSprinting())
	{
		CameraController->ChangeToSprintCamera();
		if (!CharacterOwner->TryVaulting())
			CharacterOwner->TryClimbing();
	}
	else
		CameraController->ChangeToDefaultCamera();
}

void UPS_Human_InputState_Grounded::Jump()
{
	if (LocController->GetLocomotionStance() == ELocomotionStance::Crouch)
		return;

	if (!CharacterOwner->TryVaulting())
	{
		if (!CharacterOwner->TryClimbing())
		{
			CharacterOwner->DoJump();
		}
	}
}

void UPS_Human_InputState_Grounded::Aim()
{

	CharacterOwner->DoZoom();
}

void UPS_Human_InputState_Grounded::StopAim()
{
	CharacterOwner->DoUnZoom();
}

void UPS_Human_InputState_Grounded::ChangeFoot()
{
	CharacterOwner->DoToggleStandingFoot();
}

/* On Idle Turning */

void UPS_Human_InputState_Grounded::OnIdleEntered()
{
	TickBehaviorPtr = &UPS_Human_InputState_Grounded::TurningTickBehavior;
	EnableStateTick(true);
}

void UPS_Human_InputState_Grounded::OnIdleExit()
{
	StopTurningCheck();
	EnableStateTick(false);
}

void UPS_Human_InputState_Grounded::StartTurningCheck()
{
	if (TurningCheck.IsValid())
		return;

	UWorld * WorldPtr = LocController->GetWorld();
	if (!WorldPtr) return;

	WorldPtr->GetTimerManager().SetTimer(TurningCheck, this, &UPS_Human_InputState_Grounded::DoTurning, OnIdleTimeForTurningToHappen, false);
}

void UPS_Human_InputState_Grounded::StopTurningCheck()
{
	if (!TurningCheck.IsValid())
		return;

	UWorld * WorldPtr = LocController->GetWorld();
	if (!WorldPtr) return;

	WorldPtr->GetTimerManager().ClearTimer(TurningCheck);
}

float UPS_Human_InputState_Grounded::GetLookingAngle()
{
	return UKismetMathLibrary::NormalizedDeltaRotator(LocController->GetActorCurrentRotation(), CharacterOwner->GetController()->GetControlRotation()).Yaw;
}

void UPS_Human_InputState_Grounded::TurningTickBehavior()
{
	float LookingDelta = GetLookingAngle();
	if (FMath::Abs(LookingDelta) >= OnIdleDeltaToStartTurning)
		StartTurningCheck();
	else
		StopTurningCheck();
}

void UPS_Human_InputState_Grounded::DoTurning()
{
	CharacterOwner->DoTurning(CharacterOwner->GetController()->GetControlRotation().Yaw);
}