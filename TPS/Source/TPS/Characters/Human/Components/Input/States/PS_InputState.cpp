#pragma once

#include "PS_InputState.h"

/* Methods. */

void UPS_InputState::OnEnter_Implementation()
{
	Register_Input();
}

void UPS_InputState::OnExit_Implementation()
{
	Unregister_Input();
}