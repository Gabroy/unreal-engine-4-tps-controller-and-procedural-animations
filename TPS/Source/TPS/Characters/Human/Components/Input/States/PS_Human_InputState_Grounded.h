#pragma once

#include "CoreMinimal.h"
#include "TPS/FSM/FSM_State.h"
#include "Components/InputComponent.h"
#include "../../../../Common/Structs/PS_ActionFlags.h"
#include "PS_Human_InputState_Grounded.generated.h"

class APS_Character;
class UPS_Human_CameraController;
class UCharacterMovementComponent;
class UPS_Human_LocomotionController;
class UPS_Human_AnimationInstance;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_Human_InputState_Grounded : public UFSM_State
{
protected:

	GENERATED_BODY()

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;

	UPROPERTY()
	UPS_Human_CameraController * CameraController = nullptr;

	UPROPERTY()
	UCharacterMovementComponent * MovComponent = nullptr;

	UPROPERTY()
	UPS_Human_LocomotionController * LocController = nullptr;

	UPROPERTY()
	UPS_Human_AnimationInstance* AnimInstance = nullptr;

	/* Forward variables. */

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	F_ActionWithTimeWindow ForwardActionHandler;

	/* Turning check. */
	
	// Delta that will allow turning to happen while on idle.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float OnIdleDeltaToStartTurning = 70.0f;

	// Time before the character turns if on idle and looking at a direction bigger than
	// min delta to turn.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float OnIdleTimeForTurningToHappen = 3.0f;

	FTimerHandle TurningCheck;

	/* Input binding. */

	FInputActionBinding ForwardAction;
	FInputActionBinding WalkAction;
	FInputActionBinding StopWalkAction;
	FInputActionBinding CrouchAction;
	FInputActionBinding UncrouchAction;
	FInputActionBinding SprintAction;
	FInputActionBinding UnSprintAction;
	FInputActionBinding JumpAction;
	FInputActionBinding AimBtnPressed;
	FInputActionBinding AimBtnReleased;
	FInputActionBinding ToggleStandingFootBtnPressed;


	typedef void (UPS_Human_InputState_Grounded::* TickBehavior)();
	TickBehavior TickBehaviorPtr = nullptr;

public:

	/* Methods. */

	void Init_Implementation() override;
	
	void OnEnter_Implementation() override;

	void OnExit_Implementation() override;

	void Tick_Implementation(float DeltaTime) override;

private:

	/* Actions. */

	void Forward();

	void PerformRolling();

	void StopForward();

	void Crouch();
	
	void Stand();
	
	void Walk();
	
	void StopWalk();
	
	void Sprint();
	
	void StopSprint();

	void SprintTickBehavior();
	
	void Jump();
	
	void Aim();
	
	void StopAim();

	void ChangeFoot();

	/* On Idle Turning */

	UFUNCTION()
	void OnIdleEntered();

	UFUNCTION()
	void OnIdleExit();

	inline void StartTurningCheck();

	inline void StopTurningCheck();

	float GetLookingAngle();

	void TurningTickBehavior();

	UFUNCTION()
	void DoTurning();
};
