#pragma once

#include "CoreMinimal.h"
#include "TPS/FSM/FSM_State.h"
#include "PS_InputState.generated.h"

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_InputState : public UFSM_State
{
protected:

	GENERATED_BODY()

public:

	/* Methods. */

	// By default, we call Register_Input when we enter this state. Overwrite it
	// if necessary.
	virtual void OnEnter_Implementation();

	// By default, we call Unregister_Input when we exit the state. Overwrite it
	// if necessary.
	virtual void OnExit_Implementation();

	// Use this function to register input to be used by the state.
	// Can be called anywhere, by default is called by the OnEnter implementation.
	UFUNCTION(BlueprintNativeEvent)
	void Register_Input();
	virtual void Register_Input_Implementation() {}

	// Use this function to register input to be removed by the state.
	// Can be called anywhere, by default is called by the OnExit implementation.
	UFUNCTION(BlueprintNativeEvent)
	void Unregister_Input();
	virtual void Unregister_Input_Implementation() {}
};
