#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_State.h"
#include "Components/InputComponent.h"
#include "PS_Human_Input_Carrying_Unarmed.generated.h"

class APS_Character;
class UPS_Human_Input_CarryingController;

UCLASS(BlueprintType, Blueprintable)
class TPS_API UPS_Human_Input_Carrying_Unarmed : public UFSM_State
{
protected:

	GENERATED_BODY()

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;
	
	UPROPERTY()
	UPS_Human_Input_CarryingController * InputComponent = nullptr;

public:

	/* Methods. */

	void Init_Implementation() override;
	
	void OnEnter_Implementation() override;

	void OnExit_Implementation() override;

	void Tick_Implementation(float DeltaTime) override;

private:

	/* Actions. */
};