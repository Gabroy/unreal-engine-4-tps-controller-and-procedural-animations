#include "PS_Human_Input_Carrying_Gun.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Input/Carrying/PS_Human_Input_CarryingController.h"

void UPS_Human_Input_Carrying_Gun::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
	{
		return;
	}

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_Carrying_Gun::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	InputComponent = Cast<UPS_Human_Input_CarryingController>(GetFSMOwner().GetObject());
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_Carrying_Gun::Init_Implementation InputComponent Returned Null"));
		return;
	}

	TickByDefault = false;
}

void UPS_Human_Input_Carrying_Gun::OnEnter_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	// Bind actions here if you need any.
	DropAction = InputComponent->BindAction("Drop", IE_Pressed, this, &UPS_Human_Input_Carrying_Gun::Drop);
	HolsterAction = InputComponent->BindAction("Holster", IE_Pressed, this, &UPS_Human_Input_Carrying_Gun::Holster);
}

void UPS_Human_Input_Carrying_Gun::OnExit_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	// Unbind actions here if you need use any.
	InputComponent->RemoveActionBindingForHandle(DropAction.GetHandle());
	InputComponent->RemoveActionBindingForHandle(HolsterAction.GetHandle());
}

void UPS_Human_Input_Carrying_Gun::Tick_Implementation(float DeltaTime)
{

}

void UPS_Human_Input_Carrying_Gun::Drop()
{
	if (CharacterOwner)
		CharacterOwner->DropCarryingItem();
}

void UPS_Human_Input_Carrying_Gun::Holster()
{
	if (CharacterOwner)
		CharacterOwner->SetHolsterWeapon(!CharacterOwner->IsDoingHolster());
}