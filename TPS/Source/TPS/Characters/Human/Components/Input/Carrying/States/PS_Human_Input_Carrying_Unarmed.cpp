#include "PS_Human_Input_Carrying_Unarmed.h"
#include "Characters/Human/PS_Character.h"
#include "Characters/Human/Components/Input/Carrying/PS_Human_Input_CarryingController.h"

void UPS_Human_Input_Carrying_Unarmed::Init_Implementation()
{
	// The actor that owns the FSM that owns this state.
	AActor * Actor = GetOwnerActor();
	if (!Actor)
	{
		return;
	}

	CharacterOwner = Cast<APS_Character>(Actor);
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_Carrying_Unarmed::Init_Implementation CharacterOwner Returned Null"));
		return;
	}

	InputComponent = Cast<UPS_Human_Input_CarryingController>(GetFSMOwner().GetObject());
	if (!InputComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_Carrying_Unarmed::Init_Implementation InputComponent Returned Null"));
		return;
	}

	TickByDefault = false;
}

void UPS_Human_Input_Carrying_Unarmed::OnEnter_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	// Bind actions here if you need any.
}

void UPS_Human_Input_Carrying_Unarmed::OnExit_Implementation()
{
	if (!InputComponent)
	{
		return;
	}

	// Unbind actions here if you need use any.
}

void UPS_Human_Input_Carrying_Unarmed::Tick_Implementation(float DeltaTime)
{

}