#include "PS_Human_Input_CarryingController.h"
#include "Characters/Human/PS_Character.h"
#include "Gameplay/Items/PS_BaseItem.h"
#include "Characters/Human/Components/Inventory/PS_Human_InventoryComponent.h"

void UPS_Human_Input_CarryingController::Init()
{
	Owner = Cast<APS_Character>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_CarryingController::Init Owner Returned Null"));
		return;
	}

	OwnerController = Owner->GetController();
	if (!OwnerController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_CarryingController::Init OwnerController Returned Null"));
		return;
	}

	UPS_Human_InventoryComponent* InventoryController = Owner->FindComponentByClass<UPS_Human_InventoryComponent>();
	if (!InventoryController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_CarryingController::Init InventoryController Returned Null"));
		return;
	}
	InventoryController->OnCarryingEquipedItemChanged.AddUObject(this, &UPS_Human_Input_CarryingController::OnCarryingEquipedItemChanged);

	BindBasicInputs();

	Super::Init();
}

void UPS_Human_Input_CarryingController::BindBasicInputs()
{
	UInputComponent * InputCmp = Owner->InputComponent;
	if (!InputCmp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_Input_CarryingController::BindBasicInputs InputComponent Returned Null"));
		return;
	}

	// Interact action.
	InteractAction = InputCmp->BindAction("Action", IE_Pressed, this, &UPS_Human_Input_CarryingController::Interact);
	ChangeFirstItemAction = InputCmp->BindAction("FirstItem", IE_Pressed, this, &UPS_Human_Input_CarryingController::ChangeFirstItem);
	ChangeSecondItemAction = InputCmp->BindAction("SecondItem", IE_Pressed, this, &UPS_Human_Input_CarryingController::ChangeSecondItem);
}

/* Input functions */

void UPS_Human_Input_CarryingController::Interact()
{
	if(Owner)
		Owner->DoInteract();
}

void UPS_Human_Input_CarryingController::ChangeFirstItem()
{
	if (Owner)
		Owner->ChangeOrSaveItem(EInventoryItemSlot::MARKUP_PRIMARY_SLOT);
}

void UPS_Human_Input_CarryingController::ChangeSecondItem()
{
	if (Owner)
		Owner->ChangeOrSaveItem(EInventoryItemSlot::MARKUP_SECONDARY_SLOT);
}

void UPS_Human_Input_CarryingController::SetStateBasedOnStance(ECarryingStance CarryingStance)
{
	switch (CarryingStance)
	{
	case ECarryingStance::Unarmed:
		ChangeToStateByName("Unarmed");
		break;
	case ECarryingStance::OneHandedGun:
		ChangeToStateByName("Gun");
		break;
	case ECarryingStance::TwoHandedGun:
		ChangeToStateByName("Gun");
		break;
	default:
		break;
	}
}

void UPS_Human_Input_CarryingController::OnCarryingEquipedItemChanged(APS_BaseItemActorInstance * NewCarryingInstance)
{
	ECarryingStance CarryingStance = NewCarryingInstance ? NewCarryingInstance->GetItemCarryingStance() : ECarryingStance::Unarmed;
	SetStateBasedOnStance(CarryingStance);
}