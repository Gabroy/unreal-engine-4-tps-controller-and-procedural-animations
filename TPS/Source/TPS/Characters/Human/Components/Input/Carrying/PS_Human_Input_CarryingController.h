#pragma once

#include "CoreMinimal.h"
#include "Base/FSM/FSM_Component.h"
#include "Components/InputComponent.h"
#include "Characters/Common/Components/Carrying/PS_Carrying_Structs_Enums.h"
#include "Characters/Common/Components/Input/PS_Base_InputController.h"
#include "PS_Human_Input_CarryingController.generated.h"

class APS_Character;
class AController;
class APS_BaseItemActorInstance;
class UPS_Human_CarryingController;

/*
* Input Carrying Controller FSM.
* 
* Binds to input devices and controls actions depending of the current object being carried.
* Similar to the Input Controller but only used for carrying objects and not for general behaviour.
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_Input_CarryingController : public UPS_Base_InputController
{

protected:

	GENERATED_BODY()

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Owner;

	UPROPERTY(BlueprintReadOnly)
	AController * OwnerController;

	FInputActionBinding InteractAction;
	FInputActionBinding ChangeFirstItemAction;
	FInputActionBinding ChangeSecondItemAction;

public:

	// Initializes the controller, storing all necessary values.
	virtual void Init() override;

protected:

	/* Init functions. */

	// Bind basic inputs.
	void BindBasicInputs();

	/* General Actions. */

	// Called when we press the interact button.
	void Interact();
	
	void ChangeFirstItem();

	void ChangeSecondItem();

	void SetStateBasedOnStance(ECarryingStance CarryingStance);

	/* Callbacks */

	void OnCarryingEquipedItemChanged(APS_BaseItemActorInstance * NewCarryingInstance);
};
