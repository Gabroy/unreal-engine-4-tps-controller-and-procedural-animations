#include "PS_Human_InputController.h"
#include "../../PS_Character.h"
#include "../../../../Camera/Controllers/PS_Human_CameraController.h"

void UPS_Human_InputController::Init()
{
	Owner = Cast<APS_Character>(GetOwner());
	if (!Owner) return;

	OwnerController = Owner->GetController();
	if (!OwnerController) return;

	// Get sure USkeletalMeshComponent gets updated after human locomotion has computed new transform position
	// for the mesh.
	USkeletalMeshComponent * SKMesh = Owner->FindComponentByClass<USkeletalMeshComponent>();
	if (SKMesh)
		SKMesh->AddTickPrerequisiteComponent(this);

	CameraContoller = Owner->FindComponentByClass<UPS_Human_CameraController>();

	BindBasicInputs();

	Start();
}

void UPS_Human_InputController::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	// Here we update the current locomotion state.
	UFSM_Component::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UPS_Human_InputController::ChangeToFallingState()
{
	ChangeToStateByName("InAir");
}

void UPS_Human_InputController::ChangeToClimbingState()
{
	ChangeToStateByName("Climbing");
}

void UPS_Human_InputController::BindBasicInputs()
{
	UInputComponent * InputCmp = Owner->InputComponent;

	if (!InputCmp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_InputController::BindBasicInputs InputComponent Returned Null"));
		return;
	}

	// Movement.
	SetEnabledMovementBasedOnController(true);

	// Camera rotation.
	LookupActionBind = InputCmp->BindAxis("Lookup", Owner, &APS_Character::AddControllerPitchInput);
	TurnActionBind = InputCmp->BindAxis("Turn", Owner, &APS_Character::AddControllerYawInput);

	// FPS and TPS
	FlipViewModeHandle = Owner->InputComponent->BindAction("FlipViewMode", IE_Pressed, this, &UPS_Human_InputController::FlipViewMode);

	// Upwards action.
	UpwardsAction = InputCmp->BindAction("Upwards", IE_Pressed, this, &UPS_Human_InputController::Upwards);
	ReleasedUpwardsAction = InputCmp->BindAction("Upwards", IE_Released, this, &UPS_Human_InputController::ReleasedUpwards);
	// Backwards action.
	DownwardsAction = InputCmp->BindAction("Backwards", IE_Pressed, this, &UPS_Human_InputController::Downwards);
	ReleasedDownwardsAction = InputCmp->BindAction("Backwards", IE_Released, this, &UPS_Human_InputController::ReleasedDownwards);
	// Rightwards action.
	RightwardsAction = InputCmp->BindAction("Rightwards", IE_Pressed, this, &UPS_Human_InputController::Rightwards);
	ReleasedRightwardsAction = InputCmp->BindAction("Rightwards", IE_Released, this, &UPS_Human_InputController::ReleasedRightwards);
	// Leftwards action.
	LeftwardsAction = InputCmp->BindAction("Leftwards", IE_Pressed, this, &UPS_Human_InputController::Leftwards);
	ReleasedLeftwardsAction = InputCmp->BindAction("Leftwards", IE_Released, this, &UPS_Human_InputController::ReleasedLeftwards);
}

void UPS_Human_InputController::OnMovementComponentStateChanged(EMovementMode PrevMovementMode, EMovementMode NewMovementMode)
{
	switch (NewMovementMode)
	{
	case MOVE_NavWalking:
	case MOVE_Walking:
		ChangeToStateByName("Grounded");
		break;

	case MOVE_Falling:
		ChangeToStateByName("InAir");
		break;
	}
}

// Change the camera view mode.
void UPS_Human_InputController::FlipViewMode()
{
	if (!CameraContoller) return;

	CameraContoller->ToogleViewMode();
}

/* Input functions */

void UPS_Human_InputController::SetEnabledMovementBasedOnController(bool EnableMov)
{
	if (EnableMov)
	{
		MoveForwardActionBind = Owner->InputComponent->BindAxis("MoveForward", Owner, &APS_Character::MoveForward);
		MoveSidewaysActionBind = Owner->InputComponent->BindAxis("MoveSideways", Owner, &APS_Character::MoveSideways);
	}
	else
	{
		RemoveAxis(MoveForwardActionBind);
		RemoveAxis(MoveSidewaysActionBind);
	}
}

void UPS_Human_InputController::RemoveAxis(const FInputAxisBinding& AxisBind)
{
	for (int i = 0; i < Owner->InputComponent->AxisBindings.Num(); ++i)
	{
		FInputAxisBinding& AxisToCheckToRemove = Owner->InputComponent->AxisBindings[i];

		if (AxisToCheckToRemove.AxisName == AxisBind.AxisName) {
			Owner->InputComponent->AxisBindings.RemoveAt(i);
			break;
		}
	}
}

void UPS_Human_InputController::Upwards()
{
	UpwardsKeyPressed = true;
}

void UPS_Human_InputController::ReleasedUpwards()
{
	UpwardsKeyPressed = false;
}

void UPS_Human_InputController::Downwards()
{
	DownwardsKeyPressed = true;
}

void UPS_Human_InputController::ReleasedDownwards()
{
	DownwardsKeyPressed = false;
}

void UPS_Human_InputController::Rightwards()
{
	RightwardsKeyPressed = true;
}

void UPS_Human_InputController::ReleasedRightwards()
{
	RightwardsKeyPressed = false;
}

void UPS_Human_InputController::Leftwards()
{
	LeftwardsKeyPressed = true;
}

void UPS_Human_InputController::ReleasedLeftwards()
{
	LeftwardsKeyPressed = false;
}