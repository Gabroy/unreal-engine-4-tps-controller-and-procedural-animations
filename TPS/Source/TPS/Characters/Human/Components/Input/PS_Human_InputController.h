#pragma once

#include "CoreMinimal.h"
#include "TPS/FSM/FSM_Component.h"
#include "Components/InputComponent.h"
#include "PS_Human_InputController.generated.h"

class APS_Character;
class AController;
class UPS_Human_CameraController;

/*
* Input controller FSM.
* 
* Allows a player to control a human character and execute certain actions based on the current
* state the player is at. Examples of states are: Grounded, Falling, Blocked, ...
* 
* The input controller binds to input devices and allows executing actions given a input state.
* Actions are defined in the APS_Character class and, once called, might trigger changes in other
* components of the actor.
* 
* Changes in the states of the input controller are normally triggered by the PS_Character when an
* event or action is done.
* 
* In the future, this controller will be combined with one to control what the character is currently
* carrying.
* 
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_InputController : public UFSM_Component
{

protected:

	GENERATED_BODY()

	/* References. */

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Owner;

	UPROPERTY(BlueprintReadOnly)
	AController * OwnerController;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_CameraController * CameraContoller;

	// If true, input is deactivated.
	UPROPERTY(BlueprintReadOnly)
	bool BlockInput = false;

	/* General actions. */

	FInputAxisBinding MoveForwardActionBind;
	FInputAxisBinding MoveSidewaysActionBind;
	FInputAxisBinding LookupActionBind;
	FInputAxisBinding TurnActionBind;
	FInputActionBinding FlipViewModeHandle;

	FInputActionBinding UpwardsAction;
	FInputActionBinding ReleasedUpwardsAction;
	FInputActionBinding DownwardsAction;
	FInputActionBinding ReleasedDownwardsAction;
	FInputActionBinding RightwardsAction;
	FInputActionBinding ReleasedRightwardsAction;
	FInputActionBinding LeftwardsAction;
	FInputActionBinding ReleasedLeftwardsAction;

	bool UpwardsKeyPressed = false;
	bool DownwardsKeyPressed = false;
	bool LeftwardsKeyPressed = false;
	bool RightwardsKeyPressed = false;

public:

	// Initializes the controller, storing all necessary values.
	void Init();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	/* Changing state functions */

	void ChangeToFallingState();

	void ChangeToClimbingState();

	// Called by PS Character when the movement component changes states. Push states like falling, flying or
	// swimming will be received here. Use this function to control the transition logic in each case.
	void OnMovementComponentStateChanged(EMovementMode PrevMovementMode, EMovementMode NewMovementMode);

	/* Input functions */

	// Enable movement based on the controller rotator.
	// This is used by default by locomotion modes like grounded or in air.
	void SetEnabledMovementBasedOnController(bool EnableMov);

	void RemoveAxis(const FInputAxisBinding & AxisBind);

	bool GetUpwardsKeyPressed() const { return UpwardsKeyPressed; }
	bool GetDownwardsKeyPressed() const { return DownwardsKeyPressed; }
	bool GetLeftwardsKeyPressed() const { return LeftwardsKeyPressed; }
	bool GetRightwardsKeyPressed() const { return RightwardsKeyPressed; }

protected:

	/* Protected Init functions. */

	// Bind basic inputs.
	void BindBasicInputs();

	/* Protected, player specific functions. */

	// Change the camera view mode.
	void FlipViewMode();

	// Check keys pressed.
	void Upwards();
	void ReleasedUpwards();
	void Downwards();
	void ReleasedDownwards();
	void Rightwards();
	void ReleasedRightwards();
	void Leftwards();
	void ReleasedLeftwards();
};
