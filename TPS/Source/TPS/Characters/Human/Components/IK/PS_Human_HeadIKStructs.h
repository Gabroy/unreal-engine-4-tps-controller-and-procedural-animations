#pragma once

#include "CoreMinimal.h"
#include "PS_Human_HeadIKStructs.generated.h"

/*	Struct with all variables used for the configuration of a Head IK system. */

USTRUCT(Blueprintable, BlueprintType)
struct F_HeadIKConfiguration
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float IK_CurrentBlendInOut = 0.50f;

	// Interpolation speed for head rotation.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float IK_CurrentInterpolationRotationSpeed = 2.0f;

	// IK bone name of head.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	FName HeadBoneName;

	UPROPERTY(BlueprintReadOnly, Category = "HeadIK")
	FRotator CurrentLookAtRotation;

	UPROPERTY(BlueprintReadOnly, Category = "HeadIK")
	FRotator TargetLookAtRotation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float MinYawRotation;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float MaxYawRotation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float MinPitchRotation;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float MaxPitchRotation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float MinRollRotation;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HeadIK")
	float MaxRollRotation;
};