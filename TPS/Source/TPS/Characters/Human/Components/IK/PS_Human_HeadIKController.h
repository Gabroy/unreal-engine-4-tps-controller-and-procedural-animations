#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_Human_HeadIKStructs.h"
#include "PS_Human_HeadIKController.generated.h"

class APS_Character;
class USkeletalMeshComponent;
class UCapsuleComponent;
class UPS_Human_LocomotionController;

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_HeadIKController : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Component variables */

	APS_Character * CharacterOwner = nullptr;
	USkeletalMeshComponent * SkeletalMeshComp = nullptr;
	UCapsuleComponent * CapsuleComponent = nullptr;
	UPS_Human_LocomotionController * LocomotionController = nullptr;

	/* Head IKs Variables */

	UPROPERTY(BlueprintReadOnly)
	bool EnabledHeadIK = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	F_HeadIKConfiguration HeadIKConfiguration;

	typedef void (UPS_Human_HeadIKController::* IKInterpolationFunctionPtr)();
	IKInterpolationFunctionPtr PtrToInterpFunction = nullptr;

	/* Look at variables */

	// Position to look at if we are looking at a position.
	FVector PositionToLookAt;

	// Component to look at if we are looking at a component.
	const USceneComponent * SceneComponentToLookAt = nullptr;

	// Interpolation speed on look at position and look at component functions.
	UPROPERTY(EditAnywhere)
	float LookAt_InterpolationRotationSpeed = 3.0f;

	// Interpolation speed on look at controller.
	UPROPERTY(EditAnywhere)
	float LookAtController_InterpolationRotationSpeed = 3.0f;

	// Interpolation speed on look at random position.
	UPROPERTY(EditAnywhere)
	float RandomLookat_InterpolationRotationSpeed = 1.0f;
	
	/* IK functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// Sets default values for this component's properties
	UPS_Human_HeadIKController();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Make character look at a given position in WS.
	UFUNCTION(BlueprintCallable)
	void LookAtPosition(const FVector & NewPositionToLookAt);

	// Make character look at a given scene component in WS.
	UFUNCTION(BlueprintCallable)
	void LookAtComponent(const USceneComponent * NewSceneCompToLookAt);

	// Make character follow controller rotation so it faces the camera.
	UFUNCTION(BlueprintCallable)
	void LookAtControllerDirection();

	// Makes player head look at a random direction.
	UFUNCTION(BlueprintCallable)
	void LookAtRandomDirection();

	// Deactivate head IK.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKDisabled();

	/* Head  IK Getters */

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetEnabledHeadIK() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FRotator & GetHeadLookAtRotation() const;

private:

	/* General functions */

	// Activate head IK.
	UFUNCTION(BlueprintCallable)
	void SetHeadIKEnabled();

	UFUNCTION(BlueprintCallable)
	void InterpolateHeadTarget(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void UpdateLookAtPosition();

	UFUNCTION(BlueprintCallable)
	void UpdateLookAtComponent();

	UFUNCTION(BlueprintCallable)
	void UpdateLookAtControllerDirection();
};
