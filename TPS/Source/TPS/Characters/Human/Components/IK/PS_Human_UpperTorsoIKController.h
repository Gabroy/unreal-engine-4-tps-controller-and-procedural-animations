#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKStructs.h"
#include "Characters/Human/Components/IK/UpperTorsoIK/PS_Human_UpperTorso_IK_Hanging.h"
#include "Characters/Human/Components/IK/UpperTorsoIK/PS_Human_UpperTorso_IK_Ground_Gun.h"
#include "Characters/Human/Components/IK/UpperTorsoIK/PS_Human_UpperTorso_IK_GroundSurfaceUnarmed.h"
#include "PS_Human_UpperTorsoIKController.generated.h"

class APS_Character;
class USkeletalMeshComponent;
class UCapsuleComponent;
class UPS_Human_LocomotionController;
class UPS_Human_CarryingController;

UCLASS(BlueprintType, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_UpperTorsoIKController : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Component variables */

	UPROPERTY()
	APS_Character * CharacterOwner = nullptr;
	UPROPERTY()
	USkeletalMeshComponent * SkeletalMeshComponent = nullptr;
	UPROPERTY()
	UCapsuleComponent * CapsuleComponent = nullptr;
	UPROPERTY()
	UPS_Human_LocomotionController * LocomotionController = nullptr;
	UPROPERTY()
	UPS_Human_CarryingController * CarryingController = nullptr;

	/* Upper Torso IKs Variables */

	// Each mode implements it's own unique IK upper torso behaviour.
	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<EUpperTorsoIKModes> UpperTorsoIKMode = EUpperTorsoIKModes::UB_IK_DEACTIVATED;

	// Behaviours that computes the position where the IK should end at. Allow setting up frequency of computation.
	float IntervalCheckTime = 0.0f;
	float TimeSinceLastCheckCalled = 0.0f;
	typedef void (UPS_Human_UpperTorsoIKController::* IKComputePositionFunctioPtr)();
	IKComputePositionFunctioPtr PtrToComputeIKPosition = nullptr;

	// Controls interpolation behaviour between current IK position and target position.
	// Different states can implement their own functionality.
	// For example, making interpolation faster or slower based on current speed.
	typedef void (UPS_Human_UpperTorsoIKController::* IKInterpolationFunctionPtr)(float);
	IKInterpolationFunctionPtr PtrToInterpFunction = nullptr;

	// Used for some behaviours, like disabling IK temporarily.
	FTimerHandle UpperBodyTimerHandle;

	/* Hands and Pelvis IK configurations. */

	UPROPERTY(BlueprintReadOnly)
	bool EnabledHandIKs = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	F_HandIKConfiguration HandIKConfiguration;

	UPROPERTY(BlueprintReadOnly)
	bool EnabledPelvisIKs = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
	F_PelvisIKConfiguration PelvisIKConfiguration;

	/* Upper Torso IK Modes */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Modes")
	FHuman_UpperTorsoIK_GroundSurfaceUnarmed GroundedUnarmedIKSettings;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Modes")
	FHuman_UpperTorsoIK_Grounded_Gun GroundedGunIKSettings;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Modes")
	FHuman_UpperTorsoIK_Hanging HangingIKSettings;

	/* IK functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// Sets default values for this component's properties
	UPS_Human_UpperTorsoIKController();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/* Upper Torso IK Modes */

	UFUNCTION(BlueprintCallable)
	void SetUpperTorsoIKToLastUsedMode();
	UFUNCTION(BlueprintCallable)
	void SetUpperTorsoIKEnabled(EUpperTorsoIKModes NewUpperTorsoIKMode);
	// Disables IK, if float is > 0, kill reactivate to last used IK after time runs out.
	UFUNCTION(BlueprintCallable)
	void SetUpperTorsoIKDisabled(float TimeToKeepDisabled = 0.0f);

	/* Getter functions */

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetEnabledUpperBodyIKs() const;

	// Hand IK Getters.
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetEnabledHandsIKs() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FVector & GetRightHandPosition() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FRotator & GetRightHandRotator() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FVector & GetRightHandJoint() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const bool GetRightHandEnabled() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FVector & GetLeftHandPosition() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FRotator & GetLeftHandRotator() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FVector& GetLeftHandJoint() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const bool GetLeftHandEnabled() const;

	// Pelvis IK Getters.
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetEnabledPelvisIKs() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const float GetPelvisYawOffset() const;

private:

	void SetUpperTorsoIKMode(EUpperTorsoIKModes NewHandIKMode);
	
	/* Compute IK functionality */
	void SetComputeIKBehaviour(EUpperTorsoIKModes NewHandIKMode);
	void GroundedUnarmed_IKChecks();
	void GroundedGun_IKChecks();
	void Hanging_IKChecks();

	/* Interpolation */
	void SetInterpolationIKBehaviour(EUpperTorsoIKModes NewHandIKMode);
	void InterpolateHandPositions(float DeltaTime);
	void InterpolatePelvisPositions(float DeltaTime);
	void GroundedUnarmed_InterpolationFunction(float DeltaTime);

	/* Hands */
	void SetHandIKTargetToAnimationLocation(bool IsRightBone);

	friend struct FHuman_UpperTorsoIK_GroundSurfaceUnarmed;
	friend struct FHuman_UpperTorsoIK_Grounded_Gun;
	friend struct FHuman_UpperTorsoIK_Hanging;
};
