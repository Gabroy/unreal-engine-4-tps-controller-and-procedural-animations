#include "PS_Human_LegIKController.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "../Locomotion/PS_Human_LocomotionController.h"
#include "Kismet/KismetSystemLibrary.h"

UPS_Human_LegIKController::UPS_Human_LegIKController() : UActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UPS_Human_LegIKController::BeginPlay()
{
	Super::BeginPlay();

	CharacterOwner = Cast<APS_Character>(GetOwner());
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_IKController::BeginPlay GetOwner Returned Null"));
		return;
	}

	SkeletalMeshComponent = CharacterOwner->FindComponentByClass<USkeletalMeshComponent>();
	if (!SkeletalMeshComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_IKController::BeginPlay USkeletalMeshComponent Returned Null"));
		return;
	}

	CapsuleComponent = CharacterOwner->FindComponentByClass<UCapsuleComponent>();
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_IKController::BeginPlay UCapsuleComponent Returned Null"));
		return;
	}

	// Get sure we update after locomotion controller has calculated this frame's values.
	LocomotionController = CharacterOwner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (LocomotionController)
		this->AddTickPrerequisiteComponent(LocomotionController);

	SetComponentTickEnabled(FeetIKConfiguration.EnableLegsIK);
}

void UPS_Human_LegIKController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Start by fetching the new interpolation speed.
	FeetIKConfiguration.CurrentFeetInterpolationSpeed = FeetIKConfiguration.FeetInterpolationSpeedBasedOnCharacterSpeed->GetFloatValue(LocomotionController->GetSpeed());

	float NewFootHeight;
	FRotator NewFootRotation;

	// Compute left leg IK values.
	FeetIKConfiguration.EnableLeftLegIK = ComputeLegIK(FeetIKConfiguration.LeftFootName, FeetIKConfiguration.DistanceToTraceFromFeetSole,
		FeetIKConfiguration.BoneEffectorHeightOffsetForFeet, NewFootHeight, NewFootRotation);

	// Clamp rotations.
	NewFootRotation.Roll = FMath::Clamp(NewFootRotation.Roll, FeetIKConfiguration.FootRollRotationMinMaxValueInDegrees.X, FeetIKConfiguration.FootRollRotationMinMaxValueInDegrees.Y);
	NewFootRotation.Pitch = FMath::Clamp(NewFootRotation.Pitch, FeetIKConfiguration.FootPitchRotationMinMaxValueInDegrees.X, FeetIKConfiguration.FootPitchRotationMinMaxValueInDegrees.Y);

	// Get new IK value interpolated for the left foot.
	FeetIKConfiguration.IK_LeftFootHeightOffset = FMath::FInterpTo(FeetIKConfiguration.IK_LeftFootHeightOffset, NewFootHeight, DeltaTime, FeetIKConfiguration.CurrentFeetInterpolationSpeed);
	FeetIKConfiguration.IK_LeftFootRotation.Roll = FMath::FInterpTo(FeetIKConfiguration.IK_LeftFootRotation.Roll, NewFootRotation.Roll, DeltaTime, FeetIKConfiguration.InterpolationRotationSpeed);
	FeetIKConfiguration.IK_LeftFootRotation.Pitch = FMath::FInterpTo(FeetIKConfiguration.IK_LeftFootRotation.Pitch, NewFootRotation.Pitch, DeltaTime, FeetIKConfiguration.InterpolationRotationSpeed);

	// Compute right leg IK values.
	FeetIKConfiguration.EnableRightLegIK = ComputeLegIK(FeetIKConfiguration.RightFootName, FeetIKConfiguration.DistanceToTraceFromFeetSole,
		FeetIKConfiguration.BoneEffectorHeightOffsetForFeet, NewFootHeight, NewFootRotation);

	// Clamp rotations.
	NewFootRotation.Roll = FMath::Clamp(NewFootRotation.Roll, FeetIKConfiguration.FootRollRotationMinMaxValueInDegrees.X, FeetIKConfiguration.FootRollRotationMinMaxValueInDegrees.Y);
	NewFootRotation.Pitch = FMath::Clamp(NewFootRotation.Pitch, FeetIKConfiguration.FootPitchRotationMinMaxValueInDegrees.X, FeetIKConfiguration.FootPitchRotationMinMaxValueInDegrees.Y);

	// Get new IK values interpolated for the right foot.
	FeetIKConfiguration.IK_RightFootHeightOffset = FMath::FInterpTo(FeetIKConfiguration.IK_RightFootHeightOffset, NewFootHeight, DeltaTime, FeetIKConfiguration.CurrentFeetInterpolationSpeed);
	FeetIKConfiguration.IK_RightFootRotation.Roll = FMath::FInterpTo(FeetIKConfiguration.IK_RightFootRotation.Roll, NewFootRotation.Roll, DeltaTime, FeetIKConfiguration.InterpolationRotationSpeed);
	FeetIKConfiguration.IK_RightFootRotation.Pitch = FMath::FInterpTo(FeetIKConfiguration.IK_RightFootRotation.Pitch, NewFootRotation.Pitch, DeltaTime, FeetIKConfiguration.InterpolationRotationSpeed);

	FeetIKConfiguration.IK_HipsOffset = FMath::Min(FeetIKConfiguration.IK_RightFootHeightOffset, FeetIKConfiguration.IK_LeftFootHeightOffset);
}

bool UPS_Human_LegIKController::ComputeLegIK(const FName & FootBoneName, float IK_RayDistanceFromFootSole,
	float IK_BoneOffset, float & IK_FootFinalHeight, FRotator & IK_FootFinalRotation)
{
	FVector ActorLocation = CharacterOwner->GetActorLocation();
	FVector GroundLocation = SkeletalMeshComponent->GetComponentLocation();
	FVector FootLocation = SkeletalMeshComponent->GetSocketLocation(FootBoneName);
	float TraceDistanceFromActorCenterOfMass = CapsuleComponent->GetScaledCapsuleHalfHeight() + IK_RayDistanceFromFootSole;

	FVector FeetTraceStartLocation = FVector(FootLocation.X, FootLocation.Y, ActorLocation.Z);
	FVector FeetTraceEndLocation = FVector(FootLocation.X, FootLocation.Y, ActorLocation.Z - TraceDistanceFromActorCenterOfMass);

	// Leave this empty.
	TArray<AActor *> ActorsToIgnore;
	FHitResult FootHitResult;
	UKismetSystemLibrary::LineTraceSingle(this, FeetTraceStartLocation, FeetTraceEndLocation, ETraceTypeQuery::TraceTypeQuery1,
		true, ActorsToIgnore, DebugTrace, FootHitResult, true);

	if (FootHitResult.bBlockingHit)
	{
		FootHitResult.ImpactPoint.Z += IK_BoneOffset;
		IK_FootFinalHeight = FootHitResult.ImpactPoint.Z - GroundLocation.Z;

		float Roll = FMath::RadiansToDegrees(FMath::Atan2(FootHitResult.ImpactNormal.Y, FootHitResult.ImpactNormal.Z));
		float Pitch = FMath::RadiansToDegrees(FMath::Atan2(FootHitResult.ImpactNormal.X, FootHitResult.ImpactNormal.Z));
		IK_FootFinalRotation = FRotator(-Pitch, 0.0f, Roll);
	}
	else
	{
		IK_FootFinalHeight = 0.0f;
		IK_FootFinalRotation = FRotator(0.0f, 0.0f, 0.0f);
	}

	return FootHitResult.bBlockingHit;
}

UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
void UPS_Human_LegIKController::SetLegIKEnabledStatus(bool NewEnableLegIK)
{
	FeetIKConfiguration.EnableLegsIK = NewEnableLegIK;
	FeetIKConfiguration.EnableLeftLegIK = NewEnableLegIK;
	FeetIKConfiguration.EnableRightLegIK = NewEnableLegIK;
	SetComponentTickEnabled(FeetIKConfiguration.EnableLegsIK);
}

/* Getters. */

float UPS_Human_LegIKController::GetRightFootHeightOffset() const
{
	return FeetIKConfiguration.IK_RightFootHeightOffset - FeetIKConfiguration.IK_HipsOffset;
}

const FRotator & UPS_Human_LegIKController::GetRightFootRotation() const
{
	return FeetIKConfiguration.IK_RightFootRotation;
}

float UPS_Human_LegIKController::GetLeftFootHeightOffset() const
{
	return FeetIKConfiguration.IK_LeftFootHeightOffset - FeetIKConfiguration.IK_HipsOffset;
}

const FRotator & UPS_Human_LegIKController::GetLeftFootRotation() const
{
	return FeetIKConfiguration.IK_LeftFootRotation;
}

float UPS_Human_LegIKController::GetHipsOffset() const
{
	return FeetIKConfiguration.IK_HipsOffset;
}

bool UPS_Human_LegIKController::IsLeftLegIKActive() const
{
	return FeetIKConfiguration.EnableLeftLegIK;
}

bool UPS_Human_LegIKController::IsRightLegIKActive() const
{
	return FeetIKConfiguration.EnableRightLegIK;
}

bool UPS_Human_LegIKController::IsLegIKActive() const
{
	return FeetIKConfiguration.EnableLegsIK;
}