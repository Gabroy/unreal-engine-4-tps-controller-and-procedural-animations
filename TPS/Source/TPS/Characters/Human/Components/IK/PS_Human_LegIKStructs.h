#pragma once

#include "CoreMinimal.h"
#include "PS_Human_LegIKStructs.generated.h"

/*	Struct with all variables used for the configuration of a Feet IK system
	for a biped character. It's use can be seen in the PS_Human_LegIKController class. */

USTRUCT(Blueprintable, BlueprintType)
struct F_BipedFeetIKConfiguration
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "LegsIK")
	bool EnableLegsIK = false;

	/* Left foot variables */

	UPROPERTY(BlueprintReadOnly, Category = "LegsIK")
	bool EnableLeftLegIK = false;
	
	// IK bone name of left foot.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	FName LeftFootName;

	float Current_IK_LeftFootHeightOffset = 0.0f;

	// IK new height offset for the left foot. This foot offset
	// needs to be added with the HipOffset in order to get the correct
	// final position for the foot due to the way our IK system is done.
	// This is due to not using IK bones and instead using VBones that are
	// inside the pelvis hierarchy. This allows use to have correct animations
	// for those without IK bone data.
	float IK_LeftFootHeightOffset = 0.0f;

	// IK new rotation for the left foot.
	FRotator IK_LeftFootRotation;

	/* Right foot variables */

	UPROPERTY(BlueprintReadOnly, Category = "LegsIK")
	bool EnableRightLegIK = false;

	// IK bone name of right foot.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	FName RightFootName;

	// IK new height offset for the right foot. This foot offset
	// needs to be added with the HipOffset in order to get the correct
	// final position for the foot due to the way our IK system is done.
	// This is due to not using IK bones and instead using VBones that are
	// inside the pelvis hierarchy. This allows use to have correct animations
	// for those animations without IK bone data.
	float IK_RightFootHeightOffset;

	// IK new rotation for the right foot.
	FRotator IK_RightFootRotation;

	/* Hip variables */

	// Hip offset for the pelvis bone based on difference
	// between left and right feet.
	float IK_HipsOffset = 0.0f;

	/* General feet variables */

	// How many units we mus trace downwards from the feet soles.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	float DistanceToTraceFromFeetSole = 0.0f;

	// Height offset added to the foot bone positions.
	// Used to make the actual effector location be the sole of the foot.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	float BoneEffectorHeightOffsetForFeet = 0.0f;

	// Minimum and maximum rotation a foot can roll in degrees.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	FVector2D FootRollRotationMinMaxValueInDegrees = FVector2D(-30.0f, 30.0f);

	// Minimum and maximum rotation a foot can pitch in degrees.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	FVector2D FootPitchRotationMinMaxValueInDegrees = FVector2D(-30.0f, 30.0f);

	// Curve that controls IK interpolation speed to smooth IK results.
	// Uses the speed of the character to control how fast or slow the IK
	// interpolation is done. Right now, interpolation is faster when idle
	// to avoid clipping issues with feet and slower when running to avoid
	// jittery movement.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	UCurveFloat * FeetInterpolationSpeedBasedOnCharacterSpeed;

	// Speed at which the feet interpolation is done.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "LegsIK")
	float InterpolationRotationSpeed = 15.0f;

	// Current interpolation speed based on the value fetched from the
	// interpolation speed curve above.
	float CurrentFeetInterpolationSpeed = 0.0f;
};