#include "PS_Human_UpperTorso_IK_Ground_Gun.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Gameplay/Items/Weapons/PS_WeaponItemDefinition.h"
#include "Gameplay/Items/Weapons/PS_WeaponItem.h"

/* IKs for Grounded Gun Human character */

void FHuman_UpperTorsoIK_Grounded_Gun::Init(UPS_Human_UpperTorsoIKController * TorsoController)
{
}

void FHuman_UpperTorsoIK_Grounded_Gun::Start(UPS_Human_UpperTorsoIKController * TorsoController)
{
	UPS_Human_CarryingController * CarryingController = TorsoController ? TorsoController->CarryingController : nullptr;
	if (!CarryingController)
		return;

	APS_BaseItemActorInstance * CarryingItem = CarryingController->GetCarryingItem();
	if (!CarryingItem)
		return;

	APS_WeaponActorInstance * CastedWeapon = Cast<APS_WeaponActorInstance>(CarryingItem);
	if (!CastedWeapon)
		return;

	CarryingWeapon = TWeakObjectPtr<APS_WeaponActorInstance>(CastedWeapon);
	if (!CarryingWeapon.IsValid())
		return;

	TorsoController->EnabledHandIKs = true;
	TorsoController->EnabledPelvisIKs = false;

	F_HandIKConfiguration & HandIKConfiguration = TorsoController->HandIKConfiguration;
	HandIKConfiguration.IK_CurrentBlendInOut = HandsBlendInOut;
	HandIKConfiguration.IK_CurrentInterpolationSpeed = IK_CurrentInterpolationSpeed;
	HandIKConfiguration.IK_CurrentInterpolationRotationSpeed = IK_CurrentInterpolationRotationSpeed;
	HandIKConfiguration.RightHand_IK = false; // Set right hand disabled by default.
	HandIKConfiguration.RightHandJoint = RightHandJoint;
	HandIKConfiguration.LeftHandJoint = LeftHandJoint;
}

void FHuman_UpperTorsoIK_Grounded_Gun::ComputeHandsPositions(UPS_Human_UpperTorsoIKController * TorsoController)
{
	USkeletalMeshComponent * WeaponSkeleton = CarryingWeapon->GetSkeletalComponent();
	if (!WeaponSkeleton)
	{
		TorsoController->HandIKConfiguration.LeftHand_IK = false;
		return;
	}

	UPS_WeaponBaseDefinitionAsset * WeaponDefinition = CarryingWeapon->GetWeaponDefinition();
	if (!WeaponDefinition)
	{
		TorsoController->HandIKConfiguration.LeftHand_IK = false;
		return;
	}

	// Set left hand to grip position.
	F_HandIKConfiguration& HandIKConfiguration = TorsoController->HandIKConfiguration;
	const FName& SecondHandPlacementSocketName = WeaponDefinition->GetSecondHandSocketPlacement();
	FTransform SecondHandPlacementSocketTransform = WeaponSkeleton->GetSocketTransform(SecondHandPlacementSocketName);

	AActor * OwnerActor = TorsoController->GetOwner();
	FRotator ActorRotation = OwnerActor->GetActorRotation();
	FRotator TargetRotation = (SecondHandPlacementSocketTransform.GetLocation() - OwnerActor->GetActorLocation()).Rotation();
	FRotator DeltaToTargetRotation = UKismetMathLibrary::NormalizedDeltaRotator(ActorRotation, TargetRotation);

	// Hand can rotate more than a maximum amount of degrees, this is to prevent weird rotation issues like having
	// the hand pass across the boddy, you normally want some value around 75� or so.
	if (FMath::Abs(DeltaToTargetRotation.Yaw) > MaxRotationAngleForHand)
	{
		TorsoController->HandIKConfiguration.LeftHand_IK = false;
		return;
	}

	HandIKConfiguration.LeftHand_IK = true;
	HandIKConfiguration.LeftHandTargetPosition = SecondHandPlacementSocketTransform.GetLocation();
	HandIKConfiguration.LeftHandTargetRotation = SecondHandPlacementSocketTransform.GetRotation().Rotator();
}