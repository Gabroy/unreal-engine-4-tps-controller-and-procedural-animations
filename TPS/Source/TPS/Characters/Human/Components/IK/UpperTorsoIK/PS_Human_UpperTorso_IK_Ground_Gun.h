#pragma once

#include "PS_Human_UpperTorso_IK_Ground_Gun.generated.h"

class UPS_Human_UpperTorsoIKController;
class APS_WeaponActorInstance;

/* IKs for Grounded Gun Carrying Human character */

/* Provide control for hand placement on gun. */

USTRUCT(Blueprintable)
struct FHuman_UpperTorsoIK_Grounded_Gun
{
	GENERATED_BODY()

protected:

	/* Grounded Gun*/

	/* --------------- Hands ---------------  */

	// Updates of weapon hand position.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float IKComputeCheckFrequency = 1.0f / 60.0f;

	// Time to blend out or blend in hands while in this mode.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float HandsBlendInOut = 0.50f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float IK_CurrentInterpolationSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float IK_CurrentInterpolationRotationSpeed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float MaxRotationAngleForHand = 60.0f;

	// Right hand joint position.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	FVector RightHandJoint;

	// Left hand joint position.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	FVector LeftHandJoint;

	// Weapon being carried.
	TWeakObjectPtr<APS_WeaponActorInstance> CarryingWeapon;

	/* Trace type queries */
	ETraceTypeQuery GroundedSurfaceStaticQuery;
	ETraceTypeQuery GroundedSurfaceDynamicQuery;

public:

	void Init(UPS_Human_UpperTorsoIKController * TorsoController);

	void Start(UPS_Human_UpperTorsoIKController * NewTorsoController);

	void ComputeHandsPositions(UPS_Human_UpperTorsoIKController * TorsoController);

	float GetIKComputeCheckFrequency() const { return IKComputeCheckFrequency; }
};