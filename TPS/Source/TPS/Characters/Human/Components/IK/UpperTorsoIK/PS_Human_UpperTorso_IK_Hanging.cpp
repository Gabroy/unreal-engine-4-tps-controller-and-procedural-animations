#include "PS_Human_UpperTorso_IK_Hanging.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKController.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"

void FHuman_UpperTorsoIK_Hanging::Init(UPS_Human_UpperTorsoIKController * TorsoController)
{
	ClimbingTraceQuery = UEngineTypes::ConvertToTraceType(ClimbingChannel);
}

void FHuman_UpperTorsoIK_Hanging::Start(UPS_Human_UpperTorsoIKController * TorsoController)
{
	if (!TorsoController)
	{
		return;
	}

	TorsoController->EnabledHandIKs = true;
	TorsoController->EnabledPelvisIKs = false;

	F_HandIKConfiguration & HandIKConfiguration = TorsoController->HandIKConfiguration;
	HandIKConfiguration.IK_CurrentBlendInOut = HandsBlendInOut;
	HandIKConfiguration.IK_CurrentInterpolationSpeed = HandsInterpolationSpeed;
	HandIKConfiguration.RightHandJoint = RightHandJoint;
	HandIKConfiguration.LeftHandJoint = LeftHandJoint;
}

void FHuman_UpperTorsoIK_Hanging::ComputeClimbingHandPositions(UPS_Human_UpperTorsoIKController * TorsoController)
{
	if (!TorsoController)
	{
		return;
	}

	UCapsuleComponent * CapsuleComponent = TorsoController->CapsuleComponent;
	USkeletalMeshComponent * SkeletalMeshComponent = TorsoController->SkeletalMeshComponent;
	if (!CapsuleComponent || !SkeletalMeshComponent)
	{
		return;
	}

	FVector NewPosition;
	FRotator NewRotation = FRotator(EForceInit::ForceInitToZero);

	F_HandIKConfiguration & HandIKConfiguration = TorsoController->HandIKConfiguration;

	// Right Hand Final Position.
	HandIKConfiguration.RightHand_IK = FindPositionHandInSurface(HandIKConfiguration, CapsuleComponent, SkeletalMeshComponent,
		true, NewPosition, NewRotation);
	if (HandIKConfiguration.RightHand_IK)
	{
		HandIKConfiguration.RightHandTargetPosition = NewPosition;
		HandIKConfiguration.RightHandTargetRotation = NewRotation;
	}
	else
	{
		TorsoController->SetHandIKTargetToAnimationLocation(true);
	}

	// Left Hand Final Position.
	HandIKConfiguration.LeftHand_IK = FindPositionHandInSurface(HandIKConfiguration, CapsuleComponent, SkeletalMeshComponent,
		false, NewPosition, NewRotation);
	if (HandIKConfiguration.LeftHand_IK)
	{
		HandIKConfiguration.LeftHandTargetPosition = NewPosition;
		HandIKConfiguration.LeftHandTargetRotation = NewRotation;
	}
	else
	{
		TorsoController->SetHandIKTargetToAnimationLocation(false);
	}

	// Compute this hanging factor. Positive values (right hand is above), negative values, left hand is above.
	if (HandIKConfiguration.RightHand_IK && HandIKConfiguration.LeftHand_IK)
	{
		HandsHeightDifference = HandIKConfiguration.RightHandTargetPosition.Z - HandIKConfiguration.LeftHandTargetPosition.Z;
		HandsHeightDifference = HandsHeightDifference * HandsHeightDifferenceMultiplicationFactor;
	}
	else
	{
		HandsHeightDifference = 0.0f;
	}
}

bool FHuman_UpperTorsoIK_Hanging::FindPositionHandInSurface(const F_HandIKConfiguration & HandIKConfiguration,
	UCapsuleComponent * CapsuleComponent, USkeletalMeshComponent * SkeletalMeshComponent,
	bool IsRightBone, FVector & NewPosition, FRotator & NewRotation)
{
	FName HandBone = IsRightBone ? HandIKConfiguration.RightHandName : HandIKConfiguration.LeftHandName;
	FVector HandLocation = SkeletalMeshComponent->GetBoneLocation(HandBone);
	FVector HandTestCheckLocation = HandLocation + CapsuleComponent->GetForwardVector() * ForwardChecksOffset
		+ CapsuleComponent->GetRightVector() * (IsRightBone ? -SideOffsetFromBone : SideOffsetFromBone);

	FVector SignVector = CapsuleComponent->GetRightVector() * (IsRightBone ? -1.0f : 1.0f); // Want to perform checks inward.
	const float DistanceBetweenChecks = HandCheckSize * 2.0f;

	// Perform a series of side check to see if we can place hand in the given position.
	for (int i = 0; i < NumberSideHandChecks; ++i)
	{
		FVector HandTestCheckPosition = HandTestCheckLocation + SignVector * i * DistanceBetweenChecks;

		// Check we can place hand in surface.
		FHitResult FoundResult;
		if (PerformHandPlacementCheck(CapsuleComponent, IsRightBone, HandTestCheckPosition, FoundResult))
		{
			// Get degrees of surface. If flat, 0 degrees is returned.
			float DegreesToRotate = ComputeSurfaceHeightStep(FoundResult.ImpactNormal);

			/* Perform last check on hit surface to find where the hand bone should end at. */

			// Start position is computed from VB hand location with some extra offsets to help correctly place the hand at the correct location.
			// This is due to VB not being exactly in the hands position.
			FVector StartCheckPosition = HandLocation - CapsuleComponent->GetForwardVector() * (HandCheckSize * 4.0f);
			StartCheckPosition += CapsuleComponent->GetRightVector() * (IsRightBone ? -SideOffsetFromBone : SideOffsetFromBone);
			StartCheckPosition.Z = FoundResult.ImpactPoint.Z + HeightOffsetFromBone;

			// End position is simply start position plus a forward distance added.
			FVector EndCheckPosition = StartCheckPosition + CapsuleComponent->GetForwardVector() * HandCheckSize * 8.0f;

			// Now, check if there is a surface infront of the hand. If so, sent the position we must end at.
			TArray<AActor*> ActorsToIgnore;
			bool OnSurfaceHitFound = UKismetSystemLibrary::LineTraceSingle(CapsuleComponent, StartCheckPosition, EndCheckPosition,
				ClimbingTraceQuery, false, ActorsToIgnore, EDrawDebugTrace::None, FoundResult, true, FLinearColor::Blue);

			// Add depth offset to avoid putting hand clipping into geometry.
			NewPosition = FoundResult.ImpactPoint + FoundResult.ImpactNormal * DepthOffsetFromHit;

			if (IsRightBone)
				NewRotation = FRotationMatrix::MakeFromXY(-CapsuleComponent->GetUpVector(), -FoundResult.ImpactNormal).Rotator();
			else
				NewRotation = FRotationMatrix::MakeFromXY(CapsuleComponent->GetUpVector(), FoundResult.ImpactNormal).Rotator();

			// Compute final rotation. We substract because sign is the opposite.
			NewRotation.Pitch -= DegreesToRotate;

			return OnSurfaceHitFound && !FoundResult.bStartPenetrating;
		}
	}

	// If we didn't find any hit, we simply get the position of the normal hand.
	NewPosition = HandLocation;

	return false;
}

bool FHuman_UpperTorsoIK_Hanging::PerformHandPlacementCheck(UCapsuleComponent * CapsuleComponent, bool IsRightBone,
	const FVector & HandPositionToCheck, FHitResult & FoundResult)
{
	TArray<AActor*> ActorsToIgnore;

	// Compute extra height for checks if we are moving in a step surface.
	float TopExtraHeight = MaxHeightToPutHandAt;
	float BottomExtraHeight = MaxHeightToPutHandAt * 2.0f;
	if (HandsHeightDifference > 0.0f)
	{
		if (IsRightBone)
			TopExtraHeight += HandsHeightDifference;
		else
			BottomExtraHeight += HandsHeightDifference;
	}
	else
	{
		if (IsRightBone)
			BottomExtraHeight += HandsHeightDifference;
		else
			TopExtraHeight += HandsHeightDifference;
	}

	// Now, calculate start and end check position and perform test.
	FVector StartCheckPosition = HandPositionToCheck + CapsuleComponent->GetUpVector() * TopExtraHeight;
	FVector EndCheckPosition = HandPositionToCheck - CapsuleComponent->GetUpVector() * BottomExtraHeight;
	bool VerticalHitFound = UKismetSystemLibrary::SphereTraceSingle(CapsuleComponent, StartCheckPosition, EndCheckPosition,
		HandCheckSize, ClimbingTraceQuery, false, ActorsToIgnore, EDrawDebugTrace::None, FoundResult, true, FLinearColor::Blue);

	return VerticalHitFound && !FoundResult.bStartPenetrating;
}

float FHuman_UpperTorsoIK_Hanging::ComputeSurfaceHeightStep(const FVector & SurfaceNormal)
{
	return FMath::FindDeltaAngleDegrees(SurfaceNormal.Rotation().Pitch, FVector::UpVector.Rotation().Pitch);
}