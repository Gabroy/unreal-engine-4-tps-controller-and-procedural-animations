#include "PS_Human_UpperTorso_IK_GroundSurfaceUnarmed.h"
#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKController.h"
#include "Engine/EngineTypes.h"
#include "Characters/Human/PS_Character.h"
#include "Components/CapsuleComponent.h"
#include "Base/Collision/PS_CollisionTypes.h"

/* IKs for Grounded Unarmed Human character */

void FHuman_UpperTorsoIK_GroundSurfaceUnarmed::Init(UPS_Human_UpperTorsoIKController * TorsoController)
{
	GroundedSurfaceStaticQuery = UEngineTypes::ConvertToTraceType(PS_ECC_StaticCollisions);
	GroundedSurfaceDynamicQuery = UEngineTypes::ConvertToTraceType(PS_ECC_DynamicCollisions);
}

void FHuman_UpperTorsoIK_GroundSurfaceUnarmed::Start(UPS_Human_UpperTorsoIKController * TorsoController)
{
	if (!TorsoController)
	{
		return;
	}

	TorsoController->EnabledHandIKs = true;
	TorsoController->EnabledPelvisIKs = true;

	F_HandIKConfiguration & HandIKConfiguration = TorsoController->HandIKConfiguration;
	HandIKConfiguration.IK_CurrentBlendInOut = HandsBlendInOut;
	HandIKConfiguration.RightHandJoint = RightHandJoint;
	HandIKConfiguration.LeftHandJoint = LeftHandJoint;
}

void FHuman_UpperTorsoIK_GroundSurfaceUnarmed::InterpolationFunction(UPS_Human_UpperTorsoIKController * TorsoController, float DeltaTime)
{
	if (!TorsoController)
	{
		return;
	}

	APS_Character * CharacterOwner = TorsoController->CharacterOwner;
	if (!CharacterOwner)
	{
		return;
	}

	// Set interpolation speed based on character speed.
	if (PelvisInterpolationSpeedWithSpeed)
	{
		TorsoController->PelvisIKConfiguration.IK_CurrentInterpolationRotationSpeed = PelvisInterpolationSpeedWithSpeed->GetFloatValue(CharacterOwner->GetSpeed());
	}

	if (HandsInterpolationSpeedWithSpeed)
	{
		TorsoController->HandIKConfiguration.IK_CurrentInterpolationSpeed = HandsInterpolationSpeedWithSpeed->GetFloatValue(CharacterOwner->GetSpeed());
	}


	// Enable, disable IKs based on character speed.
	float CharacterSpeed = CharacterOwner->GetSpeed();
	TorsoController->EnabledHandIKs = HandsMaxSpeedStopHandIK >= CharacterSpeed && CharacterSpeed > 0.0f;
	TorsoController->EnabledPelvisIKs = CharacterSpeed > 0.0f; // Allow Pelvis IK only if moving.
}

void FHuman_UpperTorsoIK_GroundSurfaceUnarmed::ComputeHandsAndPelvisIK(UPS_Human_UpperTorsoIKController * TorsoController)
{
	if (!TorsoController)
	{
		return;
	}

	UCapsuleComponent * CapsuleComponent = TorsoController->CapsuleComponent;
	if (!CapsuleComponent)
	{
		return;
	}

	F_HandIKConfiguration & HandIKConfiguration = TorsoController->HandIKConfiguration;

	// Perform side checks for both sides of the character to spot obstacles.
	FHitResult RightHitInformation;
	bool RightHitFound = PerformSideObstacleCheck(CapsuleComponent, true, RightHitInformation);
	FHitResult LeftHitInformation;
	bool LeftHitFound = PerformSideObstacleCheck(CapsuleComponent, false, LeftHitInformation);

	// If no hit, disable both hands IKs and pelvis and let the IK target position be the one of the animation.
	bool NoHitFound = !(RightHitFound || LeftHitFound);
	if (NoHitFound)
	{
		TorsoController->PelvisIKConfiguration.TargetPelvisYaw = 0.0f;

		// Disable IKs for both hands.
		HandIKConfiguration.RightHand_IK = false;
		TorsoController->SetHandIKTargetToAnimationLocation(true);
		HandIKConfiguration.LeftHand_IK = false;
		TorsoController->SetHandIKTargetToAnimationLocation(false);

		return;
	}

	/* Pelvis and hands computation */

	ComputeHandPositionsAndPelvisRotation(TorsoController, CapsuleComponent, RightHitInformation, LeftHitInformation);
}

void FHuman_UpperTorsoIK_GroundSurfaceUnarmed::ComputeHandPositionsAndPelvisRotation(UPS_Human_UpperTorsoIKController * TorsoController,
	UCapsuleComponent * CapsuleComponent, const FHitResult & RightHitInformation, const FHitResult & LeftHitInformation)
{
	/* Compute Pelvis Target Rotation */

	// Get the closest hit from both sides.
	float ClosestDistance = FMath::Min(RightHitInformation.Distance, LeftHitInformation.Distance);
	bool RightHandClosest = FMath::IsNearlyEqual(ClosestDistance, RightHitInformation.Distance);
	float YawRotationSign = RightHandClosest ? 1.0f : -1.0f;

	// Calculate the target rotation for the pelvis.
	F_PelvisIKConfiguration & PelvisIKConfiguration = TorsoController->PelvisIKConfiguration;
	PelvisIKConfiguration.TargetPelvisYaw = YawRotationSign * (PelvisRotationByDistance ? PelvisRotationByDistance->GetFloatValue(ClosestDistance) : 0.0f);

	/* Calculate Hands Final Positions */

	F_HandIKConfiguration & HandIKConfiguration = TorsoController->HandIKConfiguration;
	const float FinalBonePositionDepth = 5.0f;

	// Right hand check.
	bool RightHandCloseEnoughToPlace = RightHitInformation.Distance < HandsMaxDistanceToPlace;
	if (RightHandCloseEnoughToPlace)
	{
		HandIKConfiguration.RightHand_IK = true;
		HandIKConfiguration.RightHandTargetPosition = RightHitInformation.ImpactPoint + RightHitInformation.ImpactNormal * FinalBonePositionDepth;
		HandIKConfiguration.RightHandTargetRotation = FRotationMatrix::MakeFromXY(
			-CapsuleComponent->GetForwardVector(), -RightHitInformation.ImpactNormal).Rotator();
	}
	else
	{
		HandIKConfiguration.RightHand_IK = false;
		TorsoController->SetHandIKTargetToAnimationLocation(true);
	}

	/* Check Left Hand placement. */
	bool LeftHandCloseEnoughToPlace = LeftHitInformation.Distance < HandsMaxDistanceToPlace;
	if (LeftHandCloseEnoughToPlace)
	{
		HandIKConfiguration.LeftHand_IK = true;
		HandIKConfiguration.LeftHandTargetPosition = LeftHitInformation.ImpactPoint + LeftHitInformation.ImpactNormal * FinalBonePositionDepth;
		HandIKConfiguration.LeftHandTargetRotation = FRotationMatrix::MakeFromYZ(
			LeftHitInformation.ImpactNormal, CapsuleComponent->GetUpVector()).Rotator();
	}
	else
	{
		HandIKConfiguration.LeftHand_IK = false;
		TorsoController->SetHandIKTargetToAnimationLocation(false);
	}
}

bool FHuman_UpperTorsoIK_GroundSurfaceUnarmed::PerformSideObstacleCheck(UCapsuleComponent * CapsuleComponent, bool RightSide, FHitResult & HitInformation)
{
	FVector StartCheckPosition = CapsuleComponent->GetComponentLocation()
		+ CapsuleComponent->GetForwardVector() * StartCheckOffset.Y
		+ CapsuleComponent->GetRightVector() * (RightSide ? StartCheckOffset.X : -StartCheckOffset.X)
		+ CapsuleComponent->GetUpVector() * StartCheckOffset.Z;
	FVector EndCheckPosition = StartCheckPosition + CapsuleComponent->GetRightVector() * (RightSide ? IKCheckLength : -IKCheckLength);

	// Perform test against static geometry.
	if (PerformObstacleTrace(CapsuleComponent, StartCheckPosition, EndCheckPosition, GroundedSurfaceStaticQuery, HitInformation))
	{
		return true;
	}

	// If we have not found any static obstacles, we perform a second test against dynamic geometry.
	// For dynamic geometry, we won't move the character hands but will rotate the pelvis.
	if (!PerformObstacleTrace(CapsuleComponent, StartCheckPosition, EndCheckPosition, GroundedSurfaceDynamicQuery, HitInformation))
	{
		HitInformation.Distance = TNumericLimits<float>::Max();
		return false;
	}

	return true;
}

bool FHuman_UpperTorsoIK_GroundSurfaceUnarmed::PerformObstacleTrace(UCapsuleComponent * CapsuleComponent,
	const FVector & StartCheckPosition, const FVector & EndCheckPosition,
	ETraceTypeQuery TraceQueryType, FHitResult & HitInformation)
{
	TArray<AActor*> ActorsToIgnore;
	bool HitFound = UKismetSystemLibrary::SphereTraceSingle(CapsuleComponent, StartCheckPosition, EndCheckPosition, IKCheckSize,
		TraceQueryType, false, ActorsToIgnore, EDrawDebugTrace::ForOneFrame, HitInformation, true, FLinearColor::Blue);

	// Make 2D Projection of hit if found.
	if (HitFound)
	{
		FVector StartCheckPosition2DProjection = StartCheckPosition * CapsuleComponent->GetForwardVector()
			+ StartCheckPosition * CapsuleComponent->GetRightVector();
		FVector Hit2DProjection = HitInformation.ImpactPoint * CapsuleComponent->GetForwardVector()
			+ HitInformation.ImpactPoint * CapsuleComponent->GetRightVector();
		HitInformation.Distance = FVector::Distance(StartCheckPosition2DProjection, Hit2DProjection);
	}

	return HitFound;
}