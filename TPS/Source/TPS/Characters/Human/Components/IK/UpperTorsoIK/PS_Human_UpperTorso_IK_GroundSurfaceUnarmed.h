#pragma once

#include "PS_Human_UpperTorso_IK_GroundSurfaceUnarmed.generated.h"

class UPS_Human_UpperTorsoIKController;
class UCapsuleComponent;

/* IKs for Grounded Unarmed Human character */

/* Provide control for hand placement on surfaces and pelvis rotation when approaching one. */

USTRUCT(Blueprintable)
struct FHuman_UpperTorsoIK_GroundSurfaceUnarmed
{
	GENERATED_BODY()

protected:

	/* Grounded Surface Hand */

	// Frequency to perform IK compue checks in seconds while in this mode.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Unarmed-SurfacePlacement")
	float IKComputeCheckFrequency = 1.0f/30.0f;

	// Controls position of start check offset in all three cords.
	// Normally should be around the capsule radius of the char approx.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Unarmed-SurfacePlacement")
	FVector StartCheckOffset;

	// Radius of check sphere used to detect when to start applying IK effects.
	// Normally you want an area big enough to cover any place where we can place the hand.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Unarmed-SurfacePlacement")
	float IKCheckSize = 20.0f;

	// Length of the check radius.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Unarmed-SurfacePlacement")
	float IKCheckLength = 30.0f;

	/* --------------- Pelvis ---------------  */

	// Controls pelvis yaw rotation based on distance from player check start. Origin of player
	// capsule if StartCheckOffset is zero.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-SurfacePlacement")
	UCurveFloat * PelvisRotationByDistance = nullptr;

	// Controls interpolation speed of pelvis based on player current speed. Allows the character
	// to rotate faster or slower based on the current speed and the values present in the curve.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-SurfacePlacement")
	UCurveFloat * PelvisInterpolationSpeedWithSpeed = nullptr;

	/* --------------- Hands ---------------  */

	// Time to blend out or blend in hands while in this mode.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float HandsBlendInOut = 0.50f;

	// Max distance from start check position of hand check to surface hit we will place hands at.
	// Normally, should be same value as IKCheckLength, unless you need the IK check to be longer
	// than the distance hands can be placed at.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float HandsMaxDistanceToPlace = 30.0f;

	// Hands interpolation speed based on player current speed. Identical to PelvisInterpolationSpeedWithSpeed.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	UCurveFloat * HandsInterpolationSpeedWithSpeed = nullptr;

	// Maximum speed at which we will stop computing hands IK.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	float HandsMaxSpeedStopHandIK = 175.0f;

	// Right hand joint position.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	FVector RightHandJoint;

	// Left hand joint position.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grounded-Hands-Placement")
	FVector LeftHandJoint;

	/* Trace type queries */
	ETraceTypeQuery GroundedSurfaceStaticQuery;
	ETraceTypeQuery GroundedSurfaceDynamicQuery;

public:

	void Init(UPS_Human_UpperTorsoIKController * TorsoController);

	void Start(UPS_Human_UpperTorsoIKController * NewTorsoController);

	void InterpolationFunction(UPS_Human_UpperTorsoIKController * TorsoController, float DeltaTime);

	void ComputeHandsAndPelvisIK(UPS_Human_UpperTorsoIKController * TorsoController);

	float GetIKComputeCheckFrequency() const { return IKComputeCheckFrequency;	}

private:

	bool PerformSideObstacleCheck(UCapsuleComponent * CapsuleComponent, bool RightSide, FHitResult & HitInformation);
	
	bool PerformObstacleTrace(UCapsuleComponent * CapsuleComponent,	const FVector & StartCheckPosition, const FVector & EndCheckPosition,
		ETraceTypeQuery TraceQueryType, FHitResult & HitInformation);

	void ComputeHandPositionsAndPelvisRotation(UPS_Human_UpperTorsoIKController * TorsoController, UCapsuleComponent* CapsuleComponent,
		const FHitResult & RightHitInformation, const FHitResult & LeftHitInformation);
};