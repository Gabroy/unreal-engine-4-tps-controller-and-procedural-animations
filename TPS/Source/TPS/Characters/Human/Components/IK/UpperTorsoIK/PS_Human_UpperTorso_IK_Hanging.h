#pragma once

#include "Characters/Human/Components/IK/PS_Human_UpperTorsoIKStructs.h"
#include "PS_Human_UpperTorso_IK_Hanging.generated.h"

class UPS_Human_UpperTorsoIKController;
class UCapsuleComponent;
class USkeletalMeshComponent;

/* IKs for Hanging Human character */

/* Controls IK while in climbing mode.
Responsible for finding, if possible, a suitable position for hand placement when moving on surfaces. */

USTRUCT(Blueprintable)
struct FHuman_UpperTorsoIK_Hanging
{
	GENERATED_BODY()

protected:

	/* Hanging variables. */

	// Frequency to perform Hanging IK Checks in seconds.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float IKComputeCheckFrequency = 1.0f/30.0f;

	// Time to blend out or blend in hands while in this mode.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hanging")
	float HandsBlendInOut = 0.50f;

	// Number of side checks we will perform to see if we can grab to a surface.
	// Checks start at the hand position and move closer to the center of the player capsule.
	// Allow adapting the hand to not remain on no surface when hanging.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	int NumberSideHandChecks = 3;

	// Maximum height difference from current hand height we can climb.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float MaxHeightToPutHandAt = 10.0f;

	// Distance from hands to perform check.
	// Pushes the check a bit forward to get sure it hits the surface we are hanging at.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float ForwardChecksOffset = 5.0f;

	// Radius of sphere hand checks. Should be around the size of the character hands.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float HandCheckSize = 5.0f;

	// Offsets added to hand bone location.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float HeightOffsetFromBone = -5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float SideOffsetFromBone = 5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float DepthOffsetFromHit = 8.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float HandsInterpolationSpeed = 10.0f;

	// Right hand joint position.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	FVector RightHandJoint;
	
	// Left hand joint position.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	FVector LeftHandJoint;

	// Factor used to increase or decrease the increase of checks of height checks with stepper surfaces.
	// Factor of one will increase check height by one which each degree.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	float HandsHeightDifferenceMultiplicationFactor = 1.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hanging")
	TEnumAsByte<ECollisionChannel> ClimbingChannel = ECollisionChannel::ECC_GameTraceChannel2;

	float HandsHeightDifference = 0.0f;

	/* Trace type queries */

	ETraceTypeQuery ClimbingTraceQuery;

public:

	void Init(UPS_Human_UpperTorsoIKController * TorsoController);

	void Start(UPS_Human_UpperTorsoIKController * TorsoController);

	void ComputeClimbingHandPositions(UPS_Human_UpperTorsoIKController * TorsoController);

	float GetIKComputeCheckFrequency() const { return IKComputeCheckFrequency; }

private:

	bool FindPositionHandInSurface(const F_HandIKConfiguration & HandIKConfiguration,
		UCapsuleComponent * CapsuleComponent, USkeletalMeshComponent * SkeletalMeshComponent,
		bool IsRightBone, FVector & NewPosition, FRotator & NewRotation);

	bool PerformHandPlacementCheck(UCapsuleComponent* CapsuleComponent, bool IsRightBone,
		const FVector & HandPositionToCheck, FHitResult & FoundResult);

	float ComputeSurfaceHeightStep(const FVector & SurfaceNormal);
};