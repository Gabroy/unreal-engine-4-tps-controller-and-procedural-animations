#include "PS_Human_UpperTorsoIKController.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "../Locomotion/PS_Human_LocomotionController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_Human_UpperTorsoIKController.h"

UPS_Human_UpperTorsoIKController::UPS_Human_UpperTorsoIKController() : UActorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UPS_Human_UpperTorsoIKController::BeginPlay()
{
	Super::BeginPlay();

	/* Fetch Components */

	CharacterOwner = Cast<APS_Character>(GetOwner());
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_UpperTorsoIKController::BeginPlay GetOwner Returned Null"));
		return;
	}

	SkeletalMeshComponent = CharacterOwner->FindComponentByClass<USkeletalMeshComponent>();
	if (!SkeletalMeshComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_UpperTorsoIKController::BeginPlay USkeletalMeshComponent Returned Null"));
		return;
	}

	CapsuleComponent = CharacterOwner->FindComponentByClass<UCapsuleComponent>();
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_UpperTorsoIKController::BeginPlay UCapsuleComponent Returned Null"));
		return;
	}

	// Get sure we update after locomotion controller has calculated this frame's values.
	LocomotionController = CharacterOwner->FindComponentByClass<UPS_Human_LocomotionController>();
	if(!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_UpperTorsoIKController::BeginPlay LocomotionController Returned Null"));
		return;
	}
	this->AddTickPrerequisiteComponent(LocomotionController);

	CarryingController = CharacterOwner->FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_UpperTorsoIKController::BeginPlay CarryingController Returned Null"));
		return;
	}

	// Start with the tick disabled. Enable it if we ever start using a HandIKCurrentRoutine.
	SetComponentTickEnabled(false);

	/* Initialize IK modes */

	GroundedUnarmedIKSettings.Init(this);
	GroundedGunIKSettings.Init(this);
	HangingIKSettings.Init(this);
}

void UPS_Human_UpperTorsoIKController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Perform any extra logic that affects interpolation behaviour.
	// For example, we might change interpolation speed based on player speed.
	if (PtrToInterpFunction)
		(this->*PtrToInterpFunction)(DeltaTime);

	// Call compute check functionality whenever necessary.
	TimeSinceLastCheckCalled += DeltaTime;
	if (PtrToComputeIKPosition && (TimeSinceLastCheckCalled > IntervalCheckTime))
	{
		(this->*PtrToComputeIKPosition)();
		TimeSinceLastCheckCalled = 0.0f;
	}

	// Finally, perform interpolation for hands or pelvis if active.
	if(EnabledHandIKs)
		InterpolateHandPositions(DeltaTime);

	if (EnabledPelvisIKs)
		InterpolatePelvisPositions(DeltaTime);
}

/* Upper Torso IK Modes */

void UPS_Human_UpperTorsoIKController::SetUpperTorsoIKEnabled(EUpperTorsoIKModes NewUpperTorsoIKMode)
{
	SetUpperTorsoIKMode(NewUpperTorsoIKMode);
}

void UPS_Human_UpperTorsoIKController::SetUpperTorsoIKToLastUsedMode()
{
	SetUpperTorsoIKEnabled(UpperTorsoIKMode);
}

void UPS_Human_UpperTorsoIKController::SetUpperTorsoIKMode(EUpperTorsoIKModes NewHandIKMode)
{
	UpperTorsoIKMode = NewHandIKMode;

	switch (UpperTorsoIKMode)
	{
	case EUpperTorsoIKModes::UB_IK_DEACTIVATED:
		SetUpperTorsoIKDisabled();
		break;
	case EUpperTorsoIKModes::UB_IK_GROUNDED_UNARMED:
		GroundedUnarmedIKSettings.Start(this);
		break;
	case EUpperTorsoIKModes::UB_IK_GROUNDED_GUN:
		GroundedGunIKSettings.Start(this);
		break;
	case EUpperTorsoIKModes::UB_IK_CLIMBING:
		HangingIKSettings.Start(this);
		break;
	default:
		break;
	}

	SetComputeIKBehaviour(NewHandIKMode);
	SetInterpolationIKBehaviour(NewHandIKMode);
	SetComponentTickEnabled(NewHandIKMode != EUpperTorsoIKModes::UB_IK_DEACTIVATED);
}

void UPS_Human_UpperTorsoIKController::SetUpperTorsoIKDisabled(float TimeToKeepDisabled)
{
	EnabledHandIKs = false;
	EnabledPelvisIKs = false;
	PtrToComputeIKPosition = nullptr;
	PtrToInterpFunction = nullptr;

	if (TimeToKeepDisabled > 0)
	{
		UWorld * WorldPtr = GetWorld();
		if (WorldPtr)
		{
			WorldPtr->GetTimerManager().SetTimer(UpperBodyTimerHandle, this, &UPS_Human_UpperTorsoIKController::SetUpperTorsoIKToLastUsedMode,
				TimeToKeepDisabled, false);
		}
	}
	else
	{
		UpperTorsoIKMode = EUpperTorsoIKModes::UB_IK_DEACTIVATED;
	}
}

/* Compute IK functionality */

void UPS_Human_UpperTorsoIKController::SetComputeIKBehaviour(EUpperTorsoIKModes NewHandIKMode)
{
	switch (NewHandIKMode)
	{
	case EUpperTorsoIKModes::UB_IK_GROUNDED_UNARMED:
		IntervalCheckTime = GroundedUnarmedIKSettings.GetIKComputeCheckFrequency();
		PtrToComputeIKPosition = &UPS_Human_UpperTorsoIKController::GroundedUnarmed_IKChecks;
		break;
	case EUpperTorsoIKModes::UB_IK_GROUNDED_GUN:
		IntervalCheckTime = GroundedGunIKSettings.GetIKComputeCheckFrequency();
		PtrToComputeIKPosition = &UPS_Human_UpperTorsoIKController::GroundedGun_IKChecks;
		break;
	case EUpperTorsoIKModes::UB_IK_CLIMBING:
		IntervalCheckTime = HangingIKSettings.GetIKComputeCheckFrequency();
		PtrToComputeIKPosition = &UPS_Human_UpperTorsoIKController::Hanging_IKChecks;
		break;
	default:
		IntervalCheckTime = 0.0f;
		PtrToComputeIKPosition = nullptr;
		break;
	}

	TimeSinceLastCheckCalled = 0.0f;
}

void UPS_Human_UpperTorsoIKController::GroundedUnarmed_IKChecks()
{
	GroundedUnarmedIKSettings.ComputeHandsAndPelvisIK(this);
}

void UPS_Human_UpperTorsoIKController::GroundedGun_IKChecks()
{
	GroundedGunIKSettings.ComputeHandsPositions(this);
}

void UPS_Human_UpperTorsoIKController::Hanging_IKChecks()
{
	HangingIKSettings.ComputeClimbingHandPositions(this);
}

/* Interpolation */

void UPS_Human_UpperTorsoIKController::SetInterpolationIKBehaviour(EUpperTorsoIKModes NewHandIKMode)
{
	switch (NewHandIKMode)
	{
	case EUpperTorsoIKModes::UB_IK_GROUNDED_UNARMED:
		PtrToInterpFunction = &UPS_Human_UpperTorsoIKController::GroundedUnarmed_InterpolationFunction;
		break;
	default:
		PtrToInterpFunction = nullptr;
		break;
	}
}

void UPS_Human_UpperTorsoIKController::InterpolateHandPositions(float DeltaTime)
{
	HandIKConfiguration.LeftHandPosition = FMath::VInterpTo(HandIKConfiguration.LeftHandPosition, HandIKConfiguration.LeftHandTargetPosition, DeltaTime, HandIKConfiguration.IK_CurrentInterpolationSpeed);
	HandIKConfiguration.RightHandPosition = FMath::VInterpTo(HandIKConfiguration.RightHandPosition, HandIKConfiguration.RightHandTargetPosition, DeltaTime, HandIKConfiguration.IK_CurrentInterpolationSpeed);
	HandIKConfiguration.LeftHandRotation = FMath::RInterpTo(HandIKConfiguration.LeftHandRotation, HandIKConfiguration.LeftHandTargetRotation, DeltaTime, HandIKConfiguration.IK_CurrentInterpolationRotationSpeed);
	HandIKConfiguration.RightHandRotation = FMath::RInterpTo(HandIKConfiguration.RightHandRotation, HandIKConfiguration.RightHandTargetRotation, DeltaTime, HandIKConfiguration.IK_CurrentInterpolationRotationSpeed);
}

void UPS_Human_UpperTorsoIKController::InterpolatePelvisPositions(float DeltaTime)
{
	PelvisIKConfiguration.CurrentPelvisYaw = FMath::FInterpTo(PelvisIKConfiguration.CurrentPelvisYaw, PelvisIKConfiguration.TargetPelvisYaw, DeltaTime, PelvisIKConfiguration.IK_CurrentInterpolationRotationSpeed);
}

void UPS_Human_UpperTorsoIKController::GroundedUnarmed_InterpolationFunction(float DeltaTime)
{
	GroundedUnarmedIKSettings.InterpolationFunction(this, DeltaTime);
}

/* Hands */

void UPS_Human_UpperTorsoIKController::SetHandIKTargetToAnimationLocation(bool IsRightBone)
{
	if (IsRightBone)
	{
		FName HandBone = HandIKConfiguration.RightHandName;
		FVector HandLocation = SkeletalMeshComponent->GetBoneLocation(HandBone);
		HandIKConfiguration.RightHandTargetPosition = HandLocation;
	}
	else
	{
		FName HandBone = HandIKConfiguration.LeftHandName;
		FVector HandLocation = SkeletalMeshComponent->GetBoneLocation(HandBone);
		HandIKConfiguration.LeftHandTargetPosition = HandLocation;
	}
}

/* Getters. */

bool UPS_Human_UpperTorsoIKController::GetEnabledUpperBodyIKs() const
{
	return UpperTorsoIKMode == EUpperTorsoIKModes::UB_IK_DEACTIVATED;
}

bool UPS_Human_UpperTorsoIKController::GetEnabledHandsIKs() const
{
	return EnabledHandIKs;
}

bool UPS_Human_UpperTorsoIKController::GetEnabledPelvisIKs() const
{
	return EnabledPelvisIKs;
}

const FVector & UPS_Human_UpperTorsoIKController::GetRightHandPosition() const
{
	return HandIKConfiguration.RightHandPosition;
}

const FRotator & UPS_Human_UpperTorsoIKController::GetRightHandRotator() const
{
	return HandIKConfiguration.RightHandRotation;
}

const FVector & UPS_Human_UpperTorsoIKController::GetRightHandJoint() const
{
	return HandIKConfiguration.RightHandJoint;
}

const bool UPS_Human_UpperTorsoIKController::GetRightHandEnabled() const
{
	return HandIKConfiguration.RightHand_IK;
}

const FVector & UPS_Human_UpperTorsoIKController::GetLeftHandPosition() const
{
	return HandIKConfiguration.LeftHandPosition;
}

const FRotator & UPS_Human_UpperTorsoIKController::GetLeftHandRotator() const
{
	return HandIKConfiguration.LeftHandRotation;
}

const FVector& UPS_Human_UpperTorsoIKController::GetLeftHandJoint() const
{
	return HandIKConfiguration.LeftHandJoint;
}

const bool UPS_Human_UpperTorsoIKController::GetLeftHandEnabled() const
{
	return HandIKConfiguration.LeftHand_IK;
}

const float UPS_Human_UpperTorsoIKController::GetPelvisYawOffset() const
{
	return PelvisIKConfiguration.CurrentPelvisYaw;
}