#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PS_Human_IKStructs.h"
#include "Kismet/KismetSystemLibrary.h"
#include "PS_Human_IKController.generated.h"

class APS_Character;
class USkeletalMeshComponent;
class UCapsuleComponent;
class UPS_Human_LocomotionController;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_IKController : public UActorComponent
{
	GENERATED_BODY()

protected:

	/* Leg IKs Variables */

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	F_BipedFeetIKConfiguration FeetIKConfiguration;

	UPROPERTY(BlueprintReadOnly)
	bool EnabledLegsIK = true;

	/* Component variables */

	APS_Character * CharacterOwner = nullptr;

	USkeletalMeshComponent * SkeletalMeshComponent = nullptr;

	UCapsuleComponent * CapsuleComponent = nullptr;

	UPS_Human_LocomotionController * LocomotionController = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TEnumAsByte<EDrawDebugTrace::Type> DebugTrace = EDrawDebugTrace::None;

	/* IK functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

	void ComputeLegIK(const FName & FootBoneName, float IKRayDistanceFromActorCenterOfMassToGround,
		float IK_BoneOffset, float& IK_FootFinalHeight, FRotator& IK_FootFinalRotation);

public:

	// Sets default values for this component's properties
	UPS_Human_IKController();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "LegsIK")
	void SetLegIKEnabledStatus(bool NewEnableLegIK);

	/* Getter functions. */

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
	float GetRightFootHeightOffset() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
	FRotator GetRightFootRotation() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
	float GetLeftFootHeightOffset() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
	FRotator GetLeftFootRotation() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
	float GetHipsOffset() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "LegsIK")
	bool IsLegIKActive() const;
};
