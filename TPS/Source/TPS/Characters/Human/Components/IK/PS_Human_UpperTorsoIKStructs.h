#pragma once

#include "CoreMinimal.h"
#include "PS_Human_UpperTorsoIKStructs.generated.h"

/*  Upper Torso IK Modes.
	Each mode may implement different behavior when calculating IK */
UENUM(BlueprintType)
enum EUpperTorsoIKModes
{
	UB_IK_DEACTIVATED,
	UB_IK_GROUNDED_UNARMED,
	UB_IK_GROUNDED_GUN,
	UB_IK_CLIMBING,
};

/*	Struct with all variables used for the configuration of the Hand IK system. */

USTRUCT(Blueprintable, BlueprintType)
struct F_HandIKConfiguration
{
	GENERATED_BODY()

	/* General variables */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HandIK")
	float IK_CurrentBlendInOut = 0.50f;

	// Interpolation speed for hand position to reach target.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HandIK")
	float IK_CurrentInterpolationSpeed = 2.0f;

	// Interpolation speed for hand rotation to reach target.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HandIK")
	float IK_CurrentInterpolationRotationSpeed = 2.0f;

	/* Left hand variables */

	// IK bone name of left hand.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HandIK")
	FName LeftHandName;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FVector LeftHandPosition;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FVector LeftHandTargetPosition;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FRotator LeftHandRotation;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FRotator LeftHandTargetRotation;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FVector LeftHandJoint;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	bool LeftHand_IK = false;

	/* Right hand variables */

	// IK bone name of right foot.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HandIK")
	FName RightHandName;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FVector RightHandPosition;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FVector RightHandTargetPosition;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FRotator RightHandRotation;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FRotator RightHandTargetRotation;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	FVector RightHandJoint;
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	bool RightHand_IK = false;
};

/*	Struct with all variables used for the configuration of a pelvis IK system for a biped character. */

USTRUCT(Blueprintable, BlueprintType)
struct F_PelvisIKConfiguration
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	float IK_CurrentBlendInOut = 0.50f;

	// Interpolation speed for pelvis rotations.
	UPROPERTY(BlueprintReadOnly, Category = "HandIK")
	float IK_CurrentInterpolationRotationSpeed = 2.0f;

	// Bone name of pelvis
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pelvis")
	FName PelvisBoneName;

	UPROPERTY(BlueprintReadOnly, Category = "Pelvis")
	float CurrentPelvisYaw = 0.0f;
	
	UPROPERTY(BlueprintReadOnly, Category = "Pelvis")
	float TargetPelvisYaw = 0.0f;
};