#include "PS_Human_HeadIKController.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "../Locomotion/PS_Human_LocomotionController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"

UPS_Human_HeadIKController::UPS_Human_HeadIKController() : UActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UPS_Human_HeadIKController::BeginPlay()
{
	Super::BeginPlay();

	CharacterOwner = Cast<APS_Character>(GetOwner());
	if (!CharacterOwner)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_HeadIKController::BeginPlay GetOwner Returned Null"));
		return;
	}

	SkeletalMeshComp = CharacterOwner->FindComponentByClass<USkeletalMeshComponent>();
	if (!SkeletalMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_HeadIKController::BeginPlay USkeletalMeshComponent Returned Null"));
		return;
	}

	CapsuleComponent = CharacterOwner->FindComponentByClass<UCapsuleComponent>();
	if (!CapsuleComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_HeadIKController::BeginPlay UCapsuleComponent Returned Null"));
		return;
	}

	// Get sure we update after locomotion controller has calculated this frame's values.
	LocomotionController = CharacterOwner->FindComponentByClass<UPS_Human_LocomotionController>();
	if (LocomotionController)
		this->AddTickPrerequisiteComponent(LocomotionController);

	// Start with the tick disabled. Enable it if we ever start using a HandIKCurrentRoutine.
	SetComponentTickEnabled(false);
}

void UPS_Human_HeadIKController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	InterpolateHeadTarget(DeltaTime);

	if (PtrToInterpFunction)
		(this->*PtrToInterpFunction)();
}

void UPS_Human_HeadIKController::LookAtPosition(const FVector & NewPositionToLookAt)
{
	PositionToLookAt = NewPositionToLookAt;
	HeadIKConfiguration.IK_CurrentInterpolationRotationSpeed = LookAt_InterpolationRotationSpeed;
	UpdateLookAtPosition();
	PtrToInterpFunction = &UPS_Human_HeadIKController::UpdateLookAtPosition;
	SetHeadIKEnabled();
}

void UPS_Human_HeadIKController::UpdateLookAtPosition()
{
	FVector CurrentHeadPosition = SkeletalMeshComp->GetBoneLocation(HeadIKConfiguration.HeadBoneName);
	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(CurrentHeadPosition, PositionToLookAt);
	FRotator ActorRotation = CharacterOwner->GetActorRotation();

	// Roll and pitch are swapped due to head bone orientation.
	FRotator ResultAngle;
	FRotator DeltaAngle = UKismetMathLibrary::NormalizedDeltaRotator(LookAtRotation, ActorRotation);
	ResultAngle.Yaw = FMath::ClampAngle(DeltaAngle.Yaw, HeadIKConfiguration.MinYawRotation, HeadIKConfiguration.MaxYawRotation);
	ResultAngle.Pitch = FMath::ClampAngle(DeltaAngle.Roll, HeadIKConfiguration.MinRollRotation, HeadIKConfiguration.MaxRollRotation);
	ResultAngle.Roll = FMath::ClampAngle(DeltaAngle.Pitch * -1.0f, HeadIKConfiguration.MinPitchRotation, HeadIKConfiguration.MaxPitchRotation);
	HeadIKConfiguration.TargetLookAtRotation = ResultAngle;
}

void UPS_Human_HeadIKController::LookAtComponent(const USceneComponent * NewSceneCompToLookAt)
{
	if (!NewSceneCompToLookAt) return;

	SceneComponentToLookAt = NewSceneCompToLookAt;
	HeadIKConfiguration.IK_CurrentInterpolationRotationSpeed = LookAt_InterpolationRotationSpeed;
	UpdateLookAtPosition();
	PtrToInterpFunction = &UPS_Human_HeadIKController::UpdateLookAtPosition;
	SetHeadIKEnabled();
}

void UPS_Human_HeadIKController::UpdateLookAtComponent()
{
	FVector CurrentHeadPosition = SkeletalMeshComp->GetBoneLocation(HeadIKConfiguration.HeadBoneName);
	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(CurrentHeadPosition, SceneComponentToLookAt->GetComponentLocation());
	FRotator ActorRotation = CharacterOwner->GetActorRotation();

	// Roll and pitch are swapped due to head bone orientation.
	FRotator ResultAngle;
	FRotator DeltaAngle = UKismetMathLibrary::NormalizedDeltaRotator(LookAtRotation, ActorRotation);
	ResultAngle.Yaw = FMath::ClampAngle(DeltaAngle.Yaw, HeadIKConfiguration.MinYawRotation, HeadIKConfiguration.MaxYawRotation);
	ResultAngle.Pitch = FMath::ClampAngle(DeltaAngle.Roll, HeadIKConfiguration.MinRollRotation, HeadIKConfiguration.MaxRollRotation);
	ResultAngle.Roll = FMath::ClampAngle(DeltaAngle.Pitch * -1.0f, HeadIKConfiguration.MinPitchRotation, HeadIKConfiguration.MaxPitchRotation);
	HeadIKConfiguration.TargetLookAtRotation = ResultAngle;
}

void UPS_Human_HeadIKController::LookAtControllerDirection()
{
	HeadIKConfiguration.IK_CurrentInterpolationRotationSpeed = LookAtController_InterpolationRotationSpeed;
	UpdateLookAtControllerDirection();
	PtrToInterpFunction = &UPS_Human_HeadIKController::UpdateLookAtControllerDirection;
	SetHeadIKEnabled();
}

void UPS_Human_HeadIKController::UpdateLookAtControllerDirection()
{
	FRotator ControllerRotation = CharacterOwner->GetControlRotation();
	FRotator ActorRotation = CharacterOwner->GetActorRotation();

	// Roll and pitch are swapped due to head bone orientation.
	FRotator ResultAngle;
	FRotator DeltaAngle = UKismetMathLibrary::NormalizedDeltaRotator(ControllerRotation, ActorRotation);
	ResultAngle.Yaw = FMath::ClampAngle(DeltaAngle.Yaw, HeadIKConfiguration.MinYawRotation, HeadIKConfiguration.MaxYawRotation);
	ResultAngle.Pitch = FMath::ClampAngle(DeltaAngle.Roll, HeadIKConfiguration.MinRollRotation, HeadIKConfiguration.MaxRollRotation);
	ResultAngle.Roll = FMath::ClampAngle(DeltaAngle.Pitch * -1.0f, HeadIKConfiguration.MinPitchRotation, HeadIKConfiguration.MaxPitchRotation);
	HeadIKConfiguration.TargetLookAtRotation = ResultAngle;
}

void UPS_Human_HeadIKController::LookAtRandomDirection()
{
	// Roll and pitch are swapped due to head bone orientation.
	FRotator RandomRotator;
	RandomRotator.Yaw = FMath::RandRange(HeadIKConfiguration.MinYawRotation, HeadIKConfiguration.MaxYawRotation);
	RandomRotator.Pitch = FMath::RandRange(HeadIKConfiguration.MinRollRotation, HeadIKConfiguration.MaxRollRotation);
	RandomRotator.Roll = FMath::RandRange(HeadIKConfiguration.MinPitchRotation, HeadIKConfiguration.MaxPitchRotation);
	HeadIKConfiguration.TargetLookAtRotation = RandomRotator;

	HeadIKConfiguration.IK_CurrentInterpolationRotationSpeed = RandomLookat_InterpolationRotationSpeed;
	PtrToInterpFunction = nullptr;

	SetHeadIKEnabled();
}

// Activate head IK.
void UPS_Human_HeadIKController::SetHeadIKEnabled()
{
	EnabledHeadIK = true;
	SetComponentTickEnabled(true);
}

// Deactivate head IK.
void UPS_Human_HeadIKController::SetHeadIKDisabled()
{
	EnabledHeadIK = false;
	SetComponentTickEnabled(false);
	PtrToInterpFunction = nullptr;
}

void UPS_Human_HeadIKController::InterpolateHeadTarget(float DeltaTime)
{
	HeadIKConfiguration.CurrentLookAtRotation = FMath::RInterpTo(HeadIKConfiguration.CurrentLookAtRotation, HeadIKConfiguration.TargetLookAtRotation, DeltaTime, HeadIKConfiguration.IK_CurrentInterpolationRotationSpeed);
}

/* Head  IK Getters */

bool UPS_Human_HeadIKController::GetEnabledHeadIK() const
{
	return EnabledHeadIK;
}

const FRotator & UPS_Human_HeadIKController::GetHeadLookAtRotation() const
{
	return HeadIKConfiguration.CurrentLookAtRotation;
}