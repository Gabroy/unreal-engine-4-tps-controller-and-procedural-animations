#include "PS_Human_MontageComponent.h"
#include "../../PS_Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Locomotion/PS_Human_LocomotionController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../Locomotion/States/PS_LocState_Climb.h"

void UPS_Human_MontageComponent::BeginPlay()
{
	Super::BeginPlay();

	Character = Cast<APS_Character>(GetOwner());
	if (!Character) return;

	LocomotionController = Character->FindComponentByClass<UPS_Human_LocomotionController>();

	LocomotionController = Cast<UPS_Human_LocomotionController>(Character->GetComponentByClass(UPS_Human_LocomotionController::StaticClass()));
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageComponent::BeginPlay LocomotionController Returned Null"));
		return;
	}

	MovComponent = Cast<UCharacterMovementComponent>(Character->GetComponentByClass(UCharacterMovementComponent::StaticClass()));
	if (!MovComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPS_Human_MontageComponent::BeginPlay MovComponent Returned Null"));
		return;
	}
}

/* Rolling Montage. */

void UPS_Human_MontageComponent::PlayRollingMontage(bool ForceMontage)
{
	bool Success = PlayAnimationMontage(RollingForwardMontage, RollingAnimSpeed, 0.0, "None", ForceMontage);
	if (!Success) return;
	
	IsRolling = true;

	FOnMontageStartedDynDelegate StartedMontage;
	StartedMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnRollingMontageStarted);
	BindOnMontageStarted(RollingForwardMontage, StartedMontage);

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnRollingMontageEndedOrBlendingOut);
	BindOnMontageBlendingOut(RollingForwardMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnRollingMontageEndedOrBlendingOut);
	BindOnMontageEnded(RollingForwardMontage, EndedMontage);
}

bool UPS_Human_MontageComponent::CanPlayRollingMontage(bool ForceMontage)
{
	return ForceMontage || (!IsAnyMontagePlaying() && !IsPlayingMontage(RollingForwardMontage));
}

void UPS_Human_MontageComponent::OnRollingMontageStarted(UAnimMontage* Montage)
{
	Character->SetEnableCharacterMovementAndActions(false);
	LocomotionController->SetLegIKEnabledStatus(false);
}

void UPS_Human_MontageComponent::OnRollingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	Character->SetEnableCharacterMovementAndActions(true);
	LocomotionController->SetLegIKEnabledStatus(true);
	IsRolling = false;
}

/* Slide Montage. */

void UPS_Human_MontageComponent::PlaySlideMontage(bool ForceMontage)
{
	bool Success = PlayAnimationMontage(SlidingMontage, SlideAnimSpeed, 0.0, "None", ForceMontage);
	if (!Success) return;

	IsSliding = true;

	FOnMontageStartedDynDelegate StartedMontage;
	StartedMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnSlideMontageStarted);
	BindOnMontageStarted(SlidingMontage, StartedMontage);

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnSlideMontageEndedOrBlendingOut);
	BindOnMontageBlendingOut(SlidingMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnSlideMontageEndedOrBlendingOut);
	BindOnMontageEnded(SlidingMontage, EndedMontage);
}

bool UPS_Human_MontageComponent::CanPlaySlideMontage(bool ForceMontage)
{
	return (LocomotionController->GetSpeed() >= MinSlideSpeedNecessaryForSliding)
		&& (ForceMontage || (!IsAnyMontagePlaying() && !IsPlayingMontage(SlidingMontage)));
}

void UPS_Human_MontageComponent::OnSlideMontageStarted(UAnimMontage* Montage)
{
	Character->DoCrouch();
	Character->SetEnableCharacterMovementAndActions(false);
	LocomotionController->SetLegIKEnabledStatus(false);
}

void UPS_Human_MontageComponent::OnSlideMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	Character->DoUncrouch();
	Character->SetEnableCharacterMovementAndActions(true);
	LocomotionController->SetLegIKEnabledStatus(true);
	IsSliding = false;
}

/* Climbing Montage. */

void UPS_Human_MontageComponent::PlayClimbingMontage(UAnimMontage * ClimbMontage, float PlayRate, bool ForceMontage)
{
	// Remove the montage if it's already playing.
	if (ForceMontage && IsAnyMontagePlaying())
		MontageStop(ClimbMontage, 0.0f);

	bool Success = PlayAnimationMontage(ClimbMontage, PlayRate, 0.0, "None", ForceMontage);
	if (!Success) return;

	ClimbingMontagePlaying = ClimbMontage;

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnClimbingMontageEndedOrBlendingOut);
	BindOnMontageBlendingOut(ClimbMontage, BlendingOutMontage);
}

void UPS_Human_MontageComponent::SetClimbingMontagePlayrate(bool Backwards)
{
	if (!ClimbingMontagePlaying)
		return;
	float Playrate = Backwards ? -1.0f : 1.0f;
	SetMontagePlayRate(ClimbingMontagePlaying, Playrate);
}

void UPS_Human_MontageComponent::OnClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	// Ignore any on end call if it gets called and this is null. This might happen if, somehow, UE didn't remove
	// the previous montage. The Montage stop on PlayClimb call should already fix this.
	if (!ClimbingMontagePlaying)
		return;

	ClimbingMontagePlaying = nullptr;
	ClimbingAnimEnded.Broadcast();
}

bool UPS_Human_MontageComponent::PlayTryClimbingMontage(UAnimMontage* ClimbMontage, float PlayRate, bool ForceMontage)
{
	// Don't play any montage if we are already playing something.
	if (IsAnyMontagePlaying())
		return false;

	bool Success = PlayAnimationMontage(ClimbMontage, PlayRate, 0.0, "None", ForceMontage);
	if (!Success) return false;

	ClimbingMontagePlaying = ClimbMontage;
	TryClimbMontagePaused = false;

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnTryClimbingMontageEndedOrBlendingOut);
	BindOnMontageBlendingOut(ClimbMontage, BlendingOutMontage);

	return true;
}

void UPS_Human_MontageComponent::OnTryClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	// Ignore any on end call if it gets called and this is null. This might happen if, somehow, UE didn't remove
	// the previous montage. The Montage stop on PlayClimb call should already fix this.
	if (!ClimbingMontagePlaying)
		return;

	ClimbingMontagePlaying = nullptr;
	TryClimbMontagePaused = false;
	TryClimbingAnimEnded.Broadcast();
}

void UPS_Human_MontageComponent::OnTryClimbingStartBlendOut()
{
	if (!ClimbingMontagePlaying)
		return;

	bool GoingBackwards = GetMontagePlayrate(ClimbingMontagePlaying) < 0.0f;
	if (GoingBackwards)
		MontageStop(ClimbingMontagePlaying, ClimbingMontagePlaying->BlendOut.GetBlendTime());
}

void UPS_Human_MontageComponent::OnTryClimbingStop()
{
	if (!ClimbingMontagePlaying)
		return;

	bool GoingForwards = GetMontagePlayrate(ClimbingMontagePlaying) > 0.0f;
	if (GoingForwards)
	{
		MontagePause(ClimbingMontagePlaying);
		TryClimbMontagePaused = true;
	}
}

void UPS_Human_MontageComponent::SetTryClimbingMontagePlayrate(bool Backwards)
{
	if (!ClimbingMontagePlaying)
		return;

	float Playrate = Backwards ? -1.0f : 1.0f;
	SetMontagePlayRate(ClimbingMontagePlaying, Playrate);
	if (Backwards)
	{
		if (TryClimbMontagePaused)
		{
			MontageResume(ClimbingMontagePlaying);
			TryClimbMontagePaused = false;
		}
	}
}

void UPS_Human_MontageComponent::PlayClimbingTurnMontage(UAnimMontage* ClimbTurnMontage, float PlayRate, bool ForceMontage)
{
	bool Success = PlayAnimationMontage(ClimbTurnMontage, PlayRate, 0.0, "None", ForceMontage);
	if (!Success) return;

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnClimbingTurnMontageEndedOrBlendingOut);
	BindOnMontageBlendingOut(ClimbTurnMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnClimbingTurnMontageEndedOrBlendingOut);
	BindOnMontageEnded(ClimbTurnMontage, EndedMontage);
}

void UPS_Human_MontageComponent::OnClimbingTurnMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	ClimbingTurnAnimEnded.Broadcast();
}

void UPS_Human_MontageComponent::PlayLeaveClimbingMontage(UAnimMontage* ClimbMontage, float PlayRate, bool ForceMontage)
{
	bool Success = PlayAnimationMontage(ClimbMontage, PlayRate, 0.0, "None", ForceMontage);
	if (!Success) return;

	FOnMontageBlendingOutDynDelegate BlendingOutMontage;
	BlendingOutMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnLeaveClimbingMontageEndedOrBlendingOut);
	BindOnMontageBlendingOut(ClimbMontage, BlendingOutMontage);

	FOnMontageEndedDynDelegate EndedMontage;
	EndedMontage.BindDynamic(this, &UPS_Human_MontageComponent::OnLeaveClimbingMontageEndedOrBlendingOut);
	BindOnMontageEnded(ClimbMontage, EndedMontage);
}

void UPS_Human_MontageComponent::OnLeaveClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted)
{
	// Set falling mode if somehow, the anim notification for setting the falling mode when leaving climbing was not fired.
	if(MovComponent->IsFlying())
		MovComponent->SetMovementMode(MOVE_Falling);
}