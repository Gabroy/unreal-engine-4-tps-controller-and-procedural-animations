#pragma once

#include "CoreMinimal.h"
#include "PS_Human_MontageStruct.generated.h"

class UAnimSequence;

/* Marks if this animation uses two feet, or the left or right one. */
UENUM(BlueprintType)
enum EAnimationFeetPosition
{
	USES_BOTH_FEET,
	USES_LEFT_FOOT,
	USES_RIGHT_FOOT
};

/* Random idle animation that can be played while in Idle state. */
USTRUCT(BlueprintType, Blueprintable)
struct FIdleRandomAnimation
{
	GENERATED_BODY()

	// Idle sequence that will be played if this random animation is chosen.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence * IdleSequenceAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EAnimationFeetPosition> FeetPosition;

	// Weight value representing a % probability that this animation might be played at any given time.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float WeightAnimation = 0.0f;

	// Computed on initialization. Used to decide which animation to play. Represents a range in 0.0 to 1.0 where the anim will be played.
	// To check the actual % get the next animation and subtract its FinalWeight from this anim FinalWeight.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float FinalWeight = 0.0f;
};