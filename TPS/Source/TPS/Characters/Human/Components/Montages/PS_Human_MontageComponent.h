#pragma once

#include "CoreMinimal.h"
#include "../../../Common/Components/PS_Base_MontageComponent.h"
#include "../Animations/PS_Human_AnimationsStruct.h"
#include "PS_Human_MontageComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FClimbingAnimationEnded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTryClimbingAnimationEnded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FClimbingTurnAnimationEnded);

class APS_Character;
class UPS_Human_LocomotionController;
class UCharacterMovementComponent;

/*
*
* This component controls the playing of animation clip and animation montages for a human character
* and the implementation of the necessary logic to make each one of them work correctly at any given time.
* 
*/

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPS_Human_MontageComponent : public UPS_Base_MontageComponent
{
	GENERATED_BODY()

public:

	FClimbingAnimationEnded ClimbingAnimEnded;
	FTryClimbingAnimationEnded TryClimbingAnimEnded;
	FClimbingTurnAnimationEnded ClimbingTurnAnimEnded;

protected:

	UPROPERTY(BlueprintReadOnly)
	APS_Character * Character = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UPS_Human_LocomotionController * LocomotionController = nullptr;

	UPROPERTY(BlueprintReadOnly)
	UCharacterMovementComponent * MovComponent = nullptr;

	/* Rolling montage. */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montages")
	UAnimMontage* RollingForwardMontage;

	// Playrate at which the rolling animations is played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montages")
	float RollingAnimSpeed = 1.35f;

	bool IsRolling = false;

	/* Slide montage. */

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montages")
	UAnimMontage * SlidingMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montages")
	float MinSlideSpeedNecessaryForSliding = 400.0f;

	// Playrate at which the rolling animations is played.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montages")
	float SlideAnimSpeed = 1.0f;

	bool IsSliding = false;

	/* Current climbing montage */

	UAnimMontage * ClimbingMontagePlaying = nullptr;
	bool TryClimbMontagePaused = true;;

	/* Protected functions. */

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	/* Rolling Montage. */

	UFUNCTION(BlueprintCallable)
	void PlayRollingMontage(bool ForceMontage = false);
	UFUNCTION(BlueprintCallable)
	bool CanPlayRollingMontage(bool ForceMontage = false);
	UFUNCTION()
	void OnRollingMontageStarted(UAnimMontage* Montage);
	UFUNCTION()
	void OnRollingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	/* Slide Montage. */

	UFUNCTION(BlueprintCallable)
	void PlaySlideMontage(bool ForceMontage = false);
	UFUNCTION(BlueprintCallable)
	bool CanPlaySlideMontage(bool ForceMontage = false);
	UFUNCTION()
	void OnSlideMontageStarted(UAnimMontage* Montage);
	UFUNCTION()
	void OnSlideMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	/* Climbing Montage. */

	UFUNCTION(BlueprintCallable)
	void PlayClimbingMontage(UAnimMontage* ClimbMontage, float PlayRate, bool ForceMontage = true);
	UFUNCTION(BlueprintCallable)
	void SetClimbingMontagePlayrate(bool Backwards);
	UFUNCTION()
	void OnClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

	UFUNCTION(BlueprintCallable)
	bool PlayTryClimbingMontage(UAnimMontage* ClimbMontage, float PlayRate, bool ForceMontage = true);
	UFUNCTION(BlueprintCallable)
	void OnTryClimbingStartBlendOut();
	UFUNCTION(BlueprintCallable)
	void OnTryClimbingStop();
	UFUNCTION()
	void OnTryClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);
	UFUNCTION(BlueprintCallable)
	void SetTryClimbingMontagePlayrate(bool Backwards);

	UFUNCTION(BlueprintCallable)
	void PlayClimbingTurnMontage(UAnimMontage * ClimbTurnMontage, float PlayRate, bool ForceMontage = true);
	UFUNCTION()
	void OnClimbingTurnMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);
	
	UFUNCTION(BlueprintCallable)
	void PlayLeaveClimbingMontage(UAnimMontage * ClimbMontage, float PlayRate, bool ForceMontage = true);
	UFUNCTION()
	void OnLeaveClimbingMontageEndedOrBlendingOut(UAnimMontage* Montage, bool Interrupted);

public:

	/* Getters. */

	bool GetIsRolling() const { return IsRolling; }
	bool GetIsSliding() const { return IsSliding; }
};
