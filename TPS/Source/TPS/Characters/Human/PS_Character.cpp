#include "PS_Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Characters/Human/Components/Input/General/PS_Human_Input_GeneralController.h"
#include "Characters/Human/Components/Input/Carrying/PS_Human_Input_CarryingController.h"
#include "Characters/Human/Components/Locomotion/PS_Human_LocomotionController.h"
#include "Characters/Human/Components/Carrying/PS_Human_CarryingController.h"
#include "Characters/Human/Components/Locomotion/States/PS_LocState_Grounded.h"
#include "Characters/Human/Components/Locomotion/States/PS_LocState_Climb.h"
#include "Components/SkeletalMeshComponent.h"
#include "Characters/Human/Components/Mantling/PS_Human_VaultingComponent.h"
#include "Characters/Human/Components/Mantling/PS_Human_ClimbingComponent.h"
#include "Characters/Human/Components/Animations/PS_Human_AnimationInstance.h"
#include "Components/CapsuleComponent.h"
#include "Characters/Human/Components/Interactor/PS_Human_InteractorComponent.h"
#include "Characters/Human/Components/Interactor/PS_Human_InteractorFinderComponent.h"
#include "Characters/Human/Components/Animations/PS_Human_MontageController.h"
#include "Gameplay/Camera/Controllers/PS_Human_CameraController.h"
#include "Characters/Common/Components/Animations/PS_TurningController.h"

APS_Character::APS_Character() : ACharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	CreateComponents();
}

void APS_Character::BeginPlay()
{
	Super::BeginPlay();

	InitParameters();
}

void APS_Character::CreateComponents()
{
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComp->bUsePawnControlRotation = true;

	USceneComponent * RootComp = GetRootComponent();
	if (RootComp)
		SpringArmComp->SetupAttachment(RootComp);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComp->SetupAttachment(SpringArmComp);
}

void APS_Character::InitParameters()
{
	MovementModeChangedDelegate.RemoveDynamic(this, &APS_Character::OnMovementModeChangedCallback);
	MovementModeChangedDelegate.AddDynamic(this, &APS_Character::OnMovementModeChangedCallback);

	CameraController = FindComponentByClass<UPS_Human_CameraController>();
	if (!CameraController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters CameraController Returned Null"));
		return;
	}

	GeneralInputController = FindComponentByClass<UPS_Human_Input_GeneralController>();
	if (!GeneralInputController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters GeneralInputController Returned Null"));
		return;
	}

	CarryingInputController = FindComponentByClass<UPS_Human_Input_CarryingController>();
	if (!CarryingInputController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters CarryingInputController Returned Null"));
		return;
	}
	
	LocomotionController = FindComponentByClass<UPS_Human_LocomotionController>();
	if (!LocomotionController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters LocomotionController Returned Null"));
		return;
	}

	TurningController = FindComponentByClass<UPS_TurningController>();
	if (!TurningController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters TurningController Returned Null"));
		return;
	}

	CarryingController = FindComponentByClass<UPS_Human_CarryingController>();
	if (!CarryingController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters CarryingController Returned Null"));
		return;
	}

	InteractorComponent = FindComponentByClass<UPS_Human_InteractorComponent>();
	if (!InteractorComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters InteractorComponent Returned Null"));
		return;
	}

	InteractorFinderComp = FindComponentByClass<UPS_Human_InteractorFinderComponent>();
	if (!InteractorFinderComp)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters InteractorFinderComp Returned Null"));
		return;
	}

	VaultingComponent = FindComponentByClass<UPS_Human_VaultingComponent>();
	if (!VaultingComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters VaultingComponent Returned Null"));
		return;
	}
	
	ClimbingComponent = FindComponentByClass<UPS_Human_ClimbingComponent>();
	if (!ClimbingComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters ClimbingComponent Returned Null"));
		return;
	}

	SkeletalMeshComp = FindComponentByClass<USkeletalMeshComponent>();
	if (!SkeletalMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters SkeletalMeshComp Returned Null"));
		return;
	}
	
	AnimInstance = Cast<UPS_Human_AnimationInstance>(SkeletalMeshComp->GetAnimInstance());
	if (!AnimInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters AnimInstance Returned Null"));
		return;
	}

	MontageController = FindComponentByClass<UPS_Human_MontageController>();
	if (!MontageController)
	{
		UE_LOG(LogTemp, Error, TEXT("APS_Character::InitParameters MontageController Returned Null"));
		return;
	}

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanJump = true;
	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

	CameraController->Init();
	GeneralInputController->Init();
	CarryingInputController->Init();
	LocomotionController->Init();
	CarryingController->Init();
}

void APS_Character::OnMovementModeChangedCallback(ACharacter* Character, EMovementMode PrevMovementMode, uint8 PreviousCustomMode)
{
	UCharacterMovementComponent* MovComp = GetCharacterMovement();
	if (!MovComp) return;

	if (PrevMovementMode == MovComp->MovementMode)
		return;

	if (LocomotionController)
		LocomotionController->OnMovementComponentStateChanged(PrevMovementMode, MovComp->MovementMode);
}

void APS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APS_Character::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

/* Actions. */

void APS_Character::MoveForward(float AxisValue)
{
	if (!IsMovementEnabled)
		return;

	FRotator CameraRotation = FRotator(0.0f, GetControlRotation().Yaw, 0.0f);
	AddMovementInput(UKismetMathLibrary::GetForwardVector(CameraRotation), AxisValue * SpeedModifier);
}

void APS_Character::MoveSideways(float AxisValue)
{	
	if (!IsMovementEnabled)
		return;

	FRotator CameraRotation = FRotator(0.0f, GetControlRotation().Yaw, 0.0f);
	AddMovementInput(UKismetMathLibrary::GetRightVector(CameraRotation), AxisValue * SpeedModifier);
}

void APS_Character::MoveInDir(float AxisValue, const FVector& MoveDir)
{
	if (!IsMovementEnabled)
		return;

	AddMovementInput(MoveDir, AxisValue * SpeedModifier);
}

// Walk action. If we are in a state that supports walking, this action will apply a walking effect.
void APS_Character::DoWalk()
{
	if (CanDoWalk())
		LocomotionController->SetWalk(true);
}

void APS_Character::DoStopWalk()
{
	LocomotionController->SetWalk(false);
}

bool APS_Character::CanDoWalk()
{
	return AreActionsEnabled;
}

// Sprint action.
void APS_Character::DoSprint()
{
	if (CanDoSprint())
	{
		if (LocomotionController->GetLocomotionStance() != ELocomotionStance::Normal)
			LocomotionController->ChangeLocomotionStance(ELocomotionStance::Normal);

		LocomotionController->SetWantsToSprint(true);
	}
}

void APS_Character::DoStopSprint()
{
	LocomotionController->SetWantsToSprint(false);

	// Moving to sprinting camera is done by the input controller as we need to check the velocity
	// of the player to start or stop it.
	if(CameraController)
		CameraController->ChangeToDefaultCamera();
}

bool APS_Character::CanDoSprint()
{
	return AreActionsEnabled;
}

// Crouch actions.
void APS_Character::DoCrouch()
{
	if (CanDoCrouch())
	{
		Crouch();
		LocomotionController->ChangeLocomotionStance(ELocomotionStance::Crouch);
	}
}

bool APS_Character::CanDoCrouch()
{
	return AreActionsEnabled && CrouchCooldown.IsCooldownFinished()
		&& LocomotionController && LocomotionController->CanChangeLocomotionStance(ELocomotionStance::Crouch);
}

void APS_Character::DoUncrouch()
{
	if (!CanDoUncrouch()) return;

	UnCrouch();
	CrouchCooldown.StartCooldown(GetWorld());
	LocomotionController->ChangeLocomotionStance(ELocomotionStance::Normal);
}

bool APS_Character::CanDoUncrouch()
{
	return LocomotionController && LocomotionController->CanChangeLocomotionStance(ELocomotionStance::Normal);
}

// Jump actions.
void APS_Character::DoIntendJump()
{
	if (CanDoJump())
	{
		WantsToJump = true;
		AnimInstance->PlayJumpingAnimation();
	}
}

void APS_Character::PerformJump()
{
	Jump();
	WantsToJump = false;
}

bool APS_Character::CanDoJump()
{
	return AreActionsEnabled && !WantsToJump && CanJump();
}

// Change facing mode.
void APS_Character::ChangeToVelocityFacingMode()
{
	LocomotionController->ChangeTargetRotationMode(ELocomotionRotationModes::VelocityFacing);
}

void APS_Character::ChangeToCameraFacingMode()
{
	LocomotionController->ChangeTargetRotationMode(ELocomotionRotationModes::CameraFacing);
}

// Zoom action.
void APS_Character::DoAim()
{
	if (CanDoAim())
	{
		CarryingController->SetAiming(true);

		if (CameraController)
			CameraController->ChangeToZoomCamera();
	}
}

void APS_Character::DoUnAim()
{
	if (CarryingController->GetIsAiming())
	{
		CarryingController->SetAiming(false);

		if (CameraController)
			CameraController->ChangeToDefaultCamera();
	}
}

bool APS_Character::CanDoAim()
{
	return AreActionsEnabled && CarryingController && CarryingController->CanAim();
}

// Interact action.
void APS_Character::DoInteract()
{
	if(CanInteract())
		InteractorFinderComp->StartInteraction();
}

void APS_Character::DoStopInteract()
{
	if (CanInteract())
		InteractorComponent->CancelInteraction();
}

bool APS_Character::CanInteract()
{
	return InteractorComponent && InteractorComponent->CheckInteractionsAreEnabled();
}

// Drop object.
void APS_Character::DropCarryingItem()
{
	if(CarryingController)
		CarryingController->DropCarryingItem();
}

bool APS_Character::CanDropCarryingItem()
{
	return CarryingController && CarryingController->CanDropCarryingItem();
}

/* Change Item functions */

void APS_Character::ChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave)
{
	if (CarryingController)
		CarryingController->ChangeOrSaveItem(ItemToChangeOrSave);
}

bool APS_Character::CanChangeOrSaveItem(EInventoryItemSlot ItemToChangeOrSave)
{
	return CarryingController && CarryingController->CanChangeOrSaveItem(ItemToChangeOrSave);
}

void APS_Character::CancelChangeOrSaveItem()
{
	if (CarryingController)
		CarryingController->CancelChangeOrSaveItem();
}

// Rolling.
void APS_Character::DoRolling()
{
	if (CanDoRolling())
		MontageController->PlayRollingMontage(false);
}

bool APS_Character::CanDoRolling()
{
	return AreActionsEnabled && MontageController && MontageController->CanPlayRollingMontage(false);
}

// Slide.
void APS_Character::DoSlide()
{
	if (CanDoSlide())
		MontageController->PlaySlideMontage(false);
}

bool APS_Character::CanDoSlide()
{
	return AreActionsEnabled && MontageController && MontageController->CanPlaySlideMontage(false);
}

// Turning.
void APS_Character::StartTurning(float AngleToTurnTo, float TurningVelocity)
{
	if (IsMovementEnabled && TurningController->CanTurn())
		TurningController->StartTurning(AngleToTurnTo, TurningVelocity);
}

void APS_Character::BindToTurningFinishedEvent(UObject * ObjectBindToTurning, TurningFinishedCallback FuncPtr, bool RemoveBindingOnCallbackCalled)
{
	if (TurningController)
	{
		TurningController->BindTurningEndOrInterrupted(ObjectBindToTurning, FuncPtr, RemoveBindingOnCallbackCalled);
	}
}

void APS_Character::UnbindFromTurningFinishedEvent(UObject* ObjectBindToTurning)
{
	if (TurningController)
	{
		TurningController->UnbindFromTurningEndedCallback(ObjectBindToTurning);
	}
}

void APS_Character::StopTurning()
{
	if(TurningController)
		TurningController->StopTurning();
}

bool APS_Character::CanTurn() const
{
	return TurningController && TurningController->CanTurn();
}

// Vaulting.
bool APS_Character::TryVaulting()
{
	if(AreActionsEnabled)
		return VaultingComponent->TryVaulting();
	return false;
}

// Climbing.
bool APS_Character::TryClimbing()
{
	if(AreActionsEnabled)
		return ClimbingComponent->TryClimb();
	return false;
}

/* Weapon actions */

void APS_Character::SetHolsterWeapon(bool HolsterWeapon)
{
	if(CarryingController)
		CarryingController->SetIsHolsteringWeapon(HolsterWeapon);
}

bool APS_Character::IsDoingHolster()
{
	return CarryingController ? CarryingController->GetIsHolsteringWeapon() : false;
}

// Hanging.
bool APS_Character::TryLedgeGrab()
{
	if (CanLedgeGrab())
		return LocomotionController->TryLedgeGrab();
	return false;
}

bool APS_Character::CanLedgeGrab()
{
	return AreActionsEnabled && HangLedgeCooldown.IsCooldownFinished() && !ClimbingComponent->GetIsCurrentlyClimbing();
}

void APS_Character::Hanging_TryMove(float Axis)
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_TryMove(Axis);
}

void APS_Character::Hanging_TryClimb(float Axis)
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_TryClimb(Axis);
}

void APS_Character::Hanging_PerformJumpingFromHanged()
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_PerformJumpingFromHanged();
}

void APS_Character::Hanging_PerformVerticalJump()
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_PerformVerticalJump();
}

void APS_Character::Hanging_PerformHorizontalJump(bool Right)
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_PerformHorizontalJump(Right);
}

// Release hanging.
void APS_Character::Hanging_ReleaseHang()
{
	if (LocomotionController)
		LocomotionController->Hanging_ReleaseHang();
	HangLedgeCooldown.StartCooldown(GetWorld());
}

void APS_Character::Hanging_Crouch()
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_Crouch();
}

void APS_Character::Hanging_Stand()
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_Stand();
}

void APS_Character::Hanging_Walk()
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_Walk();
}

void APS_Character::Hanging_StopWalk()
{
	if (AreActionsEnabled && LocomotionController)
		LocomotionController->Hanging_StopWalk();
}

// Feet toogle.
void APS_Character::DoToggleStandingFoot()
{
	if(CanToggleFeet)
		LocomotionController->DoToggleStandingFoot();
}

void APS_Character::SetCanToogleFeet(bool Enabled)
{
	CanToggleFeet = Enabled;
}

float APS_Character::GetSpeed() const
{
	return LocomotionController->GetSpeed();
}

FVector APS_Character::GetActorLocationFromFeet() const
{
	if (SkeletalMeshComp)
		return SkeletalMeshComp->GetComponentLocation();

	ensure(SkeletalMeshComp && "This shouldn't be null");
	return GetActorLocation();
}

bool APS_Character::IsDirFacingCharacterForward(const FVector & Direction) const
{
	return FVector::DotProduct(GetActorForwardVector(), Direction.GetSafeNormal()) > 0.0f;
}

void APS_Character::SetSpeedModifier(float NewSpeedModifier)
{
	SpeedModifier = NewSpeedModifier;
}

void APS_Character::SetRagdollStatus(bool EnableRagdolls)
{
	LocomotionController->SetRagdollStatus(EnableRagdolls);
}

void APS_Character::SetEnableCharacterMovementAndActions(bool EnableCharacter)
{
	SetEnableMovement(EnableCharacter);
	SetEnableActions(EnableCharacter);
}

void APS_Character::SetEnableMovement(bool EnableMovement)
{
	IsMovementEnabled = EnableMovement;
}

void APS_Character::SetEnableActions(bool EnableActions)
{
	if(AreActionsEnabled != EnableActions && InteractorComponent)
		InteractorComponent->SetInteractorEnabled(EnableActions);

	AreActionsEnabled = EnableActions;
}