#pragma once

#include "CoreMinimal.h"
#include "FSM_State.h"
#include "FSM.h"
#include "FSM_Interface.h"
#include "FSM_Structs.h"
#include "FSM_StateWithFSM.generated.h"

/*
	Base state class from which any BP FSM state should inherit.

	Allows for the creation of BP friendly states and FSMs that can be used
	for implementing FSM behaviour in BPs.

	Contrary to the FSM_State class, this class has a bigger overhead due to
	the use of Unreal's C++ UClass which only allows for allocation of objects
	in the heap. Nevertheless, it's a good option for low frequency code that won't
	affect performance much.
*/

class IFSM_Interface;

UCLASS(BlueprintType, Blueprintable, config = FSMState, meta = (ShortTooltip = "FSM State with a FSM embedded. Use it if you want to implement a FSM inside another FSM to hide complexity."))
class TPS_API UFSM_StateWithFSM : public UFSM_State, public FSM, public IFSM_Interface
{	
protected:

	GENERATED_BODY()

	// Initial state.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName StartState;

	// TMap with all the state classes that will be instantiated on Init.
	// Gets emptied once they get instantiated.
	UPROPERTY(EditAnywhere)
	TMap<FName, FStateData> StatesToInstantiate;

	// The actual array that holds all state classes. This array is marked as a UPROPERTY
	// and then passed to the FSM class. This prevents the GC from removing state UObjects.
	UPROPERTY()
	TArray<UFSM_State *> States;

	// If true, each time we enter the FSM, it gets reset.
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool ResetFSMOnEnter = false;

public:

	/* Construction. */
	UFSM_StateWithFSM() {}
	
	virtual ~UFSM_StateWithFSM(){}

	virtual void OnEnter_Implementation();

	virtual void Tick_Implementation(float DeltaTime);

	/* FSM Start functions. */

	// Starts the FSM.
	void Start() override;

	// Resumes the FSM at whatever state and time if it's stopped.
	void Resume() override;

	// Stops the FSM completely.
	void Stop() override;

	/* State behavior. */

	// State registering function. States stored inside the "States" array
	// will get registered automatically once the BeginPlay function is called.
	// This function is public in case someone fins it useful to register new states dynamically.
	void RegisterState(FName StateNameToRegister, int StateIntIDToRegister, UFSM_State * StateToRegister) override;

	/* Ticking functions.*/

	// Enables or disables the ticking behaviour of the FSM completely
	void EnableFSMTick(bool Enabled) override;

	// Allows states to indicate the FSM that they need ticking behaviour. This allows the FSM
	// to directly activate or deactivate it's ticking when it's needed to allow performance gains.
	// States that use the ticking function should set this function to true if they want to use.
	// States that don't, should set it to false to disable the FSM tick and help improve performance.
	// Attention: This behaviour doesn't directly disable the FSM ticking. If called during a transition,
	// tick won't be disabled until the transition ends.
	void SetStateNeedsTick(bool Enabled) override;

	/* State change functions. */

	// Changes the FSM to the new state.
	void ChangeToState(UFSM_State* StateToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false) override;

	void ChangeToStateByID(int StateIntIDToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false) override;

	void ChangeToStateByName(FName StateNameToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false) override;

	/* Getters. */

	UFSM_State * GetStateByID(int StateIntIDToCheck) override;

	UFSM_State * GetState(FName SubStateName) override;

	UFSM_State * GetCurrentState() override;

	UFSM_State * GetPreviousState() override;

	FName GetCurrentStateName() const override;

	int GetCurrentStateID() const override;

	FName GetPreviousStateName() const override;

	int GetPreviousStateID() const override;

	bool IsCurrentState(const UFSM_State* StateToCheck) const override;

	bool IsCurrentStateByID(int StateIntIDToCheck) const override;

	bool IsCurrentStateByName(FName SubStateName) const override;

	bool IsFSMTickEnabled() const override;

	bool IsInTransition() const override;

	AActor* GetOwnerActor() const override;
};