#pragma once

#include "CoreMinimal.h"
#include "FSM_Structs.h"

/*
	Finite State Machine.

	FSM component class written using Unreal's UClass system. Provides a BP ready FSM that can be implemented in
	both C++ and BP code.

	Due to the use of states deriving from UClass, all states in the machine are allocated in the heap and might be
	slightly less performant than UPS_FSMController. Nevertheless, it allows work in both C++ and BP providing easy
	manipulation, serialization, editor functionality and replication being much more suitable for a normal workflow.

	If FSMs are found to be a bottleneck, translate the code to PS_FSMController class.
*/

class IFSM_Interface;
class UFSM_State;

class TPS_API FSM
{

protected:

	/* Variables. */

	// The FSM interface of the UObject that owns this FSM. In general the UObject that owns this
	// FSM is a UActorComponent or FSM State. Apart from the interface, it can also reference the original
	// UObject with GetObject.
	TScriptInterface<IFSM_Interface> FSMOwnerInterface;

	// Array with pointers to all created states. This array is received from the child class inheriting from this.
	// The array should be marked as a UPROPERTY to prevent GC from removing the states UObjects after creation.
	TArray<UFSM_State *> * States;

	// TMap will all the states objects created for this FSM indexed by name,
	TMap<FName, UFSM_State *> StatesByName;

	// TMap will all the states objects created for this FSM indexed by int.
	// We use a TMap instead of an array in case not all positions where actually filled.
	TMap<int, UFSM_State *> StatesByID;

	// Current state the FSM is in.
	UFSM_State * CurrentState = nullptr;

	// Previous state the FSM was in.
	UFSM_State * PreviousState = nullptr;

	// Whether the current state is ticking or not.
	bool CurrentStateUsesTick = true;

	// Whether the FSM is ticking or not.
	// Responsibility of the inheriting class to avoid calling the Tick method of the FSM.
	bool FSM_Tick_Enabled = true;

	// Transitions.
	float TransitionTimeToNewState = 0.0f;
	float CurrentTimeToEndTransition = 0.0f;
	float TransitionFactor = 0.0f;

	FSM() {}

	// Initializes the FSM registering all defined states and setting the start state.
	void FSM_Init(TScriptInterface<IFSM_Interface> NewFSMOwnerInterface, FName StartState, TArray<UFSM_State*> & StatesArrayToUse, const TMap<FName, FStateData> & StatesToInstantiate);

	// Starts the FSM by going to the marked start state.
	void FSM_Start(const FName & StartState);

	/* FSM Start functions. */

	// Resumes the FSM at whatever state and time if it's stopped.
	void FSM_Resume();

	// Stops the FSM completely.
	void FSM_Stop();

	/* State behavior. */

	// State registering function. States stored inside the "States" array
	// will get registered automatically once the BeginPlay function is called.
	// This function is public in case someone fins it useful to register new states dynamically.
	void FSM_RegisterState(FName StateName, int StateIntID, UFSM_State * StateToRegister);

	/* Ticking functions.*/

	void Tick(float DeltaTime);

	// Enables or disables the ticking behavior of the FSM completely.
	// Can be overridden by inhering classes to actually disable any ticking functionality (like
	// actors or components).
	void FSM_EnableFSMTick(bool Enabled);

	// Allows states to indicate the FSM that they need ticking behavior. This allows the FSM
	// to directly activate or deactivate it's ticking when it's needed to allow performance gains.
	// States that use the ticking function should set this function to true if they want to use.
	// States that don't, should set it to false to disable the FSM tick and help improve performance.
	// Attention: This behavior doesn't directly disable the FSM ticking. If called during a transition,
	// tick won't be disabled until the transition ends.
	void FSM_SetStateNeedsTick(bool Enabled);

	/* State change functions. */

	// Changes the FSM to the new state.
	void FSM_ChangeToState(UFSM_State * StateToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false);

	void FSM_ChangeToState(int StateIntIDToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false);

	void FSM_ChangeToState(FName StateNameToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false);

	/* Getters. */

	UFSM_State * FSM_GetState(int StateIntID);

	UFSM_State * FSM_GetState(FName StateName);

	UFSM_State * FSM_GetCurrentState();

	UFSM_State * FSM_GetPreviousState();

	FName FSM_GetCurrentStateName() const;

	int FSM_GetCurrentStateID() const;

	FName FSM_GetPreviousStateName() const;

	int FSM_GetPreviousStateID() const;

	bool FSM_IsCurrentState(const UFSM_State * StateToCheck) const;

	bool FSM_IsCurrentState(int StateIntID) const;

	bool FSM_IsCurrentState(const FName & StateName) const;

	bool FSM_IsFSMTickEnabled() const;

	/* Transition behavior. */

	void FSM_SetTransition(float TransitionTime);

	void FSM_UpdateTransition(float dt);

	bool FSM_IsInTransition() const;
};
