#pragma once

#include "FSM_Structs.generated.h"

class UFSM_State;

/* Identifiers for a FSM state. */
USTRUCT(BlueprintType)
struct FStateData
{
	GENERATED_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int StateIntID; // An integer ID, used for enums.

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UFSM_State> StateClass; // The state class.
};
