#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FSM.h"
#include "FSM_Interface.h"
#include "FSM_Structs.h"
#include "FSM_Component.generated.h"

/*
	Finite State Machine Component.

	FSM component class written using Unreal's UClass system.
	
	Provides a BP ready FSM that can be implemented in both C++ and BP code.

	Provides easy manipulation, serialization, editor functionality and replication
	thanks to the use of Unreal's UClass API.

	One possible performance improvement if FSM's where deemed to b a bottleneck would be
	to translate code from Unreal's UClass system to pure C++ clases which would allow states
	to be directly allocated in the stack (Unreal's UClass are are forced to be allocated in the heap).

	To allow a comfortable experience when working with these classes in UEs workflow, code from UFSM_Component
	is also repeated in the FSM class, which is used by FSM states that support embedded FSMs.
	This is due to it not being possible to directly inherit from FSM in this case as we would have two different
	classes pointing to UObject.
*/

class UFSM_State;

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UFSM_Component : public UActorComponent, public FSM, public IFSM_Interface
{

protected:

	GENERATED_BODY()

	// Initial state.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName StartState;

	// TMap with all the state classes that will be instantiated on Init.
	// Gets emptied once they get instantiated.
	UPROPERTY(EditAnywhere)
	TMap<FName, FStateData> StatesToInstantiate;

	// The actual array that holds all state classes. This array is marked as a UPROPERTY
	// and then passed to the FSM class. This prevents the GC from removing state UObjects.
	UPROPERTY()
	TArray<UFSM_State *> States;

	virtual void BeginPlay() override;

public:

	UFSM_Component();

	/* FSM Start functions. */

	// Starts the FSM.
	void Start() override;

	// Resumes the FSM at whatever state and time if it's stopped.
	void Resume() override;

	// Stops the FSM completely.
	void Stop() override;

	/* State behavior. */

	// State registering function. States stored inside the "States" array
	// will get registered automatically once the BeginPlay function is called.
	// This function is public in case someone fins it useful to register new states dynamically.
	void RegisterState(FName StateName, int StateIntID, UFSM_State * StateToRegister) override;

	/* Ticking functions.*/

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;

	// Enables or disables the ticking behavior of the FSM completely
	void EnableFSMTick(bool Enabled) override;

	// Allows states to indicate the FSM that they need ticking behavior. This allows the FSM
	// to directly activate or deactivate it's ticking when it's needed to allow performance gains.
	// States that use the ticking function should set this function to true if they want to use.
	// States that don't, should set it to false to disable the FSM tick and help improve performance.
	// Attention: This function doesn't directly disable the FSM ticking. If called during a transition,
	// tick won't be disabled until the transition ends.
	void SetStateNeedsTick(bool Enabled) override;

	/* State change functions. */

	// Changes the FSM to the new state.
	virtual void ChangeToState(UFSM_State * StateToChangeTo, float TransitionTime = 0.0f, bool ReduceTransitionTimeIfReturningToPrevState = false) override;

	virtual void ChangeToStateByID(int StateIntIDToChangeTo, float TransitionTime = 0.0f, bool ReduceTransitionTimeIfReturningToPrevState = false) override;

	virtual void ChangeToStateByName(FName StateNameToChangeTo, float TransitionTime = 0.0f, bool ReduceTransitionTimeIfReturningToPrevState = false) override;

	/* Getters. */

	UFSM_State * GetStateByID(int StateIntID) override;

	UFSM_State * GetState(FName StateName) override;

	UFSM_State * GetCurrentState() override;

	UFSM_State * GetPreviousState() override;

	FName GetCurrentStateName() const override;

	int GetCurrentStateID() const override;

	FName GetPreviousStateName() const override;

	int GetPreviousStateID() const override;

	bool IsCurrentState(const UFSM_State* StateToCheck) const override;

	bool IsCurrentStateByID(int StateIntID) const override;

	bool IsCurrentStateByName(FName StateName) const override;

	bool IsFSMTickEnabled() const override;

	bool IsInTransition() const override;

	AActor * GetOwnerActor() const override;
};
