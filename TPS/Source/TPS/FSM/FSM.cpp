#include "FSM.h"
#include "FSM_State.h"
#include "FSM_Interface.h"

// Initializes the FSM registering all defined states and setting the start state.
void FSM::FSM_Init(TScriptInterface<IFSM_Interface> NewFSMOwnerInterface, FName StartState, TArray<UFSM_State*> & StatesArrayToUse, const TMap<FName, FStateData> & StatesToInstantiate)
{
	FSMOwnerInterface = NewFSMOwnerInterface;

	States = &StatesArrayToUse;

	States->Empty();
	StatesByName.Empty();
	StatesByID.Empty();

	for (const TPair<FName, FStateData> & StateObj : StatesToInstantiate)
	{
		UFSM_State * NewState = NewObject<UFSM_State>(FSMOwnerInterface.GetObject(), StateObj.Value.StateClass);
		FSM_RegisterState(StateObj.Key, StateObj.Value.StateIntID, NewState);
		NewState->Init();
	}
}

// Starts the FSM by going to the marked start state.
void FSM::FSM_Start(const FName & StartState)
{
	if (!StatesByName.Contains(StartState)) return;

	FSM_SetTransition(0.0f);

	PreviousState = nullptr;

	CurrentState = StatesByName[StartState];
	CurrentState->OnEnter();

	// Activate ticking behavior if the state uses it by default.
	FSM_SetStateNeedsTick(CurrentState->GetTickByDefault());
}

/* FSM Start functions. */

// Resumes the FSM at whatever state and time it as stopped.
void FSM::FSM_Resume()
{
	if (FSM_IsInTransition() || CurrentStateUsesTick)
		FSM_EnableFSMTick(true);
}


// Stops the FSM completely.
void FSM::FSM_Stop()
{
	FSM_EnableFSMTick(false);
}

/* State behavior. */

// State registering function. Called inside the FSM but is public in case
// someone would find it useful to register new states dynamically.
void FSM::FSM_RegisterState(FName StateName, int StateIntID, UFSM_State * StateToRegister)
{
	if (StateToRegister == nullptr) return;

	FName StateToRegisterName = StateToRegister->GetStateName();
	bool HasState = StatesByName.Contains(StateToRegisterName);
	if (HasState) return;

	States->Add(StateToRegister);
	StatesByName.Add(StateName, StateToRegister);
	StatesByID.Add(StateIntID, StateToRegister);

	StateToRegister->Bind(FSMOwnerInterface, StateIntID, StateName);
}

void FSM::Tick(float DeltaTime)
{
	if (!CurrentState) return;

	bool Transition = FSM_IsInTransition();

	if (!Transition && !CurrentStateUsesTick)
		FSM_EnableFSMTick(false);

	if (Transition)
		FSM_UpdateTransition(DeltaTime);

	CurrentState->Tick(DeltaTime);
}

// Enables or disables the ticking behavior of the FSM completely
void FSM::FSM_EnableFSMTick(bool Enabled)
{
	FSM_Tick_Enabled = Enabled;
}

// Allows states to indicate the FSM that they need ticking behavior. This allows the FSM
// to directly activate or deactivate it's ticking when it's needed to allow performance gains.
// States that use the ticking function should set this function to true if they want to use.
// States that don't, should set it to false to disable the FSM tick and help improve performance.
// Attention: This behavior doesn't directly disable the FSM ticking. If called during a transition,
// tick won't be disabled until the transition ends.
void FSM::FSM_SetStateNeedsTick(bool Enabled)
{
	CurrentStateUsesTick = Enabled;

	if (Enabled)
		FSM_EnableFSMTick(true);
}

// Changes the FSM to the new state.
void FSM::FSM_ChangeToState(UFSM_State * StateToChangeTo, float TransitionTime, bool ModifyTransitionTimeIfReturningToPrevState)
{
	// We don't allow changing to same state.
	if (StateToChangeTo == CurrentState)
		return;

	if (ModifyTransitionTimeIfReturningToPrevState) {
		if (StateToChangeTo == PreviousState)
			TransitionTime = TransitionFactor * TransitionTime;
	}

	FSM_SetTransition(TransitionTime);
	PreviousState = CurrentState;
	CurrentState = StateToChangeTo;

	if (PreviousState)
		PreviousState->OnExit();
	CurrentState->OnEnter();

	// Activate ticking behaviour if the state uses it by default or we have a transition.
	// Second case, once the transition ends, the ticks gets disabled.
	FSM_SetStateNeedsTick(CurrentState->GetTickByDefault());
}

void FSM::FSM_ChangeToState(int StateIntIDToChangeTo, float TransitionTime, bool ModifyTransitionTimeIfReturningToPrevState)
{
	if (StatesByID.Contains(StateIntIDToChangeTo))
		FSM_ChangeToState(StatesByID[StateIntIDToChangeTo], TransitionTime, ModifyTransitionTimeIfReturningToPrevState);
}

void FSM::FSM_ChangeToState(FName StateNameToChangeTo, float TransitionTime, bool ModifyTransitionTimeIfReturningToPrevState)
{
	if (StatesByName.Contains(StateNameToChangeTo))
		FSM_ChangeToState(StatesByName[StateNameToChangeTo], TransitionTime, ModifyTransitionTimeIfReturningToPrevState);
}

/* Getters. */

UFSM_State * FSM::FSM_GetState(int StateIntID)
{
	return StatesByID.Contains(StateIntID) ? StatesByID[StateIntID] : nullptr;
}

UFSM_State * FSM::FSM_GetState(FName StateName)
{
	return StatesByName.Contains(StateName) ? StatesByName[StateName] : nullptr;
}

UFSM_State * FSM::FSM_GetCurrentState()
{
	return CurrentState;
}

UFSM_State * FSM::FSM_GetPreviousState()
{
	return PreviousState;
}

FName FSM::FSM_GetCurrentStateName() const
{
	return CurrentState != nullptr ? CurrentState->GetStateName() : FName("");
}

int FSM::FSM_GetCurrentStateID() const
{
	return CurrentState != nullptr ? CurrentState->GetStateID() : 0;
}

FName FSM::FSM_GetPreviousStateName() const
{
	return PreviousState != nullptr ? PreviousState->GetStateName() : FName("");
}

int FSM::FSM_GetPreviousStateID() const
{
	return PreviousState != nullptr ? PreviousState->GetStateID() : 0;
}

bool FSM::FSM_IsCurrentState(const UFSM_State * StateToCheck) const
{
	return CurrentState == StateToCheck;
}

bool FSM::FSM_IsCurrentState(int StateIntID) const
{
	return CurrentState->GetStateID() == StateIntID;
}

bool FSM::FSM_IsCurrentState(const FName & StateName) const
{
	return CurrentState->GetStateName() == StateName;
}

bool FSM::FSM_IsFSMTickEnabled() const
{
	return FSM_Tick_Enabled;
}

/* Transition behavior. */

void FSM::FSM_SetTransition(float TransitionTime)
{
	TransitionTimeToNewState = TransitionTime;
	CurrentTimeToEndTransition = 0.0f;
	TransitionFactor = 0.0f;

	if (TransitionTimeToNewState > 0.0f)
		FSM_EnableFSMTick(true);
}

void FSM::FSM_UpdateTransition(float dt)
{
	CurrentTimeToEndTransition += dt;
	TransitionFactor = CurrentTimeToEndTransition / TransitionTimeToNewState;
	CurrentState->OnTransition(TransitionFactor);
}

bool FSM::FSM_IsInTransition() const
{
	return CurrentTimeToEndTransition < TransitionTimeToNewState;
}