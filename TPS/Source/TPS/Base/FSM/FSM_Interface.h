#pragma once

#include "CoreMinimal.h"
#include "FSM_Interface.generated.h"

/*
	Interface class that provides an API for all classes that inherit from FSM class and provide this type of behaviour.

	Allows state objects to directly call functions from the state machine without having to care if the state machine
	is implemented by a component actor or another class.
	
	Unreal doesn't allow multiple UObject inheritance, therefore we can't simply add all the blueprint callable functions
	directly in the FSM class. This interface allows us to circumvent this issue and provide the necessary functionality in BPs.
*/

class AActor;
class UFSM_State;
class UFSM_Component;

UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UFSM_Interface : public UInterface
{
	GENERATED_BODY()
};

class IFSM_Interface
{
	GENERATED_BODY()

public:

	/* FSM Start functions. */

	// Starts the FSM.
	UFUNCTION(BlueprintCallable)
	virtual void Start() = 0;

	// Resumes the FSM at whatever state and time if it's stopped.
	UFUNCTION(BlueprintCallable)
	virtual void Resume() = 0;

	// Stops the FSM completely.
	UFUNCTION(BlueprintCallable)
	virtual void Stop() = 0;

	/* State behavior. */

	// State registering function. States stored inside the "States" array
	// will get registered automatically once the BeginPlay function is called.
	// This function is public in case someone fins it useful to register new states dynamically.
	UFUNCTION(BlueprintCallable)
	virtual void RegisterState(FName StateName, int StateIntID, UFSM_State* StateToRegister) = 0;

	// Enables or disables the ticking behaviour of the FSM completely
	UFUNCTION(BlueprintCallable)
	virtual void EnableFSMTick(bool Enabled) = 0;

	// Allows states to indicate the FSM that they need ticking behaviour. This allows the FSM
	// to directly activate or deactivate it's ticking when it's needed to allow performance gains.
	// States that use the ticking function should set this function to true if they want to use.
	// States that don't, should set it to false to disable the FSM tick and help improve performance.
	// Attention: This behaviour doesn't directly disable the FSM ticking. If called during a transition,
	// tick won't be disabled until the transition ends.
	UFUNCTION(BlueprintCallable)
	virtual void SetStateNeedsTick(bool Enabled) = 0;

	/* State change functions. */

	// Changes the FSM to the new state.
	UFUNCTION(BlueprintCallable)
	virtual void ChangeToState(UFSM_State * StateToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false) = 0;

	UFUNCTION(BlueprintCallable)
	virtual void ChangeToStateByID(int StateIntIDToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false) = 0;

	UFUNCTION(BlueprintCallable)
	virtual void ChangeToStateByName(FName StateNameToChangeTo, float TransitionTime = 0.0f, bool ModifyTransitionTimeIfReturningToPrevState = false) = 0;

	/* Getters. */

	UFUNCTION(BlueprintCallable)
	virtual UFSM_State * GetStateByID(int StateIntID) = 0;

	UFUNCTION(BlueprintCallable)
	virtual UFSM_State * GetState(FName StateName) = 0;

	UFUNCTION(BlueprintCallable)
	virtual UFSM_State * GetCurrentState() = 0;

	UFUNCTION(BlueprintCallable)
	virtual UFSM_State * GetPreviousState() = 0;

	UFUNCTION(BlueprintCallable)
	virtual FName GetCurrentStateName() const = 0;

	UFUNCTION(BlueprintCallable)
	virtual int GetCurrentStateID() const = 0;

	UFUNCTION(BlueprintCallable)
	virtual FName GetPreviousStateName() const = 0;

	UFUNCTION(BlueprintCallable)
	virtual int GetPreviousStateID() const = 0;

	UFUNCTION(BlueprintCallable)
	virtual bool IsCurrentState(const UFSM_State* StateToCheck) const = 0;

	UFUNCTION(BlueprintCallable)
	virtual bool IsCurrentStateByID(int StateIntID) const = 0;

	UFUNCTION(BlueprintCallable)
	virtual bool IsCurrentStateByName(FName StateName) const = 0;

	UFUNCTION(BlueprintCallable)
	virtual bool IsFSMTickEnabled() const = 0;

	UFUNCTION(BlueprintCallable)
	virtual bool IsInTransition() const = 0;

	UFUNCTION(BlueprintCallable)
	virtual AActor* GetOwnerActor() const = 0;
};