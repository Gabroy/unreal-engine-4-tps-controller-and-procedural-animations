#include "FSM_Component.h"
#include "FSM_State.h"

UFSM_Component::UFSM_Component()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UFSM_Component::BeginPlay()
{
	UActorComponent::BeginPlay();

	TScriptInterface<IFSM_Interface> FSMInterfaceScript;
	FSMInterfaceScript.SetObject(this);
	FSMInterfaceScript.SetInterface(Cast<IFSM_Interface>(this));
	FSM_Init(FSMInterfaceScript, StartState, States, StatesToInstantiate);

	// Once we have initiated the FSM, we can remove this states if we want to.
	StatesToInstantiate.Empty();
}

/* FSM Start functions. */

// Starts the FSM.
void UFSM_Component::Start()
{
	FSM_Start(StartState);
}

// Resumes the FSM at whatever state and time if it was stopped.
void UFSM_Component::Resume()
{
	FSM_Resume();
	SetActive(true);
}

// Stops the FSM completely.
void UFSM_Component::Stop()
{
	FSM_Stop();
	SetActive(false);
}

/* State behavior. */

// State registering function.
void UFSM_Component::RegisterState(FName StateName, int StateIntID, UFSM_State * StateToRegister)
{
	FSM_RegisterState(StateName, StateIntID, StateToRegister);
}

/* Ticking functions.*/

void UFSM_Component::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	UActorComponent::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FSM::Tick(DeltaTime);
}

void UFSM_Component::EnableFSMTick(bool Enabled)
{
	FSM_EnableFSMTick(Enabled);
	SetComponentTickEnabled(Enabled);
}

void UFSM_Component::SetStateNeedsTick(bool Enabled)
{
	FSM_SetStateNeedsTick(Enabled);
}

/* State change functions. */

// Changes the FSM to the new state.
void UFSM_Component::ChangeToState(UFSM_State * StateToChangeTo, float TransitionTime, bool ReduceTransitionTimeIfReturningToPrevState)
{
	bool SuccessState = FSM_ChangeToState(StateToChangeTo, TransitionTime, ReduceTransitionTimeIfReturningToPrevState);
	if(SuccessState && OnChangedStateMulticast.IsBound())
		OnChangedStateMulticast.Broadcast(StateToChangeTo->GetStateName(), StateToChangeTo->GetStateID());
}

void UFSM_Component::ChangeToStateByID(int StateIntIDToChangeTo, float TransitionTime, bool ReduceTransitionTimeIfReturningToPrevState)
{
	bool SuccessState = FSM_ChangeToState(StateIntIDToChangeTo, TransitionTime, ReduceTransitionTimeIfReturningToPrevState);
	if (SuccessState && OnChangedStateMulticast.IsBound())
		OnChangedStateMulticast.Broadcast(CurrentState->GetStateName(), StateIntIDToChangeTo);
}

void UFSM_Component::ChangeToStateByName(FName StateNameToChangeTo, float TransitionTime, bool ReduceTransitionTimeIfReturningToPrevState)
{
	bool SuccessState = FSM_ChangeToState(StateNameToChangeTo, TransitionTime, ReduceTransitionTimeIfReturningToPrevState);
	if (SuccessState && OnChangedStateMulticast.IsBound())
		OnChangedStateMulticast.Broadcast(StateNameToChangeTo, CurrentState->GetStateID());
}

/* Getters. */

UFSM_State * UFSM_Component::GetStateByID(int StateIntID)
{
	return FSM_GetState(StateIntID);
}

UFSM_State * UFSM_Component::GetState(FName StateName)
{
	return FSM_GetState(StateName);
}

UFSM_State * UFSM_Component::GetCurrentState()
{
	return FSM_GetCurrentState();
}

UFSM_State * UFSM_Component::GetPreviousState()
{
	return FSM_GetPreviousState();
}

FName UFSM_Component::GetCurrentStateName() const
{
	return FSM_GetCurrentStateName();
}

int UFSM_Component::GetCurrentStateID() const
{
	return FSM_GetCurrentStateID();
}

FName UFSM_Component::GetPreviousStateName() const
{
	return FSM_GetPreviousStateName();
}

int UFSM_Component::GetPreviousStateID() const
{
	return FSM_GetPreviousStateID();
}

bool UFSM_Component::IsCurrentState(const UFSM_State * StateToCheck) const
{
	return FSM_IsCurrentState(StateToCheck);
}

bool UFSM_Component::IsCurrentStateByID(int StateIntID) const
{
	return FSM_IsCurrentState(StateIntID);
}

bool UFSM_Component::IsCurrentStateByName(FName StateName) const
{
	return FSM_IsCurrentState(StateName);
}

bool UFSM_Component::IsFSMTickEnabled() const
{
	return FSM_IsFSMTickEnabled();
}

bool UFSM_Component::IsInTransition() const
{
	return FSM_IsInTransition();
}

AActor * UFSM_Component::GetOwnerActor() const
{
	return GetOwner();
}