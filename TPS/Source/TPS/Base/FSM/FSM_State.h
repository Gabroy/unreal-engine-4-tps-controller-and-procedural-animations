#pragma once

#include "CoreMinimal.h"
#include "FSM_State.generated.h"

/*
	Base state class from which any BP FSM state should inherit.

	Allows for the creation of BP friendly states and FSMs that can be used
	for implementing FSM behaviour in BPs.

	Contrary to the FSM_State class, this class has a bigger overhead due to
	the use of Unreal's C++ UClass which only allows for allocation of objects
	in the heap. Nevertheless, it's a good option for low frequency code that won't
	affect performance much.
*/

class IFSM_Interface;

UCLASS(BlueprintType, Blueprintable, config = FSMState, meta = (ShortTooltip = "FSM Base state. Inherit from it to implement a FSM state with custom behaviour."))
class TPS_API UFSM_State : public UObject
{
protected:

	GENERATED_BODY()

	// FSM the state belongs to.
	UPROPERTY(BlueprintReadOnly)
	TScriptInterface<IFSM_Interface> FSMOwner;

	// State integer ID.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int StateIntID;

	// Name of the state. Used as ID.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName StateName;

	// Controls whether this state uses it's ticking function.
	// If true, ticking is activated each time the state OnEnter function is called.
	// Otherwise, ticking is disabled by default and it's the coder responsibility
	// to use EnableStateTick if it wants to activate it inside this state.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool TickByDefault = true;

public:
	
	/* Construction. */
	UFSM_State() : UObject(){}
	
	virtual ~UFSM_State(){}

public:

	/* Bind state to FSM. Called when the state gets registered in it's FSM. */
	void Bind(TScriptInterface<IFSM_Interface> NewFSMOwner, int NewStateIntID, const FName & NewStateName);

	/* Called at the beginning of begin play. Allows configurating the state with any
	necessary values. */
	
	UFUNCTION(BlueprintNativeEvent)
	void Init();
	virtual void Init_Implementation() {}

	/* Methods. */

	// Called when we enter the state on a state change.
	UFUNCTION(BlueprintNativeEvent)
	void OnEnter();
	virtual void OnEnter_Implementation() {}

	// Called when we exit the state on a state change.
	UFUNCTION(BlueprintNativeEvent)
	void OnExit();
	virtual void OnExit_Implementation() {}

	// Called while we are transitioning to the state (if there is a transition time).
	// If no transition, this function won't be called.
	// Transition Factor goes from 0.0f (just started) to 1.0f (full transition).
	UFUNCTION(BlueprintNativeEvent)
	void OnTransition(float TransitionFactor);
	virtual void OnTransition_Implementation(float TransitionFactor) {}

	// Tick function.
	UFUNCTION(BlueprintNativeEvent)
	void Tick(float DeltaTime);
	virtual void Tick_Implementation(float DeltaTime) {}

	// Activates or deactivates ticking of the FSM.
	// The state must be the current state for this command to work.
	// Furthermore, if the state is transitioning and ticking is to be disabled,
	// it won't be deactivated until the transition ends.
	UFUNCTION(BlueprintCallable)
	void EnableStateTick(bool Enable);

	/* Getters. */

	// Returns the FSM this state belongs to.
	UFUNCTION(BlueprintCallable)
	TScriptInterface<IFSM_Interface> GetFSMOwner() { return FSMOwner; }

	// Returns the actor that owns the FSM that owns this state.
	UFUNCTION(BlueprintCallable)
	AActor * GetOwnerActor();

	int GetStateID() const { return StateIntID; }

	FName GetStateName() const { return StateName; }

	bool GetTickByDefault() const { return TickByDefault; }
};