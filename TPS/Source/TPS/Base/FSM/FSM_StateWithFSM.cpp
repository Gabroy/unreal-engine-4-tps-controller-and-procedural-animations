#include "FSM_StateWithFSM.h"

void UFSM_StateWithFSM::OnEnter_Implementation()
{
	if (ResetFSMOnEnter) {
		TScriptInterface<IFSM_Interface> FSMInterfaceScript;
		FSMInterfaceScript.SetObject(this);
		FSMInterfaceScript.SetInterface(Cast<IFSM_Interface>(this));
		FSM::FSM_Init(FSMInterfaceScript, StartState, States, StatesToInstantiate);
	}
}

void UFSM_StateWithFSM::Tick_Implementation(float DeltaTime)
{
	// Update the FSM if we have one.
	FSM::Tick(DeltaTime);
}

/* FSM Start functions. */

// Starts the FSM.
void UFSM_StateWithFSM::Start()
{
	FSM_Start(StartState);
}

// Resumes the FSM at whatever state and time if it was stopped.
void UFSM_StateWithFSM::Resume()
{
	FSM_Resume();
}

// Stops the FSM completely.
void UFSM_StateWithFSM::Stop()
{
	FSM_Stop();
}

/* State behavior. */

// State registering function.
void UFSM_StateWithFSM::RegisterState(FName StateNameToRegister, int StateIntIDToRegister, UFSM_State* StateToRegister)
{
	FSM_RegisterState(StateNameToRegister, StateIntIDToRegister, StateToRegister);
}

/* Ticking functions.*/

void UFSM_StateWithFSM::EnableFSMTick(bool Enabled)
{
	FSM_EnableFSMTick(Enabled);
}

void UFSM_StateWithFSM::SetStateNeedsTick(bool Enabled)
{
	FSM_SetStateNeedsTick(Enabled);
}

/* State change functions. */

// Changes the FSM to the new state.
void UFSM_StateWithFSM::ChangeToState(UFSM_State* StateToChangeTo, float TransitionTime, bool ModifyTransitionTimeIfReturningToPrevState)
{
	FSM_ChangeToState(StateToChangeTo, TransitionTime, ModifyTransitionTimeIfReturningToPrevState);
}

void UFSM_StateWithFSM::ChangeToStateByID(int StateIntIDToChangeTo, float TransitionTime, bool ModifyTransitionTimeIfReturningToPrevState)
{
	FSM_ChangeToState(StateIntIDToChangeTo, TransitionTime, ModifyTransitionTimeIfReturningToPrevState);
}

void UFSM_StateWithFSM::ChangeToStateByName(FName StateNameToChangeTo, float TransitionTime, bool ModifyTransitionTimeIfReturningToPrevState)
{
	FSM_ChangeToState(StateNameToChangeTo, TransitionTime, ModifyTransitionTimeIfReturningToPrevState);
}

/* Getters. */

UFSM_State* UFSM_StateWithFSM::GetStateByID(int StateIntIDToCheck)
{
	return FSM_GetState(StateIntIDToCheck);
}

UFSM_State * UFSM_StateWithFSM::GetState(FName SubStateName)
{
	return FSM_GetState(SubStateName);
}

UFSM_State* UFSM_StateWithFSM::GetCurrentState()
{
	return FSM_GetCurrentState();
}

int UFSM_StateWithFSM::GetCurrentStateID() const
{
	return FSM_GetCurrentStateID();
}

UFSM_State* UFSM_StateWithFSM::GetPreviousState()
{
	return FSM_GetPreviousState();
}

int UFSM_StateWithFSM::GetPreviousStateID() const
{
	return FSM_GetPreviousStateID();
}

FName UFSM_StateWithFSM::GetCurrentStateName() const
{
	return FSM_GetCurrentStateName();
}

FName UFSM_StateWithFSM::GetPreviousStateName() const
{
	return FSM_GetPreviousStateName();
}

bool UFSM_StateWithFSM::IsCurrentState(const UFSM_State* StateToCheck) const
{
	return FSM_IsCurrentState(StateToCheck);
}

bool UFSM_StateWithFSM::IsCurrentStateByID(int StateIntIDToCheck) const
{
	return FSM_IsCurrentState(StateIntIDToCheck);
}

bool UFSM_StateWithFSM::IsCurrentStateByName(FName SubStateName) const
{
	return FSM_IsCurrentState(SubStateName);
}

bool UFSM_StateWithFSM::IsFSMTickEnabled() const
{
	return FSM_IsFSMTickEnabled();
}

bool UFSM_StateWithFSM::IsInTransition() const
{
	return FSM_IsInTransition();
}

AActor * UFSM_StateWithFSM::GetOwnerActor() const
{
	return GetOwnerActor();
}