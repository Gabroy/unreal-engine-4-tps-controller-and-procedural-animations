#include "FSM_State.h"
#include "FSM.h"
#include "FSM_Interface.h"

void UFSM_State::Bind(TScriptInterface<IFSM_Interface> NewFSMOwner, int NewStateIntID, const FName & NewStateName)
{
	FSMOwner = NewFSMOwner;
	StateIntID = NewStateIntID;
	StateName = NewStateName;
}

void UFSM_State::EnableStateTick(bool Enable)
{
	if (FSMOwner->IsCurrentState(this))
		FSMOwner->SetStateNeedsTick(Enable);
}

AActor * UFSM_State::GetOwnerActor()
{
	return FSMOwner->GetOwnerActor();
}