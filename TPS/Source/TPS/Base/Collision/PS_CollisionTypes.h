#pragma once

#include "Engine/EngineTypes.h"

// Defines from EngineTypes.h ECollisionChannel enum.

#define PS_ECC_Vaultable ECC_GameTraceChannel1
#define PS_ECC_Climbable ECC_GameTraceChannel2
#define PS_ECC_Interactuable ECC_GameTraceChannel3
#define PS_ECC_Object ECC_GameTraceChannel4
#define PS_ECC_SkeletalMesh ECC_GameTraceChannel5
#define PS_ECC_StaticCollisions ECC_GameTraceChannel6
#define PS_ECC_DynamicCollisions ECC_GameTraceChannel7