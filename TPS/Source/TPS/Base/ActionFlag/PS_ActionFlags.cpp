#include "PS_ActionFlags.h"

// Call this when you want cooldown to get activated. Normally after you execute the action.
void FAction_Flags::StartCooldown(UWorld* World)
{
	if (World->GetTimerManager().IsTimerPending(CooldownTimer))
		World->GetTimerManager().ClearTimer(CooldownTimer);

	// Reset cooldown flag.
	FTimerDelegate ActionCooldownDelegate;
	ActionCooldownDelegate.BindLambda([this]
	{
		CooldownTimer.Invalidate();
	});

	// Set cooldown count.
	World->GetTimerManager().SetTimer(CooldownTimer, ActionCooldownDelegate, CooldownActionTime, false);
}

bool FAction_Flags::IsCooldownFinished()
{
	return !CooldownTimer.IsValid();
}


// Call this and initialize the action, pass the world and delegates for what to call when the action is pressed enough time or for when time out happens.
bool FActionWithTimeWindow::Init(UWorld * World, FDelegateAction const& NewOnPressedActionEnoughTimes)
{
	WorldPtr = World;
	if (!WorldPtr)
		return false;

	OnPressedActionEnoughTimes = NewOnPressedActionEnoughTimes;

	return true;
}

// Call this and initialize the action, pass the world and delegates for what to call when the action is pressed enough time or for when time out happens.
bool FActionWithTimeWindow::Init(UWorld * World, FDelegateAction const & NewOnPressedActionEnoughTimes, FDelegateAction const & NewTimeOutDelegate)
{
	WorldPtr = World;
	if (!WorldPtr)
		return false;

	OnPressedActionEnoughTimes = NewOnPressedActionEnoughTimes;
	OnTimeOutDelegate = NewTimeOutDelegate;

	return true;
}

// Called whenever time out is called.
void FActionWithTimeWindow::StartTimeOut()
{
	if (!WorldPtr)
		return;

	FTimerManager & TimeManager = WorldPtr->GetTimerManager();
	TimeManager.ClearTimer(TimerHandle);

	FTimerDelegate TimeOutDelegate;
	TimeOutDelegate.BindLambda([this]
	{
		CancelAction(true);
	});
	TimeManager.SetTimer(TimerHandle, TimeOutDelegate, TimeToCancelCountOnPressed, false);
}

// Call this when you press the action. If the action is completed, it will call the OnPressedEnoughTimes delegate automatically.
void FActionWithTimeWindow::PressAction()
{
	CurrentNumberOfTimesPressed++;
	if (CurrentNumberOfTimesPressed >= NumTimesPressedToActivate)
	{
		OnPressedActionEnoughTimes.ExecuteIfBound();
		CancelAction();
	}
	else if (ResetTimeToCancelActionOnActionPress || !TimerHandle.IsValid())
	{
		StartTimeOut();
	}
}

// Call this if you want to cancel the action for any reason. You can pass a bool set to true if you want to call the timeout function.
void FActionWithTimeWindow::CancelAction(bool CallTimeOutFunction)
{
	CurrentNumberOfTimesPressed = 0;
	if (!WorldPtr)
		return;

	if (CallTimeOutFunction)
		OnTimeOutDelegate.ExecuteIfBound();

	WorldPtr->GetTimerManager().ClearTimer(TimerHandle);
}