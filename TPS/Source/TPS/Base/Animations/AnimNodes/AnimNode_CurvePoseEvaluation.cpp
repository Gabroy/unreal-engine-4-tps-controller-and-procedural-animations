#pragma once

#include "AnimNode_CurvePoseEvaluation.h"
#include "Animation/AnimNodeBase.h"
#include "AnimInstanceProxy.h"
#include "BinarySearch.h"

void FAnimNode_CurvePoseEvaluation::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Initialize_AnyThread)

	FAnimNode_Base::Initialize_AnyThread(Context);

	FetchCurveUID(Context.AnimInstanceProxy);
}

void FAnimNode_CurvePoseEvaluation::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(CacheBones_AnyThread)
}

void FAnimNode_CurvePoseEvaluation::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Update_AnyThread)

	GetEvaluateGraphExposedInputs().Execute(Context);

	FetchCurveUID(Context.AnimInstanceProxy);
}

void FAnimNode_CurvePoseEvaluation::Evaluate_AnyThread(FPoseContext& Output)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)

	if (!AnimSequence)
		return;

	const FRawCurveTracks& CurveTracksData = AnimSequence->GetCurveData();

	const FFloatCurve * CurveAsFloat = static_cast<const FFloatCurve*>(CurveTracksData.GetCurveData(CurveUID));
	if (!CurveAsFloat)
		return;

	const FRichCurve & CurveData = CurveAsFloat->FloatCurve;
	if (CurveData.Keys.Num() < 1)
		return;

	/* Extract the X value (time the anim should play) based on the Y value of the curve (distance to target). */

	// Get closest future key, we check for value that is closer in the future to the current one.
	int FutureKeyIndx = CurveData.Keys.Num() - 1;
	for (int i = 0; i < CurveData.Keys.Num(); ++i)
	{
		if (CurveValue <= CurveData.Keys[i].Value)
		{
			FutureKeyIndx = i;
			break;
		}
	}

	// If we get a value that is smaller or equal to first key, we use 0.0f as time to play the anim at.
	// Otherwise, we interpolate between the current keyframe (previous one to FutureKeyIndx) and the FutureKeyIndx value.
	float TimeToPlayAnim = 0.0f;
	if (FutureKeyIndx != 0)
	{
		const FRichCurveKey& CurrentKey = CurveData.Keys[FutureKeyIndx - 1];
		const FRichCurveKey& FutureKey = CurveData.Keys[FutureKeyIndx];

		float InterpFactor = FMath::GetMappedRangeValueClamped(FVector2D(CurrentKey.Value, FutureKey.Value), FVector2D(0.0f, 1.0f), CurveValue);
		TimeToPlayAnim = CurrentKey.Time * (1.0f - InterpFactor) + FutureKey.Time * InterpFactor;
	}

	AnimSequence->GetAnimationPose(Output.Pose, Output.Curve, FAnimExtractContext(TimeToPlayAnim, Output.AnimInstanceProxy->ShouldExtractRootMotion()));

}

void FAnimNode_CurvePoseEvaluation::GatherDebugData(FNodeDebugData & DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);
	DebugData.AddDebugItem(DebugLine);
}

void FAnimNode_CurvePoseEvaluation::FetchCurveUID(FAnimInstanceProxy * AnimInstanceProxy)
{
	if (CurrentSequence == AnimSequence)
		return;

	USkeleton * Skeleton = AnimInstanceProxy->GetSkeleton();
	if (!Skeleton)
		return;

	const FSmartNameMapping* NameMapping = Skeleton->GetSmartNameContainer(USkeleton::AnimCurveMappingName);
	if (!NameMapping->Exists(CurveName))
		return;

	if (!Skeleton->IsCompatible(AnimSequence->GetSkeleton()))
		return;

	CurrentSequence = AnimSequence;
	CurveUID = NameMapping->FindUID(CurveName);
}