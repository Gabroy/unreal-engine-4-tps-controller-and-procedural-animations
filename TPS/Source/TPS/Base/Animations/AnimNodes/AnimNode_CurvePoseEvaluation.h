#pragma once

#include "Animation/AnimNodeBase.h"
#include "AnimNode_HelperStructs.h"
#include "AnimNode_CurvePoseEvaluation.generated.h"

struct FAnimInstanceProxy;

/*
* This node uses an animation curve whose x value is, by implementation, the keyframe of the animation and
* a y value that should be unique for each keyframe and is used to find the correct keyframe to play the animation at.
* It can be used to implement Distance Matching by using the distance to the goal (or a normalized factor) as y value
* and then passing it to the node as CurveValue so it returns the time to play the animation at and therefore, the animation
* played is directly ruled by the character locomotion. Like an inverse root motion.
*/

USTRUCT(BlueprintType)
struct TPS_API FAnimNode_CurvePoseEvaluation : public FAnimNode_Base
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinShownByDefault))
	UAnimSequenceBase * AnimSequence;

	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinShownByDefault))
	FName CurveName;

	UPROPERTY(EditAnywhere, Category = Settings, meta = (PinShownByDefault))
	float CurveValue = 0.0f;

	// Stores the current anim being played, if we detect the sequence changes, we change this value.
	UAnimSequenceBase * CurrentSequence = nullptr;

	USkeleton::AnimCurveUID CurveUID;

public:
	FAnimNode_CurvePoseEvaluation() : FAnimNode_Base() {}

	// Initializes the AnimNode. Whenever we need to Initialize/Reinitialize (when changing mesh for instance). 
	virtual void Initialize_AnyThread(const FAnimationInitializeContext & Context) override;

	// Caches bones that needs to be tracked by this animation node.
	// Used for refreshing bone indices that are referenced by the node.
	virtual void CacheBones_AnyThread(const FAnimationCacheBonesContext & Context) override;

	// Called to update current state(such as advancing play time or updating blend weights).
	// This function takes an FAnimationUpdateContext that knows the DeltaTime for the
	// update and the current nodes blend weight.
	virtual void Update_AnyThread(const FAnimationUpdateContext & Context) override;

	// Called to generate a �pose� (list of bone transforms). 
	virtual void Evaluate_AnyThread(FPoseContext&  Output) override;
	
	// Used for debugging using "ShowDebug Animation" data
	virtual void GatherDebugData(FNodeDebugData & DebugData) override;

private:

	void FetchCurveUID(FAnimInstanceProxy * AnimInstanceProxy);
};