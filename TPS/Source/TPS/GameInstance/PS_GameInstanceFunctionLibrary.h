#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PS_GameInstanceFunctionLibrary.generated.h"

class UPS_GameInstance;
class UPS_ItemManager;

/*
* GameInstance function library for the TPS project.
* 
* Provides easy access to game instance functionality without having to perform multiple calls.
* Mostly used for ease of use and avoid repetition of codes.
*/

UCLASS(BlueprintType)
class TPS_API UPS_GameInstanceFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
		
	// Returns the game GameInstance.
	static UPS_GameInstance * GetPSGameInstance(const UObject * WorldContextObject);

	// Returns the Item Manager.
	static UPS_ItemManager * GetItemManagerInstance(const UObject* WorldContextObject);
};
