#pragma once

#include "CoreMinimal.h"

#include "UniquePtr.h"
#include "Managers/Base/PS_BaseManager.h"

#include "Engine/GameInstance.h"
#include "PS_GameInstance.generated.h"

class UPS_ItemManager;

/*
*	Macros for registering and unregistering new modules.
*
*	Register modules inside the GameInstance Init function before InitializeManagers is called.
*/

#define REGISTER_GAMEINSTANCE_MODULE(GameInstance, ModuleType, ModuleName)										\
ModuleType * NewManager = new ModuleType();																		\
NewManager->SetManagerName(ModuleName);																			\
GameInstance->RegisterManager((IBaseManager *)NewManager);

#define UNREGISTER_GAMEINSTANCE_MODULE_BYNAME(GameInstance, ModuleName) GameInstance->UnregisterManager(GameInstance->GetManagerByName(ModuleName));

#define UNREGISTER_GAMEINSTANCE_MODULE_BYTYPE(GameInstance, ModuleType) GameInstance->UnregisterManager(GameInstance->GetManagerByType(ModuleType::GetManagerType()));

/* Use this two macros to declare and define the getter for the module.
For declare macro, add the module type as a forward declaration at the beginning of the header.
For the define macro, include the module header file in the cpp. */
#define DECLARE_GAMEINSTANCE_MODULE_GETTER(ModuleType, ModuleName)							\
ModuleType * Get##ModuleName##();

#define DEFINE_GAMEINSTANCE_MODULE_GETTER(ModuleType, ModuleName)							\
ModuleType * UPS_GameInstance::Get##ModuleName##() { return (ModuleType *)GetManagerByType(ModuleType::GetManagerType()); }

/*
* GameInstance for the TPS project.
* 
* GameInstances are unique per game and exist from game creation until shutdown.
* 
* They can be used to store data that should exist during the whole game lifescope,
* preferring gamemodes for gamemode specific data.
*/

UCLASS(BlueprintType)
class TPS_API UPS_GameInstance : public UGameInstance
{
	GENERATED_BODY()


	/* Variables */

	// An array with all game managers present during the whole life scope of the game instance.
	// Managers should be registered on Init before InitializeManagers method is called.
	// Managers can be retrieved by their template type O(1) or by querying their name O(n).
	TArray<TUniquePtr<IBaseManager>> GameManagers;

public:
		
	// Perform initialization once the game instance starts.
	virtual void Init();

	// Perform cleaning up once the game instance is shutdown.
	virtual void Shutdown();

	/* Managers methods */

	void InitializeManagers();

	void ShutdownManagers();

	void RegisterManager(IBaseManager * ManagerToRegister);
	
	void UnregisterManager(IBaseManager * ManagerToRegister);

	IBaseManager * GetManagerByType(ManagerTypeID TypeID);

	IBaseManager * GetManagerByName(const FName & ManagerName);

	DECLARE_GAMEINSTANCE_MODULE_GETTER(UPS_ItemManager, ItemManager)
};
