#pragma once

#include "Base/PS_BaseManager.h"
#include "Base/UUID/PS_SimpleUUID.h"

class UPS_ItemManager : public BaseManager<UPS_ItemManager>
{
	TSharedPtr<SimpleUUIDGenerator> ItemIDGenerator;

public:

	virtual ~UPS_ItemManager() {}

	virtual void Init() override;

	virtual void Shutdown() override;

	// Returns a new UUID to be used for items. Keep the handle stored with the item.
	// Once the item is destroyed the UUID will be freed automatically.
	UUIDHandle GenerateNewUUID();

	// Returns a UUID handle from a UUID value.
	// To be used if you are supporting loading of objects from serialized data. In such case,
	// pass the serialized UUID to this method to retrieve the handle again.
	UUIDHandle GenerateUUIDFromValue(SimpleUUID UUIDToGenerateFrom);
};
