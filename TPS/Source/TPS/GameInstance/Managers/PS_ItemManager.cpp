#include "PS_ItemManager.h"

void UPS_ItemManager::Init()
{
	// Initiate UUID generator for items.
	//SimpleUUIDGenerator * SimpleGenerator = new SimpleUUIDGenerator();
	//ItemIDGenerator = TSharedPtr<SimpleUUIDGenerator>(SimpleGenerator);

	// If we were to support savegames, here we would pass the last item value generated
	// by the UUID generator so the generator would keep returning UUIDs from the last one created.
	//SimpleUUID StartingUUID = SimpleUUIDGenerator::InvalidUUID;
	//SimpleGenerator->Init(ItemIDGenerator);
}

void UPS_ItemManager::Shutdown()
{
	if (ItemIDGenerator.IsValid())
	{
		ItemIDGenerator.Get()->Shutdown();
	}
}

UUIDHandle UPS_ItemManager::GenerateNewUUID()
{
	if (ItemIDGenerator.IsValid())
	{
		return ItemIDGenerator.Get()->GenerateNewUUID();
	}

	return UUIDHandle();
}

UUIDHandle UPS_ItemManager::GenerateUUIDFromValue(SimpleUUID UUIDToGenerateFrom)
{
	if (ItemIDGenerator.IsValid())
	{
		return ItemIDGenerator.Get()->GenerateUUIDFromValue(UUIDToGenerateFrom);
	}

	return UUIDHandle();
}