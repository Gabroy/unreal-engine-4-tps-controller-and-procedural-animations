#pragma once

typedef int32 ManagerTypeID;

/*
* Counter class used to keep a counter that returns a compile time value
* for each type of manager that inherits from it. This value is used for identification
* and retrieval.
*/
struct ManagerTypesCounter
{
	// Counter that returns a unique type ID per type of Base Manager
	static ManagerTypeID TypeIDCounter;
};
ManagerTypeID ManagerTypesCounter::TypeIDCounter = 0;

/*
* Basic Manager from which all game managers will inherit.
* 
* While not enforced in code, managers should be created by either the GameInstance or Gamemodes depending on how
* long is their lifescope.
* 
* The interface below provides generic functionality so all managers can be easily registred, loaded, started and closed automatically.
* Coders should inherit from the class UPS_BaseManager which implements this interface and requires as template argument the child class
* it's implementing the manager.
*/

class IBaseManager : public ManagerTypesCounter
{

	// A simple name for identification of the base manager.
	FName ManagerName;

public:

	IBaseManager() : ManagerName() {}

	virtual ~IBaseManager() {}

	// Should be called when initializing the manager. Use it to set up any necessary data.
	virtual void Init() = 0;

	// Should be called when shutting down the manager. Use it to shutdown the game.
	virtual void Shutdown() = 0;

	FName GetManagerName() const { return ManagerName; }

	void SetManagerName(const FName & NewManagerName) { ManagerName = NewManagerName; }

	// Returns a unique integer for type of manager. Each manager that inherits from this class
	// will have their own unique ID.
	static ManagerTypeID GetManagerType()
	{
		static ManagerTypeID ManagerID = ManagerTypesCounter::TypeIDCounter++;
		return ManagerID;
	}
};

// The actual base class all should inherit from.
template<typename ManagerType>
class BaseManager : public IBaseManager
{

public:

	virtual ~BaseManager() {}
};
