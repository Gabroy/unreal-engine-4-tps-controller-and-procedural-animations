#include "PS_SimpleUUIDStructs.h"

/* UUID Handle */

UUIDHandle::UUIDHandle() {}

UUIDHandle::UUIDHandle(SimpleUUID NewHandleUUID, TWeakPtr<SimpleUUIDGenerator>& NewUUIDGenerator)
	: UUIDGenerator(NewUUIDGenerator), HandleUUID(NewHandleUUID), ShouldFreeHandleOnDestruction(true) {}

UUIDHandle::UUIDHandle(UUIDHandle& CopyObj)
{
	UUIDGenerator = CopyObj.UUIDGenerator;
	HandleUUID = CopyObj.HandleUUID;
	CopyObj.SetShouldFreeHandleOnDestruction(false);
}

UUIDHandle::UUIDHandle(const UUIDHandle& CopyObj)
{
	ensure("You can't copy a UUIDHandle from a constant object. Either convert the object to not constant or don't perform copy." && false);
}

UUIDHandle::~UUIDHandle()
{
	FreeHandle();
}

SimpleUUID UUIDHandle::GetUUID() const
{
	return HandleUUID;
}

void UUIDHandle::FreeHandle()
{
	if (ShouldFreeHandleOnDestruction && IsUUIDValid() && UUIDGenerator.IsValid())
	{
		TSharedPtr<SimpleUUIDGenerator> SharedPtrGenerator = UUIDGenerator.Pin();
		if (SharedPtrGenerator.IsValid())
		{
			SharedPtrGenerator->FreeUUID(HandleUUID);
			HandleUUID = SimpleUUIDGenerator::InvalidUUID;
			ShouldFreeHandleOnDestruction = false;
		}
	}
}

bool UUIDHandle::IsUUIDValid() const
{
	return HandleUUID != SimpleUUIDGenerator::InvalidUUID;
}

void UUIDHandle::SetShouldFreeHandleOnDestruction(bool FreeOnDestruction)
{
	ShouldFreeHandleOnDestruction = FreeOnDestruction;
}