#include "PS_SimpleUUID.h"

/* Simple UUID Generator */

void SimpleUUIDGenerator::Init(TWeakPtr<SimpleUUIDGenerator> NewWeakPtrToGenerator, SimpleUUID StartingUUIDValue)
{
	Counter = StartingUUIDValue;
	UsedIDS.Empty();
	WeakPtrToGenerator = NewWeakPtrToGenerator;
}

void SimpleUUIDGenerator::Shutdown()
{

}

UUIDHandle SimpleUUIDGenerator::GenerateNewUUID()
{
	if (UsedIDS.Num() == TNumericLimits<uint32>::Max())
	{
		UE_LOG(LogTemp, Warning, TEXT("SimpleUUIDGenerator::GenerateUUID All IDs are being used, this should never happen!"));
		ensure(false && "SimpleUUIDGenerator::GenerateUUID All IDs are being used, this should never happen!");
		return UUIDHandle();
	}

	Counter++;
	while (Counter == 0 || UsedIDS.Contains(Counter))
	{
		UE_LOG(LogTemp, Warning, TEXT("SimpleUUIDGenerator::Looping to return a new unused ID: %d"), Counter);
		Counter++;
	}

	UsedIDS.Add(Counter);
	return UUIDHandle(Counter, WeakPtrToGenerator);
}

UUIDHandle SimpleUUIDGenerator::GenerateUUIDFromValue(SimpleUUID UUIDToGenerateFrom)
{
	if (UsedIDS.Num() == TNumericLimits<uint32>::Max())
	{
		UE_LOG(LogTemp, Warning, TEXT("SimpleUUIDGenerator::GenerateUUIDFromValue All IDs are being used, this should never happen!"));
		ensure(false && "SimpleUUIDGenerator::GenerateUUIDFromValue All IDs are being used, this should never happen!");
		return UUIDHandle();
	}

	if (UsedIDS.Contains(UUIDToGenerateFrom))
	{
		UE_LOG(LogTemp, Warning, TEXT("SimpleUUIDGenerator::GenerateUUIDFromValue UUIDToGenerateFrom is already in use. This shouldn't happen. Returning nullptr."));
		ensure(false && "SimpleUUIDGenerator::GenerateUUIDFromValue UUIDToGenerateFrom is already in use. This shouldn't happen. Returning nullptr.");
		return UUIDHandle();
	}

	return UUIDHandle(UUIDToGenerateFrom, WeakPtrToGenerator);
}

void SimpleUUIDGenerator::FreeUUID(SimpleUUID UUIDFreed)
{
	if (UsedIDS.Contains(UUIDFreed))
	{
		UsedIDS.Remove(UUIDFreed);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("SimpleUUIDGenerator::FreeUUID ID being freed was not found in generator. This shouldn't happen."));
		ensure(false && "SimpleUUIDGenerator::FreeUUID ID being freed was not found in generator. This shouldn't happen.");
	}
}