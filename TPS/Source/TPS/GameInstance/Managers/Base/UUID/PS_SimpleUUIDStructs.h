#pragma once

#include "SharedPointer.h"

/* A uint32 is used as base for UUIDs created by the SimpleUUIDGenerator.
   Gives support to values from 0 to 4294967295. */
typedef uint32 SimpleUUID;

/*
* Handle for UUIDs returned by the SimpleUUIDGenerator.
*
* Contains the UUID requested.
*
* Handle is responsible of freeing the ID once it's out of scope or freed is called.
*/

class SimpleUUIDGenerator;

struct UUIDHandle
{

private:

	// Generator the UUID was generated from.
	TWeakPtr<SimpleUUIDGenerator> UUIDGenerator;

	// UUID used for the handle.
	SimpleUUID HandleUUID = 0;

	// If true handle should be free on destruction. False
	// if the handle is not responsible of freeing on destruction.
	// Set to false when copying the data.
	bool ShouldFreeHandleOnDestruction = true;

public:

	UUIDHandle();

	UUIDHandle(SimpleUUID NewHandleUUID, TWeakPtr<SimpleUUIDGenerator> & NewUUIDGenerator);

	// copy constructor
	UUIDHandle(UUIDHandle & CopyObj);
	// copy constructor
	UUIDHandle(const UUIDHandle & CopyObj);

	// On destruction of the handle, ID is freed.
	~UUIDHandle();

	SimpleUUID GetUUID() const;

	void FreeHandle();

	bool IsUUIDValid() const;

private:

	void SetShouldFreeHandleOnDestruction(bool FreeOnDestruction);
};
