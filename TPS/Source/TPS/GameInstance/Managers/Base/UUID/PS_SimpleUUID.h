#pragma once

#include "SharedPointer.h"
#include "Set.h"
#include "PS_SimpleUUIDStructs.h"

/*
* A very basic game object UUID provider which can be used to generate a simple, not network-secure Universal Unique Identifier.
* 
* The main purpose of this generator is to generate simple unique ID to easily identify game objects from the moment of creation
* afterwards even after the game is closed and loaded again.
* 
* To generate the IDs a simple integer counter is used, each time a new ID is requested, the counter gets incremented by one.
* As long as no ID is kept for enough time as to allow the counter to overflow and return the same exact ID again, no issue will happen. IE:
* We generated a ID = 1, the counter overflowed after returning all the other IDS (1 to 4294967295!) and returned to 1 again and it still is in use!
* The big size of the UUID (0 to 4294967295) mostly protects against such issues for the typical use cases.
* 
* Nevertheless, for extra protection, a set with UsedIDs is kept in memory. If the counter detects the key is being used, it will try to generate another one
* and prompt a message.
* 
* To support savegames, the generator provides a method to retrieve and setup the counter which can be
* used to store the value in a savegame and fed it to the generator on startup.
* 
* Counter is safe to use as long as the following situations are not present:
*
* IDs are not generated extremely fast in such a way it's very likely it will overflow while previous IDs are still in use. This can happen if each frame
* new IDs are being generated or if UUIDs have an very long lifespan.
* 
* Avoid using this provider if you think your IDs might end up overlapping somehow or if you need a secure and Unique ID that should not be sequential
* in nature. Use FGUID or other classes for more serious UUIDs.
* 
*/

class SimpleUUIDGenerator
{
	// Current UUID being used. 0 is invalid. Each new ID increments this counter by 1.
	SimpleUUID Counter = 0;

	// Used IDs.
	TSet<SimpleUUID> UsedIDS;

	// A weak ptr returned by the owner of the generator, used to pass to the handles to
	// communicate with the generator.
	TWeakPtr<SimpleUUIDGenerator> WeakPtrToGenerator;

public:

	static SimpleUUID InvalidUUID;
	
	// Should be called when initializing the generator.
	// Weak ptr passed as parameter is a weak ptr from the shared ptr that contains this generator in memory.
	// Ptr is used for handles to communicate with generator when freeing their handle.
	// StartingUUID can be any ID, in case you supporting saved files, you would pass the last value stored by the counter at the moment of saving
	// so the generator keeps generating values from there onwards.
	void Init(TWeakPtr<SimpleUUIDGenerator> NewWeakPtrToGenerator, SimpleUUID StartingUUIDValue = SimpleUUIDGenerator::InvalidUUID);

	// Should be called when shutting down the generator.
	// Normally here you would save any data necessary for the future.
	void Shutdown();

	// Returns a new UUID to be used for items. Keep the handle stored with the item.
	// Once the item is destroyed the UUID will be freed automatically.
	UUIDHandle GenerateNewUUID();

	// Returns a UUID handle from a UUID value.
	// To be used if you are supporting loading of objects from serialized data. In such case,
	// pass the serialized UUID to this method to retrieve the handle again.
	// Before using this method, get sure to pass in the init method as second parameter the last UUID used in the previous playthrough
	// so the counter starts from the point it was at the beginning to avoid collisions.
	UUIDHandle GenerateUUIDFromValue(SimpleUUID UUIDToGenerateFrom);

private:

	// Stops using an UUID allowing it to be reused again.
	void FreeUUID(SimpleUUID UUIDFreed);

	friend struct UUIDHandle;
};

SimpleUUID SimpleUUIDGenerator::InvalidUUID = 0;