#include "PS_GameInstanceFunctionLibrary.h"
#include "PS_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/PS_ItemManager.h"

// Returns the PS Game GameInstance.
UPS_GameInstance * UPS_GameInstanceFunctionLibrary::GetPSGameInstance(const UObject * WorldContextObject)
{
	return Cast<UPS_GameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject));
}

// Returns the Item Manager.
UPS_ItemManager * UPS_GameInstanceFunctionLibrary::GetItemManagerInstance(const UObject* WorldContextObject)
{
	UPS_GameInstance * GameInstance = Cast<UPS_GameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject));
	return GameInstance != nullptr ? GameInstance->GetItemManager() : nullptr;
}