#include "PS_GameInstance.h"
#include "Managers/PS_ItemManager.h"
#include "Managers/Base/PS_BaseManager.h"

void UPS_GameInstance::Init()
{
	Super::Init();
	
	REGISTER_GAMEINSTANCE_MODULE(this, UPS_ItemManager, "ItemManager");

	InitializeManagers();
}

void UPS_GameInstance::Shutdown()
{
	Super::Shutdown();

	ShutdownManagers();
}

/* Managers methods */

void UPS_GameInstance::InitializeManagers()
{
	for (const TUniquePtr<IBaseManager> & ManagerPtr : GameManagers)
	{
		if (ManagerPtr.IsValid())
		{
			ManagerPtr.Get()->Init();
		}
	}
}

void UPS_GameInstance::ShutdownManagers()
{
	for (const TUniquePtr<IBaseManager> & ManagerPtr : GameManagers)
	{
		if (ManagerPtr.IsValid())
		{
			ManagerPtr->Shutdown();
		}
	}
}

void UPS_GameInstance::RegisterManager(IBaseManager * ManagerToRegister)
{
	if (ManagerToRegister == nullptr)
		return;

	ManagerTypeID TypeID = ManagerToRegister->GetManagerType();
	if (TypeID >= GameManagers.Num())
		GameManagers.SetNumZeroed(((int32)TypeID) +1);

	GameManagers[(int32)TypeID] = TUniquePtr<IBaseManager>(ManagerToRegister);
}

void UPS_GameInstance::UnregisterManager(IBaseManager * ManagerToRegister)
{
	if (ManagerToRegister == nullptr)
		return;

	ManagerTypeID TypeID = ManagerToRegister->GetManagerType();
	if (TypeID >= GameManagers.Num())
		return;

	TUniquePtr<IBaseManager> & SharedPtrRef = GameManagers[(int32)TypeID];
	if(SharedPtrRef.IsValid())
		SharedPtrRef.Reset();
}


IBaseManager * UPS_GameInstance::GetManagerByType(ManagerTypeID TypeID)
{
	if (TypeID >= GameManagers.Num())
		return nullptr;

	TUniquePtr<IBaseManager> & SharedPtrRef = GameManagers[(int32)TypeID];
	if (SharedPtrRef.IsValid())
		return SharedPtrRef.Get();

	return nullptr;
}

IBaseManager * UPS_GameInstance::GetManagerByName(const FName & ManagerName)
{
	for (const TUniquePtr<IBaseManager> & ManagerUniquePtr : GameManagers)
	{
		if (ManagerUniquePtr.IsValid())
		{
			IBaseManager * ManagerPtr = ManagerUniquePtr.Get();
			if (ManagerPtr->GetManagerName() == ManagerName)
				return ManagerPtr;
		}
	}
	return nullptr;
}

DEFINE_GAMEINSTANCE_MODULE_GETTER(UPS_ItemManager, ItemManager)