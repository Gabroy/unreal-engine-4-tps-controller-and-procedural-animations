#include "PS_Human_SpringCameraController.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "../States/PS_CameraStructs.h"

void UPS_Human_SpringCameraController::Init()
{
	AActor * Owner = GetOwner();
	if (!Owner) return;

	// Fetch components.
	UCameraComponent * CameraComponent = Cast<UCameraComponent>(Owner->GetComponentByClass(UCameraComponent::StaticClass()));
	USpringArmComponent * CameraSpringArmComponent = Cast<USpringArmComponent>(Owner->GetComponentByClass(USpringArmComponent::StaticClass()));

	Start();
}