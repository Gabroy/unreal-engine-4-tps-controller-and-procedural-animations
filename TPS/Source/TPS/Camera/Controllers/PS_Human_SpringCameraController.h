#pragma once

#include "CoreMinimal.h"
#include "TPS/FSM/FSM_Component.h"
#include "PS_Human_SpringCameraController.generated.h"

/*
	The camera controller class for normal humanoid characters.
*/
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UPS_Human_SpringCameraController : public UFSM_Component
{
protected:

	GENERATED_BODY()

public:

	void Init();
};