#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "PS_PlayerCameraManager.generated.h"

UCLASS()
class TPS_API APS_PlayerCameraManager : public APlayerCameraManager
{

private:

	GENERATED_BODY()

	// Stack controlling cameras influencing the player camera manager.
	// When a camera gets removed through a call to PopCamera(), the previous one in the
	// stack is blended into the player's viewport.
	// Allows for easy stacking of cameras inside triggers for example.
	UPROPERTY(Transient)
	TArray<AActor *> CameraStack;

public:

	// Returns the current camera being shown to the user.
	// It can be the player controller camera or another one.
	// Same behaviour that GetViewTarget() from the base class, but this sounds clearer.
	UFUNCTION(BlueprintCallable)
	AActor * GetCurrentCamera();

	// Returns the player's actual camera. The one from the pawn controlling.
	UFUNCTION(BlueprintCallable)
	AActor * GetPlayerControllerCamera();

	// Blends a new camera into the player's view target. The camera gets added at the top of the camera stack.
	// In order to remove it call PopCamera().
	UFUNCTION(BlueprintCallable)
	void PushCamera(AActor * NewViewTarget, FViewTargetTransitionParams TransitionParams);

	// Removes from the camera stack the current camera being shown in the viewport.
	// If there is only one camera, the one from the player, the stack returns the player camera but no pop is done.
	UFUNCTION(BlueprintCallable)
	AActor * PopCamera(FViewTargetTransitionParams TransitionParams);

	// Check if there are any blended cameras in the stack. Player camera is not considered as part of it.
	UFUNCTION(BlueprintCallable)
	bool HasBlendedCamerasInStack();
};
