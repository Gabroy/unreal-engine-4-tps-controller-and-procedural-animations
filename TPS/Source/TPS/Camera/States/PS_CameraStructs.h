#pragma once

#include "PS_CameraStructs.generated.h"

USTRUCT(BlueprintType)
struct FCameraStateProperties
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraProperties")
	bool FetchInitCamera = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraProperties")
	FVector CameraOffset;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraProperties")
	float SpringArmLength = 300.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraProperties")
	float CameraFOV;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraProperties")
	float CameraDOF;
};