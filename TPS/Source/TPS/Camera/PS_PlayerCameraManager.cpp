#include "PS_PlayerCameraManager.h"

// Returns the current camera being shown to the user.
// It can be the player controller camera or another one.
// Same behaviour that GetViewTarget() from the base class, but this sounds clearer.
AActor * APS_PlayerCameraManager::GetCurrentCamera()
{
	if (CameraStack.Num() < 1)
		return GetViewTargetPawn();

	AActor * CurrentCamera = CameraStack.Top();
	return CurrentCamera;
}

// Returns the player's actual camera. The one from the pawn controlling.
AActor * APS_PlayerCameraManager::GetPlayerControllerCamera()
{
	return GetViewTargetPawn();
}

// Blends a new camera into the player's view target. The camera gets added at the top of the camera stack.
// In order to remove it call PopCamera().
void APS_PlayerCameraManager::PushCamera(AActor * NewViewTarget, FViewTargetTransitionParams TransitionParams)
{
	CameraStack.Add(NewViewTarget);
	SetViewTarget(NewViewTarget, TransitionParams);
}

// Removes from the camera stack the current camera being shown in the viewport.
// If there is only one camera, the one from the player, the stack returns the player camera but no pop is done.
AActor * APS_PlayerCameraManager::PopCamera(FViewTargetTransitionParams TransitionParams)
{

	AActor * PoppedCamera = nullptr;
	if (HasBlendedCamerasInStack())
	{
		CameraStack.Pop();
		PoppedCamera = CameraStack.Last();
	}
	else
	{
		PoppedCamera = PCOwner->GetPawn();
	}

	SetViewTarget(PoppedCamera, TransitionParams);
	return PoppedCamera;
}

// Check if there are any blended cameras in the stack. Player camera is not considered as part of the stack.
bool APS_PlayerCameraManager::HasBlendedCamerasInStack()
{
	return CameraStack.Num() > 1;
}