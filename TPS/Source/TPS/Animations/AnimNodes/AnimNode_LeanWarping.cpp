#pragma once

#include "AnimNode_LeanWarping.h"
#include "Animation/AnimNodeBase.h"
#include "AnimInstanceProxy.h"
#include "AnimationRuntime.h"
#include "Kismet/KismetMathLibrary.h"

void FAnimNode_LeanWarping::OnInitializeAnimInstance(const FAnimInstanceProxy* InProxy, const UAnimInstance* InAnimInstance)
{
	FAnimNode_Base::OnInitializeAnimInstance(InProxy, InAnimInstance);

	// Fetch skeleton bones.
	float SpineChainTotalWeight = 0.0f;

	// Initialize bones for the bone references.
	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData& BoneData = SpineChainBones[i];
		SpineChainTotalWeight += BoneData.Weight;
	}

	if (!PerBoneWeightCanApplyFullRotation) {
		for (int i = 0; i < SpineChainBones.Num(); ++i) {
			FBoneChainData& BoneData = SpineChainBones[i];
			BoneData.Weight = BoneData.Weight / SpineChainTotalWeight;
		}
	}
}

void FAnimNode_LeanWarping::Initialize_AnyThread(const FAnimationInitializeContext & Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Initialize_AnyThread)

	FAnimNode_Base::Initialize_AnyThread(Context);

	InputPose.Initialize(Context);

	const FBoneContainer& RequiredBoneContainer = Context.AnimInstanceProxy->GetRequiredBones();

	RootBone.Initialize(RequiredBoneContainer);
	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData& BoneData = SpineChainBones[i];
		BoneData.Bone.Initialize(RequiredBoneContainer);
	}

	FinalLeaningValue = 0.0f;
}

void FAnimNode_LeanWarping::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(CacheBones_AnyThread)

	InputPose.CacheBones(Context);
}

void FAnimNode_LeanWarping::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Update_AnyThread)

	GetEvaluateGraphExposedInputs().Execute(Context);

	// Update input pose.
	InputPose.Update(Context);

	// Smoothing should be here.
	FinalLeaningValue = UKismetMathLibrary::FInterpTo_Constant(FinalLeaningValue, LeaningValue, Context.GetDeltaTime(), LeaningInterpolationSpeed) * WarpAlpha;
}

void FAnimNode_LeanWarping::Evaluate_AnyThread(FPoseContext & Output)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)

	// Fills the output pose with the data from the input pose.
	InputPose.Evaluate(Output);

	if (FMath::IsNearlyZero(FinalLeaningValue, KINDA_SMALL_NUMBER)) return;

	const FBoneContainer & RequiredBoneContainer = Output.AnimInstanceProxy->GetRequiredBones();

	// Start by fetching the root bone index, if it doesn't exist we stop.
	FCompactPoseBoneIndex RootBoneIndex = RootBone.GetCompactPoseIndex(RequiredBoneContainer);
	if (RootBoneIndex == INDEX_NONE) return;

	// Create a component space pose context. We use this context to so we can compute the CS transform for the bones we will modify.
	FComponentSpacePoseContext CSOutput(Output.AnimInstanceProxy);
	CSOutput.Pose.InitPose(Output.Pose);

	// Get delta rotations for root bone and upper body.
	FRotator DeltaRotation;
	DeltaRotation.Roll = 0.0f;
	DeltaRotation.Pitch = FMath::Clamp(FinalLeaningValue * PerLeanUnitLeanDegrees, -MaxLean, MaxLean);
	DeltaRotation.Yaw = 0.0f;

	RotateBone(Output, CSOutput, RootBoneIndex, FQuat(DeltaRotation));

	DeltaRotation.Pitch = 0.0f;
	DeltaRotation.Yaw = FMath::Clamp(FinalLeaningValue * PerLeanUnitTwistDegrees, -MaxTwist, MaxTwist);

	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData & BoneData = SpineChainBones[i];
		
		FCompactPoseBoneIndex BoneIndex = BoneData.Bone.GetCompactPoseIndex(RequiredBoneContainer);
		if (BoneIndex == INDEX_NONE) continue;
		
		const FRotator SpineDeltaRotation(DeltaRotation.Pitch * BoneData.Weight, DeltaRotation.Yaw * BoneData.Weight, DeltaRotation.Roll * BoneData.Weight);
		const FQuat SpineDeltaQuat(SpineDeltaRotation);

		RotateBone(Output, CSOutput, BoneIndex, SpineDeltaQuat);
	}
}

void FAnimNode_LeanWarping::RotateBone(FPoseContext & Output, FComponentSpacePoseContext & CSOutput, FCompactPoseBoneIndex BoneIndex, const FQuat & DeltaQuaternionRotationWS)
{
	// We use this function to compute the space transform instead of accessing it through the array.
	// By using it we will only compute the necessary component space transforms to compute the CS for the given bone.
	FTransform BoneTransformInCS = CSOutput.Pose.GetComponentSpaceTransform(BoneIndex);
	FQuat BoneRotationInCS = BoneTransformInCS.GetRotation();
	FTransform & LSBoneTransform = Output.Pose[BoneIndex];
	LSBoneTransform.ConcatenateRotation(BoneRotationInCS.Inverse() * (DeltaQuaternionRotationWS * BoneRotationInCS));
	LSBoneTransform.NormalizeRotation();
}

void FAnimNode_LeanWarping::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);

	DebugData.AddDebugItem(DebugLine);

	InputPose.GatherDebugData(DebugData);
}