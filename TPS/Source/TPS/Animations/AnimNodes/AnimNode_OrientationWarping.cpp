#pragma once

#include "AnimNode_OrientationWarping.h"
#include "Animation/AnimNodeBase.h"
#include "AnimInstanceProxy.h"
#include "AnimationRuntime.h"
#include "Kismet/KismetMathLibrary.h"

void FAnimNode_OrientationWarping::OnInitializeAnimInstance(const FAnimInstanceProxy* InProxy, const UAnimInstance* InAnimInstance)
{
	FAnimNode_Base::OnInitializeAnimInstance(InProxy, InAnimInstance);

	// Fetch skeleton bones.
	float SpineChainTotalWeight = 0.0f;

	// Initialize bones for the bone references.
	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData& BoneData = SpineChainBones[i];
		SpineChainTotalWeight += BoneData.Weight;
	}

	if (!PerBoneWeightCanApplyFullRotation)
	{
		for (int i = 0; i < SpineChainBones.Num(); ++i)
		{
			FBoneChainData& BoneData = SpineChainBones[i];
			BoneData.Weight = BoneData.Weight / SpineChainTotalWeight;
		}
	}
}

void FAnimNode_OrientationWarping::Initialize_AnyThread(const FAnimationInitializeContext & Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Initialize_AnyThread)

	FAnimNode_Base::Initialize_AnyThread(Context);

	InputPose.Initialize(Context);

	const FBoneContainer & RequiredBoneContainer = Context.AnimInstanceProxy->GetRequiredBones();

	RootBone.Initialize(RequiredBoneContainer);
	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData& BoneData = SpineChainBones[i];
		BoneData.Bone.Initialize(RequiredBoneContainer);
	}

	DirectionToRotateTo = Direction + Offset;
}

void FAnimNode_OrientationWarping::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(CacheBones_AnyThread)

	InputPose.CacheBones(Context);
}

void FAnimNode_OrientationWarping::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Update_AnyThread)

	GetEvaluateGraphExposedInputs().Execute(Context);

	// Update input pose.
	InputPose.Update(Context);

	// Smoothing should be here.
	DirectionToRotateTo = UKismetMathLibrary::FInterpTo(DirectionToRotateTo, Direction + Offset, Context.GetDeltaTime(), DirectionInterpSpeed) * WarpAlpha;
}

void FAnimNode_OrientationWarping::Evaluate_AnyThread(FPoseContext & Output)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)

	// Fills the output pose with the data from the input pose.
	InputPose.Evaluate(Output);

	if (FMath::IsNearlyZero(DirectionToRotateTo, KINDA_SMALL_NUMBER)) return;

	const FBoneContainer & RequiredBoneContainer = Output.AnimInstanceProxy->GetRequiredBones();

	// Create a component space pose context. We use this context to so we can compute the CS transform for the bones we will modify.
	FComponentSpacePoseContext CSOutput(Output.AnimInstanceProxy);
	CSOutput.Pose.InitPose(Output.Pose);

	// Get delta rotations for root bone and upper body.
	FRotator RootDeltaRotation(EForceInit::ForceInitToZero);
	FRotator UpperBodyDeltaRotation(EForceInit::ForceInitToZero);
	FetchDeltaRotation(RootDeltaRotation, DirectionToRotateTo);
	FetchDeltaRotation(UpperBodyDeltaRotation, -DirectionToRotateTo * UpperBodyWarpAlpha);

	// Rotate the root bone.
	FCompactPoseBoneIndex RootBoneIndex = RootBone.GetCompactPoseIndex(RequiredBoneContainer);
	if (RootBoneIndex != INDEX_NONE)
	{
		RotateBone(Output, CSOutput, RootBoneIndex, FQuat(RootDeltaRotation));
	}

	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData & BoneData = SpineChainBones[i];
		
		FCompactPoseBoneIndex BoneIndex = BoneData.Bone.GetCompactPoseIndex(RequiredBoneContainer);
		if (BoneIndex == INDEX_NONE) continue;

		const FRotator SpineDeltaRotation(UpperBodyDeltaRotation.Pitch * BoneData.Weight, UpperBodyDeltaRotation.Yaw * BoneData.Weight, UpperBodyDeltaRotation.Roll * BoneData.Weight);
		const FQuat SpineDeltaQuat(SpineDeltaRotation);

		RotateBone(Output, CSOutput, BoneIndex, SpineDeltaQuat);
	}
}

void FAnimNode_OrientationWarping::FetchDeltaRotation(FRotator& DeltaRotationToModify, float AngleToRotate)
{
	switch (RotationAxis)
	{
	case E_WarpingAxis::X_Axis:
		DeltaRotationToModify.Roll = AngleToRotate;
		break;
	case E_WarpingAxis::Y_Axis:
		DeltaRotationToModify.Pitch = AngleToRotate;
		break;
	case E_WarpingAxis::Z_Axis:
		DeltaRotationToModify.Yaw = AngleToRotate;
		break;
	default:
		break;
	}
}

void FAnimNode_OrientationWarping::RotateBone(FPoseContext & Output, FComponentSpacePoseContext & CSOutput, FCompactPoseBoneIndex BoneIndex, const FQuat & DeltaQuaternionRotationWS)
{
	// We use this function to compute the space transform instead of accessing it through the array.
	// By using it we will only compute the necessary component space transforms to compute the CS for the given bone.
	FTransform BoneTransformInCS = CSOutput.Pose.GetComponentSpaceTransform(BoneIndex);
	FQuat BoneRotationInCS = BoneTransformInCS.GetRotation();
	FTransform & LSBoneTransform = Output.Pose[BoneIndex];
	LSBoneTransform.ConcatenateRotation(BoneRotationInCS.Inverse() * (DeltaQuaternionRotationWS * BoneRotationInCS));
	LSBoneTransform.NormalizeRotation();
}

void FAnimNode_OrientationWarping::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);

	DebugData.AddDebugItem(DebugLine);

	InputPose.GatherDebugData(DebugData);
}