#pragma once

#include "Animation/AnimNodeBase.h"
#include "AnimNode_HelperStructs.h"
#include "AnimNode_OrientationWarping.generated.h"


/* Custom animation node implemented that provides orientation warping functionality
 * for an Unreal Skeleton.
 * 
 * Struct below is responsible of initializing, updating and performing the warping operations
 * necessary to warp the input pose with the desired orientation, returned as output pose.
 * 
 * Link with more data: https://docs.unrealengine.com/en-US/Programming/Animation/AnimNodes/index.html
 * 
 */

USTRUCT(BlueprintType)
struct TPS_API FAnimNode_OrientationWarping : public FAnimNode_Base
{
	GENERATED_BODY()
		
	// The pose received as input and which will be warped.
	UPROPERTY(EditAnywhere, Category = Links)
	FPoseLink InputPose;

	// Direction the orientation warping will rotate to.
	UPROPERTY(EditAnywhere, meta = (PinShownByDefault))
	float Direction = 0.0f;

	// Offset added to character when considering direction. Can be used to allow rotation of animation for sideway animations for example.
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (PinShownByDefault, ClampMin = "-180.0", ClampMax = "180.0", UIMin = "-180.0", UIMax = "180.0"))
	float Offset = 0.0f;

	// Direction that is used to rotate the character. Computed in update
	// Based in Direction, DirectionInterpSpeed and Offset.
	float DirectionToRotateTo;

	// Controls how much of the orientation warping ends up applied to the pose.
	// Goes from 0.0f to 1.0f. A value of 0.0f will disable warping, a value of
	// 1.0f will apply full warp.
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (PinShownByDefault, ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float WarpAlpha = 1.0f;

	// Controls how much of the orientation warping ends up applied to the spine chain.
	// Goes from 0.0f to 1.0f. A value of 0.0f will disable warping, a value of 1.0f
	// will apply full warp.
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (PinShownByDefault, ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float UpperBodyWarpAlpha = 1.0f;

	// Axis that will be used to apply the rotation of the orientation warping. By default the up vector, which will apply a
	// yaw rotation to the skeleton (the most common orientation warping).
	UPROPERTY(EditAnywhere, Category = "Settings")
	TEnumAsByte<E_WarpingAxis> RotationAxis = E_WarpingAxis::Z_Axis;

	// Bone of the skeleton that will be rotated in the direction defined by the orientation warping.
	UPROPERTY(EditAnywhere, Category = "Settings")
	FBoneReference RootBone;

	// A list of bones, normally, children of the bone specified as root bone, that will be rotated inversely to the direction
	// specified for the orientation warping so they remain facing their original rotation.
	UPROPERTY(EditAnywhere, Category = "Settings")
	TArray<FBoneChainData> SpineChainBones;

	// If true, weights in the spine chain will dictate how much of the total orientation rotation will be applied to each bone in the spine chain.
	// This means that, if we have 30 degrees rotation and two bone with weight 1.0, both bones will rotate 30 degrees.
	// If false, weights in the spine chain will represent the influence of each bone in the spine chain, and the rotation applied to each bone
	// will be a part of the total 30 degrees rotation, never exceeding it. For example, if two bones have weight 1.0, each one will end up having
	// 50% of the final rotation, so they will rotate 15 � each one.
	UPROPERTY(EditAnywhere, Category = "Settings")
	bool PerBoneWeightCanApplyFullRotation = false;

	// How fast animation will adapt to changes in direction. If 0.0f, we jump to the target directly.
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (ClampMin = "0.0", UIMin = "0.0"))
	float DirectionInterpSpeed = 0.0f;

public:
	FAnimNode_OrientationWarping(){}

	// Allows calling OnInitializeAnimInstance, where we setup some data before runtime and gets called only once.
	virtual bool NeedsOnInitializeAnimInstance() const { return true; }

	/** Called once, from game thread as the parent anim instance is created */
	virtual void OnInitializeAnimInstance(const FAnimInstanceProxy* InProxy, const UAnimInstance* InAnimInstance);

	// Initializes the AnimNode. Whenever we need to Initialize/Reinitialize (when changing mesh for instance). 
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;

	// Caches bones that needs to be tracked by this animation node.
	// Used for refreshing bone indices that are referenced by the node.
	virtual void CacheBones_AnyThread(const FAnimationCacheBonesContext& Context) override;

	// Called to update current state(such as advancing play time or updating blend weights).
	// This function takes an FAnimationUpdateContext that knows the DeltaTime for the
	// update and the current nodes blend weight.
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;

	// Called to generate a �pose� (list of bone transforms). 
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	
	// Used for debugging using "ShowDebug Animation" data
	virtual void GatherDebugData(FNodeDebugData& DebugData) override;

protected:

	inline void FetchDeltaRotation(FRotator & DeltaRotationToModify, float AngleToRotate);

	inline void RotateBone(FPoseContext& Output, FComponentSpacePoseContext& CSOutput, FCompactPoseBoneIndex RootBoneIndex, const FQuat& DeltaQuaternionRotationWS);
};