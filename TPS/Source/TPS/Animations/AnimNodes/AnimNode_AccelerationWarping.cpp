#pragma once

#include "AnimNode_AccelerationWarping.h"
#include "Animation/AnimNodeBase.h"
#include "AnimInstanceProxy.h"
#include "AnimationRuntime.h"
#include "Kismet/KismetMathLibrary.h"

void FAnimNode_AccelerationWarping::OnInitializeAnimInstance(const FAnimInstanceProxy* InProxy, const UAnimInstance* InAnimInstance)
{
	FAnimNode_Base::OnInitializeAnimInstance(InProxy, InAnimInstance);

	float TotalWeightsAcceleration = 0.0f;
	float TotalWeightsFaceForward = 0.0f;

	// Initialize bones for the bone references.
	for (int i = 0; i < SpineChainBones.Num(); ++i) {
		FBoneChainData& BoneData = SpineChainBones[i];
		if (BoneData.Weight >= 0.0f)
			TotalWeightsAcceleration += BoneData.Weight;
		else
			TotalWeightsFaceForward += FMath::Abs(BoneData.Weight);
	}

	if (!PerBoneWeightCanApplyFullRotation) {
		for (int i = 0; i < SpineChainBones.Num(); ++i) {
			FBoneChainData& BoneData = SpineChainBones[i];
			if (BoneData.Weight >= 0.0f)
				BoneData.Weight = BoneData.Weight / TotalWeightsAcceleration;
			else
				BoneData.Weight = BoneData.Weight / TotalWeightsFaceForward;
		}
	}
}


void FAnimNode_AccelerationWarping::Initialize_AnyThread(const FAnimationInitializeContext & Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Initialize_AnyThread)

	FAnimNode_Base::Initialize_AnyThread(Context);

	InputPose.Initialize(Context);

	const FBoneContainer& RequiredBoneContainer = Context.AnimInstanceProxy->GetRequiredBones();
	for (int i = 0; i < SpineChainBones.Num(); ++i) {
		FBoneChainData& BoneData = SpineChainBones[i];
		BoneData.Bone.Initialize(RequiredBoneContainer);
	}

	FinalAcceleration = 0.0f;
}

void FAnimNode_AccelerationWarping::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(CacheBones_AnyThread)

	InputPose.CacheBones(Context);
}

void FAnimNode_AccelerationWarping::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Update_AnyThread)

	GetEvaluateGraphExposedInputs().Execute(Context);

	// Update input pose.
	InputPose.Update(Context);

	// Smoothing should be here.
	FinalAcceleration = UKismetMathLibrary::FInterpTo(FinalAcceleration, Acceleration, Context.GetDeltaTime(), AccelerationInterpSpeed);
}

void FAnimNode_AccelerationWarping::Evaluate_AnyThread(FPoseContext & Output)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)

	// Fills the output pose with the data from the input pose.
	InputPose.Evaluate(Output);

	if (FMath::IsNearlyZero(FinalAcceleration, KINDA_SMALL_NUMBER)) return;

	const FBoneContainer & RequiredBoneContainer = Output.AnimInstanceProxy->GetRequiredBones();

	// Create a component space pose context. We use this context to so we can compute the CS transform for the bones we will modify.
	FComponentSpacePoseContext CSOutput(Output.AnimInstanceProxy);
	CSOutput.Pose.InitPose(Output.Pose);

	// Get delta rotations for root bone and upper body.
	FRotator DeltaRotation(EForceInit::ForceInitToZero);
	float AngleToRotate = FMath::Clamp(FinalAcceleration * PerAccelerationUnitBendingDegrees * WarpAlpha, MinNegativeBendingDegrees, MaxPositiveBendingDegrees);
	FetchDeltaRotation(DeltaRotation, AngleToRotate);

	for (int i = 0; i < SpineChainBones.Num(); ++i)
	{
		FBoneChainData & BoneData = SpineChainBones[i];
		
		FCompactPoseBoneIndex BoneIndex = BoneData.Bone.GetCompactPoseIndex(RequiredBoneContainer);
		if (BoneIndex == INDEX_NONE) continue;
		
		const FRotator SpineDeltaRotation(DeltaRotation.Pitch * BoneData.Weight, DeltaRotation.Yaw * BoneData.Weight, DeltaRotation.Roll * BoneData.Weight);
		const FQuat SpineDeltaQuat(SpineDeltaRotation);

		RotateBone(Output, CSOutput, BoneIndex, SpineDeltaQuat);
	}
}

void FAnimNode_AccelerationWarping::FetchDeltaRotation(FRotator & DeltaRotationToModify, float AngleToRotate)
{
	switch (AccelerationAxis)
	{
	case E_WarpingAxis::X_Axis:
		DeltaRotationToModify.Roll = AngleToRotate;
		break;
	case E_WarpingAxis::Y_Axis:
		DeltaRotationToModify.Pitch = AngleToRotate;
		break;
	case E_WarpingAxis::Z_Axis:
		DeltaRotationToModify.Yaw = AngleToRotate;
		break;
	default:
		break;
	}
}

void FAnimNode_AccelerationWarping::RotateBone(FPoseContext & Output, FComponentSpacePoseContext & CSOutput, FCompactPoseBoneIndex BoneIndex, const FQuat & DeltaQuaternionRotationWS)
{
	// We use this function to compute the space transform instead of accessing it through the array.
	// By using it we will only compute the necessary component space transforms to compute the CS for the given bone.
	FTransform BoneTransformInCS = CSOutput.Pose.GetComponentSpaceTransform(BoneIndex);
	FQuat BoneRotationInCS = BoneTransformInCS.GetRotation();
	FTransform & LSBoneTransform = Output.Pose[BoneIndex];
	LSBoneTransform.ConcatenateRotation(BoneRotationInCS.Inverse() * (DeltaQuaternionRotationWS * BoneRotationInCS));
	LSBoneTransform.NormalizeRotation();
}

void FAnimNode_AccelerationWarping::GatherDebugData(FNodeDebugData& DebugData)
{
	FString DebugLine = DebugData.GetNodeName(this);

	DebugData.AddDebugItem(DebugLine);

	InputPose.GatherDebugData(DebugData);
}