#pragma once

#include "Animation/AnimNodeBase.h"
#include "BoneContainer.h"
#include "AnimNode_HelperStructs.generated.h"

/*
	Enum representing the three posible warping axis for rotations. 
	Used by warpings like orientation and acceleration warpings.
*/
UENUM(BlueprintType)
enum E_WarpingAxis {
	X_Axis,
	Y_Axis,
	Z_Axis
};

/*
 * Data for a bone belonging to a bone chain.
 * 
 * Contains a reference to a bone in the skeleton and a weight going from
 * 0.0f to 1.0f that will control how much a given effect is applied to it.
 * 
 * Used by certain custom anim nodes to apply their operations like
 * the Orientation Warping node for example.
 *
 */

USTRUCT(BlueprintType)
struct TPS_API FBoneChainData
{
	GENERATED_USTRUCT_BODY()
		
	UPROPERTY(EditAnywhere, Category = "Settings")
	FBoneReference Bone;

	// A value of 1.0f will apply the full rotation effect of the warp effect to the bone. A value of -1.0f will do so with opposite sign. A value
	// of 0.0f will apply no rotation whatsoever.
	UPROPERTY(EditAnywhere, Category = "Settings", meta = (PinShownByDefault, ClampMin = "-1.0", ClampMax = "1.0", UIMin = "-1.0", UIMax = "1.0"))
	float Weight = 1.0f;
};