using UnrealBuildTool;
using System.Collections.Generic;

public class TPSEditorTarget : TargetRules
{
	public TPSEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.AddRange( new string[] { "TPS", "TPSEditor" } );
	}
}
