# Unreal Engine 4 TPS Controller And Procedural Animations

TPS Controller built on UE4. Main concerns where building a simple, highly modular controller and investigating different type of procedural animation techniques.

Helped me understand better the current state of animation programming and get a better grasp of why technologies like Motion Warping are so important for high-fidelity animations.

The project implements the following features:

* Hierarchical FSM fully implemented in C++. Support for all multiple layers of states allowing for easy addition and removal of new movement types.
States are highly independent between them greatly reducing the transitions to consider and making code highly extensible.

* TPS Controller built on top of Hierarchical FSM.

* Implemented multiple movement modes: Grounded, Aerial, Vaulting, Climbing.

* Implemented multiple poses modes: Unarmed, One Handed, Two Handed, .... Thanks to the use of HSM, dependencies are reduced and code is highly modular and reusable between movement modes.

* Inventory system written on C++.

* Full Camera Controller with blending ins, outs, stacks of cameras, triggers for adding and removing cameras, ...

* Outline system using cubes instead of fullscreen postprocessing, provides multiple benefits impossible to be reproduced with traditional postprocessing.

* Interactuable triggers.

* Code based Vaulting system ruled by Distance Matching animations and code variables.

* Climbing system.

* IKs for climbing, spine rotation when close to surfaces, hand placement when next to walls, head rotation ...

* Implemented multiple techniques for animation controller:

    -  Animation Warping: Orientation and Speed Warping animation nodes applied as a postprocessing of the normal pose of a character.
    The nodes help providing a more realistic movement animation by allowing character to change pelvis direction in the face of obstacles or to simulate the inertia of velocity when changing directions.

    - Distance Matching: Implementation of Distance Matching Technique first implemented by Epic Studios for Paragon.